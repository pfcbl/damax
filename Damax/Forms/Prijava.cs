﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Forms;
using MySql.Data.MySqlClient;
using Damax.Models;

namespace Damax
{
    public partial class Prijava : Form
    {
        FormHome pocetna;
        public Prijava()
        {
            InitializeComponent();
            pocetna = null;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          try
            {                
                
                List<TemplateFilter> list=new List<TemplateFilter>()
                {
                    new TemplateFilter(Radnik.F_KorisnickoIme,"'"+tbKorisnickoIme.Text+"'","="),
                    new TemplateFilter(Radnik.F_Lozinka,"'"+tbLozinka.Text+"'","=")
                };
                List<Radnik> radnik = new RadnikDAO().Select(list);
                if (radnik.Count > 0)
                {
                    this.Hide();
                    var form = new FormHome(radnik[0]);
                    form.ShowDialog();
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("Pogrešno korisničko ime ili lozinka.", "Greška pri logovanju", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    tbLozinka.Text = "";
                }



            }
            catch (Exception ex)
            {
               MessageBox.Show("Pogrešno korisničko ime ili lozinka.", "Greška pri logovanju", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                tbLozinka.Text = "";
            }
            
           


        }
        public void logOdjava(Prijava log, FormHome poc)
        {
            tbKorisnickoIme.Text = tbLozinka.Text = "";
            poc.Visible = false;
            log.Visible = true;
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1_Click(sender, e);
            }
        }

       
    }
}
