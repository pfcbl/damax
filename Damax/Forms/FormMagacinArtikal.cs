﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormMagacinArtikal : FormTemplate<MagacinArtikal>
    {
        public FormMagacinArtikal()
        {
            InitializeComponent();
            DaoObject = new MagacinArtikalDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            SetFormControlsSettings();
            lookupComboBox1.RefreshLookup += lookup_RefreashLookupDogadjaj;
            lkpMagacin.RefreshLookup += lookup_RefreashLookupNadredjeni;
            lkpArtikal.RefreshLookup += lookup_RefreashLookupProdajnoMjesto;
        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
            dtpGodina.ShowUpDown = true;

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
           
            tbKolicina.ReadOnly = !(FormMode==Constants.Form_Mode.MODE_ADD);
            tbTrenutnoStanje.ReadOnly =  !(FormMode == Constants.Form_Mode.MODE_ADD);
            lkpArtikal.Enabled = isEnable;
                dtpGodina.Enabled = false;
                lookupComboBox1.Enabled = false;
                lkpMagacin.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), Resources.Year_Date_Format_yyyy, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;
            tbKolicina.Value = SelectedObject.PocetnoStanje<0?0: SelectedObject.PocetnoStanje;
            tbTrenutnoStanje.Value = SelectedObject.TrenutnoStanje < 0 ? 0 : SelectedObject.TrenutnoStanje;


        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject == null) return;
            SelectedObject.DogadjajId = lookupComboBox1.LookupMember != null
                ? Convert.ToInt32(lookupComboBox1.LookupMember)
                : (int?)null;
            SelectedObject.MagacinId = lkpMagacin.LookupMember != null
                ? Convert.ToInt32(lkpMagacin.LookupMember)
                : (int?)null;
            SelectedObject.ArtikalId = lkpArtikal.LookupMember != null
                ? Convert.ToInt32(lkpArtikal.LookupMember)
                : (int?)null;
            SelectedObject.PocetnoStanje = tbKolicina.Value;
            SelectedObject.TrenutnoStanje = tbKolicina.Value;
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbKolicina.Value = 0;
            tbTrenutnoStanje.Value = 0;
        }
        
        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            var grid = GetDataGridView();
            grid.Columns[MagacinArtikal.F_DogadjajId].Visible = false;
            grid.Columns[MagacinArtikal.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[MagacinArtikal.F_NazivDogadjaja].Width = 200;
            grid.Columns[MagacinArtikal.F_Godina].Width = 70;
            grid.Columns[MagacinArtikal.F_MagacinId].Visible = false;
            grid.Columns[MagacinArtikal.F_ArtikalId].Visible = false;
            grid.Columns[MagacinArtikal.F_ArtikalKategorijaId].Visible = false;
            grid.Columns[MagacinArtikal.F_MagacinNaziv].Width = 150;
            grid.Columns[MagacinArtikal.F_MagacinNaziv].HeaderText = "Magacin";
            grid.Columns[MagacinArtikal.F_ArtikalNaziv].Width = 200;
            grid.Columns[MagacinArtikal.F_ArtikalNaziv].HeaderText = "Artikal";
            grid.Columns[MagacinArtikal.F_ArtikalKategorijaNaziv].Width = 100;
            grid.Columns[MagacinArtikal.F_ArtikalKategorijaNaziv].HeaderText = "Kategorija";
            grid.Columns[MagacinArtikal.F_TrenutnoStanje].HeaderText = "Stanje(kom)";
            grid.Columns[MagacinArtikal.F_PocetnoStanje].HeaderText = "Početno stanje(kom)";

        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            
            FillLookups();
        }

        public override void Temp_BtnEdit()
        {
            base.Temp_BtnEdit();
            //if (SelectedObject != null && lkpMagacin.SelectedLookupValue != null && ((Magacin)lkpMagacin.SelectedLookupValue).Centralni)
            //{

            //    tbKolicina.ReadOnly = !(FormMode == Constants.Form_Mode.MODE_EDIT);
            //    tbTrenutnoStanje.ReadOnly = !(FormMode == Constants.Form_Mode.MODE_EDIT);
            //}
            //else
            //{
            //    tbKolicina.ReadOnly = false;
            //    tbTrenutnoStanje.ReadOnly = false;
            //}
        }

        public override void Temp_BtnSaveYes()
        {
            base.Temp_BtnSaveYes();
            tbKolicina.ReadOnly = false;
            tbTrenutnoStanje.ReadOnly = false;

        }

        public override bool DeleteObject()
        {
            if (HomeFilters.HomeFilterEvent.ZabranaBrisanja)
            {
                MessageBox.Show("Brisanje artikala nije dozvoljeno.", "Zabrana", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return base.DeleteObject();
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lookupComboBox1.DisplayMember = Event.F_Naziv;
            lookupComboBox1.ValueMember = Event.F_DogadjajId;
            lookupComboBox1.DataSource = source;
            
                lkpArtikal.DisplayMember = Artikal.F_Naziv;
                lkpArtikal.ValueMember = Artikal.F_ArtikalId;
                lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
                });

            lkpMagacin.DisplayMember = Magacin.F_Naziv;
            lkpMagacin.ValueMember = Magacin.F_MagacinId;
            lkpMagacin.DataSource = (new MagacinDao()).Select(Filters);
        }

        private void FillLookups()
        {
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lookupComboBox1.DisplayMember = Event.F_Naziv;
            lookupComboBox1.ValueMember = Event.F_DogadjajId;
            lookupComboBox1.DataSource = source;

            if (SelectedObject != null)
            {
                lkpArtikal.DisplayMember = Artikal.F_Naziv;
                lkpArtikal.ValueMember = Artikal.F_ArtikalId;
                lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                    new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                    new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                    new TemplateFilter(Artikal.F_ArtikalId, SelectedObject.ArtikalId.ToString(), "=") });
            }
            else
            {
                lkpArtikal.DisplayMember = Artikal.F_Naziv;
                lkpArtikal.ValueMember = Artikal.F_ArtikalId;
                lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
                });
            }

            lkpMagacin.DisplayMember = Magacin.F_Naziv;
            lkpMagacin.ValueMember = Magacin.F_MagacinId;
            lkpMagacin.DataSource = (new MagacinDao()).Select(Filters);

        }

        protected void lookup_RefreashLookupDogadjaj(object sender, EventArgs e)
        {

            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lookupComboBox1.DisplayMember = Event.F_Naziv;
            lookupComboBox1.ValueMember = Event.F_DogadjajId;
            lookupComboBox1.DataSource = source;
        }
        protected void lookup_RefreashLookupNadredjeni(object sender, EventArgs e)
        {

            var source = (new MagacinDao()).Select(Filters);
            lkpMagacin.DisplayMember = Magacin.F_Naziv;
            lkpMagacin.ValueMember = Magacin.F_MagacinId;
            lkpMagacin.DataSource = source;
        }

        protected void lookup_RefreashLookupProdajnoMjesto(object sender, EventArgs e)
        {

            var source = (new ArtikalDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpArtikal.DisplayMember = Artikal.F_Naziv;
            lkpArtikal.ValueMember = Artikal.F_ArtikalId;
            lkpArtikal.DataSource = source;
        }

        private void tbKolicina_ValueChanged(object sender, EventArgs e)
        {
            if(FormMode==Constants.Form_Mode.MODE_ADD)
            {
                tbTrenutnoStanje.Value = tbKolicina.Value;
            }
        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            if (FormMode == Constants.Form_Mode.MODE_VIEW)
            {
                if (HomeFilters.HomeFilterEvent.ZabranaBrisanja)
                {
                    MessageBox.Show("Brisanje artikala nije dozvoljeno.", "Zabrana", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (DialogResult.Yes == MessageBox.Show("Da li zelite da obrišete sve artikle iz magacina?", "Brisanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    (new MagacinArtikalDao()).DeleteAll();
                    RefreshGrid();
                }
            }
        }

        private void btnDeleteAll_MouseHover(object sender, EventArgs e)
        {
            ttObrisiSve.SetToolTip(btnDeleteAll, "Obrisi sve artikle");
        }
    }
}
