﻿using System;
using System.Globalization;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Properties;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormEvent : FormTemplate<Event>
    {
        public FormEvent()
        {
            InitializeComponent();
            DaoObject=new EventDao();
            SetFormControlsSettings();
        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format=DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
            dtpGodina.ShowUpDown = true;
            dtpGodina.Value = DateTime.TryParseExact(HomeFilters.HomeFilterGodina.ToString(), Resources.Year_Date_Format_yyyy, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;

            dtpDatumOd.Format=DateTimePickerFormat.Custom;
            dtpDatumOd.CustomFormat = Resources.DateTime_Format;

            dtpDatumDo.Format = DateTimePickerFormat.Custom;
            dtpDatumDo.CustomFormat = Resources.DateTime_Format;
        }

        
        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbNaziv.ReadOnly = !isEnable;
            tbMjesto.ReadOnly = !isEnable;
            tbAdresa.ReadOnly =!isEnable;
            tbOdgovornaOsoba.ReadOnly = !isEnable;
            dtpDatumOd.Enabled = isEnable;
            dtpDatumDo.Enabled = isEnable;
            tbValuta.ReadOnly = !isEnable;
            chAktivan.Enabled = false;
            cbZavrsen.Enabled = false;
            dtpGodina.Enabled = false;
            cbZabranaBrisanja.Enabled =  isEnable;
        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(HomeFilters.HomeFilterGodina.ToString(), Resources.Year_Date_Format_yyyy, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;
            tbNaziv.Text = SelectedObject.Naziv;
            tbMjesto.Text = SelectedObject.Mjesto;
            tbAdresa.Text = SelectedObject.Adresa;
            tbOdgovornaOsoba.Text = SelectedObject.OdgovornaOsoba;
            dtpDatumOd.Value = DateTime.TryParse(SelectedObject.DatumOd.ToString(), out dt) ? dt : DateTime.Now;
            dtpDatumDo.Value = DateTime.TryParse(SelectedObject.DatumDo.ToString(), out dt) ? dt : DateTime.Now;
            chAktivan.Checked = SelectedObject.Aktivan ;
            cbZavrsen.Checked = SelectedObject.Zavrsen;
            tbValuta.Text = SelectedObject.Valuta;
            cbZabranaBrisanja.Checked = SelectedObject.ZabranaBrisanja;


        }

        public override void FillObject()
        {
            base.FillObject();
            SelectedObject.Naziv = tbNaziv.Text;
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
            SelectedObject.Mjesto = tbMjesto.Text;
            SelectedObject.Adresa = tbAdresa.Text;
            SelectedObject.OdgovornaOsoba = tbOdgovornaOsoba.Text;
            SelectedObject.DatumOd = dtpDatumOd.Value;
            SelectedObject.DatumDo = dtpDatumDo.Value;
            SelectedObject.Aktivan = chAktivan.Checked;
            SelectedObject.Zavrsen = cbZavrsen.Checked;
            SelectedObject.Valuta = tbValuta.Text;
            SelectedObject.ZabranaBrisanja = cbZabranaBrisanja.Checked;

        }

        public override void ClearControls()
        {
            base.ClearControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(HomeFilters.HomeFilterGodina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now; ;
            tbNaziv.Text = "";
            tbMjesto.Text = "";
            tbAdresa.Text = "";
            tbOdgovornaOsoba.Text = "";
            dtpDatumOd.Value = DateTime.Now;
            dtpDatumDo.Value = DateTime.Now;
            chAktivan.Checked = false;
            cbZavrsen.Checked = false;
            tbValuta.Text = "";
            cbZabranaBrisanja.Checked = false;
        }

        public override bool ValidateControls()
        {
            if (tbNaziv.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbNaziv, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (dtpDatumOd.Value>dtpDatumDo.Value)
            {
                GetErrorProvider().SetError(dtpDatumDo, "Datum početka ne moze biti veci od datuma završetka događaja.");
                IsValid = false;
            }
            if (tbValuta.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbValuta, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void Temp_BtnAdd()
        {
            if (GeneralDao.ImaLiAktivanDogadjaj() != 0)
            {
                MessageBox.Show(
                    Resources.FormEvent_Error_PostojiAktivanDogadjaj,
                    Resources.FormEvent_Error_PostojiAktivanDogadjaj_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            base.Temp_BtnAdd();
        }

        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            if (SelectedObject != null)
            {
                btnZakljucajDogadjaj.Enabled = SelectedObject.Aktivan;
            }
        }

        public override void AddNextForms()
        {
            base.AddNextForms();
            //{
            //    FormBlagajna form = new FormBlagajna();
            //    form.Filters.Add(new TemplateFilter(Blagajna.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "="));
            //    form.Filters.Add(new TemplateFilter(Blagajna.F_DogadjajId, HomeFilters.HomeFilterDogadjajId.ToString(), "="));
            //    IsInNextMode = true;
            //    NextForms.Add(form);
            //}
            //{
            //    FormProdajnoMjesto form = new FormProdajnoMjesto();
            //    form.Filters.Add(new TemplateFilter(ProdajnoMjesto.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "="));
            //    form.Filters.Add(new TemplateFilter(ProdajnoMjesto.F_DogadjajId, HomeFilters.HomeFilterDogadjajId.ToString(), "="));
            //    IsInNextMode = true;
            //    NextForms.Add(form);
            //}
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Event.F_DogadjajId].HeaderText = "Šifra";
            grid.Columns[Event.F_DogadjajId].Width = 50;
            grid.Columns[Event.F_Godina].Width = 70;
            grid.Columns[Event.F_Naziv].Width = 300;
            grid.Columns[Event.F_Mjesto].Width = 100;
            grid.Columns[Event.F_Adresa].Width = 200;
            grid.Columns[Event.F_DatumOd].DefaultCellStyle.Format = "dd.MM.yyyy. HH:mm";
            grid.Columns[Event.F_DatumDo].DefaultCellStyle.Format = "dd.MM.yyyy. HH:mm";
            grid.Columns[Event.F_OdgovornaOsoba].HeaderText = "Odgovorno lice";
            grid.Columns[Event.F_DatumOd].HeaderText = "Od";
            grid.Columns[Event.F_DatumDo].HeaderText = "Do";
            grid.Columns[Event.F_DatumOd].Width = 120;
            grid.Columns[Event.F_DatumDo].Width = 120;
            grid.Columns[Event.F_Valuta].Width = 70;
            grid.Columns[Event.F_ZabranaBrisanja].Visible = false;

        }

        private void btnZakljucajDogadjaj_Click(object sender, EventArgs e)
        {
            if (SelectedObject != null && SelectedObject.DogadjajId!=null)
            {
                var rez =
                    MessageBox.Show(
                        "Nakon zaključivanja događaja više neće biti moguće mijenjati podatke vezane za ovaj događaj. Da li ste sigurni da želite da zaključite odabrani događaj?",
                        "Zaključivanje događaja", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (rez != DialogResult.Yes) return;
                if (GeneralDao.ZakljucavanjeDogadjaja(SelectedObject) == 0)
                {
                    MessageBox.Show("Događaj je uspješno zaključen.", "Uspješno zaključivanje", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    base.RefreshGrid();
                }
                else
                {
                    MessageBox.Show("Desila se greška prilikom zaključavanja događaja. Obratite se administratorima.", "Neuspješno zaključavanje", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Nije odabran ni jedan događaj.", "Zaključivanje događaja", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
