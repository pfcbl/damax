﻿using Damax.DAO;
using Damax.Models;
using Damax.Properties;
using Damax.Template;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
    public partial class FormPrenosnica : FormTemplate<Prenosnica>
    {
        public FormPrenosnica()
        {
            InitializeComponent();
            DaoObject = new PrenosnicaDao();
            LockUnlockMenuButtons(false, false, false);
            SetFormControlsSettings();

        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
            dtpGodina.ShowUpDown = true;

            dtDatum.Format = DateTimePickerFormat.Custom;
            dtDatum.CustomFormat = Resources.DateTime_Format;
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            dtpGodina.Enabled = false;
            lkpDogadjaj.Enabled = false;
            lkpMagacinIzlaz.Enabled = false;
            lkpMagacinUlaz.Enabled = false;
            tbTipPrenosa.ReadOnly = true;
            dtDatum.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), Resources.Year_Date_Format_yyyy, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            dtDatum.Value = DateTime.TryParse(SelectedObject.VrijemePrenosa.ToString(), out dt) ? dt : DateTime.Now;
            tbTipPrenosa.Text = SelectedObject.TipPrenosa;



        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject == null) return;
            SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                : (int?)null;
            SelectedObject.MagacinSourceId = lkpMagacinIzlaz.LookupMember != null
                ? Convert.ToInt32(lkpMagacinIzlaz.LookupMember)
                : (int?)null;
            SelectedObject.MagacinOdredisteId = lkpMagacinUlaz.LookupMember != null
                ? Convert.ToInt32(lkpMagacinUlaz.LookupMember)
                : (int?)null;
            SelectedObject.VrijemePrenosa = dtDatum.Value;
            SelectedObject.TipPrenosa = tbTipPrenosa.Text.Equals("Prenos")?"P":"N";
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbTipPrenosa.Text = "";
            dtDatum.Value = DateTime.Now;
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            var grid = GetDataGridView();
            grid.Columns[Prenosnica.F_DogadjajId].Visible = false;
            grid.Columns[Prenosnica.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Prenosnica.F_NazivDogadjaja].Width = 200;
            grid.Columns[Prenosnica.F_Godina].Width = 70;
            grid.Columns[Prenosnica.F_MagacinOdredisteId].Visible = false;
            grid.Columns[Prenosnica.F_MagacinSourceId].Visible = false;
            grid.Columns[Prenosnica.F_PrenosnicaId].Visible = true;
            grid.Columns[Prenosnica.F_PrenosnicaId].HeaderText = "Broj prenosnice";
            grid.Columns[Prenosnica.F_NazivSuorce].Width = 150;
            grid.Columns[Prenosnica.F_NazivSuorce].HeaderText = "Sa magacina";
            grid.Columns[Prenosnica.F_NazivOdrediste].Width = 150;
            grid.Columns[Prenosnica.F_NazivOdrediste].HeaderText = "Na magacin";
            grid.Columns[Prenosnica.F_VrijemePrenosa].Width = 100;
            grid.Columns[Prenosnica.F_VrijemePrenosa].HeaderText = "Vrijeme prenosa";
            grid.Columns[Prenosnica.F_VrijemePrenosa].DefaultCellStyle.Format = "dd.MM.yyyy. HH:mm";
            grid.Columns[Prenosnica.F_TipPrenosa].HeaderText = "Tip prenosa";
            grid.Columns[Prenosnica.F_PrenosnicaLookup].Visible = false;

        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            FillLookups();
        }

        public override void Temp_BtnSaveYes()
        {
            base.Temp_BtnSaveYes();

        }

        public override void AddNextForms()
        {
            base.AddNextForms();

            {
                var form = new FormPrenosnicaStavka();
                form.Filters.Add(new TemplateFilter(PrenosnicaStavka.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(PrenosnicaStavka.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(PrenosnicaStavka.F_PrenosnicaId, SelectedObject.PrenosnicaId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
        }
        public override void AddReportForms()
        {
            base.AddReportForms();

            {
                if (SelectedObject != null)
                {
                    this.ReportForms.Clear();
                    var form = new FormIzvPregledPrenosnica(SelectedObject.PrenosnicaId);
                    ReportForms.Add(form);
                }
            }
        }


        private void FillLookups()
        {
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
            
            lkpMagacinUlaz.DisplayMember = Magacin.F_Naziv;
            lkpMagacinUlaz.ValueMember = Magacin.F_MagacinId;
            lkpMagacinUlaz.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]
                                            ,new TemplateFilter(Magacin.F_MagacinId,(SelectedObject.MagacinOdredisteId!=null?SelectedObject.MagacinOdredisteId.ToString():"null"),"=")});

            lkpMagacinIzlaz.DisplayMember = Magacin.F_Naziv;
            lkpMagacinIzlaz.ValueMember = Magacin.F_MagacinId;
            lkpMagacinIzlaz.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]
                                            ,new TemplateFilter(Magacin.F_MagacinId,(SelectedObject.MagacinSourceId!=null?SelectedObject.MagacinSourceId.ToString():"null"),"=")});

        }

    }
}
