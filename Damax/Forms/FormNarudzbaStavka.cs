﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormNarudzbaStavka : FormTemplate<NarudzbaStavka>
    {
        public FormNarudzbaStavka()
        {
            InitializeComponent();
            DaoObject = new NarudzbaStavkaDao();
            SetFormControlsSettings();
            LockUnlockMenuButtons(false, false, false);
            lkpDogadjaj.RefreshLookup += new EventHandler(lookup_RefreashLookupDogadjaj);
            lkpProdajnoMjesto.RefreshLookup += new EventHandler(lookup_RefreashLookupProdajnoMjesto);
            lkpArtikalId.RefreshLookup += new EventHandler(lookup_RefreashLookupArtikal);

        }


        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbKolicina.ReadOnly = !isEnable;
            tbIznos.ReadOnly = !isEnable;
            lkpArtikalId.Enabled = isEnable;
            lkpDogadjaj.Enabled = false;
            dtpGodina.Enabled = false;
            lkpProdajnoMjesto.Enabled = false;
            tbNarudzbaId.ReadOnly = false;

        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;
            tbIznos.Text = SelectedObject.Iznos?.ToString() ?? "0";
            tbKolicina.Text = SelectedObject.Kolicina.ToString();
            tbNarudzbaId.Text = SelectedObject.NarudzbaId.ToString();

        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                int br;
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?)null;
                SelectedObject.ProdajnoMjestoId = lkpProdajnoMjesto.LookupMember != null
                     ? Convert.ToInt32(lkpProdajnoMjesto.LookupMember)
                     : (int?)null;
                SelectedObject.ArtikalId = lkpArtikalId.LookupMember != null
                     ? Convert.ToInt32(lkpArtikalId.LookupMember)
                     : (int?)null;
                SelectedObject.Iznos = int.TryParse(tbIznos.Text, out br) ? br : 0;
                SelectedObject.Godina = (short?)dtpGodina.Value.Year;
                SelectedObject.Kolicina = int.TryParse(tbKolicina.Text, out br) ? br : 0;
                SelectedObject.NarudzbaId = int.TryParse(tbNarudzbaId.Text, out br) ? br : -1;

            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbIznos.Text = "";
            tbNarudzbaId.Text = "";
            tbKolicina.Text = "";
        }


        public override bool ValidateControls()
        {
            decimal value;
            int num;
            if (!decimal.TryParse(tbIznos.Text, out value))
            {
                tbIznos.Text = "";
                GetErrorProvider().SetError(tbIznos, "Neispravan format podatka.");
                IsValid = false;
            }
            if (!int.TryParse(tbKolicina.Text, out num))
            {
                tbKolicina.Text = "";
                GetErrorProvider().SetError(tbKolicina, "Neispravan format podatka.");
                IsValid = false;
            }

            if (lkpDogadjaj.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (lkpProdajnoMjesto.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpProdajnoMjesto, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            if (lkpArtikalId.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpArtikalId, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();

        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[NarudzbaStavka.F_DogadjajId].Visible = false;
            grid.Columns[NarudzbaStavka.F_DogadjajId].HeaderText = "Dogadjaj";
            grid.Columns[NarudzbaStavka.F_Godina].Width = 70;
            //grid.Columns[Narudzba.F_Iznos].HeaderText = "Iznos";
            //grid.Columns[Narudzba.F_Iznos].DefaultCellStyle.Format = "#.##";
            //grid.Columns[Narudzba.F_Iznos].Width = 80;
            grid.Columns[NarudzbaStavka.F_ProdajnoMjestoId].Visible = false;
            grid.Columns[NarudzbaStavka.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[NarudzbaStavka.F_NazivDogadjaja].Width = 200;
            grid.Columns[NarudzbaStavka.F_NazivProdajnogMjesta].HeaderText = "Prodajno mjesto";
            grid.Columns[NarudzbaStavka.F_NazivProdajnogMjesta].Width = 200;
            //grid.Columns[Narudzba.F_DatumNarudzbe].HeaderText = "Datum";
            //grid.Columns[Narudzba.F_DatumNarudzbe].DefaultCellStyle.Format = "dd.MM.yyyy. HH:mm";
            //grid.Columns[Narudzba.F_DatumNarudzbe].Width = 150;
            grid.Columns[NarudzbaStavka.F_NarudzbaId].HeaderText = "Rbr narudzbe";
            grid.Columns[NarudzbaStavka.F_StavkaId].HeaderText = "Rbr";
            grid.Columns[NarudzbaStavka.F_ArtikalId].Visible = false;
            grid.Columns[NarudzbaStavka.F_NazivAtrikla].HeaderText = "Artikal";





        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            setLookups();
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            setLookups();
        }

        private void setLookups()
        {
            var eventSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = eventSource;

            var source = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1], Filters[2] });
            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = source;

            List<Artikal> sourceArtikal;
            if (FormMode != Constants.Form_Mode.MODE_ADD)
                sourceArtikal =
                    (new ArtikalDao()).Select(new List<TemplateFilter>()
                    {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                        new TemplateFilter(Artikal.F_ArtikalId, SelectedObject.ArtikalId.ToString(), "=")
                    });
            else
            {
                sourceArtikal = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="), });
            }
            lkpArtikalId.DisplayMember = Artikal.F_Naziv;
            lkpArtikalId.ValueMember = Artikal.F_ArtikalId;
            lkpArtikalId.DataSource = sourceArtikal;
        }


        protected void lookup_RefreashLookupDogadjaj(object sender, EventArgs e)
        {

            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }


        protected void lookup_RefreashLookupProdajnoMjesto(object sender, EventArgs e)
        {

            var source = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1], Filters[2] });
            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = source;
        }

        protected void lookup_RefreashLookupArtikal(object sender, EventArgs e)
        {
            List<Artikal> sourceArtikal = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                new TemplateFilter(Artikal.F_ArtikalId,SelectedObject.ArtikalId.ToString(),"=")});
            lkpArtikalId.DisplayMember = Artikal.F_Naziv;
            lkpArtikalId.ValueMember = Artikal.F_ArtikalId;
            lkpArtikalId.DataSource = sourceArtikal;
        }

    }
}
