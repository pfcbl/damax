﻿namespace Damax.Forms
{
    partial class FormPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.dgArtikli = new System.Windows.Forms.DataGridView();
            this.tbArtikal = new System.Windows.Forms.TextBox();
            this.tbKolicina = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lkpMagacinOdrediste = new Damax.Template.LookupComboBox();
            this.gbSaMagacina = new System.Windows.Forms.GroupBox();
            this.lblNaStanju = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDodavanjeKolicine = new System.Windows.Forms.NumericUpDown();
            this.lkpMagacin = new Damax.Template.LookupComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgArtikli)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gbSaMagacina.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDodavanjeKolicine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDodaj
            // 
            this.btnDodaj.Image = global::Damax.Properties.Resources.right;
            this.btnDodaj.Location = new System.Drawing.Point(239, 48);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(23, 23);
            this.btnDodaj.TabIndex = 9;
            this.btnDodaj.Tag = "Dodaj na trenutnu kolicinu";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // dgArtikli
            // 
            this.dgArtikli.AllowUserToAddRows = false;
            this.dgArtikli.AllowUserToDeleteRows = false;
            this.dgArtikli.AllowUserToOrderColumns = true;
            this.dgArtikli.AllowUserToResizeRows = false;
            this.dgArtikli.BackgroundColor = System.Drawing.Color.White;
            this.dgArtikli.ColumnHeadersHeight = 30;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgArtikli.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgArtikli.Location = new System.Drawing.Point(98, 100);
            this.dgArtikli.Name = "dgArtikli";
            this.dgArtikli.RowHeadersVisible = false;
            this.dgArtikli.RowTemplate.Height = 25;
            this.dgArtikli.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgArtikli.Size = new System.Drawing.Size(209, 10);
            this.dgArtikli.TabIndex = 11;
            this.dgArtikli.Visible = false;
            // 
            // tbArtikal
            // 
            this.tbArtikal.Enabled = false;
            this.tbArtikal.Location = new System.Drawing.Point(74, 69);
            this.tbArtikal.Name = "tbArtikal";
            this.tbArtikal.Size = new System.Drawing.Size(141, 20);
            this.tbArtikal.TabIndex = 12;
            this.tbArtikal.TabStop = false;
            // 
            // tbKolicina
            // 
            this.tbKolicina.Enabled = false;
            this.tbKolicina.Location = new System.Drawing.Point(6, 69);
            this.tbKolicina.Name = "tbKolicina";
            this.tbKolicina.Size = new System.Drawing.Size(62, 20);
            this.tbKolicina.TabIndex = 13;
            this.tbKolicina.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(6, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Unesite kolicinu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Kolicina";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label5.Location = new System.Drawing.Point(74, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Artikal";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lkpMagacinOdrediste);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbArtikal);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbKolicina);
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.groupBox1.Location = new System.Drawing.Point(268, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 98);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Na magacin";
            // 
            // lkpMagacinOdrediste
            // 
            this.lkpMagacinOdrediste.BackColor = System.Drawing.Color.White;
            this.lkpMagacinOdrediste.ComboSelectedIndex = -1;
            this.lkpMagacinOdrediste.DataSource = null;
            this.lkpMagacinOdrediste.DisplayMember = "";
            this.lkpMagacinOdrediste.Enabled = false;
            this.lkpMagacinOdrediste.Location = new System.Drawing.Point(6, 19);
            this.lkpMagacinOdrediste.LookupMember = null;
            this.lkpMagacinOdrediste.Name = "lkpMagacinOdrediste";
            this.lkpMagacinOdrediste.SelectedLookupValue = null;
            this.lkpMagacinOdrediste.Size = new System.Drawing.Size(209, 21);
            this.lkpMagacinOdrediste.TabIndex = 7;
            this.lkpMagacinOdrediste.ValueMember = "";
            // 
            // gbSaMagacina
            // 
            this.gbSaMagacina.Controls.Add(this.lblNaStanju);
            this.gbSaMagacina.Controls.Add(this.label2);
            this.gbSaMagacina.Controls.Add(this.tbDodavanjeKolicine);
            this.gbSaMagacina.Controls.Add(this.lkpMagacin);
            this.gbSaMagacina.Controls.Add(this.label1);
            this.gbSaMagacina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.gbSaMagacina.Location = new System.Drawing.Point(12, 5);
            this.gbSaMagacina.Name = "gbSaMagacina";
            this.gbSaMagacina.Size = new System.Drawing.Size(221, 98);
            this.gbSaMagacina.TabIndex = 20;
            this.gbSaMagacina.TabStop = false;
            this.gbSaMagacina.Text = "Sa magacina";
            // 
            // lblNaStanju
            // 
            this.lblNaStanju.AutoSize = true;
            this.lblNaStanju.Location = new System.Drawing.Point(88, 48);
            this.lblNaStanju.Name = "lblNaStanju";
            this.lblNaStanju.Size = new System.Drawing.Size(0, 13);
            this.lblNaStanju.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Na stanju:";
            // 
            // tbDodavanjeKolicine
            // 
            this.tbDodavanjeKolicine.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbDodavanjeKolicine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbDodavanjeKolicine.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbDodavanjeKolicine.Location = new System.Drawing.Point(91, 69);
            this.tbDodavanjeKolicine.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbDodavanjeKolicine.Name = "tbDodavanjeKolicine";
            this.tbDodavanjeKolicine.Size = new System.Drawing.Size(124, 20);
            this.tbDodavanjeKolicine.TabIndex = 15;
            this.tbDodavanjeKolicine.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbDodavanjeKolicine.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // lkpMagacin
            // 
            this.lkpMagacin.BackColor = System.Drawing.Color.White;
            this.lkpMagacin.ComboSelectedIndex = -1;
            this.lkpMagacin.DataSource = null;
            this.lkpMagacin.DisplayMember = "";
            this.lkpMagacin.Location = new System.Drawing.Point(6, 19);
            this.lkpMagacin.LookupMember = null;
            this.lkpMagacin.Name = "lkpMagacin";
            this.lkpMagacin.SelectedLookupValue = null;
            this.lkpMagacin.Size = new System.Drawing.Size(209, 21);
            this.lkpMagacin.TabIndex = 6;
            this.lkpMagacin.ValueMember = "";
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "Dodaj na trenutnu kolicinu";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "Promjena stanja artikla";
            this.notifyIcon1.Visible = true;
            // 
            // FormPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(501, 113);
            this.Controls.Add(this.gbSaMagacina);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.dgArtikli);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormPopup";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Izmjena stanja artikla";
            ((System.ComponentModel.ISupportInitialize)(this.dgArtikli)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbSaMagacina.ResumeLayout(false);
            this.gbSaMagacina.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbDodavanjeKolicine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Template.LookupComboBox lkpMagacin;
        private Template.LookupComboBox lkpMagacinOdrediste;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.DataGridView dgArtikli;
        private System.Windows.Forms.TextBox tbArtikal;
        private System.Windows.Forms.TextBox tbKolicina;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbSaMagacina;
        private System.Windows.Forms.NumericUpDown tbDodavanjeKolicine;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblNaStanju;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}