﻿using Damax.Template;

namespace Damax.Forms
{
    public partial class FormArtikal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.cdColor = new System.Windows.Forms.ColorDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbBojaText = new System.Windows.Forms.TextBox();
            this.tbPuniNnaziv = new System.Windows.Forms.TextBox();
            this.lblKategorija = new System.Windows.Forms.Label();
            this.lblNeaktivan = new System.Windows.Forms.Label();
            this.lblFavorit = new System.Windows.Forms.Label();
            this.lblJedMjere = new System.Windows.Forms.Label();
            this.lblBoja = new System.Windows.Forms.Label();
            this.btnColor = new System.Windows.Forms.Button();
            this.lblKolicina = new System.Windows.Forms.Label();
            this.lblCijena = new System.Windows.Forms.Label();
            this.tbBoja = new System.Windows.Forms.TextBox();
            this.tbJedMjere = new System.Windows.Forms.TextBox();
            this.cbNeaktivann = new System.Windows.Forms.CheckBox();
            this.cbFavorit = new System.Windows.Forms.CheckBox();
            this.lblPuniNaziv = new System.Windows.Forms.Label();
            this.lkpKategorija = new Damax.Template.LookupComboBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.tbCijena = new System.Windows.Forms.NumericUpDown();
            this.tbKolicina = new System.Windows.Forms.NumericUpDown();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.ttObrisiSve = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tbCijena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKolicina)).BeginInit();
            this.SuspendLayout();
            // 
            // materialDivider2
            // 
            this.materialDivider2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(0, 253);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(856, 1);
            this.materialDivider2.TabIndex = 41;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(79, 80);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(756, 20);
            this.textBox1.TabIndex = 21;
            // 
            // tbBojaText
            // 
            this.tbBojaText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbBojaText.Location = new System.Drawing.Point(243, 270);
            this.tbBojaText.Name = "tbBojaText";
            this.tbBojaText.Size = new System.Drawing.Size(126, 20);
            this.tbBojaText.TabIndex = 9;
            this.tbBojaText.Visible = false;
            // 
            // tbPuniNnaziv
            // 
            this.tbPuniNnaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPuniNnaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbPuniNnaziv.Location = new System.Drawing.Point(101, 153);
            this.tbPuniNnaziv.Name = "tbPuniNnaziv";
            this.tbPuniNnaziv.Size = new System.Drawing.Size(767, 20);
            this.tbPuniNnaziv.TabIndex = 2;
            // 
            // lblKategorija
            // 
            this.lblKategorija.AutoSize = true;
            this.lblKategorija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKategorija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKategorija.Location = new System.Drawing.Point(19, 93);
            this.lblKategorija.Name = "lblKategorija";
            this.lblKategorija.Size = new System.Drawing.Size(64, 13);
            this.lblKategorija.TabIndex = 55;
            this.lblKategorija.Text = "Kategorija";
            // 
            // lblNeaktivan
            // 
            this.lblNeaktivan.AutoSize = true;
            this.lblNeaktivan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNeaktivan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNeaktivan.Location = new System.Drawing.Point(591, 268);
            this.lblNeaktivan.Name = "lblNeaktivan";
            this.lblNeaktivan.Size = new System.Drawing.Size(56, 13);
            this.lblNeaktivan.TabIndex = 54;
            this.lblNeaktivan.Text = "Neaktivan";
            this.lblNeaktivan.Visible = false;
            // 
            // lblFavorit
            // 
            this.lblFavorit.AutoSize = true;
            this.lblFavorit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFavorit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblFavorit.Location = new System.Drawing.Point(19, 224);
            this.lblFavorit.Name = "lblFavorit";
            this.lblFavorit.Size = new System.Drawing.Size(39, 13);
            this.lblFavorit.TabIndex = 53;
            this.lblFavorit.Text = "Favorit";
            // 
            // lblJedMjere
            // 
            this.lblJedMjere.AutoSize = true;
            this.lblJedMjere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblJedMjere.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblJedMjere.Location = new System.Drawing.Point(712, 269);
            this.lblJedMjere.Name = "lblJedMjere";
            this.lblJedMjere.Size = new System.Drawing.Size(21, 13);
            this.lblJedMjere.TabIndex = 52;
            this.lblJedMjere.Text = "JM";
            this.lblJedMjere.Visible = false;
            // 
            // lblBoja
            // 
            this.lblBoja.AutoSize = true;
            this.lblBoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBoja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblBoja.Location = new System.Drawing.Point(19, 273);
            this.lblBoja.Name = "lblBoja";
            this.lblBoja.Size = new System.Drawing.Size(28, 13);
            this.lblBoja.TabIndex = 51;
            this.lblBoja.Text = "Boja";
            this.lblBoja.Visible = false;
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.White;
            this.btnColor.Image = global::Damax.Properties.Resources.color_picker1;
            this.btnColor.Location = new System.Drawing.Point(207, 270);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(30, 20);
            this.btnColor.TabIndex = 9;
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Visible = false;
            // 
            // lblKolicina
            // 
            this.lblKolicina.AutoSize = true;
            this.lblKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKolicina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKolicina.Location = new System.Drawing.Point(382, 272);
            this.lblKolicina.Name = "lblKolicina";
            this.lblKolicina.Size = new System.Drawing.Size(44, 13);
            this.lblKolicina.TabIndex = 49;
            this.lblKolicina.Text = "Kolicina";
            this.lblKolicina.Visible = false;
            // 
            // lblCijena
            // 
            this.lblCijena.AutoSize = true;
            this.lblCijena.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCijena.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblCijena.Location = new System.Drawing.Point(19, 187);
            this.lblCijena.Name = "lblCijena";
            this.lblCijena.Size = new System.Drawing.Size(42, 13);
            this.lblCijena.TabIndex = 48;
            this.lblCijena.Text = "Cijena";
            // 
            // tbBoja
            // 
            this.tbBoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbBoja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbBoja.Location = new System.Drawing.Point(101, 270);
            this.tbBoja.Name = "tbBoja";
            this.tbBoja.Size = new System.Drawing.Size(100, 20);
            this.tbBoja.TabIndex = 8;
            this.tbBoja.Visible = false;
            // 
            // tbJedMjere
            // 
            this.tbJedMjere.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbJedMjere.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbJedMjere.Location = new System.Drawing.Point(768, 266);
            this.tbJedMjere.Name = "tbJedMjere";
            this.tbJedMjere.Size = new System.Drawing.Size(100, 20);
            this.tbJedMjere.TabIndex = 5;
            this.tbJedMjere.Visible = false;
            // 
            // cbNeaktivann
            // 
            this.cbNeaktivann.AutoSize = true;
            this.cbNeaktivann.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbNeaktivann.Location = new System.Drawing.Point(673, 270);
            this.cbNeaktivann.Name = "cbNeaktivann";
            this.cbNeaktivann.Size = new System.Drawing.Size(15, 14);
            this.cbNeaktivann.TabIndex = 7;
            this.cbNeaktivann.UseVisualStyleBackColor = true;
            this.cbNeaktivann.Visible = false;
            // 
            // cbFavorit
            // 
            this.cbFavorit.AutoSize = true;
            this.cbFavorit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbFavorit.Location = new System.Drawing.Point(101, 226);
            this.cbFavorit.Name = "cbFavorit";
            this.cbFavorit.Size = new System.Drawing.Size(15, 14);
            this.cbFavorit.TabIndex = 6;
            this.cbFavorit.UseVisualStyleBackColor = true;
            // 
            // lblPuniNaziv
            // 
            this.lblPuniNaziv.AutoSize = true;
            this.lblPuniNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPuniNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblPuniNaziv.Location = new System.Drawing.Point(19, 156);
            this.lblPuniNaziv.Name = "lblPuniNaziv";
            this.lblPuniNaziv.Size = new System.Drawing.Size(56, 13);
            this.lblPuniNaziv.TabIndex = 43;
            this.lblPuniNaziv.Text = "Puni naziv";
            // 
            // lkpKategorija
            // 
            this.lkpKategorija.BackColor = System.Drawing.Color.White;
            this.lkpKategorija.ComboSelectedIndex = -1;
            this.lkpKategorija.DataSource = null;
            this.lkpKategorija.DeleteButtonEnable = true;
            this.lkpKategorija.DisplayMember = "";
            this.lkpKategorija.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpKategorija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpKategorija.Location = new System.Drawing.Point(101, 89);
            this.lkpKategorija.LookupMember = null;
            this.lkpKategorija.Name = "lkpKategorija";
            this.lkpKategorija.RefreashButtonEnable = true;
            this.lkpKategorija.SelectedLookupValue = null;
            this.lkpKategorija.Size = new System.Drawing.Size(268, 21);
            this.lkpKategorija.TabIndex = 0;
            this.lkpKategorija.ValueMember = "";
            // 
            // tbNaziv
            // 
            this.tbNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNaziv.Location = new System.Drawing.Point(101, 122);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(767, 20);
            this.tbNaziv.TabIndex = 1;
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(19, 125);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(39, 13);
            this.lblNaziv.TabIndex = 40;
            this.lblNaziv.Text = "Naziv";
            // 
            // tbCijena
            // 
            this.tbCijena.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbCijena.DecimalPlaces = 2;
            this.tbCijena.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbCijena.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbCijena.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbCijena.Location = new System.Drawing.Point(101, 187);
            this.tbCijena.Name = "tbCijena";
            this.tbCijena.Size = new System.Drawing.Size(120, 20);
            this.tbCijena.TabIndex = 3;
            this.tbCijena.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbCijena.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.tbCijena.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKolicina_KeyPress);
            // 
            // tbKolicina
            // 
            this.tbKolicina.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbKolicina.DecimalPlaces = 2;
            this.tbKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbKolicina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbKolicina.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbKolicina.Location = new System.Drawing.Point(443, 269);
            this.tbKolicina.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.tbKolicina.Name = "tbKolicina";
            this.tbKolicina.Size = new System.Drawing.Size(120, 20);
            this.tbKolicina.TabIndex = 4;
            this.tbKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbKolicina.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.tbKolicina.Visible = false;
            this.tbKolicina.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKolicina_KeyPress);
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(595, 61);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(273, 21);
            this.lkpDogadjaj.TabIndex = 68;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(508, 64);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 70;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(19, 64);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 69;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(101, 61);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 67;
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnDeleteAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDeleteAll.FlatAppearance.BorderSize = 0;
            this.btnDeleteAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAll.ForeColor = System.Drawing.Color.White;
            this.btnDeleteAll.Image = global::Damax.Properties.Resources.delete_all__1_;
            this.btnDeleteAll.Location = new System.Drawing.Point(435, 5);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(40, 40);
            this.btnDeleteAll.TabIndex = 80;
            this.btnDeleteAll.UseVisualStyleBackColor = false;
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            this.btnDeleteAll.MouseHover += new System.EventHandler(this.btnDeleteAll_MouseHover);
            // 
            // FormArtikal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.btnDeleteAll);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.tbKolicina);
            this.Controls.Add(this.tbCijena);
            this.Controls.Add(this.tbBojaText);
            this.Controls.Add(this.tbPuniNnaziv);
            this.Controls.Add(this.lblKategorija);
            this.Controls.Add(this.lblNeaktivan);
            this.Controls.Add(this.lblFavorit);
            this.Controls.Add(this.lblJedMjere);
            this.Controls.Add(this.lblBoja);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.lblKolicina);
            this.Controls.Add(this.lblCijena);
            this.Controls.Add(this.tbBoja);
            this.Controls.Add(this.tbJedMjere);
            this.Controls.Add(this.cbNeaktivann);
            this.Controls.Add(this.cbFavorit);
            this.Controls.Add(this.lblPuniNaziv);
            this.Controls.Add(this.lkpKategorija);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormArtikal";
            this.Text = "Artikli";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.tbNaziv, 0);
            this.Controls.SetChildIndex(this.lkpKategorija, 0);
            this.Controls.SetChildIndex(this.lblPuniNaziv, 0);
            this.Controls.SetChildIndex(this.cbFavorit, 0);
            this.Controls.SetChildIndex(this.cbNeaktivann, 0);
            this.Controls.SetChildIndex(this.tbJedMjere, 0);
            this.Controls.SetChildIndex(this.tbBoja, 0);
            this.Controls.SetChildIndex(this.lblCijena, 0);
            this.Controls.SetChildIndex(this.lblKolicina, 0);
            this.Controls.SetChildIndex(this.btnColor, 0);
            this.Controls.SetChildIndex(this.lblBoja, 0);
            this.Controls.SetChildIndex(this.lblJedMjere, 0);
            this.Controls.SetChildIndex(this.lblFavorit, 0);
            this.Controls.SetChildIndex(this.lblNeaktivan, 0);
            this.Controls.SetChildIndex(this.lblKategorija, 0);
            this.Controls.SetChildIndex(this.tbPuniNnaziv, 0);
            this.Controls.SetChildIndex(this.tbBojaText, 0);
            this.Controls.SetChildIndex(this.tbCijena, 0);
            this.Controls.SetChildIndex(this.tbKolicina, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.btnDeleteAll, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tbCijena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbKolicina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ColorDialog cdColor;
        private System.Windows.Forms.TextBox textBox1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private System.Windows.Forms.TextBox tbBojaText;
        private System.Windows.Forms.TextBox tbPuniNnaziv;
        private System.Windows.Forms.Label lblKategorija;
        private System.Windows.Forms.Label lblNeaktivan;
        private System.Windows.Forms.Label lblFavorit;
        private System.Windows.Forms.Label lblJedMjere;
        private System.Windows.Forms.Label lblBoja;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Label lblKolicina;
        private System.Windows.Forms.Label lblCijena;
        private System.Windows.Forms.TextBox tbBoja;
        private System.Windows.Forms.TextBox tbJedMjere;
        private System.Windows.Forms.CheckBox cbNeaktivann;
        private System.Windows.Forms.CheckBox cbFavorit;
        private System.Windows.Forms.Label lblPuniNaziv;
        private LookupComboBox lkpKategorija;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.NumericUpDown tbCijena;
        private System.Windows.Forms.NumericUpDown tbKolicina;
        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Button btnDeleteAll;
        private System.Windows.Forms.ToolTip ttObrisiSve;
    }
}