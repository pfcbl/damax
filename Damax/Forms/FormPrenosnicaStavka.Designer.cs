﻿namespace Damax.Forms
{
    partial class FormPrenosnicaStavka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPocetnoStanje = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPrenosnica = new System.Windows.Forms.Label();
            this.lkpArtikal = new Damax.Template.LookupComboBox();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.tbPrenosnica = new System.Windows.Forms.TextBox();
            this.tbKolicina = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblPocetnoStanje
            // 
            this.lblPocetnoStanje.AutoSize = true;
            this.lblPocetnoStanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPocetnoStanje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblPocetnoStanje.Location = new System.Drawing.Point(12, 183);
            this.lblPocetnoStanje.Name = "lblPocetnoStanje";
            this.lblPocetnoStanje.Size = new System.Drawing.Size(78, 13);
            this.lblPocetnoStanje.TabIndex = 85;
            this.lblPocetnoStanje.Text = "Pocetno stanje";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(12, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 84;
            this.label2.Text = "Artikal";
            // 
            // lblPrenosnica
            // 
            this.lblPrenosnica.AutoSize = true;
            this.lblPrenosnica.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPrenosnica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblPrenosnica.Location = new System.Drawing.Point(12, 103);
            this.lblPrenosnica.Name = "lblPrenosnica";
            this.lblPrenosnica.Size = new System.Drawing.Size(70, 13);
            this.lblPrenosnica.TabIndex = 83;
            this.lblPrenosnica.Text = "Prenosnica";
            // 
            // lkpArtikal
            // 
            this.lkpArtikal.BackColor = System.Drawing.Color.White;
            this.lkpArtikal.ComboSelectedIndex = -1;
            this.lkpArtikal.DataSource = null;
            this.lkpArtikal.DeleteButtonEnable = true;
            this.lkpArtikal.DisplayMember = "";
            this.lkpArtikal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpArtikal.Location = new System.Drawing.Point(121, 138);
            this.lkpArtikal.LookupMember = null;
            this.lkpArtikal.Name = "lkpArtikal";
            this.lkpArtikal.RefreashButtonEnable = true;
            this.lkpArtikal.SelectedLookupValue = null;
            this.lkpArtikal.Size = new System.Drawing.Size(306, 21);
            this.lkpArtikal.TabIndex = 82;
            this.lkpArtikal.ValueMember = "";
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(562, 63);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(306, 21);
            this.lkpDogadjaj.TabIndex = 78;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(488, 67);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 80;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 67);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 79;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 63);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 77;
            // 
            // tbPrenosnica
            // 
            this.tbPrenosnica.Location = new System.Drawing.Point(121, 99);
            this.tbPrenosnica.Name = "tbPrenosnica";
            this.tbPrenosnica.Size = new System.Drawing.Size(75, 20);
            this.tbPrenosnica.TabIndex = 87;
            // 
            // tbKolicina
            // 
            this.tbKolicina.Location = new System.Drawing.Point(121, 180);
            this.tbKolicina.Name = "tbKolicina";
            this.tbKolicina.Size = new System.Drawing.Size(75, 20);
            this.tbKolicina.TabIndex = 88;
            // 
            // FormPrenosnicaStavka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.tbKolicina);
            this.Controls.Add(this.tbPrenosnica);
            this.Controls.Add(this.lblPocetnoStanje);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPrenosnica);
            this.Controls.Add(this.lkpArtikal);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormPrenosnicaStavka";
            this.Text = "Stavke prenosnica";
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpArtikal, 0);
            this.Controls.SetChildIndex(this.lblPrenosnica, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblPocetnoStanje, 0);
            this.Controls.SetChildIndex(this.tbPrenosnica, 0);
            this.Controls.SetChildIndex(this.tbKolicina, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPocetnoStanje;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPrenosnica;
        private Template.LookupComboBox lkpArtikal;
        private Template.LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.TextBox tbPrenosnica;
        private System.Windows.Forms.TextBox tbKolicina;
    }
}