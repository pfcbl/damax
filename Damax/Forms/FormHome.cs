﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using Damax.DAO;
using Damax.Models;
using Damax.Models.ModelsCharts;
using Damax.Models.ModelsReports;
using Damax.Properties;
using Damax.Template;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using ModulFinansijsko.Controls;
using MessageBox = System.Windows.Forms.MessageBox;
using Point = System.Windows.Point;
using Separator = LiveCharts.Wpf.Separator;

namespace Damax.Forms
{
	public partial class FormHome : Form
	{

		public bool IsAdministrator { get; set; }

		public int AppUserMode { get; set; }

        public string Valuta { get; set; }

        #region konstruktori
        public FormHome(Radnik logovaniRadnik)
		{
			InitializeComponent();
			lblLogovaniKorisnik.Text = Resources.Lbl_Logovani_korisnik + logovaniRadnik.KorisnickoIme;
			HomeFilters.HomeFilterLogovaniRadnik = logovaniRadnik;
			HomeFilters.HomeFilterAktivanDogadjaj = false;
			SetHomeSettings();
			lkpDogadjaj.RefreshLookup += Lookup_RefreashLookup;
			lkpDogadjaj.LookupValueChanged += Lookup_ValueChanged;
            
   //         PopuniDiagramUplate();
			//PopuniPotrosnjaDijagrame();
			PopuniTabeluMagaciniArtikli();
			//PopuniDiagramNajprodavanijiArtikli();

			msGlavniMeni.Renderer = new MenuRender();
		}

		public FormHome()
		{
			InitializeComponent();

			msGlavniMeni.Renderer = new MenuRender();

		}

		#endregion

		private void SetHomeSettings()
		{
			dtpGodina.Format = DateTimePickerFormat.Custom;
			dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
			dtpGodina.ShowUpDown = true;
			dtpGodina.Value = DateTime.Now;

			dtProdajaOd.Format = DateTimePickerFormat.Custom;
			dtProdajaOd.CustomFormat = Resources.DateTime_Format;
			dtProdajaDo.Format = DateTimePickerFormat.Custom;
			dtProdajaDo.CustomFormat = Resources.DateTime_Format;

			dtUplateDatumOd.Format = DateTimePickerFormat.Custom;
			dtUplateDatumOd.CustomFormat = Resources.DateTime_Format;
			dtUplateDatumDo.Format = DateTimePickerFormat.Custom;
			dtUplateDatumDo.CustomFormat = Resources.DateTime_Format;

            TipKorisnikaPodesavanje(HomeFilters.HomeFilterLogovaniRadnik.TipKorisnika);

            var filters = new List<TemplateFilter>()
						{
								new TemplateFilter(Event.F_Godina, dtpGodina.Value.Year.ToString(), "=")
						};
			var source = (new EventDao()).Select(filters);


			lkpDogadjaj.DisplayMember = Event.F_Naziv;
			lkpDogadjaj.ValueMember = Event.F_DogadjajId;
			lkpDogadjaj.DataSource = source;

			if (source.Count <= 0) return;
			HomeFilters.HomeFilterEvent = source[0];
			HomeFilters.HomeFilterAktivanDogadjaj = HomeFilters.HomeFilterEvent.Aktivan;

            OsvjeziLabele();
			PostaviDatumeDijagrama();
		}

        private void TipKorisnikaPodesavanje(int tipKorinsika)
        {
            switch (tipKorinsika)
            {
                case Constants.Tip_Korisnika.TIP_ADMINISTRATOR:
                    administracijaToolStripMenuItem.Visible = true;
                    btnKorisnici.Visible = true;
                    btnnDogadjaji.Visible = true;
                    btnZakljucajDogadjaj.Visible = true;
                    break;

                case Constants.Tip_Korisnika.TIP_MAGACIONER:
                    sifarniciToolStripMenuItem.Visible = false;
                    uplateToolStripMenuItem.Visible = false;
                    prodajnaMjestaToolStripMenuItem.Visible = false;
                    narudžbeToolStripMenuItem.Visible = false;
                    btnProdajnaMjesta.Enabled = false;
                    btnBlagajne.Enabled = false;
                    administracijaToolStripMenuItem.Visible = false;
                    btnKorisnici.Visible = false;
                    btnnDogadjaji.Visible = false;
                    btnZakljucajDogadjaj.Visible = false;
                    tabControl1.Visible = false;
                    btnBrojizdatihKarticaPoSatima.Enabled = false;
                    btnPregledStanjaMagacina.Enabled = false;
                    btnBrojAktivPoSatima.Enabled = false;
                    btnBrojUplataPoSatima.Enabled = false;
                    btnBrojNarudzbiPoSatima.Enabled = false;
                    btnSaldoSmjene.Enabled = false;
                    btnProsjekUplata.Enabled = false;
                    tsmBrojAktivnihKarticaPoSatima.Visible = false;
                    tsmBrojIzdatihKarticaPoSatima.Visible = false;
                    tsmBrojNarudzbiPoProdajnomMjestu.Visible = false;
                    tsmBrojUplataPoSatimaPoBlagajni.Visible = false;
                    tsmPregledNaplatePoProdajnomMjestu.Visible = false;
                    tsmProsjekUplataPoKarticama.Visible = false;
                    tsmSaldoSmjene.Visible = false;
                    tsmProsjecnaPotrosnjaPoSatima.Visible = false;
                    break;
            }
        }

        #region dijagrami

        private LinearGradientBrush GetGradientChart(System.Windows.Media.Color firstColor, System.Windows.Media.Color secondColor)
		{
			var gradientBrush = new LinearGradientBrush
			{
				StartPoint = new System.Windows.Point(0, 0),
				EndPoint = new Point(0, 1)
			};
			gradientBrush.GradientStops.Add(new GradientStop(firstColor, 0));
			gradientBrush.GradientStops.Add(new GradientStop(secondColor, 1));
			return gradientBrush;
		}

        private void PopuniTabeluMagaciniArtikli()
        {
            var pm = new MagacinDao().Select(new List<TemplateFilter>()
                        {
                                new TemplateFilter(Magacin.F_Godina, dtpGodina.Value.Year.ToString(), "=")
                                ,
                                new TemplateFilter(Magacin.F_DogadjajId, lkpDogadjaj.LookupMember, "=")
                        });

            cbListMagacini.DataSource = pm;
            cbListMagacini.ValueMember = Magacin.F_MagacinId;
            cbListMagacini.DisplayMember = Magacin.F_Naziv;

            var potrosnja = new List<PotrosnjaArtikala>();
            //potrosnja.AddRange(pm.Select(GeneralDao.StanjeMagacina).ToList());
            
            if (cbListMagacini.Items.Count > 0)
            {
                cbListMagacini.SetItemChecked(0, true);
                PostaviGridStanjaMaagacina((Magacin)cbListMagacini.Items[0]);
            }
        }
        private void PopuniPotrosnjaDijagrame()
		{
			var pm = new ProdajnoMjestoDao().Select(new List<TemplateFilter>()
						{
								new TemplateFilter(ProdajnoMjesto.F_Godina, dtpGodina.Value.Year.ToString(), "=")
								,
								new TemplateFilter(ProdajnoMjesto.F_DogadjajId, lkpDogadjaj.LookupMember, "=")
						});

            var potrosnja = new List<PotrosnjaPoSatima>
            {
                GeneralDao.PotrosnjaSatiPojedinacno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId, null, dtProdajaOd.Value, dtProdajaDo.Value, "Ukupno")
            };
            potrosnja.AddRange(pm.Select(item => GeneralDao.PotrosnjaSatiPojedinacno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId, item.ProdajnoMjestoId, dtProdajaOd.Value, dtProdajaDo.Value,item.Naziv)).ToList());

			FillProdajaChart(potrosnja);
		}
		private void PopuniDiagramUplate()
		{
			var bl = new BlagajnaDao().Select(new List<TemplateFilter>()
						{
								new TemplateFilter(Blagajna.F_Godina, dtpGodina.Value.Year.ToString(), "=")
								,
								new TemplateFilter(Blagajna.F_DogadjajId, lkpDogadjaj.LookupMember, "=")
						});
            var uplate = new List<PotrosnjaPoSatima>
            {
                GeneralDao.UplateSatiPojedinacno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId, null, dtUplateDatumOd.Value, dtUplateDatumDo.Value, "Ukupno")
            };
            uplate.AddRange(bl.Select(item => GeneralDao.UplateSatiPojedinacno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId, item.BlagajnaId, dtUplateDatumOd.Value, dtUplateDatumDo.Value,item.Naziv)).ToList());
			FillUplateChart(uplate);

		}
        
        System.Windows.Media.Color[] boje = { System.Windows.Media.Color.FromArgb(255, 18, 86, 136), Colors.DimGray, Colors.Firebrick, Colors.ForestGreen, Colors.Fuchsia, Colors.Indigo, Colors.BlanchedAlmond,Colors.BlueViolet,Colors.DarkMagenta,Colors.DarkRed,Colors.Gold,Colors.Orchid,Colors.Plum,Colors.Tomato,Colors.Chartreuse, Colors.Chocolate, Colors.Crimson, Colors.DarkGoldenrod, Colors.MidnightBlue, Colors.SeaGreen, Colors.IndianRed, Colors.DarkSlateGray, Colors.DarkOliveGreen, Colors.Teal, Colors.SteelBlue, Colors.Sienna, Colors.Red, Colors.RosyBrown, Colors.Peru, Colors.PaleVioletRed, Colors.Navy, Colors.LightSlateGray,Colors.Black, Colors.LightSeaGreen, Colors.MediumPurple, Colors.Olive, Colors.Orange, Colors.Salmon, Colors.YellowGreen, Colors.DodgerBlue };
		List<Series> potrosnjaSeriesList = new List<Series>();
		List<Series> uplateSeriesList = new List<Series>();

       
        private void FillUplateChart(IReadOnlyList<PotrosnjaPoSatima> potrosnja)
		{
			crtUplateSati.Series.Clear();
			crtUplateSati.AxisX.Clear();
			crtUplateSati.AxisY.Clear();
			uplateSeriesList.Clear();
			cbListUplate.Items.Clear();
			int i = 0;
			foreach (PotrosnjaPoSatima item in potrosnja)
			{
				LineSeries ser = new LineSeries()
				{
					//Name = item.Naziv.Replace(" ", "_").Replace("-"),
					Title = item.Naziv,
					Values = GetChartValues(item.Iznos),
					DataLabels = dtUplateDatumOd.Value.AddDays(1) > dtUplateDatumDo.Value,
					LabelPoint = point => point.Y.ToString(),
					Stroke = new SolidColorBrush(boje[i]),

					//Fill = getGradientChart(Colors.Red, System.Windows.Media.Color.FromArgb(255, 248, 211, 49)),

					Foreground = new SolidColorBrush(boje[i]),

					FontSize = 10


				};
				uplateSeriesList.Add(ser);
				crtUplateSati.Series.Add(ser);
				ser.Visibility = item.IsVisible ? Visibility.Hidden : Visibility.Visible;
				cbListUplate.Items.Add(item.Naziv, item.IsVisible);
				i++;
			}

			crtUplateSati.AxisX.Add(new Axis
			{
				Name = "Sati",
				Title = "Sati",
				Labels = potrosnja[0].Sat,
				LabelFormatter = value => value + " h",
				Separator = new Separator // force the separator step to 1, so it always display all labels
				{
					Step = 1,
					IsEnabled = false, //disable it to make it invisible.,



				},
				LabelsRotation = dtUplateDatumOd.Value.AddDays(1) > dtUplateDatumDo.Value ? 20 : 40,

				FontSize = 10,
				Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(10, 50, 100))
			});

			crtUplateSati.AxisY.Add(new Axis
			{
				LabelFormatter = value => value + Valuta,
				Separator = new Separator(),
				Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(10, 50, 100)),
				MinValue = 0,
				FontSize = 10

			});
			crtUplateSati.LegendLocation = LegendLocation.Right;
		}
		private void FillProdajaChart(List<PotrosnjaPoSatima> potrosnja)
		{
			crtProdajaPoSatima.Series.Clear();
			crtProdajaPoSatima.AxisX.Clear();
			crtProdajaPoSatima.AxisY.Clear();
			potrosnjaSeriesList.Clear();
			cbListProdaja.Items.Clear();
			int i = 0;
			foreach (PotrosnjaPoSatima item in potrosnja)
			{
                var ser = new LineSeries()
                {
                    //Name = item.Naziv.Replace(" ", "").Replace(",", ""),
                    Title = item.Naziv,//.Replace(" ", "").Replace(",", ""),
                    Values = GetChartValues(item.Iznos),
                    DataLabels = dtProdajaOd.Value.AddDays(1)>dtProdajaDo.Value,
                    LabelPoint = point => point.Y.ToString(),
					Stroke = new SolidColorBrush(boje[i]),

					//Fill = getGradientChart(Colors.Red, System.Windows.Media.Color.FromArgb(255, 248, 211, 49)),

					Foreground = new SolidColorBrush(boje[i]),

					FontSize = 8


				};
				potrosnjaSeriesList.Add(ser);
				crtProdajaPoSatima.Series.Add(ser);
				ser.Visibility = item.IsVisible ? Visibility.Hidden : Visibility.Visible;
				cbListProdaja.Items.Add(item.Naziv, item.IsVisible);
				i++;
			}

			crtProdajaPoSatima.AxisX.Add(new Axis
			{
				Name = "Sati",
				Title = "Sati",
				Labels = potrosnja[0].Sat,
				LabelFormatter = value => value + " h",
				Separator = new Separator // force the separator step to 1, so it always display all labels
				{
					Step = 1,
					IsEnabled = false, //disable it to make it invisible.,



				},
				LabelsRotation = dtProdajaOd.Value.AddDays(1) > dtProdajaDo.Value?20:40,

				FontSize = 10,
				Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(10, 50, 100))
			});

			crtProdajaPoSatima.AxisY.Add(new Axis
			{
				LabelFormatter = value => value + Valuta,
				Separator = new Separator(),
				Foreground = new SolidColorBrush(System.Windows.Media.Color.FromRgb(10, 50, 100)),
				MinValue = 0,
				FontSize = 10

            });
			crtProdajaPoSatima.LegendLocation = LegendLocation.Right;
		}

		private ChartValues<ObservableValue> GetChartValues(IEnumerable<decimal> valuesList)
		{

			ChartValues<ObservableValue> values = new ChartValues<ObservableValue>();
			foreach (double t in valuesList)
			{
				values.Add(new ObservableValue(t));
			}
			return values;
		}

		private void BtnRefreshChart_Click(object sender, EventArgs e)
		{
            if (dtProdajaDo.Value.Subtract(dtProdajaOd.Value).Days > 2)
                MessageBox.Show("Odabrali ste razliku datuma veću od dva dana, što može prouzrokovati nepregledan prikaz dijagrama.\nPreporučeno je da maksimalna razlika između datuma bude dva dana.","Velika razlika u datumima",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            PopuniPotrosnjaDijagrame();
		}

		private void BtnUplateRefresh_Click(object sender, EventArgs e)
		{
            if (dtUplateDatumOd.Value.Subtract(dtUplateDatumDo.Value).Days > 2)
                MessageBox.Show("Odabrali ste razliku datuma veću od dva dana, što može prouzrokovati nepregledan prikaz dijagrama.\nPreporučeno je da maksimalna razlika između datuma bude dva dana.", "Velika razlika u datumima", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            PopuniDiagramUplate();
		}
		#endregion

		#region menu_items_click

		private void DogadjajiToolStripMenuItem_Click(object sender, EventArgs e)
		{
			var form = new FormEvent();
			form.Filters.Add(new TemplateFilter(Event.F_Godina, dtpGodina.Value.Year.ToString(), "="));
			form.ShowDialog();
		}

		private void BtnBlagajne_Click(object sender, EventArgs e)
		{
            if (lkpDogadjaj.LookupMember != null)
            {

                if (sender == blagajneToolStripMenuItem || sender == btnBlagajne)
                {
                    var form = new FormBlagajna();
                    form.Filters.Add(new TemplateFilter(Blagajna.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Blagajna.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == artikliToolStripMenuItem)
                {
                    var form = new FormArtikal();
                    form.Filters.Add(new TemplateFilter(Artikal.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Artikal.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == kategorijeArtikalaToolStripMenuItem)
                {
                    FormArtikalKategorija form = new FormArtikalKategorija();
                    form.Filters.Add(new TemplateFilter(ArtikalKategorija.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(ArtikalKategorija.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == prodajnaMjestaToolStripMenuItem || sender == btnProdajnaMjesta)
                {
                    var form = new FormProdajnoMjesto();
                    form.Filters.Add(new TemplateFilter(ProdajnoMjesto.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(ProdajnoMjesto.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == narudžbeToolStripMenuItem)
                {
                    var form = new FormNarudzba();
                    form.Filters.Add(new TemplateFilter(Narudzba.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Narudzba.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == magaciniToolStripMenuItem || sender == btnMagacini)
                {
                    var form = new FormMagacin();
                    form.Filters.Add(new TemplateFilter(Magacin.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Magacin.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == transakcijeToolStripMenuItem)
                {
                    var form = new FormTransakcija();
                    form.Filters.Add(new TemplateFilter(Transakcija.F_Godina, dtpGodina.Value.Year.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Transakcija.F_DogadjajId, lkpDogadjaj.LookupMember, "="));
                    form.ShowDialog();
                }
                else if (sender == korisniciToolStripMenuItem || sender == btnKorisnici)
                {
                    var form = new FormRadnik();
                    form.ShowDialog();
                }
                else if (sender == btnPregledStanjaMagacina)
                {
                    if (cbListMagacini.Items.Count > 0)
                        (new FormIzvStanjeMagacina((Magacin)cbListMagacini.Items[cbListMagacini.SelectedIndex])).Show();
                }
                else if (sender == tsmPregledStanjaMagacina)
                {
                    (new FormIzvStanjeMagacina()).Show();
                }
                else if (sender == tsmPregledNaplatePoProdajnomMjestu || sender == btnPregledNaplatepoSatima)
                {
                    (new FormIzvPregledNaplatePoProdajnomMjestu()).Show();
                }
                else if (sender == tsmPregledPotrošnjeArtikalaPoProdajnomMjestu || sender == btnProdajaArtikala)
                {
                    (new FormIzvProdajaArtikalaPoProdajnomMjestu()).Show();
                }
                else if (sender == tsmBrojUplataPoSatimaPoBlagajni || sender == btnBrojUplataPoSatima)
                {
                    (new FormIzvBrojUplataPoSatuPoBlagajni()).Show();
                }
                else if (sender == tsmProsjekUplataPoKarticama || sender == btnProsjekUplata)
                {
                    (new FormIzvProsjekUplataPoSatuPoBlagajni()).Show();
                }
                else if (sender == tsmProsjecnaPotrosnjaPoSatima)
                {
                    (new FormIzvProsjecnaPotrosnjaPoProdajnomMjestu()).Show();
                }
                else if (sender == tsmBrojNarudzbiPoProdajnomMjestu || sender == btnBrojNarudzbiPoSatima)
                {
                    (new FormIzvBrojNarudzbiPoProdajnomMjestu()).Show();
                }
                else if (sender == tsmBrojIzdatihKarticaPoSatima || sender == btnBrojizdatihKarticaPoSatima)
                {
                    (new FormIzvBrojIzdatihKarticaPoSatima()).Show();
                }
                else if (sender == tsmBrojAktivnihKarticaPoSatima || sender == btnBrojAktivPoSatima)
                {
                    (new FormIzvBrojAktivnihKarticaPoSatima()).Show();
                }
                else if (sender == tsmPregledPrenosaArtikala || sender == btnBrojAktivPoSatima)
                {
                    (new FormIzvPregledPrenosaArtikala()).Show();
                }
                else if (sender == tsmSaldoSmjene || sender == btnSaldoSmjene)
                {
                    (new FormIzvSaldoSmjene()).Show();
                }
                else if(sender == btnPrenosArtikala)
                    {
                    var form = new FormPrenosArtikala((Magacin)cbListMagacini.SelectedItem);
                    form.ShowDialog();
                    PostaviGridStanjaMaagacina((Magacin)cbListMagacini.SelectedItem);
                }
                else if (sender == karticeToolStripMenuItem )
                {
                    (new FormKartica()).Show();
                }
                else if (sender == prenosArtikalaToolStripMenuItem || sender == btnPrenosnice)
                {
                    var form = new FormPrenosnica();
                    form.Filters.Add(new TemplateFilter(Prenosnica.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "="));
                    form.Filters.Add(new TemplateFilter(Prenosnica.F_DogadjajId, HomeFilters.HomeFilterDogadjajId.ToString(), "="));
                    form.ShowDialog();
                }
                else if (sender == analitikaKarticaZaPlaćanjeToolStripMenuItem)
                {
                    var form = new FormIzvAnalitikaKartice();
                    form.ShowDialog();
                }
                else if (sender == tsmPregledPrenosnica || sender==btnPrenosnice)
                {
                    (new FormIzvPregledPrenosnica()).Show();
                }
								else if (sender == tsmSaldoSmjene)
								{
									(new FormIzvSaldoSmjene()).Show();
								}

            }
			else
			{
				MessageBox.Show(Resources.FormHome_NijeOdabranDogadjaj_Taxt, Resources.FormHome_NijeOdabranDogadjaj_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

        private void BtnZakljucajDogadjaj_Click(object sender, EventArgs e)
        {
            if (HomeFilters.HomeFilterEvent != null)
            {
                var rez =
                    MessageBox.Show(
                        "Nakon zaključivanja događaja više neće biti moguće mijenjati podatke vezane za ovaj događaj. Da li ste sigurni da želite da zaključite odabrani događaj?",
                        "Zaključivanje događaja", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (rez != DialogResult.Yes) return;
                if (GeneralDao.ZakljucavanjeDogadjaja(HomeFilters.HomeFilterEvent) == 0)
                {
                    MessageBox.Show("Događaj je uspješno zaključen.", "Uspješno zaključivanje", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Desila se greška prilikom zaključavanja događaja. Obratite se administratorima.", "Neuspješno zaključavanje", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Nije odabran ni jedan događaj.", "Zaključivanje događaja", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

		#endregion

		#region controls_events
		private void DtpGodina_ValueChanged(object sender, EventArgs e)
		{
			lkpDogadjaj.DataSource = null;
			var filters = new List<TemplateFilter>()
						{
								new TemplateFilter(Event.F_Godina, dtpGodina.Value.Year.ToString(), "=")
						};
			var source = (new EventDao()).Select(filters);
			lkpDogadjaj.DisplayMember = Event.F_Naziv;
			lkpDogadjaj.ValueMember = Event.F_DogadjajId;
			lkpDogadjaj.DataSource = source;

            HomeFilters.HomeFilterGodina = dtpGodina.Value.Year;
            if (source.Count > 0)
            {

                HomeFilters.HomeFilterEvent = source[0];
                HomeFilters.HomeFilterAktivanDogadjaj = HomeFilters.HomeFilterEvent.Aktivan;
                HomeFilters.HomeFilterDogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
                Valuta = HomeFilters.HomeFilterEvent.Valuta;

            }
            else {
                HomeFilters.HomeFilterEvent = null;
                HomeFilters.HomeFilterDogadjajId = null;
                Valuta = "";
            }
            OsvjeziLabele();
            PostaviDatumeDijagrama();

        }


		protected void Lookup_RefreashLookup(object sender, EventArgs e)
		{
			var filters = new List<TemplateFilter>()
						{
								new TemplateFilter(Event.F_Godina, dtpGodina.Value.Year.ToString(), "=")
						};
			var source = (new EventDao()).Select(filters);
			lkpDogadjaj.DisplayMember = Event.F_Naziv;
			lkpDogadjaj.ValueMember = Event.F_DogadjajId;
			lkpDogadjaj.DataSource = source;
			if (source.Count > 0)
			{
				HomeFilters.HomeFilterEvent = source[0];
				HomeFilters.HomeFilterAktivanDogadjaj = HomeFilters.HomeFilterEvent.Aktivan;
                Valuta = HomeFilters.HomeFilterEvent.Valuta;
                HomeFilters.HomeFilterDogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
                PostaviDatumeDijagrama();
			}
            else
            {
                HomeFilters.HomeFilterEvent = null;
                HomeFilters.HomeFilterDogadjajId = null;
                Valuta = "";
            }
		}

		private void Lookup_ValueChanged(object sender, EventArgs e)
		{

            HomeFilters.HomeFilterDogadjajId = int.TryParse(lkpDogadjaj.LookupMember, out int par) ? par : -1;
            HomeFilters.HomeFilterEvent = (Event)lkpDogadjaj.SelectedLookupValue;
            if (HomeFilters.HomeFilterEvent != null)
            {
                HomeFilters.HomeFilterAktivanDogadjaj = HomeFilters.HomeFilterEvent.Aktivan;
                Valuta = HomeFilters.HomeFilterEvent.Valuta;
                HomeFilters.HomeFilterDogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;


            }
            else
            {
                HomeFilters.HomeFilterEvent = null;
                HomeFilters.HomeFilterDogadjajId = null;
                Valuta = "";
            }
            OsvjeziLabele();
            PostaviDatumeDijagrama();
        }

        public void OsvjeziLabele()
        {
            if (HomeFilters.HomeFilterEvent == null)
            {
                lblBrIzdatihKartica.Text = "0";
                lblBrAktivnihKartica.Text = "0";
                lblUkupnoUplaceno.Text = "0.00";
                lblPotrosnjaPoKartici.Text = "0.00";
                lblUkupnaPotrosnja.Text = "0.00";
                lblValuta.Text = "" ;
                lblValutaUplate.Text = "";
                btnZakljucajDogadjaj.Enabled = false;
                return;
            }
            lblBrIzdatihKartica.Text = GeneralDao.BrojIzdatihKartica(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId);
            lblBrAktivnihKartica.Text = GeneralDao.BrojAktivnihKartica(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId);
            lblUkupnoUplaceno.Text = string.Format("{0:f2}", decimal.TryParse(GeneralDao.UkupnoUplaceno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId), out decimal pom) ? pom : 0) + " " + Valuta;
            lblUkupnoIsplaceno.Text = string.Format("{0:f2}", decimal.TryParse(GeneralDao.UkupnoIsplaceno(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId), out  pom) ? pom : 0) + " " + Valuta;
            lblPotrosnjaPoKartici.Text = string.Format("{0:f2}", decimal.TryParse(GeneralDao.ProsjecnoPoKartici(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId), out  pom) ? pom : 0) + " " + Valuta;
            lblUkupnaPotrosnja.Text = string.Format("{0:f2}", decimal.TryParse(GeneralDao.UkupnaPotrosnja(HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId), out pom) ? pom : 0) + " " + Valuta;
            lblValuta.Text = "Vrijednosti su prikazane u " + (Valuta ?? "KM");
            lblValutaUplate.Text = "Vrijednosti su prikazane u " + (Valuta ?? "KM");
            btnZakljucajDogadjaj.Enabled = HomeFilters.HomeFilterAktivanDogadjaj;
            btnPrenosArtikala.Enabled = HomeFilters.HomeFilterAktivanDogadjaj;
        }

        private void PostaviDatumeDijagrama()
        {
            if (table != null)
            {
                table.Clear();
                dgStanjeMagacina.DataSource = null;
            }
            if (HomeFilters.HomeFilterEvent == null)
            {
                dtUplateDatumOd.Value = DateTime.Now;
                dtUplateDatumDo.Value = DateTime.Now;
                dtProdajaOd.Value = DateTime.Now;
                dtProdajaDo.Value = DateTime.Now;
            }
            else
            {
                dtUplateDatumOd.Value = HomeFilters.HomeFilterEvent.DatumOd ?? DateTime.Now;
                dtUplateDatumDo.Value = HomeFilters.HomeFilterEvent.DatumDo!=null ? HomeFilters.HomeFilterEvent.DatumOd.Value.AddDays(1): DateTime.Now;
                dtProdajaOd.Value = HomeFilters.HomeFilterEvent.DatumOd ?? DateTime.Now;
                dtProdajaDo.Value = HomeFilters.HomeFilterEvent.DatumDo!=null ? HomeFilters.HomeFilterEvent.DatumOd.Value.AddDays(1) : DateTime.Now;
            }
   //         PopuniDiagramUplate();
			//PopuniPotrosnjaDijagrame();
            PopuniTabeluMagaciniArtikli();
            
        }


		private void CbListProdaja_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			if (potrosnjaSeriesList.Count >= e.Index)
				potrosnjaSeriesList[e.Index].Visibility = potrosnjaSeriesList[e.Index].Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
		}

		private void CbListUplate_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			if (uplateSeriesList.Count >= e.Index)
				uplateSeriesList[e.Index].Visibility = uplateSeriesList[e.Index].Visibility == Visibility.Visible ? Visibility.Hidden : Visibility.Visible;
		}

		#endregion

		private void CbListMagacini_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            tbPretraga.Text = "";
            if (e.NewValue != CheckState.Checked)
            {
                return;
            }
		    for (var ix = 0; ix < cbListMagacini.Items.Count; ++ix)
		    {
                if (ix != e.Index)
                {
                    cbListMagacini.SetItemChecked(ix, false);

                }
                else
                {
                    if (ix != e.Index) continue;
                    PostaviGridStanjaMaagacina((Magacin) cbListMagacini.Items[e.Index]);
		        
                }

            }
        }

	    private DataTable table;

        private void PostaviGridStanjaMaagacina(Magacin magacin)
        {
            if (magacin == null) return;
            table= GeneralDao.StanjeMagacinaGrid(magacin,(DateTime)HomeFilters.HomeFilterEvent.DatumOd, (DateTime)HomeFilters.HomeFilterEvent.DatumDo);
            if (table == null) return;
            dgStanjeMagacina.DataSource = table;
            if (table.Rows.Count <= 0) {
                btnPrenosArtikala.Enabled = false;
                return;
            }
            btnPrenosArtikala.Enabled = HomeFilters.HomeFilterAktivanDogadjaj;
            dgStanjeMagacina.Columns[0].Visible = false;
	        dgStanjeMagacina.Columns[1].Visible = false;
	        dgStanjeMagacina.Columns[2].Visible = false;
            dgStanjeMagacina.Columns[6].Visible = false;
            dgStanjeMagacina.Columns[7].Visible = false;
            dgStanjeMagacina.Columns[8].Visible = false;
            dgStanjeMagacina.Columns[9].Visible = false;
            dgStanjeMagacina.Columns[10].Visible = false;

            dgStanjeMagacina.Columns[3].AutoSizeMode= DataGridViewAutoSizeColumnMode.Fill;
            dgStanjeMagacina.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgStanjeMagacina.Columns[3].ReadOnly = true;
            dgStanjeMagacina.Columns[3].DefaultCellStyle.Padding = new Padding(10, 0, 0, 0);

            dgStanjeMagacina.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dgStanjeMagacina.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgStanjeMagacina.Columns[4].HeaderText = "Stanje (Kom)";
            dgStanjeMagacina.Columns[4].DefaultCellStyle.Padding = new Padding(0, 0, 4, 0);
            dgStanjeMagacina.Columns[4].DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            dgStanjeMagacina.Columns[4].DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(255, 199, 8);
            dgStanjeMagacina.Columns[4].DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
	        dgStanjeMagacina.Columns[4].DefaultCellStyle.Format = "N0";
            dgStanjeMagacina.Columns[4].ReadOnly = !HomeFilters.HomeFilterAktivanDogadjaj;
            dgStanjeMagacina.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgStanjeMagacina.Columns[4].ReadOnly = true;

            dgStanjeMagacina.Columns[5].DefaultCellStyle.Padding = new Padding(0, 0, 4, 0);
            dgStanjeMagacina.Columns[5].ReadOnly = true;
            dgStanjeMagacina.Columns[5].HeaderText = "Potrošeno (Kom)";
            dgStanjeMagacina.Columns[5].DefaultCellStyle.Format = "N0";
            dgStanjeMagacina.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgStanjeMagacina.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            dgStanjeMagacina.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

        }

        private void DataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column1_KeyPress);
            if (dgStanjeMagacina.CurrentCell.ColumnIndex == 4 ) //Desired Column
            {
                if (e.Control is TextBox tb)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column1_KeyPress);
                }
            }
        }

        private void Column1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

	    private void DgStanjeMagacina_CellEndEdit(object sender, DataGridViewCellEventArgs e)
	    {
	        var pom = -1;
	        short p = -1;
	        decimal d = -1;
	        if (e.ColumnIndex == 9)
	        {
	            if (table.Rows.Count <= 0) return;
	            var item = new MagacinArtikal()
	            {
	                Godina =
	                    short.TryParse(table.Rows[dgStanjeMagacina.SelectedRows[0].Index][0].ToString(), out p)
	                        ? (short?) p
	                        : -1,
	                DogadjajId =
	                    int.TryParse(table.Rows[dgStanjeMagacina.SelectedRows[0].Index][1].ToString(), out pom)
	                        ? pom
	                        : -1,
	                MagacinId =
	                    int.TryParse(table.Rows[dgStanjeMagacina.SelectedRows[0].Index][6].ToString(), out pom)
	                        ? pom
	                        : -1,
	                ArtikalId =
	                    int.TryParse(table.Rows[dgStanjeMagacina.SelectedRows[0].Index][2].ToString(), out pom)
	                        ? pom
	                        : -1,
                    TrenutnoStanje =
	                    decimal.TryParse(table.Rows[dgStanjeMagacina.SelectedRows[0].Index][4].ToString(), out d)
	                        ? d
	                        : -1
	            };

	            if (item.Godina == -1 || item.DogadjajId == -1 || item.MagacinId == -1 || item.ArtikalId == -1 ||
	                item.TrenutnoStanje == -1) return;
	            if ((new MagacinArtikalDao()).Update(item) == Constants.SQL_OK)
	            {
	                notifyIcon1.Icon = SystemIcons.Information;
                    notifyIcon1.BalloonTipTitle = "Stanje promijenjeno.";
                    notifyIcon1.BalloonTipText = "Uspješno ste promijenili stanje artikla.";
	                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
	            }
	            else
	            {
                    notifyIcon1.Icon = SystemIcons.Error;
                    notifyIcon1.BalloonTipTitle = "Stanje nije promijenjeno.";
                    notifyIcon1.BalloonTipText = "Greška u radu sa bazom podataka.";
                    notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
                }
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000);
            }
	    }

        private void DgStanjeMagacina_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (HomeFilters.HomeFilterAktivanDogadjaj)
            //{
            //    (new FormPopup((Magacin)cbListMagacini.SelectedItem, (int)dgStanjeMagacina.SelectedRows[0].Cells[2].Value)).ShowDialog();
            //    PostaviGridStanjaMaagacina((Magacin)cbListMagacini.SelectedItem);
            //}

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            OsvjeziLabele();
            PostaviDatumeDijagrama();
        }
        
        private void tbPretraga_TextChanged(object sender, EventArgs e)
        {
            (dgStanjeMagacina.DataSource as DataTable).DefaultView.RowFilter = string.Format("Artikal LIKE '%{0}%'", tbPretraga.Text);

        }

        private void btnDeleteSearch_Click(object sender, EventArgs e)
        {
            tbPretraga.Text = "";
        }


    }
}
