﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormNarudzbaStavka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.tbKolicina = new System.Windows.Forms.TextBox();
            this.tbIznos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.tbNarudzbaId = new System.Windows.Forms.TextBox();
            this.lkpArtikalId = new Damax.Template.LookupComboBox();
            this.SuspendLayout();
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(118, 94);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(273, 21);
            this.lkpDogadjaj.TabIndex = 71;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // tbKolicina
            // 
            this.tbKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbKolicina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbKolicina.Location = new System.Drawing.Point(118, 201);
            this.tbKolicina.Name = "tbKolicina";
            this.tbKolicina.Size = new System.Drawing.Size(134, 20);
            this.tbKolicina.TabIndex = 75;
            this.tbKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbIznos
            // 
            this.tbIznos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbIznos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbIznos.Location = new System.Drawing.Point(118, 165);
            this.tbIznos.Name = "tbIznos";
            this.tbIznos.Size = new System.Drawing.Size(134, 20);
            this.tbIznos.TabIndex = 74;
            this.tbIznos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label4.Location = new System.Drawing.Point(485, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 83;
            this.label4.Text = "Prodajno mjesto";
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.White;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(613, 93);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(252, 21);
            this.lkpProdajnoMjesto.TabIndex = 72;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(9, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 82;
            this.label2.Text = "Napomena";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(9, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 81;
            this.label1.Text = "Kolicina";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(9, 97);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 80;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(9, 65);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 77;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(118, 61);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 70;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(9, 134);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(85, 13);
            this.lblLokacija.TabIndex = 79;
            this.lblLokacija.Text = "Broj narudzbe";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(9, 168);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(37, 13);
            this.lblNaziv.TabIndex = 78;
            this.lblNaziv.Text = "Iznos";
            // 
            // tbNarudzbaId
            // 
            this.tbNarudzbaId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNarudzbaId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNarudzbaId.Location = new System.Drawing.Point(118, 131);
            this.tbNarudzbaId.Name = "tbNarudzbaId";
            this.tbNarudzbaId.Size = new System.Drawing.Size(134, 20);
            this.tbNarudzbaId.TabIndex = 84;
            this.tbNarudzbaId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lkpArtikalId
            // 
            this.lkpArtikalId.BackColor = System.Drawing.Color.White;
            this.lkpArtikalId.ComboSelectedIndex = -1;
            this.lkpArtikalId.DataSource = null;
            this.lkpArtikalId.DisplayMember = "";
            this.lkpArtikalId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpArtikalId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpArtikalId.Location = new System.Drawing.Point(118, 238);
            this.lkpArtikalId.LookupMember = null;
            this.lkpArtikalId.Name = "lkpArtikalId";
            this.lkpArtikalId.SelectedLookupValue = null;
            this.lkpArtikalId.Size = new System.Drawing.Size(273, 21);
            this.lkpArtikalId.TabIndex = 85;
            this.lkpArtikalId.ValueMember = "";
            // 
            // FormNarudzbaStavka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.lkpArtikalId);
            this.Controls.Add(this.tbNarudzbaId);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.tbKolicina);
            this.Controls.Add(this.tbIznos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormNarudzbaStavka";
            this.Text = "Stavke narudžbe";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lkpProdajnoMjesto, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.tbIznos, 0);
            this.Controls.SetChildIndex(this.tbKolicina, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.tbNarudzbaId, 0);
            this.Controls.SetChildIndex(this.lkpArtikalId, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.TextBox tbKolicina;
        private System.Windows.Forms.TextBox tbIznos;
        private System.Windows.Forms.Label label4;
        private LookupComboBox lkpProdajnoMjesto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.TextBox tbNarudzbaId;
        private LookupComboBox lkpArtikalId;
    }
}