﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormArtikalKategorija : FormTemplate<ArtikalKategorija>
    {
        public FormArtikalKategorija()
        {
            InitializeComponent();
            DaoObject = new ArtikalKategorijaDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            SetFormControlsSettings();
            lkpDogadjaj.RefreshLookup += new EventHandler(lookup_RefreashLookup);
        }
        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbNaziv.ReadOnly=!isEnable;
            lkpDogadjaj.Enabled = false;
            dtpGodina.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            tbNaziv.Text = SelectedObject.Naziv;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
        }

        public override void FillObject()
        {
            base.FillObject();
            SelectedObject.Naziv = tbNaziv.Text;
            SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                     ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                     : (int?)null;
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;

        }

        public override void ClearControls()
        {
            base.ClearControls();
            tbNaziv.Text = "";
            dtpGodina.Value = DateTime.Now;
        }

        public override bool ValidateControls()
        {
            if (tbNaziv.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbNaziv, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }


        public override void AddNextForms()
        {
            base.AddNextForms();
            {
                FormArtikal form = new FormArtikal();
                form.Filters.Add(new TemplateFilter(Artikal.F_ArtikalKategorijaId, SelectedObject.ArtikalKategorijaId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Artikal.F_ArtikalKategorijaId, SelectedObject.ArtikalKategorijaId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            //grid.Columns[Artikal.F_DogadjajId].Visible = false;
            grid.Columns[Artikal.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Artikal.F_Godina].Width = 70;
            grid.Columns[ArtikalKategorija.F_ArtikalKategorijaId].HeaderText = "Šifra";
            grid.Columns[ArtikalKategorija.F_ArtikalKategorijaId].Width = 50;
            grid.Columns[ArtikalKategorija.F_Naziv].AutoSizeMode=DataGridViewAutoSizeColumnMode.Fill;
        }

        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") });
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") });
        }


        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") });
        }
    }
}
