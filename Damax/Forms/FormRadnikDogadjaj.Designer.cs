﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormRadnikDogadjaj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lkpRadnik = new Damax.Template.LookupComboBox();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(11, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Dogadjaj";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(11, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Radnik";
            // 
            // lkpRadnik
            // 
            this.lkpRadnik.BackColor = System.Drawing.Color.White;
            this.lkpRadnik.ComboSelectedIndex = -1;
            this.lkpRadnik.DataSource = null;
            this.lkpRadnik.DisplayMember = "";
            this.lkpRadnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpRadnik.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpRadnik.Location = new System.Drawing.Point(89, 158);
            this.lkpRadnik.LookupMember = null;
            this.lkpRadnik.Name = "lkpRadnik";
            this.lkpRadnik.SelectedLookupValue = null;
            this.lkpRadnik.Size = new System.Drawing.Size(311, 21);
            this.lkpRadnik.TabIndex = 2;
            this.lkpRadnik.ValueMember = "";
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(89, 116);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(311, 21);
            this.lkpDogadjaj.TabIndex = 1;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(11, 78);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 53;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(89, 76);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 0;
            // 
            // FormRadnikDogadjaj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpRadnik);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormRadnikDogadjaj";
            this.Text = "Korisnici na dogadjajima";
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpRadnik, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private LookupComboBox lkpRadnik;
        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
    }
}