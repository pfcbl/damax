﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormBlagajna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.lblKarticeOdDo = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.tbBrojKarticeOd = new System.Windows.Forms.TextBox();
            this.tbBrojKarticeDo = new System.Windows.Forms.TextBox();
            this.tbLokacija = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbUplata = new System.Windows.Forms.TextBox();
            this.tbIsplata = new System.Windows.Forms.TextBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbBrojIzdatihKartica = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label5.Location = new System.Drawing.Point(605, 243);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 88;
            this.label5.Text = "Izdato kartica";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label4.Location = new System.Drawing.Point(605, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 87;
            this.label4.Text = "Ukupno isplata";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label3.Location = new System.Drawing.Point(605, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 86;
            this.label3.Text = "Ukupno uplata";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(18, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "Lozinka";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(18, 209);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 84;
            this.label1.Text = "Korisničko ime";
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(563, 70);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(306, 21);
            this.lkpDogadjaj.TabIndex = 1;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(489, 75);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 83;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(18, 72);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 79;
            this.lblGodina.Text = "Godina";
            // 
            // lblKarticeOdDo
            // 
            this.lblKarticeOdDo.AutoSize = true;
            this.lblKarticeOdDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKarticeOdDo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKarticeOdDo.Location = new System.Drawing.Point(18, 175);
            this.lblKarticeOdDo.Name = "lblKarticeOdDo";
            this.lblKarticeOdDo.Size = new System.Drawing.Size(70, 13);
            this.lblKarticeOdDo.TabIndex = 82;
            this.lblKarticeOdDo.Text = "Kartice od-do";
            // 
            // dtpGodina
            // 
            this.dtpGodina.CalendarForeColor = System.Drawing.Color.White;
            this.dtpGodina.CalendarMonthBackground = System.Drawing.Color.Transparent;
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(127, 70);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(93, 20);
            this.dtpGodina.TabIndex = 0;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(18, 141);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(47, 13);
            this.lblLokacija.TabIndex = 81;
            this.lblLokacija.Text = "Lokacija";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(18, 106);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(39, 13);
            this.lblNaziv.TabIndex = 80;
            this.lblNaziv.Text = "Naziv";
            // 
            // tbNaziv
            // 
            this.tbNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNaziv.Location = new System.Drawing.Point(127, 103);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(741, 20);
            this.tbNaziv.TabIndex = 2;
            // 
            // tbBrojKarticeOd
            // 
            this.tbBrojKarticeOd.AcceptsReturn = true;
            this.tbBrojKarticeOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbBrojKarticeOd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbBrojKarticeOd.Location = new System.Drawing.Point(127, 172);
            this.tbBrojKarticeOd.Name = "tbBrojKarticeOd";
            this.tbBrojKarticeOd.Size = new System.Drawing.Size(159, 20);
            this.tbBrojKarticeOd.TabIndex = 4;
            // 
            // tbBrojKarticeDo
            // 
            this.tbBrojKarticeDo.AcceptsReturn = true;
            this.tbBrojKarticeDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBrojKarticeDo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbBrojKarticeDo.Location = new System.Drawing.Point(324, 172);
            this.tbBrojKarticeDo.Name = "tbBrojKarticeDo";
            this.tbBrojKarticeDo.Size = new System.Drawing.Size(159, 22);
            this.tbBrojKarticeDo.TabIndex = 5;
            // 
            // tbLokacija
            // 
            this.tbLokacija.AcceptsReturn = true;
            this.tbLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbLokacija.Location = new System.Drawing.Point(127, 138);
            this.tbLokacija.Name = "tbLokacija";
            this.tbLokacija.Size = new System.Drawing.Size(356, 20);
            this.tbLokacija.TabIndex = 3;
            // 
            // tbPassword
            // 
            this.tbPassword.AcceptsReturn = true;
            this.tbPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbPassword.Location = new System.Drawing.Point(127, 240);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(159, 20);
            this.tbPassword.TabIndex = 7;
            // 
            // tbUplata
            // 
            this.tbUplata.AcceptsReturn = true;
            this.tbUplata.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbUplata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbUplata.Location = new System.Drawing.Point(709, 138);
            this.tbUplata.Name = "tbUplata";
            this.tbUplata.Size = new System.Drawing.Size(159, 20);
            this.tbUplata.TabIndex = 103;
            // 
            // tbIsplata
            // 
            this.tbIsplata.AcceptsReturn = true;
            this.tbIsplata.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbIsplata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbIsplata.Location = new System.Drawing.Point(709, 172);
            this.tbIsplata.Name = "tbIsplata";
            this.tbIsplata.Size = new System.Drawing.Size(159, 20);
            this.tbIsplata.TabIndex = 104;
            // 
            // tbUsername
            // 
            this.tbUsername.AcceptsReturn = true;
            this.tbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbUsername.Location = new System.Drawing.Point(127, 206);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(159, 20);
            this.tbUsername.TabIndex = 6;
            // 
            // tbBrojIzdatihKartica
            // 
            this.tbBrojIzdatihKartica.AcceptsReturn = true;
            this.tbBrojIzdatihKartica.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbBrojIzdatihKartica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbBrojIzdatihKartica.Location = new System.Drawing.Point(709, 240);
            this.tbBrojIzdatihKartica.Name = "tbBrojIzdatihKartica";
            this.tbBrojIzdatihKartica.Size = new System.Drawing.Size(159, 20);
            this.tbBrojIzdatihKartica.TabIndex = 106;
            // 
            // FormBlagajna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.tbBrojIzdatihKartica);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.tbIsplata);
            this.Controls.Add(this.tbUplata);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbLokacija);
            this.Controls.Add(this.tbBrojKarticeDo);
            this.Controls.Add(this.tbBrojKarticeOd);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.lblKarticeOdDo);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormBlagajna";
            this.Text = "Blagajne";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblKarticeOdDo, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.tbNaziv, 0);
            this.Controls.SetChildIndex(this.tbBrojKarticeOd, 0);
            this.Controls.SetChildIndex(this.tbBrojKarticeDo, 0);
            this.Controls.SetChildIndex(this.tbLokacija, 0);
            this.Controls.SetChildIndex(this.tbPassword, 0);
            this.Controls.SetChildIndex(this.tbUplata, 0);
            this.Controls.SetChildIndex(this.tbIsplata, 0);
            this.Controls.SetChildIndex(this.tbUsername, 0);
            this.Controls.SetChildIndex(this.tbBrojIzdatihKartica, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.Label lblKarticeOdDo;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.TextBox tbBrojKarticeOd;
        private System.Windows.Forms.TextBox tbBrojKarticeDo;
        private System.Windows.Forms.TextBox tbLokacija;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbUplata;
        private System.Windows.Forms.TextBox tbIsplata;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbBrojIzdatihKartica;
    }
}