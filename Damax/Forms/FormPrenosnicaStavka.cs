﻿using Damax.DAO;
using Damax.Models;
using Damax.Properties;
using Damax.Template;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
    public partial class FormPrenosnicaStavka : FormTemplate<PrenosnicaStavka>
    {
        public FormPrenosnicaStavka()
        {
            InitializeComponent();
            DaoObject = new PrenosnicaStavkaDao();
            LockUnlockMenuButtons(false, false, false);
            SetFormControlsSettings();
            lkpDogadjaj.RefreshLookup += lookup_RefreashLookupDogadjaj;
            lkpArtikal.RefreshLookup += lookup_RefreashLookupProdajnoMjesto;
        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
            dtpGodina.ShowUpDown = true;

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {

            tbKolicina.ReadOnly = true;
            lkpArtikal.Enabled = isEnable;
            dtpGodina.Enabled = false;
            lkpDogadjaj.Enabled = false;
            tbPrenosnica.ReadOnly = true;
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), Resources.Year_Date_Format_yyyy, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            tbKolicina.Text = SelectedObject.Kolicina.ToString();
            tbPrenosnica.Text = SelectedObject.PrenosnicaId.ToString();


        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject == null) return;
            SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                : (int?)null;
            SelectedObject.ArtikalId = lkpArtikal.LookupMember != null
                ? Convert.ToInt32(lkpArtikal.LookupMember)
                : (int?)null;
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
            SelectedObject.Kolicina = Convert.ToInt32(tbKolicina.Text);
            SelectedObject.PrenosnicaId = Convert.ToInt32(tbPrenosnica.Text);
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbKolicina.Text = "";
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            var grid = GetDataGridView();
            grid.Columns[PrenosnicaStavka.F_DogadjajId].Visible = false;
            grid.Columns[PrenosnicaStavka.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[PrenosnicaStavka.F_NazivDogadjaja].Width = 200;
            grid.Columns[PrenosnicaStavka.F_Godina].Width = 70;
            grid.Columns[PrenosnicaStavka.F_ArtikalId].Visible = false;
            grid.Columns[PrenosnicaStavka.F_StavkaBroj].Visible = false;
            grid.Columns[PrenosnicaStavka.F_NazivArtikal].Width = 200;
            grid.Columns[PrenosnicaStavka.F_NazivArtikal].HeaderText = "Artikal";
            grid.Columns[PrenosnicaStavka.F_Kolicina].HeaderText = "Kolicina(kom)";

        }

        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            FillLookups();
        }

        private void FillLookups()
        {
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;

            if (SelectedObject != null)
            {
                lkpArtikal.DisplayMember = Artikal.F_Naziv;
                lkpArtikal.ValueMember = Artikal.F_ArtikalId;
                lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                    new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                    new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                    new TemplateFilter(Artikal.F_ArtikalId, SelectedObject.ArtikalId.ToString(), "=") });
            }
            else
            {
                lkpArtikal.DisplayMember = Artikal.F_Naziv;
                lkpArtikal.ValueMember = Artikal.F_ArtikalId;
                lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
                });
            }

        }

        protected void lookup_RefreashLookupDogadjaj(object sender, EventArgs e)
        {

            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }

        protected void lookup_RefreashLookupProdajnoMjesto(object sender, EventArgs e)
        {

            var source = (new ProdajnoMjestoDao()).Select(Filters);
            lkpArtikal.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpArtikal.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpArtikal.DataSource = source;
        }
    }
}
