﻿using Damax.DAO;
using Damax.Models;
using Damax.Template;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
    public partial class FormPopup : Form
    {
        public Magacin magacin { get; set; }
        public int? DogadjajId { get; set; }
        public int? Godina { get; set; }
        public int ArtikalId { get; set; }

        public MagacinArtikal ArtikalSkidanje { get; set; }
        public MagacinArtikal ArtikalOdrediste { get; set; }

        public FormPopup(Magacin magacin,int ArtikalId)
        {
            InitializeComponent();
            this.magacin = magacin;
            DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterGodina;
            this.ArtikalId = ArtikalId;
            lkpMagacin.RefreshLookup += lkpMagacin_RefreashLookup;
            lkpMagacin.LookupValueChanged += lkpMagacin_ValueChanged;
            SetLkpMagacin();
            SetLkpMagacinOdrediste();
            //SetDgArtikli();

        }

        private void SetLkpMagacin()
        {
            lkpMagacin.DisplayMember = Magacin.F_Naziv;
            lkpMagacin.ValueMember = Magacin.F_MagacinId;
            lkpMagacin.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>()
            { new TemplateFilter(Magacin.F_Godina,Godina.ToString(),"=")
            ,new TemplateFilter(Magacin.F_DogadjajId,DogadjajId.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_MagacinId,magacin.MagacinId.ToString(),"<>") });
        }

        private void SetLkpMagacinOdrediste()
        {
            lkpMagacinOdrediste.DisplayMember = Magacin.F_Naziv;
            lkpMagacinOdrediste.ValueMember = Magacin.F_MagacinId;
            lkpMagacinOdrediste.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>()
            { new TemplateFilter(Magacin.F_Godina,Godina.ToString(),"=")
            ,new TemplateFilter(Magacin.F_DogadjajId,DogadjajId.ToString(),"=")
            ,new TemplateFilter(Magacin.F_MagacinId,magacin.MagacinId.ToString(),"=")});

            var table = (new MagacinArtikalDao()).SelectTable(new List<TemplateFilter>() {
            new TemplateFilter(MagacinArtikal.F_Godina,Godina.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_DogadjajId,DogadjajId.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_MagacinId,magacin.MagacinId.ToString(),"=")
            , new TemplateFilter(MagacinArtikal.F_ArtikalId, ArtikalId.ToString(), "=")});
            DataColumn Col = table.Columns.Add("Oznaka", System.Type.GetType("System.Boolean"));
            Col.SetOrdinal(0);
            dgArtikli.DataSource = table;
            var list = (new MagacinArtikalDao()).Select(new List<TemplateFilter>() {
            new TemplateFilter(MagacinArtikal.F_Godina,Godina.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_DogadjajId,DogadjajId.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_MagacinId,magacin.MagacinId.ToString(),"=")
            , new TemplateFilter(MagacinArtikal.F_ArtikalId, ArtikalId.ToString(), "=")});
            if (list.Count <= 0) return;
            ArtikalOdrediste = list[0];
            tbKolicina.Text = list[0].TrenutnoStanje.ToString();
            tbArtikal.Text = list[0].ArtikalNaziv.ToString();
        }

        private void SetDgArtikli()
        {
            dgArtikli.Columns[0].HeaderText = "";
            dgArtikli.Columns[0].Width = 20;
            dgArtikli.Columns[MagacinArtikal.F_Godina].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_DogadjajId].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_ArtikalKategorijaId].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_ArtikalId].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_ArtikalKategorijaNaziv].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_MagacinId].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_NazivDogadjaja].Visible = false;
            dgArtikli.Columns[MagacinArtikal.F_MagacinNaziv].Visible = false;


            dgArtikli.Columns[MagacinArtikal.F_ArtikalNaziv].HeaderText = "Artikal";
            dgArtikli.Columns[MagacinArtikal.F_TrenutnoStanje].HeaderText = "Kolicina";
            dgArtikli.Columns[MagacinArtikal.F_ArtikalNaziv].ReadOnly = true;
            dgArtikli.Columns[MagacinArtikal.F_TrenutnoStanje].ReadOnly = true;
            dgArtikli.Columns[MagacinArtikal.F_ArtikalNaziv].Width = 130;
            dgArtikli.Columns[MagacinArtikal.F_TrenutnoStanje].Width = 50;
            dgArtikli.Columns[MagacinArtikal.F_TrenutnoStanje].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dgArtikli.BorderStyle = BorderStyle.None;
            dgArtikli.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 238, 238);
            dgArtikli.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgArtikli.DefaultCellStyle.SelectionBackColor = Color.White;
            dgArtikli.DefaultCellStyle.SelectionForeColor = Color.FromArgb(10, 50, 100);
            dgArtikli.BackgroundColor = Color.White;

            dgArtikli.EnableHeadersVisualStyles = false;
            dgArtikli.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgArtikli.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(243, 247, 249);
            dgArtikli.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(10, 50, 100);
        }

        protected void lkpMagacin_RefreashLookup(object sender, EventArgs e)
        {
            SetLkpMagacin();
        }

        private void lkpMagacin_ValueChanged(object sender, EventArgs e)
        {
            tbDodavanjeKolicine.Value = 0;
            var list = (new MagacinArtikalDao()).Select(new List<TemplateFilter>() {
            new TemplateFilter(MagacinArtikal.F_Godina,Godina.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_DogadjajId,DogadjajId.ToString(),"=")
            ,new TemplateFilter(MagacinArtikal.F_MagacinId,lkpMagacin.LookupMember,"=")
            , new TemplateFilter(MagacinArtikal.F_ArtikalId, ArtikalId.ToString(), "=")});
            if (list.Count <= 0)
            {
                ArtikalSkidanje = null;
                lblNaStanju.Text = "0";
                btnDodaj.Enabled = false;
                tbDodavanjeKolicine.Enabled = false;
            }
            else
            {
                ArtikalSkidanje = list[0];
                lblNaStanju.Text = ArtikalSkidanje.TrenutnoStanje.ToString();
                btnDodaj.Enabled = true;
                tbDodavanjeKolicine.Enabled = true;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (ArtikalOdrediste == null) return;
            if (ArtikalSkidanje == null) return;
            if (ArtikalSkidanje.TrenutnoStanje <  tbDodavanjeKolicine.Value)
            {
               MessageBox.Show( "Na odabranom magacinu nema dovoljno trazenog artikla.Trenutna kolicina je "+ArtikalSkidanje.TrenutnoStanje, "Nedovolja kolicina",MessageBoxButtons.OK,MessageBoxIcon.Error);
               return;
            }
            ArtikalOdrediste.TrenutnoStanje = ArtikalOdrediste.TrenutnoStanje + tbDodavanjeKolicine.Value;
            ArtikalSkidanje.TrenutnoStanje = ArtikalSkidanje.TrenutnoStanje - tbDodavanjeKolicine.Value;
            if ((new MagacinArtikalDao()).Update(ArtikalOdrediste) == Constants.SQL_OK && (new MagacinArtikalDao()).Update(ArtikalSkidanje) == Constants.SQL_OK)
            {
                Prenosnica prenos = new Prenosnica()
                {
                    Godina = HomeFilters.HomeFilterEvent.Godina,
                    DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId,
                    MagacinOdredisteId = int.TryParse(lkpMagacinOdrediste.LookupMember, out int t) ? t : 0,
                    MagacinSourceId = int.TryParse(lkpMagacin.LookupMember, out t) ? t : 0,
                    TipPrenosa = "P"
                };
            (new PrenosnicaDao()).Insert(prenos);
                notifyIcon1.Icon = SystemIcons.Information;
                notifyIcon1.BalloonTipTitle = "Stanje promijenjeno.";
                notifyIcon1.BalloonTipText = "Uspješno ste promijenili stanje artikla.";
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
            }
            else
            {
                notifyIcon1.Icon = SystemIcons.Error;
                notifyIcon1.BalloonTipTitle = "Stanje nije promijenjeno.";
                notifyIcon1.BalloonTipText = "Greška u radu sa bazom podataka.";
                notifyIcon1.BalloonTipIcon = ToolTipIcon.Error;
            }
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(1000);
            this.Close();
        }
    }
}
