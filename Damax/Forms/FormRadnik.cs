﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;

namespace Damax.Forms
{
    public partial class FormRadnik : FormTemplate<Radnik>
    {
        public FormRadnik()
        {
            InitializeComponent();
            DaoObject = new RadnikDAO();
            SetFormControlsSettings();
        }
        private void SetFormControlsSettings()
        {
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
             tbIme.ReadOnly = !isEnable;
            tbPrezime.ReadOnly = !isEnable;
            tbKorisnickoIme.ReadOnly = !isEnable;
            tbLozinka.ReadOnly = !isEnable;
            tbTelefon.ReadOnly = !isEnable;
            tbMejl.ReadOnly = !isEnable;
            rbKorisnik.Enabled=isEnable;
            rbMagacioner.Enabled = isEnable;
            rbAdministrator.Enabled = isEnable;
        }

        public override void FillControls()
        {
            base.FillControls();
            tbIme.Text = SelectedObject.Ime;
            tbPrezime.Text = SelectedObject.Prezime;
            tbKorisnickoIme.Text = SelectedObject.KorisnickoIme;
            tbLozinka.Text = SelectedObject.Lozinka;
            tbTelefon.Text = SelectedObject.Telefon;
            tbMejl.Text = SelectedObject.Mail;
            switch (SelectedObject.TipKorisnika)
            {
                case Constants.Tip_Korisnika.TIP_KORISNIK:
                    rbKorisnik.Checked=true;
                    break;
                case Constants.Tip_Korisnika.TIP_MAGACIONER:
                    rbMagacioner.Checked = true;
                    break;
                case Constants.Tip_Korisnika.TIP_ADMINISTRATOR:
                    rbAdministrator.Checked = true;
                    break;
                default:
                    rbKorisnik.Checked = true;
                    break;
            }

        }

        public override void FillObject()
        {
            base.FillObject();
            SelectedObject.Ime = tbIme.Text;
            SelectedObject.Prezime = tbPrezime.Text;
            SelectedObject.KorisnickoIme = tbKorisnickoIme.Text;
            SelectedObject.Lozinka = tbLozinka.Text;
            SelectedObject.Telefon = tbTelefon.Text;
            SelectedObject.Mail = tbMejl.Text;
            SelectedObject.TipKorisnika = rbKorisnik.Checked ? Constants.Tip_Korisnika.TIP_KORISNIK :
                (rbMagacioner.Checked ? Constants.Tip_Korisnika.TIP_MAGACIONER : (rbAdministrator.Checked ? Constants.Tip_Korisnika.TIP_ADMINISTRATOR : Constants.Tip_Korisnika.TIP_KORISNIK));
            

        }

        public override void ClearControls()
        {
            base.ClearControls();
            tbIme.Text = "";
            tbPrezime.Text = "";
            tbKorisnickoIme.Text = "";
            tbLozinka.Text = "";
            tbTelefon.Text = "";
            tbMejl.Text = "";
            rbKorisnik.Checked = true;
        }


        public override bool ValidateControls()
        {
            if (tbIme.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbIme, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbKorisnickoIme.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbKorisnickoIme, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbPrezime.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbKorisnickoIme, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            if (tbLozinka.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbLozinka, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            return base.ValidateControls();
        }


        public override void AddNextForms()
        {
            base.AddNextForms();
            {
                FormRadnikDogadjaj form = new FormRadnikDogadjaj();
                form.Filters.Add(new TemplateFilter(RadnikDogadjaj.F_RadnikId, SelectedObject.RadnikId.ToString(), "="));
                IsInNextMode = true;
                NextForms.Add(form);
            }
            //{
            //    FormEvent form = new FormEvent();
            //    form.Filters.Add(new TemplateFilter(Event.F_Godina, "2020", "="));
            //    NextForms.Add(form);
            //}
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Radnik.F_Ime].Width = 80;
            grid.Columns[Radnik.F_Prezime].Width = 100;
            grid.Columns[Radnik.F_KorisnickoIme].Width = 80;
            grid.Columns[Radnik.F_Lozinka].Width = 80;
            grid.Columns[Radnik.F_Telefon].Width = 80;
            grid.Columns[Radnik.F_KorisnickoIme].HeaderText = "Korisničko ime";
            grid.Columns[Radnik.F_Administrator].Width =100;
            grid.Columns[Radnik.F_RadnikId].Visible = false;
            grid.Columns[Radnik.F_TipKorisnika].Width = 80;
            grid.Columns[Radnik.F_TipKorisnika].HeaderText = "Tip korisnika";

        }
    }
}

