﻿namespace Damax.Forms
{
    partial class FormRadnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbMejl = new System.Windows.Forms.TextBox();
            this.tbLozinka = new System.Windows.Forms.TextBox();
            this.tbTelefon = new System.Windows.Forms.TextBox();
            this.tbPrezime = new System.Windows.Forms.TextBox();
            this.tbIme = new System.Windows.Forms.TextBox();
            this.tbKorisnickoIme = new System.Windows.Forms.TextBox();
            this.lblMejl = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblLozinka = new System.Windows.Forms.Label();
            this.lblKorisnickoIme = new System.Windows.Forms.Label();
            this.lblImeIPrezime = new System.Windows.Forms.Label();
            this.pnlNivoLogovanja = new System.Windows.Forms.Panel();
            this.lblAdministrator = new System.Windows.Forms.Label();
            this.lblMagaioner = new System.Windows.Forms.Label();
            this.lblKorisnik = new System.Windows.Forms.Label();
            this.rbAdministrator = new System.Windows.Forms.RadioButton();
            this.rbMagacioner = new System.Windows.Forms.RadioButton();
            this.rbKorisnik = new System.Windows.Forms.RadioButton();
            this.pnlNivoLogovanja.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbMejl
            // 
            this.tbMejl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbMejl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbMejl.Location = new System.Drawing.Point(131, 232);
            this.tbMejl.Name = "tbMejl";
            this.tbMejl.Size = new System.Drawing.Size(737, 20);
            this.tbMejl.TabIndex = 5;
            // 
            // tbLozinka
            // 
            this.tbLozinka.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLozinka.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbLozinka.Location = new System.Drawing.Point(131, 151);
            this.tbLozinka.Name = "tbLozinka";
            this.tbLozinka.Size = new System.Drawing.Size(134, 20);
            this.tbLozinka.TabIndex = 3;
            // 
            // tbTelefon
            // 
            this.tbTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTelefon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbTelefon.Location = new System.Drawing.Point(131, 190);
            this.tbTelefon.Name = "tbTelefon";
            this.tbTelefon.Size = new System.Drawing.Size(134, 20);
            this.tbTelefon.TabIndex = 4;
            // 
            // tbPrezime
            // 
            this.tbPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPrezime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbPrezime.Location = new System.Drawing.Point(271, 69);
            this.tbPrezime.Name = "tbPrezime";
            this.tbPrezime.Size = new System.Drawing.Size(597, 20);
            this.tbPrezime.TabIndex = 1;
            // 
            // tbIme
            // 
            this.tbIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbIme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbIme.Location = new System.Drawing.Point(131, 69);
            this.tbIme.Name = "tbIme";
            this.tbIme.Size = new System.Drawing.Size(134, 20);
            this.tbIme.TabIndex = 0;
            // 
            // tbKorisnickoIme
            // 
            this.tbKorisnickoIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbKorisnickoIme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbKorisnickoIme.Location = new System.Drawing.Point(131, 110);
            this.tbKorisnickoIme.Name = "tbKorisnickoIme";
            this.tbKorisnickoIme.Size = new System.Drawing.Size(134, 20);
            this.tbKorisnickoIme.TabIndex = 2;
            // 
            // lblMejl
            // 
            this.lblMejl.AutoSize = true;
            this.lblMejl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMejl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblMejl.Location = new System.Drawing.Point(9, 235);
            this.lblMejl.Name = "lblMejl";
            this.lblMejl.Size = new System.Drawing.Size(26, 13);
            this.lblMejl.TabIndex = 91;
            this.lblMejl.Text = "Mejl";
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTelefon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblTelefon.Location = new System.Drawing.Point(9, 193);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(43, 13);
            this.lblTelefon.TabIndex = 90;
            this.lblTelefon.Text = "Telefon";
            // 
            // lblLozinka
            // 
            this.lblLozinka.AutoSize = true;
            this.lblLozinka.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLozinka.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLozinka.Location = new System.Drawing.Point(9, 154);
            this.lblLozinka.Name = "lblLozinka";
            this.lblLozinka.Size = new System.Drawing.Size(51, 13);
            this.lblLozinka.TabIndex = 89;
            this.lblLozinka.Text = "Lozinka";
            // 
            // lblKorisnickoIme
            // 
            this.lblKorisnickoIme.AutoSize = true;
            this.lblKorisnickoIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKorisnickoIme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKorisnickoIme.Location = new System.Drawing.Point(9, 113);
            this.lblKorisnickoIme.Name = "lblKorisnickoIme";
            this.lblKorisnickoIme.Size = new System.Drawing.Size(89, 13);
            this.lblKorisnickoIme.TabIndex = 88;
            this.lblKorisnickoIme.Text = "Korisničko ime";
            // 
            // lblImeIPrezime
            // 
            this.lblImeIPrezime.AutoSize = true;
            this.lblImeIPrezime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblImeIPrezime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblImeIPrezime.Location = new System.Drawing.Point(9, 72);
            this.lblImeIPrezime.Name = "lblImeIPrezime";
            this.lblImeIPrezime.Size = new System.Drawing.Size(81, 13);
            this.lblImeIPrezime.TabIndex = 87;
            this.lblImeIPrezime.Text = "Ime i prezime";
            // 
            // pnlNivoLogovanja
            // 
            this.pnlNivoLogovanja.Controls.Add(this.lblAdministrator);
            this.pnlNivoLogovanja.Controls.Add(this.lblMagaioner);
            this.pnlNivoLogovanja.Controls.Add(this.lblKorisnik);
            this.pnlNivoLogovanja.Controls.Add(this.rbAdministrator);
            this.pnlNivoLogovanja.Controls.Add(this.rbMagacioner);
            this.pnlNivoLogovanja.Controls.Add(this.rbKorisnik);
            this.pnlNivoLogovanja.Location = new System.Drawing.Point(131, 258);
            this.pnlNivoLogovanja.Name = "pnlNivoLogovanja";
            this.pnlNivoLogovanja.Size = new System.Drawing.Size(215, 50);
            this.pnlNivoLogovanja.TabIndex = 96;
            // 
            // lblAdministrator
            // 
            this.lblAdministrator.AutoSize = true;
            this.lblAdministrator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAdministrator.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblAdministrator.Location = new System.Drawing.Point(140, 11);
            this.lblAdministrator.Name = "lblAdministrator";
            this.lblAdministrator.Size = new System.Drawing.Size(67, 13);
            this.lblAdministrator.TabIndex = 93;
            this.lblAdministrator.Text = "Administrator";
            // 
            // lblMagaioner
            // 
            this.lblMagaioner.AutoSize = true;
            this.lblMagaioner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMagaioner.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblMagaioner.Location = new System.Drawing.Point(71, 11);
            this.lblMagaioner.Name = "lblMagaioner";
            this.lblMagaioner.Size = new System.Drawing.Size(63, 13);
            this.lblMagaioner.TabIndex = 92;
            this.lblMagaioner.Text = "Magacioner";
            // 
            // lblKorisnik
            // 
            this.lblKorisnik.AutoSize = true;
            this.lblKorisnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKorisnik.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKorisnik.Location = new System.Drawing.Point(12, 11);
            this.lblKorisnik.Name = "lblKorisnik";
            this.lblKorisnik.Size = new System.Drawing.Size(44, 13);
            this.lblKorisnik.TabIndex = 91;
            this.lblKorisnik.Text = "Korisnik";
            // 
            // rbAdministrator
            // 
            this.rbAdministrator.AutoSize = true;
            this.rbAdministrator.Location = new System.Drawing.Point(161, 27);
            this.rbAdministrator.Name = "rbAdministrator";
            this.rbAdministrator.Size = new System.Drawing.Size(14, 13);
            this.rbAdministrator.TabIndex = 3;
            this.rbAdministrator.TabStop = true;
            this.rbAdministrator.UseVisualStyleBackColor = true;
            // 
            // rbMagacioner
            // 
            this.rbMagacioner.AutoSize = true;
            this.rbMagacioner.Location = new System.Drawing.Point(97, 27);
            this.rbMagacioner.Name = "rbMagacioner";
            this.rbMagacioner.Size = new System.Drawing.Size(14, 13);
            this.rbMagacioner.TabIndex = 2;
            this.rbMagacioner.TabStop = true;
            this.rbMagacioner.UseVisualStyleBackColor = true;
            // 
            // rbKorisnik
            // 
            this.rbKorisnik.AutoSize = true;
            this.rbKorisnik.Location = new System.Drawing.Point(26, 27);
            this.rbKorisnik.Name = "rbKorisnik";
            this.rbKorisnik.Size = new System.Drawing.Size(14, 13);
            this.rbKorisnik.TabIndex = 1;
            this.rbKorisnik.TabStop = true;
            this.rbKorisnik.UseVisualStyleBackColor = true;
            // 
            // FormRadnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.pnlNivoLogovanja);
            this.Controls.Add(this.tbMejl);
            this.Controls.Add(this.tbLozinka);
            this.Controls.Add(this.tbTelefon);
            this.Controls.Add(this.tbPrezime);
            this.Controls.Add(this.tbIme);
            this.Controls.Add(this.tbKorisnickoIme);
            this.Controls.Add(this.lblMejl);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.lblLozinka);
            this.Controls.Add(this.lblKorisnickoIme);
            this.Controls.Add(this.lblImeIPrezime);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormRadnik";
            this.Text = "Korisnici";
            this.Controls.SetChildIndex(this.lblImeIPrezime, 0);
            this.Controls.SetChildIndex(this.lblKorisnickoIme, 0);
            this.Controls.SetChildIndex(this.lblLozinka, 0);
            this.Controls.SetChildIndex(this.lblTelefon, 0);
            this.Controls.SetChildIndex(this.lblMejl, 0);
            this.Controls.SetChildIndex(this.tbKorisnickoIme, 0);
            this.Controls.SetChildIndex(this.tbIme, 0);
            this.Controls.SetChildIndex(this.tbPrezime, 0);
            this.Controls.SetChildIndex(this.tbTelefon, 0);
            this.Controls.SetChildIndex(this.tbLozinka, 0);
            this.Controls.SetChildIndex(this.tbMejl, 0);
            this.Controls.SetChildIndex(this.pnlNivoLogovanja, 0);
            this.pnlNivoLogovanja.ResumeLayout(false);
            this.pnlNivoLogovanja.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbMejl;
        private System.Windows.Forms.TextBox tbLozinka;
        private System.Windows.Forms.TextBox tbTelefon;
        private System.Windows.Forms.TextBox tbPrezime;
        private System.Windows.Forms.TextBox tbIme;
        private System.Windows.Forms.TextBox tbKorisnickoIme;
        private System.Windows.Forms.Label lblMejl;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblLozinka;
        private System.Windows.Forms.Label lblKorisnickoIme;
        private System.Windows.Forms.Label lblImeIPrezime;
        private System.Windows.Forms.Panel pnlNivoLogovanja;
        private System.Windows.Forms.RadioButton rbKorisnik;
        private System.Windows.Forms.Label lblAdministrator;
        private System.Windows.Forms.Label lblMagaioner;
        private System.Windows.Forms.Label lblKorisnik;
        private System.Windows.Forms.RadioButton rbAdministrator;
        private System.Windows.Forms.RadioButton rbMagacioner;
    }
}