﻿namespace Damax.Forms
{
    partial class FormPrenosArtikala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrenosArtikala));
            this.dgStanjeMagacina = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbPretraga = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDeleteSearch = new System.Windows.Forms.Button();
            this.lkpNaMagacin = new Damax.Template.LookupComboBox();
            this.lkpSaMagacina = new Damax.Template.LookupComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgStanjeMagacina)).BeginInit();
            this.SuspendLayout();
            // 
            // dgStanjeMagacina
            // 
            this.dgStanjeMagacina.AllowUserToAddRows = false;
            this.dgStanjeMagacina.AllowUserToDeleteRows = false;
            this.dgStanjeMagacina.AllowUserToOrderColumns = true;
            this.dgStanjeMagacina.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStanjeMagacina.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStanjeMagacina.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgStanjeMagacina.BackgroundColor = System.Drawing.Color.White;
            this.dgStanjeMagacina.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgStanjeMagacina.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgStanjeMagacina.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStanjeMagacina.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgStanjeMagacina.ColumnHeadersHeight = 40;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStanjeMagacina.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgStanjeMagacina.EnableHeadersVisualStyles = false;
            this.dgStanjeMagacina.GridColor = System.Drawing.Color.LightGray;
            this.dgStanjeMagacina.Location = new System.Drawing.Point(12, 63);
            this.dgStanjeMagacina.MultiSelect = false;
            this.dgStanjeMagacina.Name = "dgStanjeMagacina";
            this.dgStanjeMagacina.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgStanjeMagacina.RowHeadersVisible = false;
            this.dgStanjeMagacina.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgStanjeMagacina.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStanjeMagacina.RowTemplate.Height = 25;
            this.dgStanjeMagacina.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStanjeMagacina.Size = new System.Drawing.Size(640, 330);
            this.dgStanjeMagacina.TabIndex = 0;
            this.dgStanjeMagacina.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStanjeMagacina_CellClick);
            this.dgStanjeMagacina.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgStanjeMagacina_CellContentClick);
            this.dgStanjeMagacina.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgStanjeMagacina_CellEndEdit);
            this.dgStanjeMagacina.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgStanjeMagacina_EditingControlShowing);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnCancel.Image = global::Damax.Properties.Resources.cancel1;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(571, 399);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 31);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Odustani";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnSave.Image = global::Damax.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(484, 399);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 31);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Dalje";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 73;
            this.label1.Text = "Sa magacina - izlaz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 74;
            this.label2.Text = "Na magacin - ulaz";
            // 
            // tbPretraga
            // 
            this.tbPretraga.Location = new System.Drawing.Point(424, 36);
            this.tbPretraga.Name = "tbPretraga";
            this.tbPretraga.Size = new System.Drawing.Size(199, 20);
            this.tbPretraga.TabIndex = 75;
            this.tbPretraga.TextChanged += new System.EventHandler(this.tbPretraga_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label3.Location = new System.Drawing.Point(421, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 76;
            this.label3.Text = "Pretraga";
            // 
            // btnDeleteSearch
            // 
            this.btnDeleteSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteSearch.Image = global::Damax.Properties.Resources.cancel1;
            this.btnDeleteSearch.Location = new System.Drawing.Point(629, 34);
            this.btnDeleteSearch.Name = "btnDeleteSearch";
            this.btnDeleteSearch.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteSearch.TabIndex = 77;
            this.btnDeleteSearch.Tag = "Obriši ";
            this.btnDeleteSearch.UseVisualStyleBackColor = true;
            this.btnDeleteSearch.Click += new System.EventHandler(this.btnDeleteSearch_Click);
            // 
            // lkpNaMagacin
            // 
            this.lkpNaMagacin.BackColor = System.Drawing.Color.White;
            this.lkpNaMagacin.ComboSelectedIndex = -1;
            this.lkpNaMagacin.DataSource = null;
            this.lkpNaMagacin.DeleteButtonEnable = true;
            this.lkpNaMagacin.DisplayMember = "";
            this.lkpNaMagacin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpNaMagacin.Location = new System.Drawing.Point(116, 35);
            this.lkpNaMagacin.LookupMember = null;
            this.lkpNaMagacin.Name = "lkpNaMagacin";
            this.lkpNaMagacin.RefreashButtonEnable = true;
            this.lkpNaMagacin.SelectedLookupValue = null;
            this.lkpNaMagacin.Size = new System.Drawing.Size(277, 21);
            this.lkpNaMagacin.TabIndex = 72;
            this.lkpNaMagacin.ValueMember = "";
            // 
            // lkpSaMagacina
            // 
            this.lkpSaMagacina.BackColor = System.Drawing.Color.White;
            this.lkpSaMagacina.ComboSelectedIndex = -1;
            this.lkpSaMagacina.DataSource = null;
            this.lkpSaMagacina.DeleteButtonEnable = true;
            this.lkpSaMagacina.DisplayMember = "";
            this.lkpSaMagacina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpSaMagacina.Location = new System.Drawing.Point(116, 8);
            this.lkpSaMagacina.LookupMember = null;
            this.lkpSaMagacina.Name = "lkpSaMagacina";
            this.lkpSaMagacina.RefreashButtonEnable = true;
            this.lkpSaMagacina.SelectedLookupValue = null;
            this.lkpSaMagacina.Size = new System.Drawing.Size(277, 21);
            this.lkpSaMagacina.TabIndex = 71;
            this.lkpSaMagacina.ValueMember = "";
            // 
            // FormPrenosArtikala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(659, 442);
            this.Controls.Add(this.btnDeleteSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbPretraga);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpNaMagacin);
            this.Controls.Add(this.lkpSaMagacina);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgStanjeMagacina);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "FormPrenosArtikala";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Prenos artikala";
            ((System.ComponentModel.ISupportInitialize)(this.dgStanjeMagacina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgStanjeMagacina;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private Template.LookupComboBox lkpSaMagacina;
        private Template.LookupComboBox lkpNaMagacin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbPretraga;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDeleteSearch;
    }
}