﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormNarudzba : FormTemplate<Narudzba>
    {
        public FormNarudzba()
        {
            InitializeComponent();
            DaoObject = new NarudzbaDao();
            SetFormControlsSettings();
            LockUnlockMenuButtons(false,false,false);
            lkpDogadjaj.RefreshLookup += new EventHandler(lookup_RefreashLookupDogadjaj);
            lkpProdajnoMjesto.RefreshLookup += new EventHandler(lookup_RefreashLookupProdajnoMjesto);
        }


        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

            dtDatum.Format = DateTimePickerFormat.Custom;
            dtDatum.CustomFormat = "dd.MM.yyyy. HH:mm";
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbBrojKartice.ReadOnly = !isEnable;
            tbIznos.ReadOnly = !isEnable;
            tbNapomena.ReadOnly = !isEnable;
            dtDatum.Enabled = isEnable;
                lkpDogadjaj.Enabled = false;
                dtpGodina.Enabled = false;
                lkpProdajnoMjesto.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            tbIznos.Text = SelectedObject.Iznos?.ToString() ?? "0";
            dtDatum.Value = SelectedObject.DatumNarudzbe ?? DateTime.Now;
            tbBrojKartice.Text = SelectedObject.KarticaUID;
            tbNapomena.Text = SelectedObject.Napomena;

        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?)null;
                SelectedObject.ProdajnoMjestoId = lkpProdajnoMjesto.LookupMember != null
                     ? Convert.ToInt32(lkpProdajnoMjesto.LookupMember)
                     : (int?)null;
                SelectedObject.Iznos = int.TryParse(tbIznos.Text, out int br) ? br : 0;
                SelectedObject.Godina = (short?)dtpGodina.Value.Year;
                SelectedObject.DatumNarudzbe = dtDatum.Value;
                SelectedObject.KarticaUID = tbBrojKartice.Text;
                SelectedObject.Napomena = tbNapomena.Text;
            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbIznos.Text = "";
            tbNapomena.Text = "";
            tbBrojKartice.Text = "";
            dtDatum.Value = DateTime.Now;
        }


        public override bool ValidateControls()
        {
            decimal value;
            if (!decimal.TryParse(tbIznos.Text, out value))
            {
                tbIznos.Text = "";
                GetErrorProvider().SetError(tbIznos, "Neispravan format podatka.");
                IsValid = false;
            }
            if (!decimal.TryParse(tbBrojKartice.Text, out value))
            {
                tbBrojKartice.Text = "";
                GetErrorProvider().SetError(tbBrojKartice, "Neispravan format podatka.");
                IsValid = false;
            }

            if (lkpDogadjaj.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (lkpProdajnoMjesto.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpProdajnoMjesto, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();

            {
                FormNarudzbaStavka form = new FormNarudzbaStavka();
                form.Filters.Add(new TemplateFilter(NarudzbaStavka.F_Godina, SelectedObject.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(NarudzbaStavka.F_DogadjajId, SelectedObject.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(NarudzbaStavka.F_ProdajnoMjestoId, SelectedObject.ProdajnoMjestoId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(NarudzbaStavka.F_NarudzbaId, SelectedObject.NarudzbaId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Narudzba.F_DogadjajId].Visible = false;
            grid.Columns[Narudzba.F_DogadjajId].HeaderText = "Dogadjaj";
            grid.Columns[Narudzba.F_Godina].Width = 70;
            grid.Columns[Narudzba.F_Iznos].HeaderText = "Iznos";
            grid.Columns[Narudzba.F_Iznos].DefaultCellStyle.Format = "#.##";
            grid.Columns[Narudzba.F_Iznos].Width = 80;
            grid.Columns[Narudzba.F_ProdajnoMjestoId].Visible = false;
            grid.Columns[Narudzba.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Narudzba.F_NazivDogadjaja].Width = 200;
            grid.Columns[Narudzba.F_NazivProdajnogMjesta].HeaderText = "Prodajno mjesto";
            grid.Columns[Narudzba.F_NazivProdajnogMjesta].Width = 200;
            grid.Columns[Narudzba.F_DatumNarudzbe].HeaderText = "Datum";
            grid.Columns[Narudzba.F_DatumNarudzbe].DefaultCellStyle.Format = "dd.MM.yyyy. HH:mm";
            grid.Columns[Narudzba.F_DatumNarudzbe].Width = 150;
            grid.Columns[Narudzba.F_NarudzbaId].Visible = true;
            grid.Columns[Narudzba.F_NarudzbaId].HeaderText = "Rbr.";
            grid.Columns[Narudzba.F_KarticaUId].HeaderText = "Id kartice";

            SetSumLabel();




        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            setLookups();
        }

        private void SetSumLabel() {

            decimal? d = 0;
            foreach (Narudzba n in this.Table)
            {
                d += n.Iznos;
            }
            lblSuma.Text = d.ToString()+" "+HomeFilters.HomeFilterEvent.Valuta;
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            setLookups();
        }

        private void setLookups()
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });

            if (SelectedObject != null)
            {
                lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
                lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
                lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]
                ,new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId,SelectedObject.ProdajnoMjestoId.ToString(),"=")});
            }
            else
            {
                lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
                lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
                lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(Filters);
            }
        }


        protected void lookup_RefreashLookupDogadjaj(object sender, EventArgs e)
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
        }


        protected void lookup_RefreashLookupProdajnoMjesto(object sender, EventArgs e)
        {
            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(Filters);
        }
        
    }
}
