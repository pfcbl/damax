﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Excel = Microsoft.Office.Interop.Excel;

namespace Damax.Forms
{
    public partial class FormMagacin : FormTemplate<Magacin>
    {
        public FormMagacin()
        {
            InitializeComponent();
            DaoObject = new MagacinDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            btnImport.Enabled = HomeFilters.HomeFilterAktivanDogadjaj;
            btnPrebaciArtikle.Enabled = HomeFilters.HomeFilterAktivanDogadjaj;
            SetFormControlsSettings();
            lkpDogadjaj.RefreshLookup += new EventHandler(Lookup_RefreashLookupDogadjaj);
            lkpMagacinNadredjeni.RefreshLookup += new EventHandler(Lookup_RefreashLookupNadredjeni);
            lkpProdajnoMjesto.RefreshLookup += new EventHandler(Lookup_RefreashLookupProdajnoMjesto);
        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;
            
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            lkpMagacinNadredjeni.Enabled = isEnable;
            lkpProdajnoMjesto.Enabled = isEnable;
            tbNaziv.ReadOnly = !isEnable;
            tbLokacija.ReadOnly = !isEnable;
            cbCentralni.Enabled = isEnable;
            lkpDogadjaj.Enabled = false;
            dtpGodina.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            tbNaziv.Text = SelectedObject.Naziv;
            tbLokacija.Text = SelectedObject.Lokacija;
            cbCentralni.Checked = (bool)SelectedObject.Centralni;


        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?)null;
                SelectedObject.MagacinIdNadredjeni = lkpMagacinNadredjeni.LookupMember != null
                    ? Convert.ToInt32(lkpMagacinNadredjeni.LookupMember)
                    : (int?)null;
                SelectedObject.ProdajnoMjestoId = lkpProdajnoMjesto.LookupMember != null
                    ? Convert.ToInt32(lkpProdajnoMjesto.LookupMember)
                    : (int?)null;
                SelectedObject.Naziv = tbNaziv.Text;
                SelectedObject.Godina = (short?)dtpGodina.Value.Year;
                SelectedObject.Lokacija = tbLokacija.Text;
                SelectedObject.Centralni = cbCentralni.Checked;
            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbNaziv.Text = "";
            tbLokacija.Text = "";
            cbCentralni.Checked = false;
        }

        public override void AddNextForms()
        {
            base.AddNextForms();

            {
                var form = new FormMagacinArtikal();
                form.Filters.Add(new TemplateFilter(MagacinArtikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(MagacinArtikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(MagacinArtikal.F_MagacinId, SelectedObject.MagacinId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
            {
                var form = new FormPrenosnica();
                form.Filters.Add(new TemplateFilter(Prenosnica.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Prenosnica.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Prenosnica.F_MagacinOdredisteId, SelectedObject.MagacinId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }

            {
                var form = new FormPrenosnica();
                form.Filters.Add(new TemplateFilter(Prenosnica.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Prenosnica.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Prenosnica.F_MagacinSourceId, SelectedObject.MagacinId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Magacin.F_DogadjajId].Visible = false;
            grid.Columns[Magacin.F_ProdajnoMjestoId].Visible = false;
            grid.Columns[Magacin.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Magacin.F_Godina].Width = 70;
            grid.Columns[Magacin.F_Naziv].Width = 200;
            grid.Columns[Magacin.F_ProdajnoMjestoNaziv].Width = 200;
            grid.Columns[Magacin.F_ProdajnoMjestoNaziv].HeaderText = "Prodajno mjesto";
            grid.Columns[Magacin.F_MagacinId].Width = 60;
            grid.Columns[Magacin.F_MagacinId].HeaderText = "Šifra";
            grid.Columns[Magacin.F_MagacinIdNadredjeni].HeaderText = "Šifra centralog";
            grid.Columns[Magacin.F_MagacinIdNadredjeni].Width = 100;
            grid.Columns[Magacin.F_MagacinIdNadredjeni].HeaderText = "Nadređeni magacin";
            grid.Columns[Magacin.F_MagacinIdNadredjeni].Visible = false;
            grid.Columns[Magacin.F_NazivNadredjenog].HeaderText = "Centralni magacin";
            grid.Columns[Magacin.F_NazivNadredjenog].Width = 150;

        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            if (SelectedObject != null && HomeFilters.HomeFilterAktivanDogadjaj)
                btnImport.Enabled = SelectedObject.Centralni;
            FillLookups();
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            FillLookups();
        }

        private void FillLookups()
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });

            SetLookups();
        }

        public override void AddReportForms()
        {
            base.AddReportForms();

            {
                if (SelectedObject != null)
                {
                    this.ReportForms.Clear();
                    var form = new FormIzvStanjeMagacina(SelectedObject);
                    ReportForms.Add(form);
                }
            }
        }

        protected void Lookup_RefreashLookupDogadjaj(object sender, EventArgs e)
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
        }
        protected void Lookup_RefreashLookupNadredjeni(object sender, EventArgs e)
        {
            lkpMagacinNadredjeni.DisplayMember = Magacin.F_Naziv;
            lkpMagacinNadredjeni.ValueMember = Magacin.F_MagacinId;
            lkpMagacinNadredjeni.DataSource = (new MagacinDao()).Select(Filters);
        }

        protected void Lookup_RefreashLookupProdajnoMjesto(object sender, EventArgs e)
        {
            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(Filters);
        }

        private void CbCentralni_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if(cb.Checked)
            {
                lkpMagacinNadredjeni.ClearLookup();
                lkpProdajnoMjesto.ClearLookup();
            }
            else
            {
                SetLookups();
            }
        }

        private void SetLookups()
        {

            if (SelectedObject != null)
            {
                lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
                lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
                lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1], new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId, SelectedObject.ProdajnoMjestoId.ToString(), SelectedObject.ProdajnoMjestoId.ToString().Equals("") ? " is null " : "=") });


                lkpMagacinNadredjeni.DisplayMember = Magacin.F_Naziv;
                lkpMagacinNadredjeni.ValueMember = Magacin.F_MagacinIdNadredjeni;
                lkpMagacinNadredjeni.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1], new TemplateFilter(Magacin.F_MagacinId, SelectedObject.MagacinIdNadredjeni.ToString().Equals("") ? "-1" : SelectedObject.MagacinIdNadredjeni.ToString(), "=") });
            }
            else
            {
                lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
                lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
                lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(Filters);

                lkpMagacinNadredjeni.DisplayMember = Magacin.F_Naziv;
                lkpMagacinNadredjeni.ValueMember = Magacin.F_MagacinIdNadredjeni;
                lkpMagacinNadredjeni.DataSource = (new MagacinDao()).Select(Filters);
            }
        }

        private void btnPrebaciArtikle_Click(object sender, EventArgs e)
        {
            if (this.FormMode == Constants.Form_Mode.MODE_VIEW)
            {
                if (SelectedObject == null)
                    return;
                var form = new FormPrenosArtikala(SelectedObject);
                form.ShowDialog();
            }
        }


        public bool ImportExcelFile(string putanja)
        {
            var artikli = new List<Artikal>();
            var lista = new List<MagacinArtikal>();
            bool returnValue = true;
            int artikalId = (new ArtikalDao()).SelectMaxId(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
                });
           

            var kategorija = new ArtikalKategorija()
            {
                Godina = (short?)HomeFilters.HomeFilterGodina,
                DogadjajId = HomeFilters.HomeFilterDogadjajId,
            };

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(putanja, ReadOnly: false);

            for (int s = 1; s <= xlWorkbook.Worksheets.Count; s++)
            {
                Excel.Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Worksheets.get_Item(s);
                Excel.Range xlRange = xlWorksheet.UsedRange;

                kategorija.Naziv = xlWorksheet.Name;
                if (!(new ArtikalKategorijaDao()).Insert(kategorija))
                    continue;
                int kategorijaId = (new ArtikalKategorijaDao()).SelectMaxId(new List<TemplateFilter>() {
                        new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
                });
                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                for (int i = 2; i <= rowCount; i++)
                {
                    var artikal = new Artikal()
                    {
                        Godina = (short?)HomeFilters.HomeFilterGodina,
                        DogadjajId = HomeFilters.HomeFilterDogadjajId,
                        ArtikalKategorijaId=kategorijaId,
                        ArtikalId = ++artikalId
                    };

                    var magacinArtikal = new MagacinArtikal()
                    {
                        Godina = (short?)HomeFilters.HomeFilterGodina,
                        DogadjajId = HomeFilters.HomeFilterDogadjajId,
                        MagacinId = SelectedObject.MagacinId
                    };

                    try
                    {
                        var test = (object[,])xlRange.Value2;


                        artikal.Naziv = test[i, 1] != null ? test[i, 1].ToString().Trim() : "";
                        artikal.Cijena = decimal.TryParse(test[i, 2] != null ? test[i, 2].ToString() : "".Trim(), out decimal dec) ? dec : 0;
                        artikal.Favorit = (int.TryParse(test[i, 4] != null ? test[i, 4].ToString() : "".Trim(), out int b) ? b : 0)==1;
                        artikal.PuniNaziv = test[i, 5] != null ? test[i, 5].ToString().Trim() : "";
                        artikal.Neaktivan = false;
                        artikal.JedinicaMjere = "kom";
                        artikli.Add(artikal);

                        magacinArtikal.ArtikalId = artikalId;
                        magacinArtikal.TrenutnoStanje = int.TryParse(test[i, 3] != null ? test[i, 3].ToString() : "".Trim(), out int kol) ? kol : 0;
                        magacinArtikal.PocetnoStanje = magacinArtikal.TrenutnoStanje;
                        lista.Add(magacinArtikal);
                    }
                    catch (Exception e)
                    {
                        returnValue = false;
                    }
                }
            }
            if ((new ArtikalDao()).InsertList(artikli))
                (new MagacinArtikalDao()).InsertList(lista);
            else
            {
                MessageBox.Show("Desila se greška prilikom inserta artikala u bazu. Provjerite odabrani eksel fajl.", "Neuspješno importovanje artikala", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return returnValue;
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (this.FormMode == Constants.Form_Mode.MODE_VIEW)
            {
                OpenFileDialog theDialog = new OpenFileDialog();
                theDialog.Title = "Odaberite fajl";
                theDialog.Filter = "Excel files|*.xlsx";
                theDialog.InitialDirectory = @"C:\";
                if (theDialog.ShowDialog() == DialogResult.OK)
                {
                    ImportExcelFile(theDialog.FileName);
                }
            }
        }

        private void btnPrebaciArtikle_MouseHover(object sender, EventArgs e)
        {
            ttPrebacivanje.SetToolTip(btnPrebaciArtikle, "Prebacivanje artikala");
        }

        private void btnImport_MouseHover(object sender, EventArgs e)
        {
            ttPrebacivanje.SetToolTip(btnImport, "Import artikala iz eksela");
        }
    }
}
