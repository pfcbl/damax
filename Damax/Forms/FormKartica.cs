﻿using Damax.DAO;
using Damax.Models;
using Damax.Template;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
    public partial class FormKartica : FormTemplate<Kartica>
    {
        public FormKartica()
        {
            InitializeComponent();
            DaoObject = new KarticaDao();
            LockUnlockMenuButtons(false, false, false);

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbKarticaUID.ReadOnly = true;

            tbStatus.ReadOnly = true;
        }

        public override void FillControls()
        {
            base.FillControls();
            tbKarticaUID.Text = SelectedObject.KarticaUID;
            tbStatus.Text = SelectedObject.Status;
        }

        public override void AddNextForms()
        {
            base.AddNextForms();
            {
                FormNarudzba form = new FormNarudzba();
                form.Filters.Add(new TemplateFilter(Narudzba.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Narudzba.F_DogadjajId, HomeFilters.HomeFilterDogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Narudzba.F_KarticaUId, "'"+SelectedObject.KarticaUID.ToString()+"'", "="));
                IsInNextMode = true;
                NextForms.Add(form);
            }
            {
                FormTransakcija form = new FormTransakcija();
                form.Filters.Add(new TemplateFilter(Transakcija.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Transakcija.F_DogadjajId, HomeFilters.HomeFilterDogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Transakcija.F_KarticaUID, "'" + SelectedObject.KarticaUID.ToString() + "'", "="));
                IsInNextMode = true;
                NextForms.Add(form);
            }

        }

        public override void FillObject()
        {
            base.FillObject();
            SelectedObject.KarticaUID = tbKarticaUID.Text;
            SelectedObject.Status = tbStatus.Text;

        }

        public override void ClearControls()
        {
            base.ClearControls();
            tbKarticaUID.Text = "";
            tbStatus.Text = "";

        }


        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Kartica.F_KarticaUid].HeaderText = "UID kartice";
            grid.Columns[Kartica.F_Status].HeaderText = "Status";
            

        }

    }
}
