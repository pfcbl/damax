﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections;

namespace DamaxDesktop
{
    public partial class Svi_korisnici : Form
    {
        public Svi_korisnici()
        {
            InitializeComponent();
            popuni_dataGridView();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void Svi_korisnici_Load(object sender, EventArgs e)
        {

        }
        private void popuni_dataGridView()
        {
            
            List<DTO.KorisnikDTO> korisnici = new List<DTO.KorisnikDTO>();
            korisnici= DAO.KorisnikDao.selectKorisnici();
            
            foreach (DTO.KorisnikDTO kor in korisnici)
            {
                dataGridView1.Rows.Add(kor.Ime,kor.Prezime,kor.IdKorisnik);
            }
           


        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[0].Value.ToString().ToLower().StartsWith(textBox1.Text) || row.Cells[0].Value.ToString().ToUpper().StartsWith(textBox1.Text) || row.Cells[0].Value.ToString().StartsWith(textBox1.Text))
                {
                    row.Selected = true;
                }
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void button2_Click(object sender, EventArgs e)
        {
            Novi_korisnik nk = new Novi_korisnik();
            nk.Show();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells[1].Value.ToString().ToLower().StartsWith(textBox1.Text) || row.Cells[1].Value.ToString().ToUpper().StartsWith(textBox1.Text) || row.Cells[1].Value.ToString().StartsWith(textBox1.Text))
                    {
                        row.Selected = true;
                    }
                }

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni da zelite obrisati korrisnika?", "Pitanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
              
                if (DAO.KorisnikDao.obrisiKorisnika(dataGridView1.SelectedRows[0].Cells[2].Value.ToString()))
                {
                    MessageBox.Show("Korisnik je izbrisan.", "Obavjestenje");
                }
                dataGridView1.Rows.Clear();
                popuni_dataGridView();
                
            }
            

            


        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            popuni_dataGridView();
        }

       
    }
}
