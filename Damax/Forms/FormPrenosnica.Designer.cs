﻿namespace Damax.Forms
{
    partial class FormPrenosnica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lkpMagacinIzlaz = new Damax.Template.LookupComboBox();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lkpMagacinUlaz = new Damax.Template.LookupComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtDatum = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.tbTipPrenosa = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 78;
            this.label1.Text = "Magacin izlaz";
            // 
            // lkpMagacinIzlaz
            // 
            this.lkpMagacinIzlaz.BackColor = System.Drawing.Color.White;
            this.lkpMagacinIzlaz.ComboSelectedIndex = -1;
            this.lkpMagacinIzlaz.DataSource = null;
            this.lkpMagacinIzlaz.DeleteButtonEnable = true;
            this.lkpMagacinIzlaz.DisplayMember = "";
            this.lkpMagacinIzlaz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpMagacinIzlaz.Location = new System.Drawing.Point(121, 96);
            this.lkpMagacinIzlaz.LookupMember = null;
            this.lkpMagacinIzlaz.Name = "lkpMagacinIzlaz";
            this.lkpMagacinIzlaz.RefreashButtonEnable = true;
            this.lkpMagacinIzlaz.SelectedLookupValue = null;
            this.lkpMagacinIzlaz.Size = new System.Drawing.Size(306, 21);
            this.lkpMagacinIzlaz.TabIndex = 77;
            this.lkpMagacinIzlaz.ValueMember = "";
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(562, 59);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(306, 21);
            this.lkpDogadjaj.TabIndex = 74;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(488, 63);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 76;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 63);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 75;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 59);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 73;
            // 
            // lkpMagacinUlaz
            // 
            this.lkpMagacinUlaz.BackColor = System.Drawing.Color.White;
            this.lkpMagacinUlaz.ComboSelectedIndex = -1;
            this.lkpMagacinUlaz.DataSource = null;
            this.lkpMagacinUlaz.DeleteButtonEnable = true;
            this.lkpMagacinUlaz.DisplayMember = "";
            this.lkpMagacinUlaz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpMagacinUlaz.Location = new System.Drawing.Point(121, 133);
            this.lkpMagacinUlaz.LookupMember = null;
            this.lkpMagacinUlaz.Name = "lkpMagacinUlaz";
            this.lkpMagacinUlaz.RefreashButtonEnable = true;
            this.lkpMagacinUlaz.SelectedLookupValue = null;
            this.lkpMagacinUlaz.Size = new System.Drawing.Size(306, 21);
            this.lkpMagacinUlaz.TabIndex = 79;
            this.lkpMagacinUlaz.ValueMember = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(12, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 80;
            this.label2.Text = "Magacin ulaz";
            // 
            // dtDatum
            // 
            this.dtDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtDatum.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDatum.Location = new System.Drawing.Point(121, 171);
            this.dtDatum.Name = "dtDatum";
            this.dtDatum.Size = new System.Drawing.Size(165, 20);
            this.dtDatum.TabIndex = 81;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(12, 173);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(41, 13);
            this.lblLokacija.TabIndex = 82;
            this.lblLokacija.Text = "Vrijeme";
            // 
            // tbTipPrenosa
            // 
            this.tbTipPrenosa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTipPrenosa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbTipPrenosa.Location = new System.Drawing.Point(121, 209);
            this.tbTipPrenosa.Name = "tbTipPrenosa";
            this.tbTipPrenosa.Size = new System.Drawing.Size(165, 20);
            this.tbTipPrenosa.TabIndex = 83;
            this.tbTipPrenosa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label3.Location = new System.Drawing.Point(12, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 84;
            this.label3.Text = "Tip prenosa";
            // 
            // FormPrenosnica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbTipPrenosa);
            this.Controls.Add(this.dtDatum);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lkpMagacinUlaz);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpMagacinIzlaz);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormPrenosnica";
            this.Text = "Prenosnice";
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpMagacinIzlaz, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lkpMagacinUlaz, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtDatum, 0);
            this.Controls.SetChildIndex(this.tbTipPrenosa, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Template.LookupComboBox lkpMagacinIzlaz;
        private Template.LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private Template.LookupComboBox lkpMagacinUlaz;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtDatum;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.TextBox tbTipPrenosa;
        private System.Windows.Forms.Label label3;
    }
}