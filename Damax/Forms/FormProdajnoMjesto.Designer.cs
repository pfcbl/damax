﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormProdajnoMjesto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.tbUplata = new System.Windows.Forms.TextBox();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.tbLokacija = new System.Windows.Forms.TextBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label3.Location = new System.Drawing.Point(22, 243);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 87;
            this.label3.Text = "Ukupan promet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(22, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 86;
            this.label2.Text = "Lozinka";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(22, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 85;
            this.label1.Text = "Korisničko ime";
            // 
            // tbPassword
            // 
            this.tbPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbPassword.Location = new System.Drawing.Point(131, 206);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(134, 20);
            this.tbPassword.TabIndex = 80;
            // 
            // tbUsername
            // 
            this.tbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbUsername.Location = new System.Drawing.Point(131, 173);
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(134, 20);
            this.tbUsername.TabIndex = 79;
            // 
            // tbUplata
            // 
            this.tbUplata.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbUplata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbUplata.Location = new System.Drawing.Point(131, 240);
            this.tbUplata.Name = "tbUplata";
            this.tbUplata.Size = new System.Drawing.Size(134, 20);
            this.tbUplata.TabIndex = 78;
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(570, 68);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(298, 21);
            this.lkpDogadjaj.TabIndex = 75;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(496, 72);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 84;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // tbLokacija
            // 
            this.tbLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbLokacija.Location = new System.Drawing.Point(131, 137);
            this.tbLokacija.Name = "tbLokacija";
            this.tbLokacija.Size = new System.Drawing.Size(737, 20);
            this.tbLokacija.TabIndex = 77;
            // 
            // tbNaziv
            // 
            this.tbNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNaziv.Location = new System.Drawing.Point(131, 102);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(737, 20);
            this.tbNaziv.TabIndex = 76;
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(22, 74);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 81;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(131, 70);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(90, 20);
            this.dtpGodina.TabIndex = 74;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(22, 140);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(47, 13);
            this.lblLokacija.TabIndex = 83;
            this.lblLokacija.Text = "Lokacija";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(22, 105);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(39, 13);
            this.lblNaziv.TabIndex = 82;
            this.lblNaziv.Text = "Naziv";
            // 
            // FormProdajnoMjesto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.tbUplata);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.tbLokacija);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormProdajnoMjesto";
            this.Text = "Prodajna mjesta";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.tbNaziv, 0);
            this.Controls.SetChildIndex(this.tbLokacija, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.tbUplata, 0);
            this.Controls.SetChildIndex(this.tbUsername, 0);
            this.Controls.SetChildIndex(this.tbPassword, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbUplata;
        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.TextBox tbLokacija;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.Label lblNaziv;
    }
}