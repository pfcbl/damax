﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHome));
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.msGlavniMeni = new System.Windows.Forms.MenuStrip();
            this.sifarniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kategorijeArtikalaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artikliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.karticeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blagajneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transakcijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodajnaMjestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.narudžbeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.magaciniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prenosArtikalaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvještajiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPregledStanjaMagacina = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPregledPrenosnica = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPregledPrenosaArtikala = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmPregledNaplatePoProdajnomMjestu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProsjecnaPotrosnjaPoSatima = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBrojNarudzbiPoProdajnomMjestu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmBrojUplataPoSatimaPoBlagajni = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmProsjekUplataPoKarticama = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBrojIzdatihKarticaPoSatima = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBrojAktivnihKarticaPoSatima = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSaldoSmjene = new System.Windows.Forms.ToolStripMenuItem();
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administracijaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.korisniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dogadjajiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label15 = new System.Windows.Forms.Label();
            this.crtArtikliMagacini = new LiveCharts.WinForms.CartesianChart();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label13 = new System.Windows.Forms.Label();
            this.pieChart1 = new LiveCharts.WinForms.PieChart();
            this.label14 = new System.Windows.Forms.Label();
            this.crtArtikli = new LiveCharts.WinForms.CartesianChart();
            this.btnBlagajne = new System.Windows.Forms.Button();
            this.btnMagacini = new System.Windows.Forms.Button();
            this.btnProdajnaMjesta = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblUkupnoUplaceno = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblBrIzdatihKartica = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblPotrosnjaPoKartici = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblBrAktivnihKartica = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblUkupnaPotrosnja = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblLogovaniKorisnik = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnPrenosnice = new System.Windows.Forms.Button();
            this.btnZakljucajDogadjaj = new System.Windows.Forms.Button();
            this.btnnDogadjaji = new System.Windows.Forms.Button();
            this.btnKorisnici = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timerMenu = new System.Windows.Forms.Timer(this.components);
            this.pnlSlide = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.panel9 = new System.Windows.Forms.Panel();
            this.crtProdajaPoSatima = new LiveCharts.WinForms.CartesianChart();
            this.cbListProdaja = new System.Windows.Forms.CheckedListBox();
            this.btnRefreshChart = new System.Windows.Forms.Button();
            this.dtProdajaDo = new System.Windows.Forms.DateTimePicker();
            this.dtProdajaOd = new System.Windows.Forms.DateTimePicker();
            this.artikalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel10 = new System.Windows.Forms.Panel();
            this.crtUplateSati = new LiveCharts.WinForms.CartesianChart();
            this.cbListUplate = new System.Windows.Forms.CheckedListBox();
            this.btnUplateRefresh = new System.Windows.Forms.Button();
            this.dtUplateDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtUplateDatumOd = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpProdaja = new System.Windows.Forms.TabPage();
            this.lblValuta = new System.Windows.Forms.Label();
            this.tpUplate = new System.Windows.Forms.TabPage();
            this.lblValutaUplate = new System.Windows.Forms.Label();
            this.cbListMagacini = new System.Windows.Forms.CheckedListBox();
            this.dgStanjeMagacina = new System.Windows.Forms.DataGridView();
            this.damaxDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnPrenosArtikala = new System.Windows.Forms.Button();
            this.btnPregledStanjaMagacina = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.btnProsjekUplata = new System.Windows.Forms.Button();
            this.btnBrojizdatihKarticaPoSatima = new System.Windows.Forms.Button();
            this.btnBrojAktivPoSatima = new System.Windows.Forms.Button();
            this.btnBrojUplataPoSatima = new System.Windows.Forms.Button();
            this.btnBrojNarudzbiPoSatima = new System.Windows.Forms.Button();
            this.btnProdajaArtikala = new System.Windows.Forms.Button();
            this.btnPregledNaplatepoSatima = new System.Windows.Forms.Button();
            this.btnDeleteSearch = new System.Windows.Forms.Button();
            this.tbPretraga = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblUkupnoIsplaceno = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSaldoSmjene = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lookupComboBox1 = new Damax.Template.LookupComboBox();
            this.msGlavniMeni.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlSlide.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.artikalBindingSource)).BeginInit();
            this.panel10.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpProdaja.SuspendLayout();
            this.tpUplate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgStanjeMagacina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.damaxDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel11.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.White;
            this.lblDogadjaj.Location = new System.Drawing.Point(844, 5);
            this.lblDogadjaj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(48, 13);
            this.lblDogadjaj.TabIndex = 3;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // msGlavniMeni
            // 
            this.msGlavniMeni.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.msGlavniMeni.AutoSize = false;
            this.msGlavniMeni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.msGlavniMeni.Dock = System.Windows.Forms.DockStyle.None;
            this.msGlavniMeni.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.msGlavniMeni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sifarniciToolStripMenuItem,
            this.uplateToolStripMenuItem,
            this.prodajaToolStripMenuItem,
            this.izvještajiToolStripMenuItem,
            this.administracijaToolStripMenuItem});
            this.msGlavniMeni.Location = new System.Drawing.Point(71, 0);
            this.msGlavniMeni.Name = "msGlavniMeni";
            this.msGlavniMeni.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.msGlavniMeni.Size = new System.Drawing.Size(1217, 19);
            this.msGlavniMeni.TabIndex = 8;
            this.msGlavniMeni.Text = "menuStrip1";
            // 
            // sifarniciToolStripMenuItem
            // 
            this.sifarniciToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kategorijeArtikalaToolStripMenuItem,
            this.artikliToolStripMenuItem,
            this.karticeToolStripMenuItem});
            this.sifarniciToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sifarniciToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sifarniciToolStripMenuItem.Image = global::Damax.Properties.Resources.sifarnici;
            this.sifarniciToolStripMenuItem.Name = "sifarniciToolStripMenuItem";
            this.sifarniciToolStripMenuItem.Size = new System.Drawing.Size(84, 17);
            this.sifarniciToolStripMenuItem.Text = "Šifarnici";
            // 
            // kategorijeArtikalaToolStripMenuItem
            // 
            this.kategorijeArtikalaToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.kategorijeArtikalaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.kategorijeArtikalaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.kategorijeArtikalaToolStripMenuItem.Image = global::Damax.Properties.Resources.kategorije;
            this.kategorijeArtikalaToolStripMenuItem.Name = "kategorijeArtikalaToolStripMenuItem";
            this.kategorijeArtikalaToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.kategorijeArtikalaToolStripMenuItem.Text = "Kategorije artikala";
            this.kategorijeArtikalaToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // artikliToolStripMenuItem
            // 
            this.artikliToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.artikliToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.artikliToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.artikliToolStripMenuItem.Image = global::Damax.Properties.Resources.artikli_meni;
            this.artikliToolStripMenuItem.Name = "artikliToolStripMenuItem";
            this.artikliToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.artikliToolStripMenuItem.Text = "Artikli";
            this.artikliToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // karticeToolStripMenuItem
            // 
            this.karticeToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.karticeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.karticeToolStripMenuItem.Image = global::Damax.Properties.Resources.transakcije;
            this.karticeToolStripMenuItem.Name = "karticeToolStripMenuItem";
            this.karticeToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.karticeToolStripMenuItem.Text = "Kartice";
            this.karticeToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // uplateToolStripMenuItem
            // 
            this.uplateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blagajneToolStripMenuItem,
            this.transakcijeToolStripMenuItem});
            this.uplateToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.uplateToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.uplateToolStripMenuItem.Image = global::Damax.Properties.Resources.blagajna_glavni;
            this.uplateToolStripMenuItem.Name = "uplateToolStripMenuItem";
            this.uplateToolStripMenuItem.Size = new System.Drawing.Size(115, 17);
            this.uplateToolStripMenuItem.Text = "Uplata/Isplata";
            // 
            // blagajneToolStripMenuItem
            // 
            this.blagajneToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.blagajneToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.blagajneToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.blagajneToolStripMenuItem.Image = global::Damax.Properties.Resources.blagajna_glavni;
            this.blagajneToolStripMenuItem.Name = "blagajneToolStripMenuItem";
            this.blagajneToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.blagajneToolStripMenuItem.Text = "Blagajne";
            this.blagajneToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // transakcijeToolStripMenuItem
            // 
            this.transakcijeToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.transakcijeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.transakcijeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.transakcijeToolStripMenuItem.Image = global::Damax.Properties.Resources.transakcije;
            this.transakcijeToolStripMenuItem.Name = "transakcijeToolStripMenuItem";
            this.transakcijeToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.transakcijeToolStripMenuItem.Text = "Transakcije";
            this.transakcijeToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // prodajaToolStripMenuItem
            // 
            this.prodajaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prodajnaMjestaToolStripMenuItem,
            this.narudžbeToolStripMenuItem,
            this.magaciniToolStripMenuItem,
            this.prenosArtikalaToolStripMenuItem});
            this.prodajaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.prodajaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.prodajaToolStripMenuItem.Image = global::Damax.Properties.Resources.prodaja;
            this.prodajaToolStripMenuItem.Name = "prodajaToolStripMenuItem";
            this.prodajaToolStripMenuItem.Size = new System.Drawing.Size(83, 17);
            this.prodajaToolStripMenuItem.Text = "Naplata";
            // 
            // prodajnaMjestaToolStripMenuItem
            // 
            this.prodajnaMjestaToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.prodajnaMjestaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.prodajnaMjestaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.prodajnaMjestaToolStripMenuItem.Image = global::Damax.Properties.Resources.prodajno_mjesto_glavni;
            this.prodajnaMjestaToolStripMenuItem.Name = "prodajnaMjestaToolStripMenuItem";
            this.prodajnaMjestaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.prodajnaMjestaToolStripMenuItem.Text = "Prodajna mjesta";
            this.prodajnaMjestaToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // narudžbeToolStripMenuItem
            // 
            this.narudžbeToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.narudžbeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.narudžbeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.narudžbeToolStripMenuItem.Image = global::Damax.Properties.Resources.narudzba;
            this.narudžbeToolStripMenuItem.Name = "narudžbeToolStripMenuItem";
            this.narudžbeToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.narudžbeToolStripMenuItem.Text = "Narudžbe";
            this.narudžbeToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // magaciniToolStripMenuItem
            // 
            this.magaciniToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.magaciniToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.magaciniToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.magaciniToolStripMenuItem.Image = global::Damax.Properties.Resources.storage;
            this.magaciniToolStripMenuItem.Name = "magaciniToolStripMenuItem";
            this.magaciniToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.magaciniToolStripMenuItem.Text = "Magacini";
            this.magaciniToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // prenosArtikalaToolStripMenuItem
            // 
            this.prenosArtikalaToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.prenosArtikalaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.prenosArtikalaToolStripMenuItem.Image = global::Damax.Properties.Resources.prenosnice1;
            this.prenosArtikalaToolStripMenuItem.Name = "prenosArtikalaToolStripMenuItem";
            this.prenosArtikalaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.prenosArtikalaToolStripMenuItem.Text = "Prenosnice";
            this.prenosArtikalaToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // izvještajiToolStripMenuItem
            // 
            this.izvještajiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmPregledStanjaMagacina,
            this.tsmPregledPrenosnica,
            this.tsmPregledPrenosaArtikala,
            this.toolStripSeparator1,
            this.tsmPregledNaplatePoProdajnomMjestu,
            this.tsmProsjecnaPotrosnjaPoSatima,
            this.tsmBrojNarudzbiPoProdajnomMjestu,
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu,
            this.toolStripSeparator2,
            this.tsmBrojUplataPoSatimaPoBlagajni,
            this.tsmProsjekUplataPoKarticama,
            this.tsmBrojIzdatihKarticaPoSatima,
            this.tsmBrojAktivnihKarticaPoSatima,
            this.tsmSaldoSmjene,
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem});
            this.izvještajiToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.izvještajiToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.izvještajiToolStripMenuItem.Image = global::Damax.Properties.Resources.izvjestaji;
            this.izvještajiToolStripMenuItem.Name = "izvještajiToolStripMenuItem";
            this.izvještajiToolStripMenuItem.Size = new System.Drawing.Size(86, 17);
            this.izvještajiToolStripMenuItem.Text = "Izvještaji";
            // 
            // tsmPregledStanjaMagacina
            // 
            this.tsmPregledStanjaMagacina.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmPregledStanjaMagacina.ForeColor = System.Drawing.Color.White;
            this.tsmPregledStanjaMagacina.Name = "tsmPregledStanjaMagacina";
            this.tsmPregledStanjaMagacina.Size = new System.Drawing.Size(321, 22);
            this.tsmPregledStanjaMagacina.Text = "Pregled stanja magacina";
            this.tsmPregledStanjaMagacina.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmPregledPrenosnica
            // 
            this.tsmPregledPrenosnica.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmPregledPrenosnica.ForeColor = System.Drawing.Color.White;
            this.tsmPregledPrenosnica.Name = "tsmPregledPrenosnica";
            this.tsmPregledPrenosnica.Size = new System.Drawing.Size(321, 22);
            this.tsmPregledPrenosnica.Text = "Pregled prenosnica";
            this.tsmPregledPrenosnica.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmPregledPrenosaArtikala
            // 
            this.tsmPregledPrenosaArtikala.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmPregledPrenosaArtikala.ForeColor = System.Drawing.Color.White;
            this.tsmPregledPrenosaArtikala.Name = "tsmPregledPrenosaArtikala";
            this.tsmPregledPrenosaArtikala.Size = new System.Drawing.Size(321, 22);
            this.tsmPregledPrenosaArtikala.Text = "Pregled prenosa artikala na magacine";
            this.tsmPregledPrenosaArtikala.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(57)))), ((int)(((byte)(90)))));
            this.toolStripSeparator1.ForeColor = System.Drawing.Color.White;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(318, 6);
            // 
            // tsmPregledNaplatePoProdajnomMjestu
            // 
            this.tsmPregledNaplatePoProdajnomMjestu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmPregledNaplatePoProdajnomMjestu.ForeColor = System.Drawing.Color.White;
            this.tsmPregledNaplatePoProdajnomMjestu.Name = "tsmPregledNaplatePoProdajnomMjestu";
            this.tsmPregledNaplatePoProdajnomMjestu.Size = new System.Drawing.Size(321, 22);
            this.tsmPregledNaplatePoProdajnomMjestu.Text = "Pregled naplate po prodajnom mjestu";
            this.tsmPregledNaplatePoProdajnomMjestu.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmProsjecnaPotrosnjaPoSatima
            // 
            this.tsmProsjecnaPotrosnjaPoSatima.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmProsjecnaPotrosnjaPoSatima.ForeColor = System.Drawing.Color.White;
            this.tsmProsjecnaPotrosnjaPoSatima.Name = "tsmProsjecnaPotrosnjaPoSatima";
            this.tsmProsjecnaPotrosnjaPoSatima.Size = new System.Drawing.Size(321, 22);
            this.tsmProsjecnaPotrosnjaPoSatima.Text = "Prosječna potrošnja po satima";
            this.tsmProsjecnaPotrosnjaPoSatima.Visible = false;
            this.tsmProsjecnaPotrosnjaPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmBrojNarudzbiPoProdajnomMjestu
            // 
            this.tsmBrojNarudzbiPoProdajnomMjestu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmBrojNarudzbiPoProdajnomMjestu.ForeColor = System.Drawing.Color.White;
            this.tsmBrojNarudzbiPoProdajnomMjestu.Name = "tsmBrojNarudzbiPoProdajnomMjestu";
            this.tsmBrojNarudzbiPoProdajnomMjestu.Size = new System.Drawing.Size(321, 22);
            this.tsmBrojNarudzbiPoProdajnomMjestu.Text = "Broj narudžbi po prodajnom mjestu";
            this.tsmBrojNarudzbiPoProdajnomMjestu.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmPregledPotrošnjeArtikalaPoProdajnomMjestu
            // 
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.ForeColor = System.Drawing.Color.White;
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.Name = "tsmPregledPotrošnjeArtikalaPoProdajnomMjestu";
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.Size = new System.Drawing.Size(321, 22);
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.Text = "Pregled potrošnje artikala po prodajnom mjestu";
            this.tsmPregledPotrošnjeArtikalaPoProdajnomMjestu.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(57)))), ((int)(((byte)(90)))));
            this.toolStripSeparator2.ForeColor = System.Drawing.Color.White;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(318, 6);
            // 
            // tsmBrojUplataPoSatimaPoBlagajni
            // 
            this.tsmBrojUplataPoSatimaPoBlagajni.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmBrojUplataPoSatimaPoBlagajni.ForeColor = System.Drawing.Color.White;
            this.tsmBrojUplataPoSatimaPoBlagajni.Name = "tsmBrojUplataPoSatimaPoBlagajni";
            this.tsmBrojUplataPoSatimaPoBlagajni.Size = new System.Drawing.Size(321, 22);
            this.tsmBrojUplataPoSatimaPoBlagajni.Text = "Broj uplata po satima i blagajni";
            this.tsmBrojUplataPoSatimaPoBlagajni.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmProsjekUplataPoKarticama
            // 
            this.tsmProsjekUplataPoKarticama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmProsjekUplataPoKarticama.ForeColor = System.Drawing.Color.White;
            this.tsmProsjekUplataPoKarticama.Name = "tsmProsjekUplataPoKarticama";
            this.tsmProsjekUplataPoKarticama.Size = new System.Drawing.Size(321, 22);
            this.tsmProsjekUplataPoKarticama.Text = "Prosjek uplata po satima i po blagajni";
            this.tsmProsjekUplataPoKarticama.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmBrojIzdatihKarticaPoSatima
            // 
            this.tsmBrojIzdatihKarticaPoSatima.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmBrojIzdatihKarticaPoSatima.ForeColor = System.Drawing.Color.White;
            this.tsmBrojIzdatihKarticaPoSatima.Name = "tsmBrojIzdatihKarticaPoSatima";
            this.tsmBrojIzdatihKarticaPoSatima.Size = new System.Drawing.Size(321, 22);
            this.tsmBrojIzdatihKarticaPoSatima.Text = "Broj izdatih kartica po satima";
            this.tsmBrojIzdatihKarticaPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmBrojAktivnihKarticaPoSatima
            // 
            this.tsmBrojAktivnihKarticaPoSatima.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmBrojAktivnihKarticaPoSatima.ForeColor = System.Drawing.Color.White;
            this.tsmBrojAktivnihKarticaPoSatima.Name = "tsmBrojAktivnihKarticaPoSatima";
            this.tsmBrojAktivnihKarticaPoSatima.Size = new System.Drawing.Size(321, 22);
            this.tsmBrojAktivnihKarticaPoSatima.Text = "Broj aktivnih kartica po satima";
            this.tsmBrojAktivnihKarticaPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // tsmSaldoSmjene
            // 
            this.tsmSaldoSmjene.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsmSaldoSmjene.ForeColor = System.Drawing.Color.White;
            this.tsmSaldoSmjene.Name = "tsmSaldoSmjene";
            this.tsmSaldoSmjene.Size = new System.Drawing.Size(321, 22);
            this.tsmSaldoSmjene.Text = "Saldo smjene";
            this.tsmSaldoSmjene.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // analitikaKarticaZaPlaćanjeToolStripMenuItem
            // 
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.AutoToolTip = true;
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.Name = "analitikaKarticaZaPlaćanjeToolStripMenuItem";
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.Text = "Analitika kartica za plaćanje";
            this.analitikaKarticaZaPlaćanjeToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // administracijaToolStripMenuItem
            // 
            this.administracijaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.korisniciToolStripMenuItem,
            this.dogadjajiToolStripMenuItem});
            this.administracijaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.administracijaToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.administracijaToolStripMenuItem.Image = global::Damax.Properties.Resources.administration;
            this.administracijaToolStripMenuItem.Name = "administracijaToolStripMenuItem";
            this.administracijaToolStripMenuItem.Size = new System.Drawing.Size(115, 17);
            this.administracijaToolStripMenuItem.Text = "Administracija";
            this.administracijaToolStripMenuItem.Visible = false;
            // 
            // korisniciToolStripMenuItem
            // 
            this.korisniciToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.korisniciToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.korisniciToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.korisniciToolStripMenuItem.Image = global::Damax.Properties.Resources.korisnici;
            this.korisniciToolStripMenuItem.Name = "korisniciToolStripMenuItem";
            this.korisniciToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.korisniciToolStripMenuItem.Text = "Korisnici";
            this.korisniciToolStripMenuItem.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // dogadjajiToolStripMenuItem
            // 
            this.dogadjajiToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.dogadjajiToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.dogadjajiToolStripMenuItem.Image = global::Damax.Properties.Resources.dogadjaj;
            this.dogadjajiToolStripMenuItem.Name = "dogadjajiToolStripMenuItem";
            this.dogadjajiToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.dogadjajiToolStripMenuItem.Text = "Dogadjaji";
            this.dogadjajiToolStripMenuItem.Click += new System.EventHandler(this.DogadjajiToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(1110, 19);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(8, 713);
            this.panel1.TabIndex = 10;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(-57, 225);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.splitContainer1.Panel1.Controls.Add(this.label15);
            this.splitContainer1.Panel1.Controls.Add(this.crtArtikliMagacini);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(60, 184);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 58;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label15.Location = new System.Drawing.Point(88, 5);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 13);
            this.label15.TabIndex = 58;
            this.label15.Text = "Pregled stanja magacina";
            // 
            // crtArtikliMagacini
            // 
            this.crtArtikliMagacini.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crtArtikliMagacini.Location = new System.Drawing.Point(0, 43);
            this.crtArtikliMagacini.Margin = new System.Windows.Forms.Padding(2);
            this.crtArtikliMagacini.Name = "crtArtikliMagacini";
            this.crtArtikliMagacini.Size = new System.Drawing.Size(23, 139);
            this.crtArtikliMagacini.TabIndex = 56;
            this.crtArtikliMagacini.Text = "cartesianChart1";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.BackColor = System.Drawing.Color.White;
            this.splitContainer2.Location = new System.Drawing.Point(2, 0);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.splitContainer2.Panel1.Controls.Add(this.label13);
            this.splitContainer2.Panel1.Controls.Add(this.pieChart1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.splitContainer2.Panel2.Controls.Add(this.label14);
            this.splitContainer2.Panel2.Controls.Add(this.crtArtikli);
            this.splitContainer2.Size = new System.Drawing.Size(12, 182);
            this.splitContainer2.SplitterDistance = 85;
            this.splitContainer2.SplitterWidth = 6;
            this.splitContainer2.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label13.Location = new System.Drawing.Point(3, 1);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(202, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Najprodavaniji artikli za odabrani magacin";
            // 
            // pieChart1
            // 
            this.pieChart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pieChart1.Location = new System.Drawing.Point(2, 14);
            this.pieChart1.Margin = new System.Windows.Forms.Padding(2);
            this.pieChart1.Name = "pieChart1";
            this.pieChart1.Size = new System.Drawing.Size(146, 69);
            this.pieChart1.TabIndex = 0;
            this.pieChart1.Text = "pieChart1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label14.Location = new System.Drawing.Point(2, 3);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Stanje najprodavanijih artikala ";
            // 
            // crtArtikli
            // 
            this.crtArtikli.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crtArtikli.Location = new System.Drawing.Point(2, 13);
            this.crtArtikli.Margin = new System.Windows.Forms.Padding(2);
            this.crtArtikli.Name = "crtArtikli";
            this.crtArtikli.Size = new System.Drawing.Size(146, 0);
            this.crtArtikli.TabIndex = 0;
            this.crtArtikli.Text = "cartesianChart1";
            // 
            // btnBlagajne
            // 
            this.btnBlagajne.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnBlagajne.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBlagajne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBlagajne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBlagajne.ForeColor = System.Drawing.Color.White;
            this.btnBlagajne.Image = global::Damax.Properties.Resources.blagajna_glavni;
            this.btnBlagajne.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBlagajne.Location = new System.Drawing.Point(5, 42);
            this.btnBlagajne.Margin = new System.Windows.Forms.Padding(2);
            this.btnBlagajne.Name = "btnBlagajne";
            this.btnBlagajne.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnBlagajne.Size = new System.Drawing.Size(61, 32);
            this.btnBlagajne.TabIndex = 1;
            this.btnBlagajne.Text = "Blagajne";
            this.btnBlagajne.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBlagajne.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnBlagajne.UseVisualStyleBackColor = false;
            this.btnBlagajne.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnMagacini
            // 
            this.btnMagacini.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMagacini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMagacini.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMagacini.ForeColor = System.Drawing.Color.White;
            this.btnMagacini.Image = global::Damax.Properties.Resources.storage;
            this.btnMagacini.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMagacini.Location = new System.Drawing.Point(5, 114);
            this.btnMagacini.Margin = new System.Windows.Forms.Padding(2);
            this.btnMagacini.Name = "btnMagacini";
            this.btnMagacini.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnMagacini.Size = new System.Drawing.Size(61, 32);
            this.btnMagacini.TabIndex = 3;
            this.btnMagacini.Text = "Magacini";
            this.btnMagacini.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMagacini.UseVisualStyleBackColor = true;
            this.btnMagacini.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnProdajnaMjesta
            // 
            this.btnProdajnaMjesta.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnProdajnaMjesta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdajnaMjesta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnProdajnaMjesta.ForeColor = System.Drawing.Color.White;
            this.btnProdajnaMjesta.Image = global::Damax.Properties.Resources.prodajno_mjesto_glavni;
            this.btnProdajnaMjesta.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnProdajnaMjesta.Location = new System.Drawing.Point(5, 78);
            this.btnProdajnaMjesta.Margin = new System.Windows.Forms.Padding(2);
            this.btnProdajnaMjesta.Name = "btnProdajnaMjesta";
            this.btnProdajnaMjesta.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnProdajnaMjesta.Size = new System.Drawing.Size(61, 32);
            this.btnProdajnaMjesta.TabIndex = 2;
            this.btnProdajnaMjesta.Text = "Prodajna mjesta";
            this.btnProdajnaMjesta.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnProdajnaMjesta.UseVisualStyleBackColor = true;
            this.btnProdajnaMjesta.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblUkupnoUplaceno);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Location = new System.Drawing.Point(400, 17);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(88, 85);
            this.panel6.TabIndex = 2;
            // 
            // lblUkupnoUplaceno
            // 
            this.lblUkupnoUplaceno.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblUkupnoUplaceno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblUkupnoUplaceno.Location = new System.Drawing.Point(-1, 34);
            this.lblUkupnoUplaceno.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUkupnoUplaceno.Name = "lblUkupnoUplaceno";
            this.lblUkupnoUplaceno.Size = new System.Drawing.Size(88, 48);
            this.lblUkupnoUplaceno.TabIndex = 1;
            this.lblUkupnoUplaceno.Text = "135 KM";
            this.lblUkupnoUplaceno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(-1, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 34);
            this.label4.TabIndex = 0;
            this.label4.Text = "Ukupno uplaćeno";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblBrIzdatihKartica);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(202, 17);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(88, 85);
            this.panel2.TabIndex = 0;
            // 
            // lblBrIzdatihKartica
            // 
            this.lblBrIzdatihKartica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBrIzdatihKartica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblBrIzdatihKartica.Location = new System.Drawing.Point(-1, 34);
            this.lblBrIzdatihKartica.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrIzdatihKartica.Name = "lblBrIzdatihKartica";
            this.lblBrIzdatihKartica.Size = new System.Drawing.Size(88, 48);
            this.lblBrIzdatihKartica.TabIndex = 1;
            this.lblBrIzdatihKartica.Text = "28";
            this.lblBrIzdatihKartica.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-1, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Broj izdatih kartica";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.lblPotrosnjaPoKartici);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Location = new System.Drawing.Point(4, 17);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(88, 85);
            this.panel7.TabIndex = 3;
            // 
            // lblPotrosnjaPoKartici
            // 
            this.lblPotrosnjaPoKartici.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPotrosnjaPoKartici.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPotrosnjaPoKartici.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblPotrosnjaPoKartici.Location = new System.Drawing.Point(-1, 34);
            this.lblPotrosnjaPoKartici.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPotrosnjaPoKartici.Name = "lblPotrosnjaPoKartici";
            this.lblPotrosnjaPoKartici.Size = new System.Drawing.Size(88, 49);
            this.lblPotrosnjaPoKartici.TabIndex = 1;
            this.lblPotrosnjaPoKartici.Text = "4.7 KM";
            this.lblPotrosnjaPoKartici.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(-1, -1);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 35);
            this.label9.TabIndex = 0;
            this.label9.Text = "Prosječno po kartici";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lblBrAktivnihKartica);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(301, 17);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(88, 85);
            this.panel5.TabIndex = 1;
            // 
            // lblBrAktivnihKartica
            // 
            this.lblBrAktivnihKartica.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBrAktivnihKartica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblBrAktivnihKartica.Location = new System.Drawing.Point(-1, 34);
            this.lblBrAktivnihKartica.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrAktivnihKartica.Name = "lblBrAktivnihKartica";
            this.lblBrAktivnihKartica.Size = new System.Drawing.Size(88, 48);
            this.lblBrAktivnihKartica.TabIndex = 1;
            this.lblBrAktivnihKartica.Text = "21";
            this.lblBrAktivnihKartica.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-1, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 34);
            this.label3.TabIndex = 0;
            this.label3.Text = "Broj aktivnih kartica";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.lblUkupnaPotrosnja);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Location = new System.Drawing.Point(103, 17);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(88, 85);
            this.panel8.TabIndex = 4;
            // 
            // lblUkupnaPotrosnja
            // 
            this.lblUkupnaPotrosnja.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUkupnaPotrosnja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblUkupnaPotrosnja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblUkupnaPotrosnja.Location = new System.Drawing.Point(-1, 34);
            this.lblUkupnaPotrosnja.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUkupnaPotrosnja.Name = "lblUkupnaPotrosnja";
            this.lblUkupnaPotrosnja.Size = new System.Drawing.Size(88, 48);
            this.lblUkupnaPotrosnja.TabIndex = 1;
            this.lblUkupnaPotrosnja.Text = "98.7 KM";
            this.lblUkupnaPotrosnja.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(-1, 0);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 34);
            this.label11.TabIndex = 0;
            this.label11.Text = "Ukupno naplaćeno";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.panel4.Controls.Add(this.lblLogovaniKorisnik);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 729);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1118, 16);
            this.panel4.TabIndex = 11;
            // 
            // lblLogovaniKorisnik
            // 
            this.lblLogovaniKorisnik.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLogovaniKorisnik.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogovaniKorisnik.ForeColor = System.Drawing.Color.White;
            this.lblLogovaniKorisnik.Location = new System.Drawing.Point(935, 3);
            this.lblLogovaniKorisnik.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLogovaniKorisnik.Name = "lblLogovaniKorisnik";
            this.lblLogovaniKorisnik.Size = new System.Drawing.Size(164, 12);
            this.lblLogovaniKorisnik.TabIndex = 0;
            this.lblLogovaniKorisnik.Text = "Boris Cvijanovic";
            this.lblLogovaniKorisnik.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.panel3.Controls.Add(this.btnPrenosnice);
            this.panel3.Controls.Add(this.btnZakljucajDogadjaj);
            this.panel3.Controls.Add(this.btnnDogadjaji);
            this.panel3.Controls.Add(this.btnKorisnici);
            this.panel3.Controls.Add(this.btnBlagajne);
            this.panel3.Controls.Add(this.btnMagacini);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.btnProdajnaMjesta);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(71, 731);
            this.panel3.TabIndex = 12;
            // 
            // btnPrenosnice
            // 
            this.btnPrenosnice.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPrenosnice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrenosnice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrenosnice.ForeColor = System.Drawing.Color.White;
            this.btnPrenosnice.Image = global::Damax.Properties.Resources.prenosnice1;
            this.btnPrenosnice.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPrenosnice.Location = new System.Drawing.Point(5, 149);
            this.btnPrenosnice.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrenosnice.Name = "btnPrenosnice";
            this.btnPrenosnice.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnPrenosnice.Size = new System.Drawing.Size(61, 32);
            this.btnPrenosnice.TabIndex = 8;
            this.btnPrenosnice.Text = "Prenosnice";
            this.btnPrenosnice.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPrenosnice.UseVisualStyleBackColor = true;
            this.btnPrenosnice.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnZakljucajDogadjaj
            // 
            this.btnZakljucajDogadjaj.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnZakljucajDogadjaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZakljucajDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnZakljucajDogadjaj.ForeColor = System.Drawing.Color.White;
            this.btnZakljucajDogadjaj.Image = global::Damax.Properties.Resources._lock;
            this.btnZakljucajDogadjaj.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnZakljucajDogadjaj.Location = new System.Drawing.Point(5, 274);
            this.btnZakljucajDogadjaj.Margin = new System.Windows.Forms.Padding(2);
            this.btnZakljucajDogadjaj.Name = "btnZakljucajDogadjaj";
            this.btnZakljucajDogadjaj.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnZakljucajDogadjaj.Size = new System.Drawing.Size(61, 32);
            this.btnZakljucajDogadjaj.TabIndex = 7;
            this.btnZakljucajDogadjaj.Text = "Zaključi događaj";
            this.btnZakljucajDogadjaj.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnZakljucajDogadjaj.UseVisualStyleBackColor = true;
            this.btnZakljucajDogadjaj.Click += new System.EventHandler(this.BtnZakljucajDogadjaj_Click);
            // 
            // btnnDogadjaji
            // 
            this.btnnDogadjaji.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnnDogadjaji.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnnDogadjaji.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnnDogadjaji.ForeColor = System.Drawing.Color.White;
            this.btnnDogadjaji.Image = global::Damax.Properties.Resources.dogadjaj;
            this.btnnDogadjaji.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnnDogadjaji.Location = new System.Drawing.Point(5, 202);
            this.btnnDogadjaji.Margin = new System.Windows.Forms.Padding(2);
            this.btnnDogadjaji.Name = "btnnDogadjaji";
            this.btnnDogadjaji.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnnDogadjaji.Size = new System.Drawing.Size(61, 32);
            this.btnnDogadjaji.TabIndex = 6;
            this.btnnDogadjaji.Text = "Dogadjaji";
            this.btnnDogadjaji.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnnDogadjaji.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnnDogadjaji.UseVisualStyleBackColor = true;
            this.btnnDogadjaji.Click += new System.EventHandler(this.DogadjajiToolStripMenuItem_Click);
            // 
            // btnKorisnici
            // 
            this.btnKorisnici.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnKorisnici.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKorisnici.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnKorisnici.ForeColor = System.Drawing.Color.White;
            this.btnKorisnici.Image = global::Damax.Properties.Resources.korisnici;
            this.btnKorisnici.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnKorisnici.Location = new System.Drawing.Point(5, 238);
            this.btnKorisnici.Margin = new System.Windows.Forms.Padding(2);
            this.btnKorisnici.Name = "btnKorisnici";
            this.btnKorisnici.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnKorisnici.Size = new System.Drawing.Size(61, 32);
            this.btnKorisnici.TabIndex = 5;
            this.btnKorisnici.Text = "Korisnici";
            this.btnKorisnici.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnKorisnici.UseVisualStyleBackColor = true;
            this.btnKorisnici.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Damax.Properties.Resources.logo_glavni;
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 35);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.Lookup_RefreashLookup);
            // 
            // timerMenu
            // 
            this.timerMenu.Interval = 1;
            // 
            // pnlSlide
            // 
            this.pnlSlide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.pnlSlide.Controls.Add(this.label8);
            this.pnlSlide.Controls.Add(this.label7);
            this.pnlSlide.Controls.Add(this.label5);
            this.pnlSlide.Location = new System.Drawing.Point(32, 32);
            this.pnlSlide.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSlide.Name = "pnlSlide";
            this.pnlSlide.Size = new System.Drawing.Size(0, 442);
            this.pnlSlide.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(2, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Artikli";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(2, 45);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "Prodajna mjesta";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(2, 18);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Blagajne";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(886, 3);
            this.dtpGodina.Margin = new System.Windows.Forms.Padding(2);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(47, 20);
            this.dtpGodina.TabIndex = 47;
            this.dtpGodina.ValueChanged += new System.EventHandler(this.DtpGodina_ValueChanged);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel9.Controls.Add(this.crtProdajaPoSatima);
            this.panel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(136)))));
            this.panel9.Location = new System.Drawing.Point(109, 21);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(910, 178);
            this.panel9.TabIndex = 53;
            // 
            // crtProdajaPoSatima
            // 
            this.crtProdajaPoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crtProdajaPoSatima.BackColor = System.Drawing.Color.Transparent;
            this.crtProdajaPoSatima.Cursor = System.Windows.Forms.Cursors.Default;
            this.crtProdajaPoSatima.ForeColor = System.Drawing.Color.White;
            this.crtProdajaPoSatima.Location = new System.Drawing.Point(5, 4);
            this.crtProdajaPoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.crtProdajaPoSatima.Name = "crtProdajaPoSatima";
            this.crtProdajaPoSatima.Size = new System.Drawing.Size(907, 174);
            this.crtProdajaPoSatima.TabIndex = 52;
            this.crtProdajaPoSatima.Text = "cartesianChart1";
            // 
            // cbListProdaja
            // 
            this.cbListProdaja.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbListProdaja.BackColor = System.Drawing.Color.White;
            this.cbListProdaja.CheckOnClick = true;
            this.cbListProdaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbListProdaja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.cbListProdaja.FormattingEnabled = true;
            this.cbListProdaja.Location = new System.Drawing.Point(4, 4);
            this.cbListProdaja.Margin = new System.Windows.Forms.Padding(2);
            this.cbListProdaja.Name = "cbListProdaja";
            this.cbListProdaja.Size = new System.Drawing.Size(102, 169);
            this.cbListProdaja.TabIndex = 56;
            this.cbListProdaja.UseCompatibleTextRendering = true;
            this.cbListProdaja.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CbListProdaja_ItemCheck);
            // 
            // btnRefreshChart
            // 
            this.btnRefreshChart.Image = global::Damax.Properties.Resources.refress_blue;
            this.btnRefreshChart.Location = new System.Drawing.Point(298, 3);
            this.btnRefreshChart.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefreshChart.Name = "btnRefreshChart";
            this.btnRefreshChart.Size = new System.Drawing.Size(14, 14);
            this.btnRefreshChart.TabIndex = 55;
            this.btnRefreshChart.UseVisualStyleBackColor = true;
            this.btnRefreshChart.Click += new System.EventHandler(this.BtnRefreshChart_Click);
            // 
            // dtProdajaDo
            // 
            this.dtProdajaDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtProdajaDo.Location = new System.Drawing.Point(202, 4);
            this.dtProdajaDo.Margin = new System.Windows.Forms.Padding(2);
            this.dtProdajaDo.Name = "dtProdajaDo";
            this.dtProdajaDo.Size = new System.Drawing.Size(95, 20);
            this.dtProdajaDo.TabIndex = 54;
            // 
            // dtProdajaOd
            // 
            this.dtProdajaOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtProdajaOd.Location = new System.Drawing.Point(108, 4);
            this.dtProdajaOd.Margin = new System.Windows.Forms.Padding(2);
            this.dtProdajaOd.Name = "dtProdajaOd";
            this.dtProdajaOd.Size = new System.Drawing.Size(93, 20);
            this.dtProdajaOd.TabIndex = 53;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel10.Controls.Add(this.crtUplateSati);
            this.panel10.Location = new System.Drawing.Point(76, 21);
            this.panel10.Margin = new System.Windows.Forms.Padding(2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(943, 305);
            this.panel10.TabIndex = 54;
            // 
            // crtUplateSati
            // 
            this.crtUplateSati.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crtUplateSati.BackColor = System.Drawing.Color.Transparent;
            this.crtUplateSati.Location = new System.Drawing.Point(2, 0);
            this.crtUplateSati.Margin = new System.Windows.Forms.Padding(2);
            this.crtUplateSati.Name = "crtUplateSati";
            this.crtUplateSati.Size = new System.Drawing.Size(923, 287);
            this.crtUplateSati.TabIndex = 51;
            this.crtUplateSati.Text = "cartesianChart1";
            // 
            // cbListUplate
            // 
            this.cbListUplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbListUplate.CheckOnClick = true;
            this.cbListUplate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.cbListUplate.FormattingEnabled = true;
            this.cbListUplate.Location = new System.Drawing.Point(4, 4);
            this.cbListUplate.Margin = new System.Windows.Forms.Padding(2);
            this.cbListUplate.Name = "cbListUplate";
            this.cbListUplate.Size = new System.Drawing.Size(70, 289);
            this.cbListUplate.TabIndex = 57;
            this.cbListUplate.UseCompatibleTextRendering = true;
            this.cbListUplate.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CbListUplate_ItemCheck);
            // 
            // btnUplateRefresh
            // 
            this.btnUplateRefresh.Image = global::Damax.Properties.Resources.refress_blue;
            this.btnUplateRefresh.Location = new System.Drawing.Point(266, 3);
            this.btnUplateRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.btnUplateRefresh.Name = "btnUplateRefresh";
            this.btnUplateRefresh.Size = new System.Drawing.Size(14, 14);
            this.btnUplateRefresh.TabIndex = 61;
            this.btnUplateRefresh.UseVisualStyleBackColor = true;
            this.btnUplateRefresh.Click += new System.EventHandler(this.BtnUplateRefresh_Click);
            // 
            // dtUplateDatumDo
            // 
            this.dtUplateDatumDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtUplateDatumDo.Location = new System.Drawing.Point(169, 4);
            this.dtUplateDatumDo.Margin = new System.Windows.Forms.Padding(2);
            this.dtUplateDatumDo.Name = "dtUplateDatumDo";
            this.dtUplateDatumDo.Size = new System.Drawing.Size(95, 20);
            this.dtUplateDatumDo.TabIndex = 60;
            // 
            // dtUplateDatumOd
            // 
            this.dtUplateDatumOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtUplateDatumOd.Location = new System.Drawing.Point(76, 4);
            this.dtUplateDatumOd.Margin = new System.Windows.Forms.Padding(2);
            this.dtUplateDatumOd.Name = "dtUplateDatumOd";
            this.dtUplateDatumOd.Size = new System.Drawing.Size(93, 20);
            this.dtUplateDatumOd.TabIndex = 59;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpProdaja);
            this.tabControl1.Controls.Add(this.tpUplate);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl1.Location = new System.Drawing.Point(2, 2);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1025, 218);
            this.tabControl1.TabIndex = 55;
            // 
            // tpProdaja
            // 
            this.tpProdaja.BackColor = System.Drawing.Color.White;
            this.tpProdaja.Controls.Add(this.lblValuta);
            this.tpProdaja.Controls.Add(this.btnRefreshChart);
            this.tpProdaja.Controls.Add(this.panel9);
            this.tpProdaja.Controls.Add(this.dtProdajaDo);
            this.tpProdaja.Controls.Add(this.dtProdajaOd);
            this.tpProdaja.Controls.Add(this.cbListProdaja);
            this.tpProdaja.Location = new System.Drawing.Point(4, 22);
            this.tpProdaja.Margin = new System.Windows.Forms.Padding(2);
            this.tpProdaja.Name = "tpProdaja";
            this.tpProdaja.Padding = new System.Windows.Forms.Padding(2);
            this.tpProdaja.Size = new System.Drawing.Size(1017, 192);
            this.tpProdaja.TabIndex = 0;
            this.tpProdaja.Text = "Naplata po satima";
            // 
            // lblValuta
            // 
            this.lblValuta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblValuta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblValuta.Location = new System.Drawing.Point(874, 5);
            this.lblValuta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblValuta.Name = "lblValuta";
            this.lblValuta.Size = new System.Drawing.Size(143, 10);
            this.lblValuta.TabIndex = 64;
            this.lblValuta.Text = "Vrijednosti su prikazane u KM";
            this.lblValuta.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tpUplate
            // 
            this.tpUplate.BackColor = System.Drawing.Color.White;
            this.tpUplate.Controls.Add(this.lblValutaUplate);
            this.tpUplate.Controls.Add(this.cbListUplate);
            this.tpUplate.Controls.Add(this.btnUplateRefresh);
            this.tpUplate.Controls.Add(this.panel10);
            this.tpUplate.Controls.Add(this.dtUplateDatumOd);
            this.tpUplate.Controls.Add(this.dtUplateDatumDo);
            this.tpUplate.Location = new System.Drawing.Point(4, 22);
            this.tpUplate.Margin = new System.Windows.Forms.Padding(2);
            this.tpUplate.Name = "tpUplate";
            this.tpUplate.Padding = new System.Windows.Forms.Padding(2);
            this.tpUplate.Size = new System.Drawing.Size(1017, 192);
            this.tpUplate.TabIndex = 1;
            this.tpUplate.Text = "Uplate po satima";
            // 
            // lblValutaUplate
            // 
            this.lblValutaUplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValutaUplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblValutaUplate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblValutaUplate.Location = new System.Drawing.Point(877, 9);
            this.lblValutaUplate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblValutaUplate.Name = "lblValutaUplate";
            this.lblValutaUplate.Size = new System.Drawing.Size(143, 10);
            this.lblValutaUplate.TabIndex = 65;
            this.lblValutaUplate.Text = "Vrijednosti su prikazane u KM";
            this.lblValutaUplate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbListMagacini
            // 
            this.cbListMagacini.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbListMagacini.BackColor = System.Drawing.Color.White;
            this.cbListMagacini.CheckOnClick = true;
            this.cbListMagacini.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbListMagacini.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.cbListMagacini.FormattingEnabled = true;
            this.cbListMagacini.Location = new System.Drawing.Point(2, 144);
            this.cbListMagacini.Margin = new System.Windows.Forms.Padding(2);
            this.cbListMagacini.Name = "cbListMagacini";
            this.cbListMagacini.Size = new System.Drawing.Size(103, 319);
            this.cbListMagacini.TabIndex = 57;
            this.cbListMagacini.ThreeDCheckBoxes = true;
            this.cbListMagacini.UseCompatibleTextRendering = true;
            this.cbListMagacini.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CbListMagacini_ItemCheck);
            // 
            // dgStanjeMagacina
            // 
            this.dgStanjeMagacina.AllowUserToAddRows = false;
            this.dgStanjeMagacina.AllowUserToDeleteRows = false;
            this.dgStanjeMagacina.AllowUserToOrderColumns = true;
            this.dgStanjeMagacina.AllowUserToResizeRows = false;
            this.dgStanjeMagacina.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgStanjeMagacina.BackgroundColor = System.Drawing.Color.White;
            this.dgStanjeMagacina.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgStanjeMagacina.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgStanjeMagacina.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgStanjeMagacina.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgStanjeMagacina.ColumnHeadersHeight = 40;
            this.dgStanjeMagacina.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgStanjeMagacina.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgStanjeMagacina.EnableHeadersVisualStyles = false;
            this.dgStanjeMagacina.GridColor = System.Drawing.Color.LightGray;
            this.dgStanjeMagacina.Location = new System.Drawing.Point(106, 114);
            this.dgStanjeMagacina.Margin = new System.Windows.Forms.Padding(2);
            this.dgStanjeMagacina.MultiSelect = false;
            this.dgStanjeMagacina.Name = "dgStanjeMagacina";
            this.dgStanjeMagacina.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgStanjeMagacina.RowHeadersVisible = false;
            this.dgStanjeMagacina.RowHeadersWidth = 62;
            this.dgStanjeMagacina.RowTemplate.Height = 25;
            this.dgStanjeMagacina.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStanjeMagacina.Size = new System.Drawing.Size(843, 349);
            this.dgStanjeMagacina.TabIndex = 59;
            this.dgStanjeMagacina.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgStanjeMagacina_CellDoubleClick);
            this.dgStanjeMagacina.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgStanjeMagacina_CellEndEdit);
            this.dgStanjeMagacina.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DataGridView1_EditingControlShowing);
            // 
            // btnPrenosArtikala
            // 
            this.btnPrenosArtikala.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(199)))), ((int)(((byte)(8)))));
            this.btnPrenosArtikala.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrenosArtikala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrenosArtikala.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrenosArtikala.ForeColor = System.Drawing.Color.Black;
            this.btnPrenosArtikala.Location = new System.Drawing.Point(4, 114);
            this.btnPrenosArtikala.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrenosArtikala.Name = "btnPrenosArtikala";
            this.btnPrenosArtikala.Size = new System.Drawing.Size(101, 26);
            this.btnPrenosArtikala.TabIndex = 60;
            this.btnPrenosArtikala.Text = "Napravi prenosnicu";
            this.btnPrenosArtikala.UseVisualStyleBackColor = false;
            this.btnPrenosArtikala.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnPregledStanjaMagacina
            // 
            this.btnPregledStanjaMagacina.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPregledStanjaMagacina.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPregledStanjaMagacina.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledStanjaMagacina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPregledStanjaMagacina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPregledStanjaMagacina.Location = new System.Drawing.Point(715, 161);
            this.btnPregledStanjaMagacina.Margin = new System.Windows.Forms.Padding(2);
            this.btnPregledStanjaMagacina.Name = "btnPregledStanjaMagacina";
            this.btnPregledStanjaMagacina.Size = new System.Drawing.Size(67, 27);
            this.btnPregledStanjaMagacina.TabIndex = 61;
            this.btnPregledStanjaMagacina.Text = "Pregled stanja magacina";
            this.btnPregledStanjaMagacina.UseVisualStyleBackColor = true;
            this.btnPregledStanjaMagacina.Visible = false;
            this.btnPregledStanjaMagacina.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "Promjena stanja artikla";
            this.notifyIcon1.Visible = true;
            // 
            // btnProsjekUplata
            // 
            this.btnProsjekUplata.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProsjekUplata.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnProsjekUplata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProsjekUplata.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnProsjekUplata.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnProsjekUplata.Location = new System.Drawing.Point(4, 11);
            this.btnProsjekUplata.Margin = new System.Windows.Forms.Padding(2);
            this.btnProsjekUplata.Name = "btnProsjekUplata";
            this.btnProsjekUplata.Size = new System.Drawing.Size(67, 27);
            this.btnProsjekUplata.TabIndex = 63;
            this.btnProsjekUplata.Text = "Prosjek uplata po satima";
            this.btnProsjekUplata.UseVisualStyleBackColor = true;
            this.btnProsjekUplata.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnBrojizdatihKarticaPoSatima
            // 
            this.btnBrojizdatihKarticaPoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrojizdatihKarticaPoSatima.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojizdatihKarticaPoSatima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrojizdatihKarticaPoSatima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBrojizdatihKarticaPoSatima.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojizdatihKarticaPoSatima.Location = new System.Drawing.Point(4, 49);
            this.btnBrojizdatihKarticaPoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrojizdatihKarticaPoSatima.Name = "btnBrojizdatihKarticaPoSatima";
            this.btnBrojizdatihKarticaPoSatima.Size = new System.Drawing.Size(67, 27);
            this.btnBrojizdatihKarticaPoSatima.TabIndex = 64;
            this.btnBrojizdatihKarticaPoSatima.Text = "Broj izdatih kartica po satima";
            this.btnBrojizdatihKarticaPoSatima.UseVisualStyleBackColor = true;
            this.btnBrojizdatihKarticaPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnBrojAktivPoSatima
            // 
            this.btnBrojAktivPoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrojAktivPoSatima.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojAktivPoSatima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrojAktivPoSatima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBrojAktivPoSatima.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojAktivPoSatima.Location = new System.Drawing.Point(4, 88);
            this.btnBrojAktivPoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrojAktivPoSatima.Name = "btnBrojAktivPoSatima";
            this.btnBrojAktivPoSatima.Size = new System.Drawing.Size(67, 27);
            this.btnBrojAktivPoSatima.TabIndex = 65;
            this.btnBrojAktivPoSatima.Text = "Broj aktivnih kartica po satima";
            this.btnBrojAktivPoSatima.UseVisualStyleBackColor = true;
            this.btnBrojAktivPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnBrojUplataPoSatima
            // 
            this.btnBrojUplataPoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrojUplataPoSatima.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojUplataPoSatima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrojUplataPoSatima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBrojUplataPoSatima.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojUplataPoSatima.Location = new System.Drawing.Point(4, 126);
            this.btnBrojUplataPoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrojUplataPoSatima.Name = "btnBrojUplataPoSatima";
            this.btnBrojUplataPoSatima.Size = new System.Drawing.Size(67, 27);
            this.btnBrojUplataPoSatima.TabIndex = 66;
            this.btnBrojUplataPoSatima.Text = "Broj uplata po satima i blagajni";
            this.btnBrojUplataPoSatima.UseVisualStyleBackColor = true;
            this.btnBrojUplataPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnBrojNarudzbiPoSatima
            // 
            this.btnBrojNarudzbiPoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrojNarudzbiPoSatima.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojNarudzbiPoSatima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrojNarudzbiPoSatima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBrojNarudzbiPoSatima.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnBrojNarudzbiPoSatima.Location = new System.Drawing.Point(4, 164);
            this.btnBrojNarudzbiPoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrojNarudzbiPoSatima.Name = "btnBrojNarudzbiPoSatima";
            this.btnBrojNarudzbiPoSatima.Size = new System.Drawing.Size(67, 27);
            this.btnBrojNarudzbiPoSatima.TabIndex = 67;
            this.btnBrojNarudzbiPoSatima.Text = "Broj narudžbi po satima i prodajnom mjestu";
            this.btnBrojNarudzbiPoSatima.UseVisualStyleBackColor = true;
            this.btnBrojNarudzbiPoSatima.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // btnProdajaArtikala
            // 
            this.btnProdajaArtikala.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProdajaArtikala.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnProdajaArtikala.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdajaArtikala.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnProdajaArtikala.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnProdajaArtikala.Location = new System.Drawing.Point(721, 127);
            this.btnProdajaArtikala.Margin = new System.Windows.Forms.Padding(2);
            this.btnProdajaArtikala.Name = "btnProdajaArtikala";
            this.btnProdajaArtikala.Size = new System.Drawing.Size(61, 32);
            this.btnProdajaArtikala.TabIndex = 70;
            this.btnProdajaArtikala.Text = "Pregled potrošnje artikala";
            this.btnProdajaArtikala.UseVisualStyleBackColor = true;
            this.btnProdajaArtikala.Visible = false;
            // 
            // btnPregledNaplatepoSatima
            // 
            this.btnPregledNaplatepoSatima.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPregledNaplatepoSatima.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPregledNaplatepoSatima.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPregledNaplatepoSatima.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPregledNaplatepoSatima.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPregledNaplatepoSatima.Location = new System.Drawing.Point(601, 276);
            this.btnPregledNaplatepoSatima.Margin = new System.Windows.Forms.Padding(2);
            this.btnPregledNaplatepoSatima.Name = "btnPregledNaplatepoSatima";
            this.btnPregledNaplatepoSatima.Size = new System.Drawing.Size(67, 27);
            this.btnPregledNaplatepoSatima.TabIndex = 69;
            this.btnPregledNaplatepoSatima.Text = "Pregled naplate po satima";
            this.btnPregledNaplatepoSatima.UseVisualStyleBackColor = true;
            this.btnPregledNaplatepoSatima.Visible = false;
            // 
            // btnDeleteSearch
            // 
            this.btnDeleteSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnDeleteSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteSearch.Image = global::Damax.Properties.Resources.cancel1;
            this.btnDeleteSearch.Location = new System.Drawing.Point(215, 140);
            this.btnDeleteSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteSearch.Name = "btnDeleteSearch";
            this.btnDeleteSearch.Size = new System.Drawing.Size(13, 13);
            this.btnDeleteSearch.TabIndex = 80;
            this.btnDeleteSearch.Tag = "Obriši ";
            this.btnDeleteSearch.UseVisualStyleBackColor = true;
            this.btnDeleteSearch.Click += new System.EventHandler(this.btnDeleteSearch_Click);
            // 
            // tbPretraga
            // 
            this.tbPretraga.Location = new System.Drawing.Point(155, 137);
            this.tbPretraga.Margin = new System.Windows.Forms.Padding(2);
            this.tbPretraga.Name = "tbPretraga";
            this.tbPretraga.Size = new System.Drawing.Size(54, 20);
            this.tbPretraga.TabIndex = 78;
            this.tbPretraga.TextChanged += new System.EventHandler(this.tbPretraga_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Location = new System.Drawing.Point(811, 26);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(97, 43);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Trenutni podaci";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.lblUkupnoIsplaceno);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Location = new System.Drawing.Point(499, 17);
            this.panel11.Margin = new System.Windows.Forms.Padding(2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(88, 85);
            this.panel11.TabIndex = 68;
            // 
            // lblUkupnoIsplaceno
            // 
            this.lblUkupnoIsplaceno.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblUkupnoIsplaceno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblUkupnoIsplaceno.Location = new System.Drawing.Point(-1, 34);
            this.lblUkupnoIsplaceno.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUkupnoIsplaceno.Name = "lblUkupnoIsplaceno";
            this.lblUkupnoIsplaceno.Size = new System.Drawing.Size(88, 48);
            this.lblUkupnoIsplaceno.TabIndex = 1;
            this.lblUkupnoIsplaceno.Text = "0.00 KM";
            this.lblUkupnoIsplaceno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(-1, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 34);
            this.label12.TabIndex = 0;
            this.label12.Text = "Ukupno isplaćeno";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnSaldoSmjene);
            this.groupBox2.Controls.Add(this.btnProsjekUplata);
            this.groupBox2.Controls.Add(this.btnBrojizdatihKarticaPoSatima);
            this.groupBox2.Controls.Add(this.btnBrojAktivPoSatima);
            this.groupBox2.Controls.Add(this.btnBrojUplataPoSatima);
            this.groupBox2.Controls.Add(this.btnBrojNarudzbiPoSatima);
            this.groupBox2.Location = new System.Drawing.Point(953, 114);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(74, 349);
            this.groupBox2.TabIndex = 82;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Izvještaji";
            // 
            // btnSaldoSmjene
            // 
            this.btnSaldoSmjene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaldoSmjene.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnSaldoSmjene.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSaldoSmjene.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSaldoSmjene.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnSaldoSmjene.Location = new System.Drawing.Point(4, 203);
            this.btnSaldoSmjene.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaldoSmjene.Name = "btnSaldoSmjene";
            this.btnSaldoSmjene.Size = new System.Drawing.Size(67, 27);
            this.btnSaldoSmjene.TabIndex = 68;
            this.btnSaldoSmjene.Text = "Saldo smjene";
            this.btnSaldoSmjene.UseVisualStyleBackColor = true;
            this.btnSaldoSmjene.Click += new System.EventHandler(this.BtnBlagajne_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer3.Location = new System.Drawing.Point(75, 24);
            this.splitContainer3.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel11);
            this.splitContainer3.Panel1.Controls.Add(this.panel6);
            this.splitContainer3.Panel1.Controls.Add(this.panel5);
            this.splitContainer3.Panel1.Controls.Add(this.panel8);
            this.splitContainer3.Panel1.Controls.Add(this.panel2);
            this.splitContainer3.Panel1.Controls.Add(this.panel7);
            this.splitContainer3.Panel1.Controls.Add(this.btnPrenosArtikala);
            this.splitContainer3.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer3.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer3.Panel1.Controls.Add(this.btnDeleteSearch);
            this.splitContainer3.Panel1.Controls.Add(this.cbListMagacini);
            this.splitContainer3.Panel1.Controls.Add(this.tbPretraga);
            this.splitContainer3.Panel1.Controls.Add(this.dgStanjeMagacina);
            this.splitContainer3.Panel1.Controls.Add(this.btnPregledStanjaMagacina);
            this.splitContainer3.Panel1.Controls.Add(this.btnPregledNaplatepoSatima);
            this.splitContainer3.Panel1.Controls.Add(this.btnProdajaArtikala);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer3.Size = new System.Drawing.Size(1031, 701);
            this.splitContainer3.SplitterDistance = 477;
            this.splitContainer3.SplitterWidth = 3;
            this.splitContainer3.TabIndex = 83;
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpDogadjaj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Location = new System.Drawing.Point(934, 3);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Margin = new System.Windows.Forms.Padding(2);
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(176, 14);
            this.lkpDogadjaj.TabIndex = 62;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lookupComboBox1
            // 
            this.lookupComboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.lookupComboBox1.ComboSelectedIndex = -1;
            this.lookupComboBox1.DataSource = null;
            this.lookupComboBox1.DeleteButtonEnable = true;
            this.lookupComboBox1.DisplayMember = "";
            this.lookupComboBox1.Location = new System.Drawing.Point(1, 1);
            this.lookupComboBox1.LookupMember = null;
            this.lookupComboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.lookupComboBox1.Name = "lookupComboBox1";
            this.lookupComboBox1.RefreashButtonEnable = true;
            this.lookupComboBox1.SelectedLookupValue = null;
            this.lookupComboBox1.Size = new System.Drawing.Size(0, 14);
            this.lookupComboBox1.TabIndex = 48;
            this.lookupComboBox1.ValueMember = "";
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1118, 745);
            this.Controls.Add(this.splitContainer3);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lookupComboBox1);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.pnlSlide);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.msGlavniMeni);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msGlavniMeni;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Početna";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Click += new System.EventHandler(this.BtnBlagajne_Click);
            this.msGlavniMeni.ResumeLayout(false);
            this.msGlavniMeni.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlSlide.ResumeLayout(false);
            this.pnlSlide.PerformLayout();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.artikalBindingSource)).EndInit();
            this.panel10.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpProdaja.ResumeLayout(false);
            this.tpUplate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgStanjeMagacina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.damaxDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel11.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel1.PerformLayout();
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.MenuStrip msGlavniMeni;
        private System.Windows.Forms.ToolStripMenuItem sifarniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kategorijeArtikalaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artikliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blagajneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transakcijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prodajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prodajnaMjestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem narudžbeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvještajiToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ToolStripMenuItem administracijaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem korisniciToolStripMenuItem;
        private System.Windows.Forms.Label lblLogovaniKorisnik;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Timer timerMenu;
        private System.Windows.Forms.Panel pnlSlide;
        private System.Windows.Forms.Button btnMagacini;
        private System.Windows.Forms.Button btnProdajnaMjesta;
        private System.Windows.Forms.Button btnBlagajne;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private LookupComboBox lookupComboBox1;
        private System.Windows.Forms.ToolStripMenuItem dogadjajiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem magaciniToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblBrAktivnihKartica;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblBrIzdatihKartica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblUkupnaPotrosnja;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblPotrosnjaPoKartici;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblUkupnoUplaceno;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.DateTimePicker dtProdajaDo;
        private System.Windows.Forms.DateTimePicker dtProdajaOd;
        private System.Windows.Forms.Button btnRefreshChart;
        private System.Windows.Forms.CheckedListBox cbListProdaja;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.CheckedListBox cbListUplate;
        private System.Windows.Forms.Button btnUplateRefresh;
        private System.Windows.Forms.DateTimePicker dtUplateDatumDo;
        private System.Windows.Forms.DateTimePicker dtUplateDatumOd;
        private System.Windows.Forms.BindingSource artikalBindingSource;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpProdaja;
        private System.Windows.Forms.TabPage tpUplate;
        private System.Windows.Forms.CheckedListBox cbListMagacini;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private LiveCharts.WinForms.PieChart pieChart1;
        private LiveCharts.WinForms.CartesianChart crtArtikli;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dgStanjeMagacina;
        private System.Windows.Forms.BindingSource damaxDataSetBindingSource;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button btnPrenosArtikala;
        private System.Windows.Forms.Button btnPregledStanjaMagacina;
        private LiveCharts.WinForms.CartesianChart crtArtikliMagacini;
        private System.Windows.Forms.Button btnnDogadjaji;
        private System.Windows.Forms.Button btnKorisnici;
        private System.Windows.Forms.Button btnZakljucajDogadjaj;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem tsmPregledStanjaMagacina;
        private System.Windows.Forms.ToolStripMenuItem tsmPregledNaplatePoProdajnomMjestu;
        private System.Windows.Forms.ToolStripMenuItem tsmPregledPotrošnjeArtikalaPoProdajnomMjestu;
        private LookupComboBox lkpDogadjaj;
        private LiveCharts.WinForms.CartesianChart crtProdajaPoSatima;
        private LiveCharts.WinForms.CartesianChart crtUplateSati;
        private System.Windows.Forms.Label lblValuta;
        private System.Windows.Forms.Label lblValutaUplate;
        private System.Windows.Forms.ToolStripMenuItem tsmBrojUplataPoSatimaPoBlagajni;
        private System.Windows.Forms.ToolStripMenuItem tsmProsjekUplataPoKarticama;
        private System.Windows.Forms.ToolStripMenuItem tsmProsjecnaPotrosnjaPoSatima;
        private System.Windows.Forms.ToolStripMenuItem tsmBrojNarudzbiPoProdajnomMjestu;
        private System.Windows.Forms.ToolStripMenuItem tsmBrojIzdatihKarticaPoSatima;
        private System.Windows.Forms.ToolStripMenuItem tsmBrojAktivnihKarticaPoSatima;
        private System.Windows.Forms.Button btnProsjekUplata;
        private System.Windows.Forms.Button btnBrojizdatihKarticaPoSatima;
        private System.Windows.Forms.Button btnBrojAktivPoSatima;
        private System.Windows.Forms.Button btnBrojUplataPoSatima;
        private System.Windows.Forms.Button btnBrojNarudzbiPoSatima;
        private System.Windows.Forms.ToolStripMenuItem tsmPregledPrenosaArtikala;
        private System.Windows.Forms.ToolStripMenuItem karticeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmPregledPrenosnica;
        private System.Windows.Forms.ToolStripMenuItem prenosArtikalaToolStripMenuItem;
        private System.Windows.Forms.Button btnProdajaArtikala;
        private System.Windows.Forms.Button btnPregledNaplatepoSatima;
        private System.Windows.Forms.Button btnDeleteSearch;
        private System.Windows.Forms.TextBox tbPretraga;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnPrenosnice;
        private System.Windows.Forms.ToolStripMenuItem tsmSaldoSmjene;
        private System.Windows.Forms.Button btnSaldoSmjene;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label lblUkupnoIsplaceno;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolStripMenuItem analitikaKarticaZaPlaćanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.SplitContainer splitContainer3;
    }
}

