﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormProdajnoMjesto : FormTemplate<ProdajnoMjesto>
    {
        public FormProdajnoMjesto()
        {
            InitializeComponent();
            DaoObject=new ProdajnoMjestoDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            SetFormControlsSettings();
            lkpDogadjaj.RefreshLookup += lookup_RefreashLookup;
        }


        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = Resources.Year_Date_Format_yyyy;
            dtpGodina.ShowUpDown = true;

            tbUplata.Enabled = false;
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbNaziv.ReadOnly = !isEnable;
            tbLokacija.ReadOnly = !isEnable;
            tbUplata.ReadOnly = !isEnable;
            tbUsername.ReadOnly = !isEnable;
            tbPassword.ReadOnly = !isEnable;
            lkpDogadjaj.Enabled = false;
                dtpGodina.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;
            tbNaziv.Text = SelectedObject.Naziv;
            tbLokacija.Text = SelectedObject.Lokacija;
            tbUplata.Text = SelectedObject.UkupnoNaplata?.ToString() ?? "0";
            tbUsername.Text = SelectedObject.Username;
            tbPassword.Text = SelectedObject.Password;


        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                int br;
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?)null;
                SelectedObject.Naziv = tbNaziv.Text;
                SelectedObject.Godina = (short?)dtpGodina.Value.Year;
                SelectedObject.Lokacija = tbLokacija.Text;
                SelectedObject.UkupnoNaplata = int.TryParse(tbUplata.Text, out br) ? br : 0;
                SelectedObject.Username = tbUsername.Text;
                SelectedObject.Password = tbPassword.Text;
            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbNaziv.Text = "";
            tbLokacija.Text = "";
            tbUplata.Text = "";
            tbUsername.Text = "";
            tbPassword.Text = "";
        }


        public override bool ValidateControls()
        {

            if (lkpDogadjaj.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbUsername.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbUsername, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbUsername.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbUsername, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbPassword.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbPassword, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();
            {
                FormNarudzba form = new FormNarudzba();
                form.Filters.Add(new TemplateFilter(Narudzba.F_Godina, SelectedObject.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Narudzba.F_DogadjajId, SelectedObject.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Narudzba.F_ProdajnoMjestoId, SelectedObject.ProdajnoMjestoId.ToString(), "="));
                IsInNextMode = true;
                NextForms.Add(form);
            }

            {
                FormMagacin form = new FormMagacin();
                form.Filters.Add(new TemplateFilter(Magacin.F_Godina, SelectedObject.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Magacin.F_DogadjajId, SelectedObject.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Magacin.F_ProdajnoMjestoId, SelectedObject.ProdajnoMjestoId.ToString(), "="));
                IsInNextMode = true;
                NextForms.Add(form);
            }
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[ProdajnoMjesto.F_DogadjajId].Visible = false;
            grid.Columns[ProdajnoMjesto.F_DogadjajId].HeaderText = "Dogadjaj";
            grid.Columns[ProdajnoMjesto.F_Godina].Width = 70;
            grid.Columns[ProdajnoMjesto.F_Naziv].Width = 300;
            grid.Columns[ProdajnoMjesto.F_UkupnoNaplata].HeaderText = "Promet";
            grid.Columns[ProdajnoMjesto.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[ProdajnoMjesto.F_UkupnoNaplata].Width = 80;
            grid.Columns[ProdajnoMjesto.F_ProdajnoMjestoId].Visible = false;


        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1] });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }


        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            var source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }

    }
}
