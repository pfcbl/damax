﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormMagacinArtikal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lookupComboBox1 = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lkpMagacin = new Damax.Template.LookupComboBox();
            this.lkpArtikal = new Damax.Template.LookupComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPocetnoStanje = new System.Windows.Forms.Label();
            this.tbKolicina = new System.Windows.Forms.NumericUpDown();
            this.tbTrenutnoStanje = new System.Windows.Forms.NumericUpDown();
            this.lblTrenutnoStanje = new System.Windows.Forms.Label();
            this.btnDeleteAll = new System.Windows.Forms.Button();
            this.ttObrisiSve = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tbKolicina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTrenutnoStanje)).BeginInit();
            this.SuspendLayout();
            // 
            // lookupComboBox1
            // 
            this.lookupComboBox1.BackColor = System.Drawing.Color.White;
            this.lookupComboBox1.ComboSelectedIndex = -1;
            this.lookupComboBox1.DataSource = null;
            this.lookupComboBox1.DeleteButtonEnable = true;
            this.lookupComboBox1.DisplayMember = "";
            this.lookupComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lookupComboBox1.Location = new System.Drawing.Point(562, 66);
            this.lookupComboBox1.LookupMember = null;
            this.lookupComboBox1.Name = "lookupComboBox1";
            this.lookupComboBox1.RefreashButtonEnable = true;
            this.lookupComboBox1.SelectedLookupValue = null;
            this.lookupComboBox1.Size = new System.Drawing.Size(306, 21);
            this.lookupComboBox1.TabIndex = 67;
            this.lookupComboBox1.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(488, 70);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 69;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 70);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 68;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 66);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 66;
            // 
            // lkpMagacin
            // 
            this.lkpMagacin.BackColor = System.Drawing.Color.White;
            this.lkpMagacin.ComboSelectedIndex = -1;
            this.lkpMagacin.DataSource = null;
            this.lkpMagacin.DeleteButtonEnable = true;
            this.lkpMagacin.DisplayMember = "";
            this.lkpMagacin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpMagacin.Location = new System.Drawing.Point(121, 103);
            this.lkpMagacin.LookupMember = null;
            this.lkpMagacin.Name = "lkpMagacin";
            this.lkpMagacin.RefreashButtonEnable = true;
            this.lkpMagacin.SelectedLookupValue = null;
            this.lkpMagacin.Size = new System.Drawing.Size(306, 21);
            this.lkpMagacin.TabIndex = 70;
            this.lkpMagacin.ValueMember = "";
            // 
            // lkpArtikal
            // 
            this.lkpArtikal.BackColor = System.Drawing.Color.White;
            this.lkpArtikal.ComboSelectedIndex = -1;
            this.lkpArtikal.DataSource = null;
            this.lkpArtikal.DeleteButtonEnable = true;
            this.lkpArtikal.DisplayMember = "";
            this.lkpArtikal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpArtikal.Location = new System.Drawing.Point(121, 143);
            this.lkpArtikal.LookupMember = null;
            this.lkpArtikal.Name = "lkpArtikal";
            this.lkpArtikal.RefreashButtonEnable = true;
            this.lkpArtikal.SelectedLookupValue = null;
            this.lkpArtikal.Size = new System.Drawing.Size(306, 21);
            this.lkpArtikal.TabIndex = 71;
            this.lkpArtikal.ValueMember = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 72;
            this.label1.Text = "Magacin";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(12, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 73;
            this.label2.Text = "Artikal";
            // 
            // lblPocetnoStanje
            // 
            this.lblPocetnoStanje.AutoSize = true;
            this.lblPocetnoStanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPocetnoStanje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblPocetnoStanje.Location = new System.Drawing.Point(12, 186);
            this.lblPocetnoStanje.Name = "lblPocetnoStanje";
            this.lblPocetnoStanje.Size = new System.Drawing.Size(78, 13);
            this.lblPocetnoStanje.TabIndex = 75;
            this.lblPocetnoStanje.Text = "Pocetno stanje";
            // 
            // tbKolicina
            // 
            this.tbKolicina.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbKolicina.DecimalPlaces = 2;
            this.tbKolicina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbKolicina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbKolicina.Location = new System.Drawing.Point(121, 183);
            this.tbKolicina.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.tbKolicina.Name = "tbKolicina";
            this.tbKolicina.Size = new System.Drawing.Size(120, 20);
            this.tbKolicina.TabIndex = 76;
            this.tbKolicina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbKolicina.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.tbKolicina.ValueChanged += new System.EventHandler(this.tbKolicina_ValueChanged);
            // 
            // tbTrenutnoStanje
            // 
            this.tbTrenutnoStanje.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbTrenutnoStanje.DecimalPlaces = 2;
            this.tbTrenutnoStanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbTrenutnoStanje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbTrenutnoStanje.Location = new System.Drawing.Point(121, 221);
            this.tbTrenutnoStanje.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.tbTrenutnoStanje.Name = "tbTrenutnoStanje";
            this.tbTrenutnoStanje.Size = new System.Drawing.Size(120, 20);
            this.tbTrenutnoStanje.TabIndex = 77;
            this.tbTrenutnoStanje.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbTrenutnoStanje.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // lblTrenutnoStanje
            // 
            this.lblTrenutnoStanje.AutoSize = true;
            this.lblTrenutnoStanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTrenutnoStanje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblTrenutnoStanje.Location = new System.Drawing.Point(12, 223);
            this.lblTrenutnoStanje.Name = "lblTrenutnoStanje";
            this.lblTrenutnoStanje.Size = new System.Drawing.Size(81, 13);
            this.lblTrenutnoStanje.TabIndex = 78;
            this.lblTrenutnoStanje.Text = "Trenutno stanje";
            // 
            // btnDeleteAll
            // 
            this.btnDeleteAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnDeleteAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDeleteAll.FlatAppearance.BorderSize = 0;
            this.btnDeleteAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAll.ForeColor = System.Drawing.Color.White;
            this.btnDeleteAll.Image = global::Damax.Properties.Resources.delete_all__1_;
            this.btnDeleteAll.Location = new System.Drawing.Point(435, 5);
            this.btnDeleteAll.Name = "btnDeleteAll";
            this.btnDeleteAll.Size = new System.Drawing.Size(40, 40);
            this.btnDeleteAll.TabIndex = 79;
            this.btnDeleteAll.UseVisualStyleBackColor = false;
            this.btnDeleteAll.Click += new System.EventHandler(this.btnDeleteAll_Click);
            this.btnDeleteAll.MouseHover += new System.EventHandler(this.btnDeleteAll_MouseHover);
            // 
            // FormMagacinArtikal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.btnDeleteAll);
            this.Controls.Add(this.lblTrenutnoStanje);
            this.Controls.Add(this.tbTrenutnoStanje);
            this.Controls.Add(this.tbKolicina);
            this.Controls.Add(this.lblPocetnoStanje);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpArtikal);
            this.Controls.Add(this.lkpMagacin);
            this.Controls.Add(this.lookupComboBox1);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormMagacinArtikal";
            this.Text = "Stanje magacina";
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lookupComboBox1, 0);
            this.Controls.SetChildIndex(this.lkpMagacin, 0);
            this.Controls.SetChildIndex(this.lkpArtikal, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lblPocetnoStanje, 0);
            this.Controls.SetChildIndex(this.tbKolicina, 0);
            this.Controls.SetChildIndex(this.tbTrenutnoStanje, 0);
            this.Controls.SetChildIndex(this.lblTrenutnoStanje, 0);
            this.Controls.SetChildIndex(this.btnDeleteAll, 0);
            ((System.ComponentModel.ISupportInitialize)(this.tbKolicina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTrenutnoStanje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LookupComboBox lookupComboBox1;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private LookupComboBox lkpMagacin;
        private LookupComboBox lkpArtikal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPocetnoStanje;
        private System.Windows.Forms.NumericUpDown tbKolicina;
        private System.Windows.Forms.NumericUpDown tbTrenutnoStanje;
        private System.Windows.Forms.Label lblTrenutnoStanje;
        private System.Windows.Forms.Button btnDeleteAll;
        private System.Windows.Forms.ToolTip ttObrisiSve;
    }
}