﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormMagacin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblProdajnoMjesto = new System.Windows.Forms.Label();
            this.lblNadredjeni = new System.Windows.Forms.Label();
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.lkpMagacinNadredjeni = new Damax.Template.LookupComboBox();
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.tbLokacija = new System.Windows.Forms.TextBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.cbCentralni = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrebaciArtikle = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.ttPrebacivanje = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // lblProdajnoMjesto
            // 
            this.lblProdajnoMjesto.AutoSize = true;
            this.lblProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblProdajnoMjesto.Location = new System.Drawing.Point(12, 213);
            this.lblProdajnoMjesto.Name = "lblProdajnoMjesto";
            this.lblProdajnoMjesto.Size = new System.Drawing.Size(82, 13);
            this.lblProdajnoMjesto.TabIndex = 69;
            this.lblProdajnoMjesto.Text = "Prodajno mjesto";
            // 
            // lblNadredjeni
            // 
            this.lblNadredjeni.AutoSize = true;
            this.lblNadredjeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNadredjeni.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNadredjeni.Location = new System.Drawing.Point(12, 176);
            this.lblNadredjeni.Name = "lblNadredjeni";
            this.lblNadredjeni.Size = new System.Drawing.Size(57, 13);
            this.lblNadredjeni.TabIndex = 68;
            this.lblNadredjeni.Text = "Nadređeni";
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.White;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DeleteButtonEnable = true;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(121, 209);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.RefreashButtonEnable = true;
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(306, 21);
            this.lkpProdajnoMjesto.TabIndex = 67;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // lkpMagacinNadredjeni
            // 
            this.lkpMagacinNadredjeni.BackColor = System.Drawing.Color.White;
            this.lkpMagacinNadredjeni.ComboSelectedIndex = -1;
            this.lkpMagacinNadredjeni.DataSource = null;
            this.lkpMagacinNadredjeni.DeleteButtonEnable = true;
            this.lkpMagacinNadredjeni.DisplayMember = "";
            this.lkpMagacinNadredjeni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpMagacinNadredjeni.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpMagacinNadredjeni.Location = new System.Drawing.Point(121, 171);
            this.lkpMagacinNadredjeni.LookupMember = null;
            this.lkpMagacinNadredjeni.Name = "lkpMagacinNadredjeni";
            this.lkpMagacinNadredjeni.RefreashButtonEnable = true;
            this.lkpMagacinNadredjeni.SelectedLookupValue = null;
            this.lkpMagacinNadredjeni.Size = new System.Drawing.Size(306, 21);
            this.lkpMagacinNadredjeni.TabIndex = 66;
            this.lkpMagacinNadredjeni.ValueMember = "";
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(562, 66);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(306, 21);
            this.lkpDogadjaj.TabIndex = 59;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(488, 70);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 65;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // tbLokacija
            // 
            this.tbLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbLokacija.Location = new System.Drawing.Point(121, 134);
            this.tbLokacija.Name = "tbLokacija";
            this.tbLokacija.Size = new System.Drawing.Size(747, 20);
            this.tbLokacija.TabIndex = 61;
            // 
            // tbNaziv
            // 
            this.tbNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNaziv.Location = new System.Drawing.Point(121, 99);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(747, 20);
            this.tbNaziv.TabIndex = 60;
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 66);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 62;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 66);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 58;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(12, 137);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(47, 13);
            this.lblLokacija.TabIndex = 64;
            this.lblLokacija.Text = "Lokacija";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(12, 102);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(39, 13);
            this.lblNaziv.TabIndex = 63;
            this.lblNaziv.Text = "Naziv";
            // 
            // cbCentralni
            // 
            this.cbCentralni.AutoSize = true;
            this.cbCentralni.Location = new System.Drawing.Point(121, 252);
            this.cbCentralni.Name = "cbCentralni";
            this.cbCentralni.Size = new System.Drawing.Size(15, 14);
            this.cbCentralni.TabIndex = 70;
            this.cbCentralni.UseVisualStyleBackColor = true;
            this.cbCentralni.CheckedChanged += new System.EventHandler(this.CbCentralni_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Centralni";
            // 
            // btnPrebaciArtikle
            // 
            this.btnPrebaciArtikle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnPrebaciArtikle.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPrebaciArtikle.FlatAppearance.BorderSize = 0;
            this.btnPrebaciArtikle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrebaciArtikle.Image = global::Damax.Properties.Resources.dodaj_artikle__2_;
            this.btnPrebaciArtikle.Location = new System.Drawing.Point(481, 5);
            this.btnPrebaciArtikle.Name = "btnPrebaciArtikle";
            this.btnPrebaciArtikle.Size = new System.Drawing.Size(40, 40);
            this.btnPrebaciArtikle.TabIndex = 72;
            this.btnPrebaciArtikle.Tag = "Prebaci artikle";
            this.btnPrebaciArtikle.UseVisualStyleBackColor = false;
            this.btnPrebaciArtikle.Click += new System.EventHandler(this.btnPrebaciArtikle_Click);
            this.btnPrebaciArtikle.MouseHover += new System.EventHandler(this.btnPrebaciArtikle_MouseHover);
            // 
            // btnImport
            // 
            this.btnImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnImport.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnImport.FlatAppearance.BorderSize = 0;
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImport.Image = global::Damax.Properties.Resources.import__2_;
            this.btnImport.Location = new System.Drawing.Point(435, 5);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(40, 40);
            this.btnImport.TabIndex = 73;
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            this.btnImport.MouseHover += new System.EventHandler(this.btnImport_MouseHover);
            // 
            // FormMagacin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.btnPrebaciArtikle);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCentralni);
            this.Controls.Add(this.lblProdajnoMjesto);
            this.Controls.Add(this.lblNadredjeni);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Controls.Add(this.lkpMagacinNadredjeni);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.tbLokacija);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormMagacin";
            this.Text = "Magacini";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.tbNaziv, 0);
            this.Controls.SetChildIndex(this.tbLokacija, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.lkpMagacinNadredjeni, 0);
            this.Controls.SetChildIndex(this.lkpProdajnoMjesto, 0);
            this.Controls.SetChildIndex(this.lblNadredjeni, 0);
            this.Controls.SetChildIndex(this.lblProdajnoMjesto, 0);
            this.Controls.SetChildIndex(this.cbCentralni, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.btnImport, 0);
            this.Controls.SetChildIndex(this.btnPrebaciArtikle, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProdajnoMjesto;
        private System.Windows.Forms.Label lblNadredjeni;
        private LookupComboBox lkpProdajnoMjesto;
        private LookupComboBox lkpMagacinNadredjeni;
        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.TextBox tbLokacija;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.CheckBox cbCentralni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrebaciArtikle;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ToolTip ttPrebacivanje;
    }
}