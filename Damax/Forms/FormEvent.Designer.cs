﻿namespace Damax.Forms
{
    partial class FormEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAktivan = new System.Windows.Forms.Label();
            this.chAktivan = new System.Windows.Forms.CheckBox();
            this.dtpDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtpDatumOd = new System.Windows.Forms.DateTimePicker();
            this.lblAdresa = new System.Windows.Forms.Label();
            this.tbAdresa = new System.Windows.Forms.TextBox();
            this.tbOdgovornaOsoba = new System.Windows.Forms.TextBox();
            this.tbMjesto = new System.Windows.Forms.TextBox();
            this.tbNaziv = new System.Windows.Forms.TextBox();
            this.lblDatumOdDo = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.lblOdgovornaOsoba = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblMjesto = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.cbZavrsen = new System.Windows.Forms.CheckBox();
            this.lblZavrsen = new System.Windows.Forms.Label();
            this.btnZakljucajDogadjaj = new System.Windows.Forms.Button();
            this.tbValuta = new System.Windows.Forms.TextBox();
            this.lblValuta = new System.Windows.Forms.Label();
            this.lblZabranaBrisanja = new System.Windows.Forms.Label();
            this.cbZabranaBrisanja = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblAktivan
            // 
            this.lblAktivan.AutoSize = true;
            this.lblAktivan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAktivan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblAktivan.Location = new System.Drawing.Point(12, 258);
            this.lblAktivan.Name = "lblAktivan";
            this.lblAktivan.Size = new System.Drawing.Size(43, 13);
            this.lblAktivan.TabIndex = 90;
            this.lblAktivan.Text = "Aktivan";
            // 
            // chAktivan
            // 
            this.chAktivan.AutoSize = true;
            this.chAktivan.Checked = true;
            this.chAktivan.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAktivan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.chAktivan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.chAktivan.Location = new System.Drawing.Point(121, 260);
            this.chAktivan.Name = "chAktivan";
            this.chAktivan.Size = new System.Drawing.Size(15, 14);
            this.chAktivan.TabIndex = 89;
            this.chAktivan.UseVisualStyleBackColor = true;
            // 
            // dtpDatumDo
            // 
            this.dtpDatumDo.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.dtpDatumDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDatumDo.Location = new System.Drawing.Point(283, 222);
            this.dtpDatumDo.Name = "dtpDatumDo";
            this.dtpDatumDo.Size = new System.Drawing.Size(149, 20);
            this.dtpDatumDo.TabIndex = 88;
            // 
            // dtpDatumOd
            // 
            this.dtpDatumOd.CalendarForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.dtpDatumOd.CalendarTitleForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.dtpDatumOd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpDatumOd.Location = new System.Drawing.Point(121, 222);
            this.dtpDatumOd.Name = "dtpDatumOd";
            this.dtpDatumOd.Size = new System.Drawing.Size(149, 20);
            this.dtpDatumOd.TabIndex = 87;
            // 
            // lblAdresa
            // 
            this.lblAdresa.AutoSize = true;
            this.lblAdresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAdresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblAdresa.Location = new System.Drawing.Point(475, 146);
            this.lblAdresa.Name = "lblAdresa";
            this.lblAdresa.Size = new System.Drawing.Size(40, 13);
            this.lblAdresa.TabIndex = 86;
            this.lblAdresa.Text = "Adresa";
            // 
            // tbAdresa
            // 
            this.tbAdresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbAdresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbAdresa.Location = new System.Drawing.Point(538, 143);
            this.tbAdresa.Multiline = true;
            this.tbAdresa.Name = "tbAdresa";
            this.tbAdresa.Size = new System.Drawing.Size(330, 60);
            this.tbAdresa.TabIndex = 85;
            // 
            // tbOdgovornaOsoba
            // 
            this.tbOdgovornaOsoba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbOdgovornaOsoba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbOdgovornaOsoba.Location = new System.Drawing.Point(121, 181);
            this.tbOdgovornaOsoba.Name = "tbOdgovornaOsoba";
            this.tbOdgovornaOsoba.Size = new System.Drawing.Size(311, 20);
            this.tbOdgovornaOsoba.TabIndex = 84;
            // 
            // tbMjesto
            // 
            this.tbMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbMjesto.Location = new System.Drawing.Point(121, 143);
            this.tbMjesto.Name = "tbMjesto";
            this.tbMjesto.Size = new System.Drawing.Size(311, 20);
            this.tbMjesto.TabIndex = 83;
            // 
            // tbNaziv
            // 
            this.tbNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNaziv.Location = new System.Drawing.Point(121, 108);
            this.tbNaziv.Name = "tbNaziv";
            this.tbNaziv.Size = new System.Drawing.Size(747, 20);
            this.tbNaziv.TabIndex = 82;
            // 
            // lblDatumOdDo
            // 
            this.lblDatumOdDo.AutoSize = true;
            this.lblDatumOdDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatumOdDo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDatumOdDo.Location = new System.Drawing.Point(12, 225);
            this.lblDatumOdDo.Name = "lblDatumOdDo";
            this.lblDatumOdDo.Size = new System.Drawing.Size(68, 13);
            this.lblDatumOdDo.TabIndex = 81;
            this.lblDatumOdDo.Text = "Datum od-do";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 75);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 77;
            this.lblGodina.Text = "Godina";
            // 
            // lblOdgovornaOsoba
            // 
            this.lblOdgovornaOsoba.AutoSize = true;
            this.lblOdgovornaOsoba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOdgovornaOsoba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblOdgovornaOsoba.Location = new System.Drawing.Point(12, 184);
            this.lblOdgovornaOsoba.Name = "lblOdgovornaOsoba";
            this.lblOdgovornaOsoba.Size = new System.Drawing.Size(79, 13);
            this.lblOdgovornaOsoba.TabIndex = 80;
            this.lblOdgovornaOsoba.Text = "Odgovorno lice";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 71);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 76;
            // 
            // lblMjesto
            // 
            this.lblMjesto.AutoSize = true;
            this.lblMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblMjesto.Location = new System.Drawing.Point(12, 146);
            this.lblMjesto.Name = "lblMjesto";
            this.lblMjesto.Size = new System.Drawing.Size(38, 13);
            this.lblMjesto.TabIndex = 79;
            this.lblMjesto.Text = "Mjesto";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(12, 111);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(39, 13);
            this.lblNaziv.TabIndex = 78;
            this.lblNaziv.Text = "Naziv";
            // 
            // cbZavrsen
            // 
            this.cbZavrsen.AutoSize = true;
            this.cbZavrsen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbZavrsen.Location = new System.Drawing.Point(121, 287);
            this.cbZavrsen.Name = "cbZavrsen";
            this.cbZavrsen.Size = new System.Drawing.Size(15, 14);
            this.cbZavrsen.TabIndex = 91;
            this.cbZavrsen.UseVisualStyleBackColor = true;
            // 
            // lblZavrsen
            // 
            this.lblZavrsen.AutoSize = true;
            this.lblZavrsen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblZavrsen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblZavrsen.Location = new System.Drawing.Point(12, 285);
            this.lblZavrsen.Name = "lblZavrsen";
            this.lblZavrsen.Size = new System.Drawing.Size(46, 13);
            this.lblZavrsen.TabIndex = 92;
            this.lblZavrsen.Text = "Završen";
            // 
            // btnZakljucajDogadjaj
            // 
            this.btnZakljucajDogadjaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnZakljucajDogadjaj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnZakljucajDogadjaj.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnZakljucajDogadjaj.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnZakljucajDogadjaj.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnZakljucajDogadjaj.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZakljucajDogadjaj.Image = global::Damax.Properties.Resources._lock;
            this.btnZakljucajDogadjaj.Location = new System.Drawing.Point(828, 5);
            this.btnZakljucajDogadjaj.Name = "btnZakljucajDogadjaj";
            this.btnZakljucajDogadjaj.Size = new System.Drawing.Size(40, 40);
            this.btnZakljucajDogadjaj.TabIndex = 93;
            this.btnZakljucajDogadjaj.Tag = "Zaključi događaj";
            this.btnZakljucajDogadjaj.UseVisualStyleBackColor = false;
            this.btnZakljucajDogadjaj.Click += new System.EventHandler(this.btnZakljucajDogadjaj_Click);
            // 
            // tbValuta
            // 
            this.tbValuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbValuta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbValuta.Location = new System.Drawing.Point(538, 224);
            this.tbValuta.Name = "tbValuta";
            this.tbValuta.Size = new System.Drawing.Size(100, 20);
            this.tbValuta.TabIndex = 94;
            // 
            // lblValuta
            // 
            this.lblValuta.AutoSize = true;
            this.lblValuta.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblValuta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblValuta.Location = new System.Drawing.Point(475, 228);
            this.lblValuta.Name = "lblValuta";
            this.lblValuta.Size = new System.Drawing.Size(43, 13);
            this.lblValuta.TabIndex = 95;
            this.lblValuta.Text = "Valuta";
            // 
            // lblZabranaBrisanja
            // 
            this.lblZabranaBrisanja.AutoSize = true;
            this.lblZabranaBrisanja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblZabranaBrisanja.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblZabranaBrisanja.Location = new System.Drawing.Point(475, 257);
            this.lblZabranaBrisanja.Name = "lblZabranaBrisanja";
            this.lblZabranaBrisanja.Size = new System.Drawing.Size(142, 13);
            this.lblZabranaBrisanja.TabIndex = 97;
            this.lblZabranaBrisanja.Text = "Zabranjeno brisanje šifarnika";
            // 
            // cbZabranaBrisanja
            // 
            this.cbZabranaBrisanja.AutoSize = true;
            this.cbZabranaBrisanja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbZabranaBrisanja.Location = new System.Drawing.Point(623, 257);
            this.cbZabranaBrisanja.Name = "cbZabranaBrisanja";
            this.cbZabranaBrisanja.Size = new System.Drawing.Size(15, 14);
            this.cbZabranaBrisanja.TabIndex = 96;
            this.cbZabranaBrisanja.UseVisualStyleBackColor = true;
            // 
            // FormEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.lblZabranaBrisanja);
            this.Controls.Add(this.cbZabranaBrisanja);
            this.Controls.Add(this.lblValuta);
            this.Controls.Add(this.tbValuta);
            this.Controls.Add(this.btnZakljucajDogadjaj);
            this.Controls.Add(this.lblZavrsen);
            this.Controls.Add(this.cbZavrsen);
            this.Controls.Add(this.lblAktivan);
            this.Controls.Add(this.chAktivan);
            this.Controls.Add(this.dtpDatumDo);
            this.Controls.Add(this.dtpDatumOd);
            this.Controls.Add(this.lblAdresa);
            this.Controls.Add(this.tbAdresa);
            this.Controls.Add(this.tbOdgovornaOsoba);
            this.Controls.Add(this.tbMjesto);
            this.Controls.Add(this.tbNaziv);
            this.Controls.Add(this.lblDatumOdDo);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.lblOdgovornaOsoba);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblMjesto);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormEvent";
            this.Text = "Događaji";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblMjesto, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblOdgovornaOsoba, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDatumOdDo, 0);
            this.Controls.SetChildIndex(this.tbNaziv, 0);
            this.Controls.SetChildIndex(this.tbMjesto, 0);
            this.Controls.SetChildIndex(this.tbOdgovornaOsoba, 0);
            this.Controls.SetChildIndex(this.tbAdresa, 0);
            this.Controls.SetChildIndex(this.lblAdresa, 0);
            this.Controls.SetChildIndex(this.dtpDatumOd, 0);
            this.Controls.SetChildIndex(this.dtpDatumDo, 0);
            this.Controls.SetChildIndex(this.chAktivan, 0);
            this.Controls.SetChildIndex(this.lblAktivan, 0);
            this.Controls.SetChildIndex(this.cbZavrsen, 0);
            this.Controls.SetChildIndex(this.lblZavrsen, 0);
            this.Controls.SetChildIndex(this.btnZakljucajDogadjaj, 0);
            this.Controls.SetChildIndex(this.tbValuta, 0);
            this.Controls.SetChildIndex(this.lblValuta, 0);
            this.Controls.SetChildIndex(this.cbZabranaBrisanja, 0);
            this.Controls.SetChildIndex(this.lblZabranaBrisanja, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAktivan;
        private System.Windows.Forms.CheckBox chAktivan;
        private System.Windows.Forms.DateTimePicker dtpDatumDo;
        private System.Windows.Forms.DateTimePicker dtpDatumOd;
        private System.Windows.Forms.Label lblAdresa;
        private System.Windows.Forms.TextBox tbAdresa;
        private System.Windows.Forms.TextBox tbOdgovornaOsoba;
        private System.Windows.Forms.TextBox tbMjesto;
        private System.Windows.Forms.TextBox tbNaziv;
        private System.Windows.Forms.Label lblDatumOdDo;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.Label lblOdgovornaOsoba;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblMjesto;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.CheckBox cbZavrsen;
        private System.Windows.Forms.Label lblZavrsen;
        private System.Windows.Forms.Button btnZakljucajDogadjaj;
        private System.Windows.Forms.TextBox tbValuta;
        private System.Windows.Forms.Label lblValuta;
        private System.Windows.Forms.Label lblZabranaBrisanja;
        private System.Windows.Forms.CheckBox cbZabranaBrisanja;
    }
}