﻿using Damax.Template;

namespace Damax.Forms
{
    partial class FormNarudzba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkpDogadjaj = new Damax.Template.LookupComboBox();
            this.tbNapomena = new System.Windows.Forms.TextBox();
            this.tbBrojKartice = new System.Windows.Forms.TextBox();
            this.dtDatum = new System.Windows.Forms.DateTimePicker();
            this.tbIznos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDogadjaj = new System.Windows.Forms.Label();
            this.lblGodina = new System.Windows.Forms.Label();
            this.dtpGodina = new System.Windows.Forms.DateTimePicker();
            this.lblLokacija = new System.Windows.Forms.Label();
            this.lblNaziv = new System.Windows.Forms.Label();
            this.lblSuma = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lkpDogadjaj
            // 
            this.lkpDogadjaj.BackColor = System.Drawing.Color.White;
            this.lkpDogadjaj.ComboSelectedIndex = -1;
            this.lkpDogadjaj.DataSource = null;
            this.lkpDogadjaj.DeleteButtonEnable = true;
            this.lkpDogadjaj.DisplayMember = "";
            this.lkpDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lkpDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpDogadjaj.Location = new System.Drawing.Point(121, 100);
            this.lkpDogadjaj.LookupMember = null;
            this.lkpDogadjaj.Name = "lkpDogadjaj";
            this.lkpDogadjaj.RefreashButtonEnable = true;
            this.lkpDogadjaj.SelectedLookupValue = null;
            this.lkpDogadjaj.Size = new System.Drawing.Size(273, 21);
            this.lkpDogadjaj.TabIndex = 57;
            this.lkpDogadjaj.ValueMember = "";
            // 
            // tbNapomena
            // 
            this.tbNapomena.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNapomena.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbNapomena.HideSelection = false;
            this.tbNapomena.Location = new System.Drawing.Point(121, 241);
            this.tbNapomena.Name = "tbNapomena";
            this.tbNapomena.Size = new System.Drawing.Size(747, 20);
            this.tbNapomena.TabIndex = 62;
            // 
            // tbBrojKartice
            // 
            this.tbBrojKartice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbBrojKartice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbBrojKartice.Location = new System.Drawing.Point(121, 207);
            this.tbBrojKartice.Name = "tbBrojKartice";
            this.tbBrojKartice.Size = new System.Drawing.Size(134, 20);
            this.tbBrojKartice.TabIndex = 61;
            this.tbBrojKartice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // dtDatum
            // 
            this.dtDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtDatum.Location = new System.Drawing.Point(121, 136);
            this.dtDatum.Name = "dtDatum";
            this.dtDatum.Size = new System.Drawing.Size(273, 20);
            this.dtDatum.TabIndex = 59;
            // 
            // tbIznos
            // 
            this.tbIznos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbIznos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbIznos.Location = new System.Drawing.Point(121, 171);
            this.tbIznos.Name = "tbIznos";
            this.tbIznos.Size = new System.Drawing.Size(134, 20);
            this.tbIznos.TabIndex = 60;
            this.tbIznos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label4.Location = new System.Drawing.Point(488, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 69;
            this.label4.Text = "Prodajno mjesto";
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.White;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DeleteButtonEnable = true;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lkpProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(616, 99);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.RefreashButtonEnable = true;
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(252, 21);
            this.lkpProdajnoMjesto.TabIndex = 58;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label2.Location = new System.Drawing.Point(12, 244);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 68;
            this.label2.Text = "Napomena";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 67;
            this.label1.Text = "Broj kartice";
            // 
            // lblDogadjaj
            // 
            this.lblDogadjaj.AutoSize = true;
            this.lblDogadjaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDogadjaj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDogadjaj.Location = new System.Drawing.Point(12, 103);
            this.lblDogadjaj.Name = "lblDogadjaj";
            this.lblDogadjaj.Size = new System.Drawing.Size(55, 13);
            this.lblDogadjaj.TabIndex = 66;
            this.lblDogadjaj.Text = "Događaj";
            // 
            // lblGodina
            // 
            this.lblGodina.AutoSize = true;
            this.lblGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGodina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblGodina.Location = new System.Drawing.Point(12, 71);
            this.lblGodina.Name = "lblGodina";
            this.lblGodina.Size = new System.Drawing.Size(47, 13);
            this.lblGodina.TabIndex = 63;
            this.lblGodina.Text = "Godina";
            // 
            // dtpGodina
            // 
            this.dtpGodina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpGodina.Location = new System.Drawing.Point(121, 67);
            this.dtpGodina.Name = "dtpGodina";
            this.dtpGodina.Size = new System.Drawing.Size(75, 20);
            this.dtpGodina.TabIndex = 56;
            // 
            // lblLokacija
            // 
            this.lblLokacija.AutoSize = true;
            this.lblLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblLokacija.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblLokacija.Location = new System.Drawing.Point(12, 138);
            this.lblLokacija.Name = "lblLokacija";
            this.lblLokacija.Size = new System.Drawing.Size(43, 13);
            this.lblLokacija.TabIndex = 65;
            this.lblLokacija.Text = "Datum";
            // 
            // lblNaziv
            // 
            this.lblNaziv.AutoSize = true;
            this.lblNaziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNaziv.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblNaziv.Location = new System.Drawing.Point(12, 174);
            this.lblNaziv.Name = "lblNaziv";
            this.lblNaziv.Size = new System.Drawing.Size(37, 13);
            this.lblNaziv.TabIndex = 64;
            this.lblNaziv.Text = "Iznos";
            // 
            // lblSuma
            // 
            this.lblSuma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSuma.AutoSize = true;
            this.lblSuma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSuma.ForeColor = System.Drawing.Color.Green;
            this.lblSuma.Location = new System.Drawing.Point(90, 639);
            this.lblSuma.Name = "lblSuma";
            this.lblSuma.Size = new System.Drawing.Size(25, 13);
            this.lblSuma.TabIndex = 70;
            this.lblSuma.Text = "0.0";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label3.Location = new System.Drawing.Point(9, 639);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 71;
            this.label3.Text = "Ukupan iznos:";
            // 
            // FormNarudzba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSuma);
            this.Controls.Add(this.lkpDogadjaj);
            this.Controls.Add(this.tbNapomena);
            this.Controls.Add(this.tbBrojKartice);
            this.Controls.Add(this.dtDatum);
            this.Controls.Add(this.tbIznos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblDogadjaj);
            this.Controls.Add(this.lblGodina);
            this.Controls.Add(this.dtpGodina);
            this.Controls.Add(this.lblLokacija);
            this.Controls.Add(this.lblNaziv);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormNarudzba";
            this.Text = "Narudžbe";
            this.Controls.SetChildIndex(this.lblNaziv, 0);
            this.Controls.SetChildIndex(this.lblLokacija, 0);
            this.Controls.SetChildIndex(this.dtpGodina, 0);
            this.Controls.SetChildIndex(this.lblGodina, 0);
            this.Controls.SetChildIndex(this.lblDogadjaj, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.lkpProdajnoMjesto, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.tbIznos, 0);
            this.Controls.SetChildIndex(this.dtDatum, 0);
            this.Controls.SetChildIndex(this.tbBrojKartice, 0);
            this.Controls.SetChildIndex(this.tbNapomena, 0);
            this.Controls.SetChildIndex(this.lkpDogadjaj, 0);
            this.Controls.SetChildIndex(this.lblSuma, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LookupComboBox lkpDogadjaj;
        private System.Windows.Forms.TextBox tbNapomena;
        private System.Windows.Forms.TextBox tbBrojKartice;
        private System.Windows.Forms.DateTimePicker dtDatum;
        private System.Windows.Forms.TextBox tbIznos;
        private System.Windows.Forms.Label label4;
        private LookupComboBox lkpProdajnoMjesto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDogadjaj;
        private System.Windows.Forms.Label lblGodina;
        private System.Windows.Forms.DateTimePicker dtpGodina;
        private System.Windows.Forms.Label lblLokacija;
        private System.Windows.Forms.Label lblNaziv;
        private System.Windows.Forms.Label lblSuma;
        private System.Windows.Forms.Label label3;
    }
}