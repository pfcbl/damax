﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormPrenosArtikala : Form
    {
        private Magacin naMagacin;
        private DataTable table;
        private short mode = 0; // mode = 0 - prenos sa magacina na magacin kada vec postoje artikli, 1 insert artikala
        private DataTable tempTable;
        public FormPrenosArtikala(Magacin magacin)
        {
            InitializeComponent();

            this.naMagacin = magacin;
            SetLookups();
            lkpNaMagacin.Enabled = false;
            lkpSaMagacina.RefreshLookup += lookup_RefreashLookup;
            lkpSaMagacina.LookupValueChanged += Lookup_ValueChanged;
            PostaviGridStanjaMaagacina();
            CreateTempTable();

        }

        private void SetLookups()
        {
            lkpSaMagacina.RefreashButtonEnable = false;
            lkpSaMagacina.DeleteButtonEnable = false;
            lkpNaMagacin.DisplayMember = Magacin.F_Naziv;
            lkpNaMagacin.ValueMember = Magacin.F_MagacinId;
            lkpNaMagacin.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>(){new TemplateFilter(Magacin.F_DogadjajId,naMagacin.DogadjajId.ToString(),"="),
                new TemplateFilter(Magacin.F_Godina,naMagacin.Godina.ToString(),"="),new TemplateFilter(Magacin.F_MagacinId,naMagacin.MagacinId.ToString(),"=") });

            lkpSaMagacina.DisplayMember = Magacin.F_Naziv;
            lkpSaMagacina.ValueMember = Magacin.F_MagacinId;
            if(!this.naMagacin.Centralni)
                lkpSaMagacina.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>(){new TemplateFilter(Magacin.F_DogadjajId,naMagacin.DogadjajId.ToString(),"="),
                    new TemplateFilter(Magacin.F_Godina,naMagacin.Godina.ToString(),"="),new TemplateFilter(Magacin.F_MagacinId,naMagacin.MagacinId.ToString(),"<>")//,
                    //new TemplateFilter(Magacin.F_Centralni,"1","=")
                });
            else
                lkpSaMagacina.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>(){new TemplateFilter(Magacin.F_DogadjajId,naMagacin.DogadjajId.ToString(),"="),
                    new TemplateFilter(Magacin.F_Godina,naMagacin.Godina.ToString(),"="),new TemplateFilter(Magacin.F_MagacinId,naMagacin.MagacinId.ToString(),"<>")
                });

        }

        private void PostaviGridStanjaMaagacina()
        {
            if (lkpSaMagacina.LookupMember == null || lkpNaMagacin.LookupMember==null) return;
            table = GeneralDao.PrenosArtikalaGrid(lkpSaMagacina.LookupMember,lkpNaMagacin.LookupMember);
            dgStanjeMagacina.DataSource = table;
            if (table.Rows.Count <= 0) return;
            dgStanjeMagacina.Columns[0].Visible = false;
            dgStanjeMagacina.Columns[1].Visible = false;
            dgStanjeMagacina.Columns[2].Visible = false;
            dgStanjeMagacina.Columns[3].Visible = false;
            dgStanjeMagacina.Columns[4].Visible = false;
            dgStanjeMagacina.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgStanjeMagacina.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgStanjeMagacina.Columns[5].ReadOnly = true;
            dgStanjeMagacina.Columns[5].HeaderText = "Artikal";
            dgStanjeMagacina.Columns[6].ReadOnly = true;
            dgStanjeMagacina.Columns[8].ReadOnly = true;
            dgStanjeMagacina.Columns[7].HeaderText = "Količina za prenos";
            dgStanjeMagacina.Columns[7].DefaultCellStyle.Padding = new Padding(0, 0, 4, 0);
            dgStanjeMagacina.Columns[7].DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            dgStanjeMagacina.Columns[7].DefaultCellStyle.SelectionBackColor = Color.FromArgb(255, 199, 8);
            dgStanjeMagacina.Columns[7].DefaultCellStyle.SelectionForeColor = Color.Black;
            dgStanjeMagacina.Columns[7].DefaultCellStyle.Format = "N0";
            dgStanjeMagacina.Columns[7].ReadOnly = !HomeFilters.HomeFilterAktivanDogadjaj;
            dgStanjeMagacina.Columns[7].Width = 100;

            dgStanjeMagacina.Columns[6].HeaderText = "Trenutno stanje - IZLAZ";

            dgStanjeMagacina.Columns[6].DefaultCellStyle.Format = "N0";
            dgStanjeMagacina.Columns[8].DefaultCellStyle.Format = "N0";
            dgStanjeMagacina.Columns[8].HeaderText = "Trenutno stanje - ULAZ";
            dgStanjeMagacina.Columns[9].Visible = false;
            dgStanjeMagacina.Columns[10].Visible = false;




        }

        private void Lookup_ValueChanged(object sender, EventArgs e)
        {

            PostaviGridStanjaMaagacina();

        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {

            lkpSaMagacina.DisplayMember = Magacin.F_Naziv;
            lkpSaMagacina.ValueMember = Magacin.F_MagacinId;
            if (!this.naMagacin.Centralni)
                lkpSaMagacina.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>(){new TemplateFilter(Magacin.F_DogadjajId,naMagacin.DogadjajId.ToString(),"="),
                    new TemplateFilter(Magacin.F_Godina,naMagacin.Godina.ToString(),"="),new TemplateFilter(Magacin.F_MagacinId,naMagacin.MagacinId.ToString(),"<>")//,
                    //new TemplateFilter(Magacin.F_Centralni,"1","=")
                });
            else
                lkpSaMagacina.DataSource = (new MagacinDao()).Select(new List<TemplateFilter>(){new TemplateFilter(Magacin.F_DogadjajId,naMagacin.DogadjajId.ToString(),"="),
                    new TemplateFilter(Magacin.F_Godina,naMagacin.Godina.ToString(),"="),new TemplateFilter(Magacin.F_MagacinId,naMagacin.MagacinId.ToString(),"<>")
                });
        }

        private void dgStanjeMagacina_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column1_KeyPress);
            if (dgStanjeMagacina.CurrentCell.ColumnIndex == 7) 
            {
                if (e.Control is TextBox tb)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column1_KeyPress);
                }
            }
        }

        private void Column1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private DataTable filteredTable;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (filteredTable != null)
                filteredTable = null;
            filteredTable = table.Clone();
            if (table != null && table.Rows.Count > 0)
            {
                DataRow[] rows = table.Select("Prenos>0");
                foreach (var row in rows)
                {
                    filteredTable.Rows.Add(row.ItemArray);
                }
                FillTempTable(filteredTable);

                //int? prenosnicaId = GeneralDao.PrenosArtikalaKreiranjePrenosnice(table);
                if (tempTable != null)
                {
                    DialogResult d = (new FormIzvPregledPrenosnica(tempTable)).ShowDialog();
                    if (d == DialogResult.OK)
                    {
                        GeneralDao.PrenosArtikalaKreiranjePrenosnice(filteredTable);
                        this.Close();
                    }
                    else if (d == DialogResult.Cancel)
                    {
                        DialogResult rez = MessageBox.Show("Prenosnica nije sačuvana. Da li želite da je sačuvate?", "Prenos artikala", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                        if (rez == DialogResult.Yes)
                        {
                            GeneralDao.PrenosArtikalaKreiranjePrenosnice(filteredTable);
                            this.Close();
                        }
                    }

                }


            }
        }

        private void CreateTempTable()
        {
            tempTable = new DataTable();
            tempTable.Columns.Add("Godina");
            tempTable.Columns.Add("DogadjajId");
            tempTable.Columns.Add("PrenosnicaId");
            tempTable.Columns.Add("NazivArtikla");
            tempTable.Columns.Add("MagacinUlaz");
            tempTable.Columns.Add("MagacinIzlaz");
            tempTable.Columns.Add("NazivDogadjaja");
            tempTable.Columns.Add("Kolicina");
            tempTable.Columns.Add("VrijemePrenosa",typeof(DateTime));
            tempTable.Columns.Add("TipPrenosa");
        }

        private void FillTempTable(DataTable dt) {
            tempTable.Rows.Clear();
            int? prenosnicaId = new PrenosnicaDao().SelectMaxId(new List<TemplateFilter>() {
                new TemplateFilter(Prenosnica.F_Godina, HomeFilters.HomeFilterGodina.ToString(),"="),
                new TemplateFilter(Prenosnica.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"="),
            });
            foreach (DataRow dr in dt.Rows)
            {
                object[] o = { HomeFilters.HomeFilterGodina, HomeFilters.HomeFilterDogadjajId, prenosnicaId, dr[5], dr[2], dr[3], HomeFilters.HomeFilterEvent.Naziv, dr[7], DateTime.Now, "P" };
                tempTable.Rows.Add(o);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DgStanjeMagacina_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                if (Convert.ToInt32(table.Rows[e.RowIndex].ItemArray[7]) > Convert.ToInt32(table.Rows[e.RowIndex].ItemArray[6]))
                {
                    dgStanjeMagacina.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
                    MessageBox.Show("Unesena količina je veća od trenutne količine na stanju na magacinu sa kojeg se prenosi.","Nedozvoljena količina",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
        }

        private void dgStanjeMagacina_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tbPretraga_TextChanged(object sender, EventArgs e)
        {
            if(dgStanjeMagacina.DataSource!=null)
            (dgStanjeMagacina.DataSource as DataTable).DefaultView.RowFilter = string.Format("Naziv LIKE '%{0}%'", tbPretraga.Text);

        }

        private void btnDeleteSearch_Click(object sender, EventArgs e)
        {
            tbPretraga.Text = "";
        }

        private void dgStanjeMagacina_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgStanjeMagacina.BeginEdit(true);
        }
    }
}
