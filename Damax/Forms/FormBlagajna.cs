﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormBlagajna : FormTemplate<Blagajna>
    {
        public FormBlagajna()
        {
            InitializeComponent();
            DaoObject=new BlagajnaDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            SetFormControlsSettings();
            lkpDogadjaj.RefreshLookup += new EventHandler(lookup_RefreashLookup);

    }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

            tbUplata.Enabled = false;
            tbIsplata.Enabled = false;
            tbBrojIzdatihKartica.Enabled = false;
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {

            tbNaziv.ReadOnly = !isEnable;
            tbLokacija.ReadOnly = !isEnable;
            tbBrojKarticeDo.ReadOnly = !isEnable;
            tbBrojKarticeOd.ReadOnly = !isEnable;
            tbUplata.ReadOnly = !isEnable;
            tbIsplata.ReadOnly = !isEnable;
            tbUsername.ReadOnly = !isEnable;
            tbPassword.ReadOnly = !isEnable;
            tbBrojIzdatihKartica.ReadOnly = !isEnable;
            lkpDogadjaj.Enabled = false;
            dtpGodina.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            tbNaziv.Text = SelectedObject.Naziv;
            tbLokacija.Text = SelectedObject.Lokacija;
            tbBrojKarticeOd.Text = SelectedObject.BrojKarticeOd;
            tbBrojKarticeDo.Text = SelectedObject.BrojKarticeDo;
            tbUplata.Text = SelectedObject.UkupnoUplata?.ToString() ?? "0";
            tbIsplata.Text = SelectedObject.UkupnoIsplata?.ToString() ?? "0";
            tbUsername.Text = SelectedObject.Username;
            tbPassword.Text = SelectedObject.Password;
            tbBrojIzdatihKartica.Text = SelectedObject.BrojIzdatihKartica?.ToString() ?? "0";


        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?) null;
                SelectedObject.Naziv = tbNaziv.Text;
                SelectedObject.Godina = (short?) dtpGodina.Value.Year;
                SelectedObject.Lokacija = tbLokacija.Text;
                SelectedObject.BrojKarticeDo = tbBrojKarticeDo.Text;
                SelectedObject.BrojKarticeOd = tbBrojKarticeOd.Text;
                SelectedObject.UkupnoUplata = int.TryParse(tbUplata.Text, out int br) ? br : 0;
                SelectedObject.UkupnoIsplata = int.TryParse(tbIsplata.Text, out br) ? br : 0;
                SelectedObject.Username = tbUsername.Text;
                SelectedObject.Password = tbPassword.Text;
                SelectedObject.BrojIzdatihKartica = int.TryParse(tbBrojIzdatihKartica.Text, out br) ? br : 0;
            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbNaziv.Text = "";
            tbLokacija.Text = "";
            tbBrojKarticeDo.Text = "";
            tbBrojKarticeOd.Text = "";
            tbUplata.Text = "";
            tbIsplata.Text = "";
            tbUsername.Text = "";
            tbPassword.Text = "";
            tbBrojIzdatihKartica.Text = "";
        }


        public override bool ValidateControls()
        {
            if (lkpDogadjaj.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbNaziv.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbNaziv, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbUsername.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbUsername, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbPassword.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbPassword, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();

            {
                var form = new FormTransakcija();
                form.Filters.Add(new TemplateFilter(Transakcija.F_Godina, SelectedObject.Godina.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Transakcija.F_DogadjajId, SelectedObject.DogadjajId.ToString(), "="));
                form.Filters.Add(new TemplateFilter(Transakcija.F_BlagajnaId, SelectedObject.BlagajnaId.ToString(), "="));
                form.IsInNextMode = true;
                NextForms.Add(form);
            }
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Blagajna.F_DogadjajId].Visible = false;
            grid.Columns[Blagajna.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Blagajna.F_Godina].Width = 70;
            grid.Columns[Blagajna.F_Naziv].Width = 300;
            grid.Columns[Blagajna.F_BrojKarticeOd].HeaderText ="Id kartice od";
            grid.Columns[Blagajna.F_BrojKarticeDo].HeaderText = "Id kartice do";
            grid.Columns[Blagajna.F_BrojKarticeOd].Width = 80;
            grid.Columns[Blagajna.F_BrojKarticeDo].Width = 80;
            grid.Columns[Blagajna.F_UkupnoUplata].HeaderText = "Uplata";
            grid.Columns[Blagajna.F_UkupnoIsplata].HeaderText = "Isplata";
            grid.Columns[Blagajna.F_UkupnoUplata].Width = 80;
            grid.Columns[Blagajna.F_UkupnoIsplata].Width = 80;
            grid.Columns[Blagajna.F_BlagajnaId].Visible = false;
            grid.Columns[Blagajna.F_BrojIzdatihKartica].HeaderText = "Izdato kartica";



        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
                var source = (new EventDao()).Select(new List<TemplateFilter>() {Filters[0],Filters[1]});
                lkpDogadjaj.DisplayMember = Event.F_Naziv;
                lkpDogadjaj.ValueMember = Event.F_DogadjajId;
                lkpDogadjaj.DataSource = source;
            
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            List<Event> source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }


        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {

            List<Event> source = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;
        }

    }
}
