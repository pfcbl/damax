﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormTransakcija : FormTemplate<Transakcija>
    {
        public FormTransakcija()
        {
            InitializeComponent();
            DaoObject = new TransakcijaDao();
            SetFormControlsSettings();
            LockUnlockMenuButtons(false, false, false);
        }


        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

            dtDatum.Format = DateTimePickerFormat.Custom;
            dtDatum.CustomFormat = "dd.MM.yyyy. HH:mm";
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            dtDatum.Enabled = isEnable;
            tbBrojKartice.ReadOnly = !isEnable;
            tbBrojRacuna.ReadOnly = !isEnable;
            tbIznos.ReadOnly = !isEnable;
            tbTipTransakcije.ReadOnly = !isEnable;
                lkpDogadjaj.Enabled = false;
                dtpGodina.Enabled = false;
                lkpBlagajna.Enabled = false;
        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;
            tbIznos.Text = SelectedObject.Iznos?.ToString() ?? "0";
            dtDatum.Value = SelectedObject.DatumTransakcije ?? DateTime.Now;
            tbBrojKartice.Text = SelectedObject.KarticaUID;
            tbBrojRacuna.Text = SelectedObject.BrojRacuna;
            tbTipTransakcije.Text = SelectedObject.TipTransakcije;

        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject == null) return;
            int br;
            SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                : (int?)null;
            SelectedObject.BlagajnaId = lkpBlagajna.LookupMember != null
                ? Convert.ToInt32(lkpBlagajna.LookupMember)
                : (int?)null;
            SelectedObject.Iznos = int.TryParse(tbIznos.Text, out br) ? br : 0;
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
            SelectedObject.DatumTransakcije = dtDatum.Value;
            SelectedObject.KarticaUID = tbBrojKartice.Text;
            SelectedObject.BrojRacuna = tbBrojRacuna.Text;
            SelectedObject.TipTransakcije = tbTipTransakcije.Text;
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbIznos.Text = "";
            tbTipTransakcije.Text = "";
            tbBrojKartice.Text = "";
            dtDatum.Value = DateTime.Now;
            tbBrojRacuna.Text = "";
        }
        
        public override bool ValidateControls()
        {
            if (lkpDogadjaj.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (lkpBlagajna.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpBlagajna, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            if (tbBrojKartice.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbBrojKartice, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbTipTransakcije.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbTipTransakcije, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();

        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();
            grid.Columns[Transakcija.F_DogadjajId].Visible = false;
            grid.Columns[Transakcija.F_NazivDogadjaja].HeaderText = "Dogadjaj";
            grid.Columns[Narudzba.F_NazivDogadjaja].Width = 200;
            grid.Columns[Transakcija.F_Godina].Width = 70;
            grid.Columns[Transakcija.F_Iznos].HeaderText = "Iznos";
            grid.Columns[Transakcija.F_Iznos].Width = 80;
            grid.Columns[Transakcija.F_BlagajnaId].Visible = false;
            grid.Columns[Transakcija.F_NazivBlagajne].HeaderText = "Blagajna";
            grid.Columns[Transakcija.F_TipTransakcije].HeaderText = "Tip";
            grid.Columns[Transakcija.F_TipTransakcije].Width = 30;
            grid.Columns[Transakcija.F_DatumTransakcije].HeaderText = "Datum";
            grid.Columns[Transakcija.F_DatumTransakcije].DefaultCellStyle.Format = "dd.MM.yyyy. hh:mm";
            grid.Columns[Transakcija.F_BrojRacuna].HeaderText = "Račun";
            grid.Columns[Transakcija.F_TransakcijaId].Visible = false;
            grid.Columns[Transakcija.F_KarticaUID].HeaderText = "Id kartice";

            SetSumLabel();




        }
        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            SetLookups();
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            SetLookups();
        }

        private void SetLookups()
        {
            var eventSource = (new EventDao()).Select(new List<TemplateFilter>() { Filters[0], Filters[1]});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = eventSource;

            var source = (new BlagajnaDao()).Select(Filters);
            lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            lkpBlagajna.DataSource = source;
        }



        private void SetSumLabel()
        {

            decimal? d = 0;
            foreach (Transakcija n in this.Table)
            {
                d += n.Iznos;
            }
            lblSuma.Text = d.ToString() + " " + HomeFilters.HomeFilterEvent.Valuta;
        }
    }
}
