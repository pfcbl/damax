﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormRadnikDogadjaj : FormTemplate<RadnikDogadjaj>
    {
        public FormRadnikDogadjaj()
        {
            InitializeComponent();
            DaoObject = new RadnikDogadjajDao();
            SetFormControlsSettings();

        }

        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;

        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {

            dtpGodina.Enabled = isEnable;
            lkpDogadjaj.Enabled = isEnable;
            if (IsInNextMode || currentState != Constants.Form_Mode.MODE_ADD)
            {
                
                lkpRadnik.Enabled = false;
            }
            else
            {
                lkpRadnik.Enabled = isEnable;
            }
        }

        public override void FillControls()
        {
            base.FillControls();
            DateTime dt;
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt) ? dt : DateTime.Now;


        }

        public override void FillObject()
        {
            base.FillObject();
            SelectedObject.Godina = (short?)dtpGodina.Value.Year;
            SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                : (int?)null;
            SelectedObject.RadnikId = lkpRadnik.LookupMember != null
                ? Convert.ToInt32(lkpRadnik.LookupMember)
                : (int?)null;


        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
        }


        public override bool ValidateControls()
        {
            if (lkpDogadjaj.ValueMember==null)
            {
                GetErrorProvider().SetError(lkpDogadjaj, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if(lkpRadnik.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpRadnik, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            return base.ValidateControls();
        }

        public override void AddNextForms()
        {
            base.AddNextForms();
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();

        }

        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            var source = (new EventDao()).Select(new List<TemplateFilter>()
            {
                new TemplateFilter(Event.F_Godina, dtpGodina.Value.Year.ToString(), "=") 
               ,new TemplateFilter(Event.F_DogadjajId, SelectedObject.DogadjajId.ToString(), "=")
            });
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;

            var radniciSource = (new RadnikDAO()).Select(new List<TemplateFilter>() { Filters[0] });
            lkpRadnik.DisplayMember = Radnik.F_Ime;
            lkpRadnik.ValueMember = Radnik.F_RadnikId;
            lkpRadnik.DataSource = radniciSource;
        }

        public override void Temp_FormActivated()
        {
            base.Temp_FormActivated();
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            var source = (new EventDao()).Select(new List<TemplateFilter> () {new TemplateFilter(Event.F_Godina,dtpGodina.Value.Year.ToString(),"=")});
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = source;


            var radniciSource = (new RadnikDAO()).Select(new List<TemplateFilter>() { Filters[0] });
            lkpRadnik.DisplayMember = Radnik.F_Ime;
            lkpRadnik.ValueMember = Radnik.F_RadnikId;
            lkpRadnik.DataSource = radniciSource;
        }
    }
}
