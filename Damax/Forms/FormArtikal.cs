﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;

namespace Damax.Forms
{
    public partial class FormArtikal : FormTemplate<Artikal>
    {
        public FormArtikal()
        {
            InitializeComponent();
            DaoObject = new ArtikalDao();
            LockUnlockMenuButtons(HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj, HomeFilters.HomeFilterAktivanDogadjaj);
            SetFormControlsSettings();
            lkpKategorija.RefreshLookup += new EventHandler(lookup_RefreashLookup);
            lkpDogadjaj.RefreshLookup += new EventHandler(lookupDogadjaj_RefreashLookup);
        }
        private void SetFormControlsSettings()
        {
            dtpGodina.Format = DateTimePickerFormat.Custom;
            dtpGodina.CustomFormat = "yyyy";
            dtpGodina.ShowUpDown = true;
        }


        public override void Temp_enableDisableControls(int previewState, int currentState, bool isEnable)
        {
            tbNaziv.ReadOnly = !isEnable;
            tbKolicina.Enabled = isEnable;
            tbPuniNnaziv.ReadOnly = !isEnable;
            tbBoja.ReadOnly = !isEnable;
            tbJedMjere.ReadOnly = !isEnable;
            tbCijena.Enabled = isEnable;
            cbFavorit.Enabled = isEnable;
            cbNeaktivann.Enabled = isEnable;
            lkpDogadjaj.Enabled = false;
            dtpGodina.Enabled = false;
            if (IsInNextMode || currentState!=Constants.Form_Mode.MODE_ADD)
                lkpKategorija.Enabled = false;
            else
            {
                lkpKategorija.Enabled=isEnable;
            }
            
        }

        public override void FillControls()
        {
            base.FillControls();
            dtpGodina.Value = DateTime.TryParseExact(SelectedObject.Godina.ToString(), "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt) ? dt : DateTime.Now;
            tbNaziv.Text = SelectedObject.Naziv;
            tbPuniNnaziv.Text = SelectedObject.PuniNaziv;
            tbBoja.Text = SelectedObject.Boja;
            tbJedMjere.Text = SelectedObject.JedinicaMjere;
            cbFavorit.Checked = SelectedObject.Favorit as bool? ?? false;
            cbNeaktivann.Checked = SelectedObject.Neaktivan as bool? ?? false;
            tbCijena.Value = SelectedObject.Cijena ?? 0;
            tbKolicina.Value = SelectedObject.Kolicina ?? 0;

        }

        public override void FillObject()
        {
            base.FillObject();
            if (SelectedObject != null)
            {
                SelectedObject.DogadjajId = lkpDogadjaj.LookupMember != null
                    ? Convert.ToInt32(lkpDogadjaj.LookupMember)
                    : (int?)null;
                SelectedObject.Godina = (short?)dtpGodina.Value.Year;
                SelectedObject.Naziv = tbNaziv.Text;
                SelectedObject.ArtikalKategorijaId = lkpKategorija.LookupMember != null
                    ? Convert.ToInt32(lkpKategorija.LookupMember) : (int?)null;
                SelectedObject.PuniNaziv = tbPuniNnaziv.Text;
                SelectedObject.Cijena = tbCijena.Value;
                SelectedObject.Kolicina = tbKolicina.Value;
                SelectedObject.Boja = tbBoja.Text;
                SelectedObject.JedinicaMjere = tbJedMjere.Text;
                SelectedObject.Favorit = cbFavorit.Checked;
                SelectedObject.Neaktivan = cbNeaktivann.Checked;
            }
        }

        public override void ClearControls()
        {
            base.ClearControls();
            dtpGodina.Value = DateTime.Now;
            tbNaziv.Text = "";
            tbPuniNnaziv.Text = "";
            tbBoja.Text = "";
            tbJedMjere.Text = "";
            tbCijena.Value = 0;
            tbKolicina.Value = 0;
            cbFavorit.Checked =  false;
            cbNeaktivann.Checked =  false;

        }

        public override bool ValidateControls()
        {
            decimal value;
            if (!decimal.TryParse(tbCijena.Text, out value))
            {
                tbCijena.Text = "";
                GetErrorProvider().SetError(tbCijena, "Neispravan format podatka.");
                IsValid = false;
            }
            if (!decimal.TryParse(tbKolicina.Text, out value))
            {
                tbKolicina.Text = "";
                GetErrorProvider().SetError(tbKolicina, "Neispravan format podatka.");
                IsValid = false;
            }

            if (lkpKategorija.ValueMember == null)
            {
                GetErrorProvider().SetError(lkpKategorija, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbNaziv.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbNaziv, "Mora da sadrži vrijednost.");
                IsValid = false;
            }
            if (tbCijena.Text.Equals(""))
            {
                GetErrorProvider().SetError(tbCijena, "Mora da sadrži vrijednost.");
                IsValid = false;
            }

            return base.ValidateControls();
        }

        public override void DgvItemsSettings()
        {
            base.DgvItemsSettings();
            DataGridView grid = GetDataGridView();

            if (grid.Columns.Count > 0)
            {
                //grid.Columns[Artikal.F_DogadjajId].Visible = false;
                grid.Columns[Artikal.F_NazivDogadjaja].HeaderText = "Dogadjaj";
                grid.Columns[Artikal.F_Godina].Width = 70;
                grid.Columns[Artikal.F_ArtikalId].HeaderText = "Šifra artikla";
                grid.Columns[Artikal.F_ArtikalId].Width = 100;
                grid.Columns[Artikal.F_ArtikalKategorijaId].Visible = false;
                grid.Columns[Artikal.F_ArtikalKategorijaNaziv].HeaderText = "Kategorija";
                grid.Columns[Artikal.F_JedinicaMjere].Visible = false;
                grid.Columns[Artikal.F_Neaktivan].Visible = false;
                grid.Columns[Artikal.F_Boja].Visible = false;
                grid.Columns[Artikal.F_Kolicina].Visible = false;
                grid.Columns[Artikal.F_JedinicaMjere].HeaderText = "JM";
                grid.Columns[Artikal.F_ArtikalKategorijaNaziv].Width = 100;
                grid.Columns[Artikal.F_Naziv].Width = 150;
                grid.Columns[Artikal.F_PuniNaziv].HeaderText = "Puni naziv";
                grid.Columns[Artikal.F_PuniNaziv].Width = 200;
                grid.Columns[Artikal.F_Kolicina].Width = 70;
                grid.Columns[Artikal.F_Neaktivan].Width = 80;
                grid.Columns[Artikal.F_Favorit].Width = 70;
                grid.Columns[Artikal.F_Cijena].Width = 90;
                grid.Columns[Artikal.F_Cijena].HeaderText = "Cijena(" + HomeFilters.HomeFilterEvent.Valuta + ")";
                grid.Columns[Artikal.F_JedinicaMjere].Width = 50;
                grid.Columns[Artikal.F_Boja].Width = 80;


            }
        }

        public override void AddNextForms()
        {
            base.AddNextForms();
            
        }

        public override bool DeleteObject()
        {
            if (HomeFilters.HomeFilterEvent.ZabranaBrisanja)
            {
                MessageBox.Show("Brisanje artikala nije dozvoljeno.", "Zabrana", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return base.DeleteObject();
        }

        public override void Temp_ItemChanged()
        {
            base.Temp_ItemChanged();
            if (SelectedObject != null)
            {
                lkpKategorija.DisplayMember = ArtikalKategorija.F_Naziv;
                lkpKategorija.ValueMember = ArtikalKategorija.F_ArtikalKategorijaId;
                lkpKategorija.DataSource = (new ArtikalKategorijaDao()).Select(new List<TemplateFilter>()
                    {
                        new TemplateFilter(ArtikalKategorija.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                        new TemplateFilter(ArtikalKategorija.F_ArtikalKategorijaId,SelectedObject.ArtikalKategorijaId.ToString(), "=")
                    });
            }
            else
            {
                lkpKategorija.DisplayMember = ArtikalKategorija.F_Naziv;
                lkpKategorija.ValueMember = ArtikalKategorija.F_ArtikalKategorijaId;
                lkpKategorija.DataSource = (new ArtikalKategorijaDao()).Select(Filters);
            } 
            
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") }); 
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            if (cdColor.ShowDialog() == DialogResult.OK)
            {
                tbBoja.BackColor = cdColor.Color;
                tbBoja.Text = cdColor.Color.Name;
            }
        }

        public override void Temp_BtnAdd()
        {
            base.Temp_BtnAdd();
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") });
            
            lkpKategorija.DisplayMember = ArtikalKategorija.F_Naziv;
            lkpKategorija.ValueMember = ArtikalKategorija.F_ArtikalKategorijaId;
            lkpKategorija.DataSource = (new ArtikalKategorijaDao()).Select(Filters);
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            lkpKategorija.DisplayMember = ArtikalKategorija.F_Naziv;
            lkpKategorija.ValueMember = ArtikalKategorija.F_ArtikalKategorijaId;
            lkpKategorija.DataSource = (new ArtikalKategorijaDao()).Select(Filters);
        }

        protected void lookupDogadjaj_RefreashLookup(object sender, EventArgs e)
        {
            lkpDogadjaj.DisplayMember = Event.F_Naziv;
            lkpDogadjaj.ValueMember = Event.F_DogadjajId;
            lkpDogadjaj.DataSource = (new EventDao()).Select(new List<TemplateFilter>() {
                        new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                        new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=") });
        }

        private void tbKolicina_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = (System.Globalization.CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }

        }

        private void btnDeleteAll_Click(object sender, EventArgs e)
        {
            if (FormMode == Constants.Form_Mode.MODE_VIEW)
            {
                if (HomeFilters.HomeFilterEvent.ZabranaBrisanja) {
                    MessageBox.Show("Brisanje artikala nije dozvoljeno.", "Zabrana", MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
                if (DialogResult.Yes == MessageBox.Show("Da li zelite da obrišete sve artikle?", "Brisanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    (new ArtikalDao()).DeleteAll();
                    RefreshGrid();
                }
            }
        }

        private void btnDeleteAll_MouseHover(object sender, EventArgs e)
        {
            ttObrisiSve.SetToolTip(btnDeleteAll, "Obrisi sve artikle");
        }
    }
}
