﻿namespace Damax.Forms
{
    partial class FormKartica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.tbKarticaUID = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblKarticaUid = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbStatus
            // 
            this.tbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbStatus.Location = new System.Drawing.Point(139, 105);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.Size = new System.Drawing.Size(78, 20);
            this.tbStatus.TabIndex = 88;
            // 
            // tbKarticaUID
            // 
            this.tbKarticaUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbKarticaUID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.tbKarticaUID.Location = new System.Drawing.Point(139, 67);
            this.tbKarticaUID.Name = "tbKarticaUID";
            this.tbKarticaUID.Size = new System.Drawing.Size(212, 20);
            this.tbKarticaUID.TabIndex = 87;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblStatus.Location = new System.Drawing.Point(30, 108);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(37, 13);
            this.lblStatus.TabIndex = 86;
            this.lblStatus.Text = "Status";
            // 
            // lblKarticaUid
            // 
            this.lblKarticaUid.AutoSize = true;
            this.lblKarticaUid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKarticaUid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKarticaUid.Location = new System.Drawing.Point(30, 70);
            this.lblKarticaUid.Name = "lblKarticaUid";
            this.lblKarticaUid.Size = new System.Drawing.Size(61, 13);
            this.lblKarticaUid.TabIndex = 85;
            this.lblKarticaUid.Text = "UID kartice";
            // 
            // FormKartica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.tbKarticaUID);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblKarticaUid);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormKartica";
            this.Text = "FormKartica";
            this.Controls.SetChildIndex(this.lblKarticaUid, 0);
            this.Controls.SetChildIndex(this.lblStatus, 0);
            this.Controls.SetChildIndex(this.tbKarticaUID, 0);
            this.Controls.SetChildIndex(this.tbStatus, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.TextBox tbKarticaUID;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblKarticaUid;
    }
}