﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Damax.Properties;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;

namespace Damax.Forms
{
    public partial class FormIzvProdajaArtikalaPoProdajnomMjestu : Form
    {

        public ProdajnoMjesto prodajnoMjesto { get; set; }

        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public DateTime? DatumOd { get; set; }

        public DateTime? DatumDo { get; set; }
        public FormIzvProdajaArtikalaPoProdajnomMjestu()
        {
            InitializeComponent();
        }

        public FormIzvProdajaArtikalaPoProdajnomMjestu(ProdajnoMjesto prodajnoMjesto,Artikal artikal)
        {
            InitializeComponent();
            this.prodajnoMjesto = prodajnoMjesto;
        }

        private void FormProdajaArtikalaPoProdajnomMjestu_Load(object sender, EventArgs e)
        {
            lkpProdajnoMjesto.RefreshLookup += lookupProdajnoMjesto_RefreashLookup;
            lkpProdajnoMjesto.LookupValueChanged += lookupProdajnoMjesto_ValueChanged;
            lkpArtikal.RefreshLookup += lookupArtikal_RefreashLookup;
            DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
            DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
            SetDateControls();

            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,DogadjajId.ToString(),"=")});       

            
        }

        private void SetDateControls()
        {
            dtDatumOd.Format = DateTimePickerFormat.Custom;
            dtDatumOd.CustomFormat = Resources.DateTime_Format;

            dtDatumDo.Format = DateTimePickerFormat.Custom;
            dtDatumDo.CustomFormat = Resources.DateTime_Format;

            if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
            if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
        }

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                //if (lkpProdajnoMjesto.SelectedLookupValue != null)
                //{
                    var table = GeneralDao.PotrosnjaArtikalaPoProdajnomMjestuReport(Godina, DogadjajId, lkpProdajnoMjesto.LookupMember, lkpArtikal.LookupMember, dtDatumOd.Value, dtDatumDo.Value);
                    dataSet.Tables.Add(table);
                //}
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.ProdajaArtikalaPoProdajnomMjestu.rdlc";
            var dataset = new DataSet("DataSet1");

            var prodajnomjestoId = lkpProdajnoMjesto.LookupMember;

            if (prodajnomjestoId == null)
            {
                GetData(ref dataset);
            }
            else
            {
                GetData(ref dataset);
            }
            if (dataset.Tables.Count <= 0) return;
            var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookupProdajnoMjesto_RefreashLookup(object sender, EventArgs e)
        {
            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,DogadjajId.ToString(),"=")}); ;
        }

        private void lookupProdajnoMjesto_ValueChanged(object sender, EventArgs e)
        {
            if (lkpProdajnoMjesto.LookupMember == null)
                return;
            lkpArtikal.DisplayMember = MagacinArtikal.F_ArtikalNaziv;
            lkpArtikal.ValueMember = MagacinArtikal.F_ArtikalId;
            lkpArtikal.DataSource = (new MagacinArtikalDao()).Select(new List<TemplateFilter>() {
                new TemplateFilter(MagacinArtikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                new TemplateFilter(MagacinArtikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "="),
                new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId, lkpProdajnoMjesto.LookupMember.ToString(), "=","b2") });
        }
        protected void lookupArtikal_RefreashLookup(object sender, EventArgs e)
        {
            //lkpArtikal.DisplayMember = Artikal.F_Naziv;
            //lkpArtikal.ValueMember = Artikal.F_ArtikalId;
            //lkpArtikal.DataSource = (new ArtikalDao()).Select(new List<TemplateFilter>() {
            //    new TemplateFilter(Artikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
            //            new TemplateFilter(Artikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")
            //} );

            string prodajno = lkpProdajnoMjesto.LookupMember ?? "";
            var filteri = new List<TemplateFilter>() {
                new TemplateFilter(MagacinArtikal.F_Godina, HomeFilters.HomeFilterEvent.Godina.ToString(), "="),
                new TemplateFilter(MagacinArtikal.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=")};
            if (lkpProdajnoMjesto.LookupMember!=null)
            {
                filteri.Add(new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId, lkpProdajnoMjesto.LookupMember.ToString(), "=", "b2"));
            }
            lkpArtikal.DisplayMember = MagacinArtikal.F_ArtikalNaziv;
            lkpArtikal.ValueMember = MagacinArtikal.F_ArtikalId;
            lkpArtikal.DataSource = (new MagacinArtikalDao()).Select(filteri);
            
        }
        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
