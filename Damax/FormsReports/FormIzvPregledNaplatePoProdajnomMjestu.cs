﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormIzvPregledNaplatePoProdajnomMjestu : Form
    {
        public ProdajnoMjesto prodajnoMjesto { get; set; }

        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public DateTime? DatumOd { get; set; }

        public DateTime? DatumDo { get; set; }

        public FormIzvPregledNaplatePoProdajnomMjestu()
        {
            InitializeComponent();
        }
        public FormIzvPregledNaplatePoProdajnomMjestu(ProdajnoMjesto prodajnoMjesto)
        {
            InitializeComponent();
            this.prodajnoMjesto = prodajnoMjesto;
            lkpProdajnoMjesto.Enabled = false;
        }

        private void FormPregledNaplatePoProdajnomMjestu_Load(object sender, EventArgs e)
        {
            lkpProdajnoMjesto.RefreshLookup += lookup_RefreashLookup;
            lkpProdajnoMjesto.LookupValueChanged += lookup_ValueChanged;
            DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
            DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
            SetDateControls();

            List<TemplateFilter> filters;
            if (prodajnoMjesto != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(ProdajnoMjesto.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId, prodajnoMjesto.ProdajnoMjestoId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,DogadjajId.ToString(),"=")};

            var source = (new ProdajnoMjestoDao()).Select(filters);

            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = source;
            rbPoDanima.Checked = true;
        }

        private void SetDateControls()
        {
            dtDatumOd.Format = DateTimePickerFormat.Custom;
            dtDatumOd.CustomFormat = Resources.DateTime_Format;

            dtDatumDo.Format = DateTimePickerFormat.Custom;
            dtDatumDo.CustomFormat = Resources.DateTime_Format;

            if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
            if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
        }

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                if (lkpProdajnoMjesto.SelectedLookupValue != null)
                {
                    var table = GeneralDao.PotrosnjaSatiReport((ProdajnoMjesto)lkpProdajnoMjesto.SelectedLookupValue, dtDatumOd.Value, dtDatumDo.Value,rbPoDanima.Checked?0:1);
                    dataSet.Tables.Add(table);
                }
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.PregledNaplatePoProdajnomMjestu.rdlc";
            var dataset = new DataSet("DataSet1");

            var prodajnomjestoId = lkpProdajnoMjesto.LookupMember;

            if (prodajnomjestoId == null)
            {
                GetData(ref dataset);
            }
            else
            {
                GetData(ref dataset);
            }

            if (dataset.Tables.Count <= 0) return;
            var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            List<TemplateFilter> filters;
            if (prodajnoMjesto != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(ProdajnoMjesto.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(ProdajnoMjesto.F_ProdajnoMjestoId, prodajnoMjesto.ProdajnoMjestoId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,DogadjajId.ToString(),"=")};

            var source = (new ProdajnoMjestoDao()).Select(filters);

            lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
            lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
            lkpProdajnoMjesto.DataSource = source;
        }

        private void lookup_ValueChanged(object sender, EventArgs e)
        {
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
