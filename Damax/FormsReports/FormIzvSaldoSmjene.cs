﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormIzvSaldoSmjene : Form
    {
        public Blagajna blagajna { get; set; }

				public Radnik radnik { get; set; }

				public short? Godina { get; set; }

				public int? DogadjajId { get; set; }

				public int? BlagajnaId { get; set; }

				public int? RadnikId { get; set; }


		public FormIzvSaldoSmjene()
        {
            InitializeComponent();
        }
        public FormIzvSaldoSmjene(Blagajna blagajna)
        {
            InitializeComponent();
            this.blagajna = blagajna;
            lkpBlagajna.Enabled = false;
        }

        private void FormIzvSaldoSmjene_Load(object sender, EventArgs e)
        {
            lkpBlagajna.RefreshLookup += lookup_RefreashLookup;
            lkpBlagajna.LookupValueChanged += lookup_ValueChanged;
						lkpRadnik.RefreshLookup += lookupRadnik_RefreashLookup;
						lkpRadnik.LookupValueChanged += lookupRadnik_ValueChanged;
            BlagajnaId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
						DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;

						List<TemplateFilter> filters;
            if (blagajna != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Blagajna.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_BlagajnaId, blagajna.BlagajnaId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Blagajna.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
                                                      ,new TemplateFilter(Blagajna.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"=")};

            var source = (new BlagajnaDao()).Select(filters);


			//			List<TemplateFilter> filtersRadnik;
			//			if (radnik != null)
			//				filtersRadnik = new List<TemplateFilter>()
			//										{
			//												 new TemplateFilter(Radnik.F_RadnikId, RadnikId.ToString(), "=")
			//										};
			//			else
			//				filtersRadnik = new List<TemplateFilter>() { };

			//			var sourceRadnik = (new BlagajnaDao()).Select(filtersRadnik);

			//			lkpRadnik.DisplayMember = Radnik.F_Prezime;
			//lkpRadnik.ValueMember = Radnik.F_RadnikId;
			//			lkpRadnik.DataSource = sourceRadnik;

						lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            lkpBlagajna.DataSource = source;
        }

       
        public void GetData(ref DataSet dataSet)
        {
            try
            {
                //if (lkpBlagajna.SelectedLookupValue != null)
                //{
                    var table = GeneralDao.SaldoSmjeneReport(Godina,DogadjajId,lkpBlagajna.LookupMember, lkpRadnik.LookupMember);
                    dataSet.Tables.Add(table);
                //}
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.SaldoSmjene.rdlc";
            var dataset = new DataSet("DataSet1");

            var blagajnaId = lkpBlagajna.LookupMember;

            if (blagajnaId == null)
            {
                GetData(ref dataset);
            }
            else
            {
                GetData(ref dataset);
            }

            if (dataset.Tables.Count <= 0) return;
            var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            List<TemplateFilter> filters;
            if (blagajna != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Blagajna.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Blagajna.F_DogadjajId, BlagajnaId.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_BlagajnaId, blagajna.BlagajnaId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Blagajna.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Blagajna.F_DogadjajId,BlagajnaId.ToString(),"=")};

            var source = (new BlagajnaDao()).Select(filters);

            lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            lkpBlagajna.DataSource = source;
        }

        private void lookup_ValueChanged(object sender, EventArgs e)
        {
        }

				protected void lookupRadnik_RefreashLookup(object sender, EventArgs e)
				{
					List<TemplateFilter> filters;
					if (radnik != null)
						filters = new List<TemplateFilter>()
										{
												 new TemplateFilter(Radnik.F_RadnikId, RadnikId.ToString(), "=")
										};
					else
						filters = new List<TemplateFilter>() { };

					var sourceRadnik = (new RadnikDAO()).Select(filters);

					lkpRadnik.DisplayMember = Radnik.F_RadnikImePrezimeLookup;
			lkpRadnik.ValueMember = Radnik.F_RadnikId;
					lkpRadnik.DataSource = sourceRadnik;
				}

				private void lookupRadnik_ValueChanged(object sender, EventArgs e)
				{
				}

		private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
