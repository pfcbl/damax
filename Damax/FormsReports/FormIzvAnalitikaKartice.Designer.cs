﻿namespace Damax.Forms
{
	partial class FormIzvAnalitikaKartice
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.lblDatum = new System.Windows.Forms.Label();
            this.lblKarticaUid = new System.Windows.Forms.Label();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.tbKartica = new System.Windows.Forms.TextBox();
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.Color.White;
            this.btnPrikazi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.Location = new System.Drawing.Point(447, 18);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 30);
            this.btnPrikazi.TabIndex = 11;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDatum.Location = new System.Drawing.Point(12, 41);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(68, 13);
            this.lblDatum.TabIndex = 10;
            this.lblDatum.Text = "Datum od-do";
            // 
            // lblKarticaUid
            // 
            this.lblKarticaUid.AutoSize = true;
            this.lblKarticaUid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblKarticaUid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblKarticaUid.Location = new System.Drawing.Point(12, 9);
            this.lblKarticaUid.Name = "lblKarticaUid";
            this.lblKarticaUid.Size = new System.Drawing.Size(40, 13);
            this.lblKarticaUid.TabIndex = 9;
            this.lblKarticaUid.Text = "Kartica";
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(281, 41);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(144, 20);
            this.dtDatumDo.TabIndex = 8;
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(130, 41);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(145, 20);
            this.dtDatumOd.TabIndex = 7;
            // 
            // tbKartica
            // 
            this.tbKartica.Location = new System.Drawing.Point(130, 9);
            this.tbKartica.Name = "tbKartica";
            this.tbKartica.Size = new System.Drawing.Size(295, 20);
            this.tbKartica.TabIndex = 12;
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.ForeColor = System.Drawing.Color.Black;
            this.rvReport.Location = new System.Drawing.Point(15, 67);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(854, 486);
            this.rvReport.TabIndex = 13;
            // 
            // FormIzvAnalitikaKartice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 565);
            this.Controls.Add(this.rvReport);
            this.Controls.Add(this.tbKartica);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.lblKarticaUid);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Name = "FormIzvAnalitikaKartice";
            this.Text = "FormIzvAnalitikaKartice";
            this.Load += new System.EventHandler(this.FormIzvAnalitikaKartice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnPrikazi;
		private System.Windows.Forms.Label lblDatum;
		private System.Windows.Forms.Label lblKarticaUid;
		private System.Windows.Forms.DateTimePicker dtDatumDo;
		private System.Windows.Forms.DateTimePicker dtDatumOd;
		private System.Windows.Forms.TextBox tbKartica;
		private Microsoft.Reporting.WinForms.ReportViewer rvReport;
	}
}