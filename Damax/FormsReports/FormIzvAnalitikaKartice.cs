﻿using Damax.DAO;
using Damax.Properties;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
	public partial class FormIzvAnalitikaKartice : Form
	{
		public int? DogadjajId { get; set; }
		public string KarticaUid { get; set; }
		public DateTime? DatumOd { get; set; }
		public DateTime? DatumDo { get; set; }

		public FormIzvAnalitikaKartice()
		{
			InitializeComponent();
		}
		private void FormIzvAnalitikaKartice_Load(object sender, EventArgs e)
		{
			DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
			DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
			DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
			SetDateControls();

			
		}

		private void SetDateControls()
		{
			dtDatumOd.Format = DateTimePickerFormat.Custom;
			dtDatumOd.CustomFormat = Resources.DateTime_Format;

			dtDatumDo.Format = DateTimePickerFormat.Custom;
			dtDatumDo.CustomFormat = Resources.DateTime_Format;

			if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
			if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
		}

		public void GetData(ref DataSet dataSet)
		{
			try
			{
				var table = GeneralDao.AnalitikaKarticeReport(HomeFilters.HomeFilterGodina,HomeFilters.HomeFilterDogadjajId, KarticaUid, dtDatumOd.Value, dtDatumDo.Value);
				dataSet.Tables.Add(table);

			}
			catch (MySqlException ex)
			{
				ConnectionPool.HandleMySQLDBException(ex);
			}
		}

		private void ShowReport()
		{
			rvReport.ProcessingMode = ProcessingMode.Local;
			var localReport = rvReport.LocalReport;
			localReport.ReportEmbeddedResource = "Damax.Reports.AnalitikaKartice.rdlc";
			var dataset = new DataSet("DataSet1");

			
			
			GetData(ref dataset);


			if (dataset.Tables.Count <= 0) return;
			var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

			localReport.DataSources.Clear();
			localReport.DataSources.Add(reportDataSource);

			this.rvReport.RefreshReport();
		}



		private void btnPrikazi_Click(object sender, EventArgs e)
		{

			KarticaUid = tbKartica.Text;
			if (!KarticaUid.Equals(""))
			{
				ShowReport();
			}
			
		}
	}
}
