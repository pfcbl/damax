﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormIzvProsjekUplataPoSatuPoBlagajni : Form
    {
        public Blagajna blagajna { get; set; }

        public short? Godina { get; set; }

        public int? BlagajnaId { get; set; }

        public DateTime? DatumOd { get; set; }

        public DateTime? DatumDo { get; set; }

        public FormIzvProsjekUplataPoSatuPoBlagajni()
        {
            InitializeComponent();
        }
        public FormIzvProsjekUplataPoSatuPoBlagajni(Blagajna blagajna)
        {
            InitializeComponent();
            this.blagajna = blagajna;
            lkpBlagajna.Enabled = false;
        }

        private void FormProsjekUplataPoSatuPoBlagajni_Load(object sender, EventArgs e)
        {
            lkpBlagajna.RefreshLookup += lookup_RefreashLookup;
            lkpBlagajna.LookupValueChanged += lookup_ValueChanged;
            BlagajnaId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
            DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
            SetDateControls();

            List<TemplateFilter> filters;
            if (blagajna != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Blagajna.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_BlagajnaId, blagajna.BlagajnaId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Blagajna.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
                                                      ,new TemplateFilter(Blagajna.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"=")};

            var source = (new BlagajnaDao()).Select(filters);

            lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            lkpBlagajna.DataSource = source;
        }

        private void SetDateControls()
        {
            dtDatumOd.Format = DateTimePickerFormat.Custom;
            dtDatumOd.CustomFormat = Resources.DateTime_Format;

            dtDatumDo.Format = DateTimePickerFormat.Custom;
            dtDatumDo.CustomFormat = Resources.DateTime_Format;

            if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
            if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
        }

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                if (lkpBlagajna.SelectedLookupValue != null)
                {
                    var table = GeneralDao.ProsjekUplataPoSatimai((Blagajna)lkpBlagajna.SelectedLookupValue, dtDatumOd.Value, dtDatumDo.Value);
                    dataSet.Tables.Add(table);
                }
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.ProsjecnaUplataPoKarticiPoSatima.rdlc";
            var dataset = new DataSet("DataSet1");

            var blagajnaId = lkpBlagajna.LookupMember;

            if (blagajnaId == null)
            {
                GetData(ref dataset);
            }
            else
            {
                GetData(ref dataset);
            }

            if (dataset.Tables.Count <= 0) return;
            var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            List<TemplateFilter> filters;
            if (blagajna != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Blagajna.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Blagajna.F_DogadjajId, BlagajnaId.ToString(),"=")
                    ,new TemplateFilter(Blagajna.F_BlagajnaId, blagajna.BlagajnaId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Blagajna.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Blagajna.F_DogadjajId,BlagajnaId.ToString(),"=")};

            var source = (new BlagajnaDao()).Select(filters);

            lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            lkpBlagajna.DataSource = source;
        }

        private void lookup_ValueChanged(object sender, EventArgs e)
        {
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
