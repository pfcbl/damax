﻿namespace Damax.Forms
{
    partial class FormIzvProsjecnaPotrosnjaPoProdajnomMjestu
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.lblProdajnoMjesto = new System.Windows.Forms.Label();
            this.lblDatum = new System.Windows.Forms.Label();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.White;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DeleteButtonEnable = true;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(125, 12);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.RefreashButtonEnable = true;
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(295, 21);
            this.lkpProdajnoMjesto.TabIndex = 0;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(125, 48);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(145, 20);
            this.dtDatumOd.TabIndex = 1;
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(276, 48);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(144, 20);
            this.dtDatumDo.TabIndex = 2;
            // 
            // lblProdajnoMjesto
            // 
            this.lblProdajnoMjesto.AutoSize = true;
            this.lblProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblProdajnoMjesto.Location = new System.Drawing.Point(7, 16);
            this.lblProdajnoMjesto.Name = "lblProdajnoMjesto";
            this.lblProdajnoMjesto.Size = new System.Drawing.Size(80, 13);
            this.lblProdajnoMjesto.TabIndex = 3;
            this.lblProdajnoMjesto.Text = "ProdajnoMjesto";
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDatum.Location = new System.Drawing.Point(7, 48);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(68, 13);
            this.lblDatum.TabIndex = 4;
            this.lblDatum.Text = "Datum od-do";
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.Color.White;
            this.btnPrikazi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.Location = new System.Drawing.Point(443, 25);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 30);
            this.btnPrikazi.TabIndex = 5;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.ForeColor = System.Drawing.Color.Black;
            this.rvReport.Location = new System.Drawing.Point(12, 77);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(858, 597);
            this.rvReport.TabIndex = 6;
            // 
            // FormIzvProsjecnaPotrosnjaPoProdajnomMjestu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 686);
            this.Controls.Add(this.rvReport);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.lblProdajnoMjesto);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Name = "FormIzvProsjecnaPotrosnjaPoProdajnomMjestu";
            this.Text = "Prosjecna potrosnja po prodajnom mjestu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormProsjecnaPotrosnjaPoProdajnomMjestu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Template.LookupComboBox lkpProdajnoMjesto;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
        private System.Windows.Forms.Label lblProdajnoMjesto;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Button btnPrikazi;
        private Microsoft.Reporting.WinForms.ReportViewer rvReport;
    }
}