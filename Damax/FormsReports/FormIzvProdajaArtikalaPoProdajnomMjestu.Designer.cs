﻿namespace Damax.Forms
{
    partial class FormIzvProdajaArtikalaPoProdajnomMjestu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.lblDatum = new System.Windows.Forms.Label();
            this.lblProdajnoMjesto = new System.Windows.Forms.Label();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.lkpArtikal = new Damax.Template.LookupComboBox();
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.SuspendLayout();
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.Location = new System.Drawing.Point(12, 91);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(858, 583);
            this.rvReport.TabIndex = 0;
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDatum.Location = new System.Drawing.Point(9, 65);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(68, 13);
            this.lblDatum.TabIndex = 9;
            this.lblDatum.Text = "Datum od-do";
            // 
            // lblProdajnoMjesto
            // 
            this.lblProdajnoMjesto.AutoSize = true;
            this.lblProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblProdajnoMjesto.Location = new System.Drawing.Point(9, 9);
            this.lblProdajnoMjesto.Name = "lblProdajnoMjesto";
            this.lblProdajnoMjesto.Size = new System.Drawing.Size(80, 13);
            this.lblProdajnoMjesto.TabIndex = 8;
            this.lblProdajnoMjesto.Text = "ProdajnoMjesto";
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(278, 65);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(144, 20);
            this.dtDatumDo.TabIndex = 7;
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(127, 65);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(145, 20);
            this.dtDatumOd.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Artikal";
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.Color.White;
            this.btnPrikazi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.Location = new System.Drawing.Point(447, 27);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 33);
            this.btnPrikazi.TabIndex = 12;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // lkpArtikal
            // 
            this.lkpArtikal.BackColor = System.Drawing.Color.White;
            this.lkpArtikal.ComboSelectedIndex = -1;
            this.lkpArtikal.DataSource = null;
            this.lkpArtikal.DeleteButtonEnable = true;
            this.lkpArtikal.DisplayMember = "";
            this.lkpArtikal.Location = new System.Drawing.Point(127, 35);
            this.lkpArtikal.LookupMember = null;
            this.lkpArtikal.Name = "lkpArtikal";
            this.lkpArtikal.RefreashButtonEnable = true;
            this.lkpArtikal.SelectedLookupValue = null;
            this.lkpArtikal.Size = new System.Drawing.Size(295, 21);
            this.lkpArtikal.TabIndex = 10;
            this.lkpArtikal.ValueMember = "";
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.White;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DeleteButtonEnable = true;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(127, 5);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.RefreashButtonEnable = true;
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(295, 21);
            this.lkpProdajnoMjesto.TabIndex = 5;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // FormIzvProdajaArtikalaPoProdajnomMjestu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 686);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lkpArtikal);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.lblProdajnoMjesto);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Controls.Add(this.rvReport);
            this.Name = "FormIzvProdajaArtikalaPoProdajnomMjestu";
            this.Text = "Prodaja artikala po prodajnom mjestu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormProdajaArtikalaPoProdajnomMjestu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvReport;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.Label lblProdajnoMjesto;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
        private Template.LookupComboBox lkpProdajnoMjesto;
        private Template.LookupComboBox lkpArtikal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrikazi;
    }
}