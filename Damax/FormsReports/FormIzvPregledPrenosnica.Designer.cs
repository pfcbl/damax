﻿namespace Damax.Forms
{
	partial class FormIzvPregledPrenosnica
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.lkpPrenosnica = new Damax.Template.LookupComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnIzmijeni = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.Location = new System.Drawing.Point(12, 55);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(858, 619);
            this.rvReport.TabIndex = 0;
            this.rvReport.WaitControlDisplayAfter = 10;
            // 
            // lkpPrenosnica
            // 
            this.lkpPrenosnica.BackColor = System.Drawing.Color.White;
            this.lkpPrenosnica.ComboSelectedIndex = -1;
            this.lkpPrenosnica.DataSource = null;
            this.lkpPrenosnica.DeleteButtonEnable = true;
            this.lkpPrenosnica.DisplayMember = "";
            this.lkpPrenosnica.Location = new System.Drawing.Point(12, 17);
            this.lkpPrenosnica.LookupMember = null;
            this.lkpPrenosnica.Name = "lkpPrenosnica";
            this.lkpPrenosnica.RefreashButtonEnable = true;
            this.lkpPrenosnica.SelectedLookupValue = null;
            this.lkpPrenosnica.Size = new System.Drawing.Size(527, 21);
            this.lkpPrenosnica.TabIndex = 1;
            this.lkpPrenosnica.ValueMember = "";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnSave.Location = new System.Drawing.Point(714, 17);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Sačuvaj";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnIzmijeni
            // 
            this.btnIzmijeni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIzmijeni.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnIzmijeni.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnIzmijeni.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIzmijeni.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnIzmijeni.Location = new System.Drawing.Point(795, 17);
            this.btnIzmijeni.Name = "btnIzmijeni";
            this.btnIzmijeni.Size = new System.Drawing.Size(75, 23);
            this.btnIzmijeni.TabIndex = 3;
            this.btnIzmijeni.Text = "Nazad";
            this.btnIzmijeni.UseVisualStyleBackColor = true;
            this.btnIzmijeni.Visible = false;
            this.btnIzmijeni.Click += new System.EventHandler(this.btnIzmijeni_Click);
            // 
            // FormIzvPregledPrenosnica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 686);
            this.Controls.Add(this.btnIzmijeni);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.rvReport);
            this.Controls.Add(this.lkpPrenosnica);
            this.Name = "FormIzvPregledPrenosnica";
            this.Text = "Pregled prenosnica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormPregledPrenosnica_Load);
            this.ResumeLayout(false);

		}

		#endregion

		private Microsoft.Reporting.WinForms.ReportViewer rvReport;
		private Template.LookupComboBox lkpPrenosnica;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnIzmijeni;
    }
}