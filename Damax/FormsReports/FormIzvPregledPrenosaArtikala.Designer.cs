﻿namespace Damax.Forms
{
    partial class FormIzvPregledPrenosaArtikala
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.lkpMagacin = new Damax.Template.LookupComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbPoStaima = new System.Windows.Forms.RadioButton();
            this.rbPoDanima = new System.Windows.Forms.RadioButton();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.Location = new System.Drawing.Point(12, 59);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(858, 615);
            this.rvReport.TabIndex = 0;
            this.rvReport.WaitControlDisplayAfter = 10;
            // 
            // lkpMagacin
            // 
            this.lkpMagacin.BackColor = System.Drawing.Color.White;
            this.lkpMagacin.ComboSelectedIndex = -1;
            this.lkpMagacin.DataSource = null;
            this.lkpMagacin.DeleteButtonEnable = true;
            this.lkpMagacin.DisplayMember = "";
            this.lkpMagacin.Location = new System.Drawing.Point(12, 5);
            this.lkpMagacin.LookupMember = null;
            this.lkpMagacin.Name = "lkpMagacin";
            this.lkpMagacin.RefreashButtonEnable = true;
            this.lkpMagacin.SelectedLookupValue = null;
            this.lkpMagacin.Size = new System.Drawing.Size(295, 21);
            this.lkpMagacin.TabIndex = 1;
            this.lkpMagacin.ValueMember = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbPoStaima);
            this.panel1.Controls.Add(this.rbPoDanima);
            this.panel1.Location = new System.Drawing.Point(336, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(55, 48);
            this.panel1.TabIndex = 8;
            // 
            // rbPoStaima
            // 
            this.rbPoStaima.AutoSize = true;
            this.rbPoStaima.Location = new System.Drawing.Point(3, 28);
            this.rbPoStaima.Name = "rbPoStaima";
            this.rbPoStaima.Size = new System.Drawing.Size(46, 17);
            this.rbPoStaima.TabIndex = 1;
            this.rbPoStaima.TabStop = true;
            this.rbPoStaima.Text = "Ulaz";
            this.rbPoStaima.UseVisualStyleBackColor = true;
            // 
            // rbPoDanima
            // 
            this.rbPoDanima.AutoSize = true;
            this.rbPoDanima.Location = new System.Drawing.Point(3, 3);
            this.rbPoDanima.Name = "rbPoDanima";
            this.rbPoDanima.Size = new System.Drawing.Size(46, 17);
            this.rbPoDanima.TabIndex = 0;
            this.rbPoDanima.TabStop = true;
            this.rbPoDanima.Text = "Izlaz";
            this.rbPoDanima.UseVisualStyleBackColor = true;
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.Color.White;
            this.btnPrikazi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.Location = new System.Drawing.Point(415, 23);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 30);
            this.btnPrikazi.TabIndex = 9;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(163, 33);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(144, 20);
            this.dtDatumDo.TabIndex = 11;
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(12, 33);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(145, 20);
            this.dtDatumOd.TabIndex = 10;
            // 
            // FormIzvPregledPrenosaArtikala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 686);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rvReport);
            this.Controls.Add(this.lkpMagacin);
            this.Name = "FormIzvPregledPrenosaArtikala";
            this.Text = "Pregled prenosa artikala";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormPregledPrenosaArtikala_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvReport;
        private Template.LookupComboBox lkpMagacin;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbPoStaima;
        private System.Windows.Forms.RadioButton rbPoDanima;
        private System.Windows.Forms.Button btnPrikazi;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
    }
}