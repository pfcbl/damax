﻿namespace Damax.Forms
{
	partial class FormIzvReport1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.mySqlDataAdapter1 = new MySql.Data.MySqlClient.MySqlDataAdapter();
            this.btnPokreniIzvjestaj1 = new System.Windows.Forms.Button();
            this.lkpProdajnoMjesto = new Damax.Template.LookupComboBox();
            this.rvTest1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // mySqlDataAdapter1
            // 
            this.mySqlDataAdapter1.DeleteCommand = null;
            this.mySqlDataAdapter1.InsertCommand = null;
            this.mySqlDataAdapter1.SelectCommand = null;
            this.mySqlDataAdapter1.UpdateCommand = null;
            // 
            // btnPokreniIzvjestaj1
            // 
            this.btnPokreniIzvjestaj1.Location = new System.Drawing.Point(327, 10);
            this.btnPokreniIzvjestaj1.Name = "btnPokreniIzvjestaj1";
            this.btnPokreniIzvjestaj1.Size = new System.Drawing.Size(75, 24);
            this.btnPokreniIzvjestaj1.TabIndex = 1;
            this.btnPokreniIzvjestaj1.Text = "Prikaži";
            this.btnPokreniIzvjestaj1.UseVisualStyleBackColor = true;
            this.btnPokreniIzvjestaj1.Click += new System.EventHandler(this.btnPokreniIzvjestaj1_Click);
            // 
            // lkpProdajnoMjesto
            // 
            this.lkpProdajnoMjesto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lkpProdajnoMjesto.BackColor = System.Drawing.Color.Transparent;
            this.lkpProdajnoMjesto.ComboSelectedIndex = -1;
            this.lkpProdajnoMjesto.DataSource = null;
            this.lkpProdajnoMjesto.DisplayMember = "";
            this.lkpProdajnoMjesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkpProdajnoMjesto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lkpProdajnoMjesto.Location = new System.Drawing.Point(12, 9);
            this.lkpProdajnoMjesto.LookupMember = null;
            this.lkpProdajnoMjesto.Name = "lkpProdajnoMjesto";
            this.lkpProdajnoMjesto.SelectedLookupValue = null;
            this.lkpProdajnoMjesto.Size = new System.Drawing.Size(268, 24);
            this.lkpProdajnoMjesto.TabIndex = 50;
            this.lkpProdajnoMjesto.ValueMember = "";
            // 
            // rvTest1
            // 
            this.rvTest1.LocalReport.ReportEmbeddedResource = "Damax.Reports.Report1.rdlc";
            this.rvTest1.Location = new System.Drawing.Point(12, 53);
            this.rvTest1.Name = "rvTest1";
            this.rvTest1.ServerReport.BearerToken = null;
            this.rvTest1.Size = new System.Drawing.Size(1000, 500);
            this.rvTest1.TabIndex = 0;
            // 
            // FormReport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 573);
            this.Controls.Add(this.lkpProdajnoMjesto);
            this.Controls.Add(this.btnPokreniIzvjestaj1);
            this.Controls.Add(this.rvTest1);
            this.Name = "FormReport1";
            this.Text = "FormReport1";
            this.Load += new System.EventHandler(this.FormReport1_Load);
            this.ResumeLayout(false);

		}

		#endregion
		private MySql.Data.MySqlClient.MySqlDataAdapter mySqlDataAdapter1;
		private System.Windows.Forms.Button btnPokreniIzvjestaj1;
		private Template.LookupComboBox lkpProdajnoMjesto;
        private Microsoft.Reporting.WinForms.ReportViewer rvTest1;
    }
}