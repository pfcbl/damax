﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Properties;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;

namespace Damax.Forms
{
    public partial class FormIzvPregledPrenosaArtikala : Form
    {
        public Magacin magacin { get; set; }

        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public DateTime? DatumOd { get; set; }

        public DateTime? DatumDo { get; set; }
        public FormIzvPregledPrenosaArtikala()
        {
            InitializeComponent();
        }
        public FormIzvPregledPrenosaArtikala(Magacin magacin)
        {
            InitializeComponent();
            this.magacin = magacin;
            lkpMagacin.Enabled = false;
        }


        private void FormPregledPrenosaArtikala_Load(object sender, EventArgs e)
        {

            lkpMagacin.RefreshLookup += lookup_RefreashLookup;
            lkpMagacin.LookupValueChanged += lookup_ValueChanged;
            DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
            DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
            SetDateControls();
            List<TemplateFilter> filters;
            if (magacin != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Magacin.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Magacin.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(Magacin.F_MagacinId, magacin.MagacinId.ToString(),"=")
                };
            else 
                filters = new List<TemplateFilter>() { new TemplateFilter(Magacin.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Magacin.F_DogadjajId,DogadjajId.ToString(),"=")};

            var source = (new MagacinDao()).Select(filters);

            lkpMagacin.DisplayMember = Magacin.F_Naziv;
            lkpMagacin.ValueMember = Magacin.F_MagacinId;
            lkpMagacin.DataSource = source;
            rbPoDanima.Checked = true;

        }


        private void SetDateControls()
        {
            dtDatumOd.Format = DateTimePickerFormat.Custom;
            dtDatumOd.CustomFormat = Resources.DateTime_Format;

            dtDatumDo.Format = DateTimePickerFormat.Custom;
            dtDatumDo.CustomFormat = Resources.DateTime_Format;

            if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
            if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
        }

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                var table = GeneralDao.PregledPrenosaArtikalaReport((Magacin) lkpMagacin.SelectedLookupValue,rbPoDanima.Checked?1:2, dtDatumOd.Value, dtDatumDo.Value);
                dataSet.Tables.Add(table);
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ZoomMode = ZoomMode.PageWidth;
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.PregledPrenosaArtikala.rdlc";
            var dataset = new DataSet("DataSet1");
            
            var magacinId = lkpMagacin.LookupMember;

            if (magacinId == null)
            {
                GetData(ref dataset);
            }
            else
            {
                GetData(ref dataset);
            }

            var reportDataSource = new ReportDataSource {Name = "DataSet1", Value = dataset.Tables[0]};

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            List<TemplateFilter> filters;
            if (magacin != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Magacin.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Magacin.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(Magacin.F_MagacinId, magacin.MagacinId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Magacin.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Magacin.F_DogadjajId,DogadjajId.ToString(),"=")};

            var source = (new MagacinDao()).Select(filters);
            lkpMagacin.DataSource = source;
        }

        private void lookup_ValueChanged(object sender, EventArgs e)
        {
            //ShowReport();
        }

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
