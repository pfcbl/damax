﻿using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Forms
{
	public partial class FormIzvReport1 : Form
	{
		private MySqlConnection conn;
		private MySqlCommand comm;
		
		public FormIzvReport1()
		{
			InitializeComponent();
		}

		private void FormReport1_Load(object sender, EventArgs e)
		{
			
			lkpProdajnoMjesto.RefreshLookup += lookup_RefreashLookup;
			lkpProdajnoMjesto.LookupValueChanged += lookup_ValueChanged;
			var source = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
																								,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,HomeFilters.HomeFilterEvent.DogadjajId.ToString(),"=")});

			lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
			lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_ProdajnoMjestoId;
			lkpProdajnoMjesto.DataSource = source;



            //// Set the processing mode for the ReportViewer to Local  
            //rvTest1.ProcessingMode = ProcessingMode.Local;

            //LocalReport localReport = rvTest1.LocalReport;

            //localReport.ReportEmbeddedResource = "Damax.Reports.Report1.rdlc";

            //DataSet dataset = new DataSet("DataSet1");

            //int prodajnoMjestoId = 1;

            ////dohvati podatke
            ////GetData(prodajnoMjestoId,ref dataset);
            //GetData(null,1,2019, ref dataset);

            ////kreiraj report sa podacima za dataset
            //ReportDataSource reportDataSource = new ReportDataSource();
            //reportDataSource.Name = "DataSet1";
            ////reportDataSource.Value = dataset.Tables["PrometZaProdajnoMjesto"];
            //reportDataSource.Value = dataset.Tables[0];

            //localReport.DataSources.Clear();
            //localReport.DataSources.Add(reportDataSource);

            //// Refresh report  
            //this.rvTest1.RefreshReport();
            this.rvTest1.RefreshReport();
        }



        public void GetData(int? prodajnoMjestoId, int dogadjajId, int godina, ref DataSet dataSet)
		{
			try
			{
				conn = ConnectionPool.checkOutConnection();
				comm = conn.CreateCommand();
				comm.CommandType = CommandType.StoredProcedure;
				comm.CommandText = "DohvatiPrometZaProdajnoMjesto";
				if (prodajnoMjestoId.HasValue)
				{
					comm.Parameters.AddWithValue("@pProdajno_mjesto_id", prodajnoMjestoId);
				}
				else
				{
					comm.Parameters.AddWithValue("@pProdajno_mjesto_id", DBNull.Value);
				}
				comm.Parameters.AddWithValue("@pDogadjajId", dogadjajId);
				comm.Parameters.AddWithValue("@pGodina", godina);
				
				MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(comm);
				mySqlDataAdapter.Fill(dataSet, "DataSet1");
				
				//comm.Connection.Close();
			}
			catch (MySqlException ex)
			{
				ConnectionPool.HandleMySQLDBException(ex);
			}
		}

		public List<ProdajnoMjesto> GetProdajnaMjesta(int dogadjajId, short godina)
		{
			List<ProdajnoMjesto> lista = new List<ProdajnoMjesto>();
			try
			{
				conn = ConnectionPool.checkOutConnection();
				comm = conn.CreateCommand();
				comm.CommandType = CommandType.Text;
				comm.CommandText = "select ProdajnoMjestoId,Naziv from damax.prodajno_mjesto where Godina = @godina and DogadjajId = @dogadjajId";
				comm.Parameters.AddWithValue("@godina", godina);
				comm.Parameters.AddWithValue("@dogadjajId", godina);
				comm.ExecuteNonQuery();

				MySqlDataReader reader = comm.ExecuteReader();
				while (reader.Read())
				{
					ProdajnoMjesto item = new ProdajnoMjesto();
					item.ProdajnoMjestoId = Convert.ToInt32(reader[0]);
					item.Naziv = reader[1].ToString();
					lista.Add(item);

				}
				reader.Close();
				ConnectionPool.checkInConnection(conn);
				return lista;

				//comm.Connection.Close();

			}
			catch (MySqlException ex)
			{
				ConnectionPool.HandleMySQLDBException(ex);
				return lista;
			}
		}

		protected void lookup_RefreashLookup(object sender, EventArgs e)
		{
			var source = (new ProdajnoMjestoDao()).Select(new List<TemplateFilter>() { new TemplateFilter(ProdajnoMjesto.F_Godina,HomeFilters.HomeFilterGodina.ToString(),"=")
																								,new TemplateFilter(ProdajnoMjesto.F_DogadjajId,HomeFilters.HomeFilterEvent.DogadjajId.ToString(),"=")});

			lkpProdajnoMjesto.DisplayMember = ProdajnoMjesto.F_Naziv;
			lkpProdajnoMjesto.ValueMember = ProdajnoMjesto.F_DogadjajId;
			lkpProdajnoMjesto.DataSource = source;
		}

		private void lookup_ValueChanged(object sender, EventArgs e)
		{
		}

		private void btnPokreniIzvjestaj1_Click(object sender, EventArgs e)
		{
            // Set the processing mode for the ReportViewer to Local  
            rvTest1.ProcessingMode = ProcessingMode.Local;
			LocalReport localReport = rvTest1.LocalReport;
			localReport.ReportEmbeddedResource = "Damax.Reports.Report1.rdlc";
			DataSet dataset = new DataSet("DataSet1");

			var godina = new TemplateFilter(ProdajnoMjesto.F_Godina, HomeFilters.HomeFilterGodina.ToString(), "=");
			var dogadjajId = new TemplateFilter(ProdajnoMjesto.F_DogadjajId, HomeFilters.HomeFilterEvent.DogadjajId.ToString(), "=");
			var prodajnoMjestoId = lkpProdajnoMjesto.LookupMember;

			//var test (ProdajnoMjesto)lkpProdajnoMjesto.SelectedLookupValue();
			//dohvati podatke
			int br;
			if (prodajnoMjestoId == null)
			{
				GetData(null, int.TryParse(dogadjajId.Value, out br) ? br:-1, int.TryParse(godina.Value,out br) ? br : -1, ref dataset);
			}
			else
			{
				GetData(int.TryParse(prodajnoMjestoId,out br) ? br : -1, int.TryParse(dogadjajId.Value,out br) ? br : -1, int.Parse(godina.Value), ref dataset);
			}
			

			//kreiraj report sa podacima za dataset
			ReportDataSource reportDataSource = new ReportDataSource();
			reportDataSource.Name = "DataSet1";
			//reportDataSource.Value = dataset.Tables["PrometZaProdajnoMjesto"];
			reportDataSource.Value = dataset.Tables[0];

			localReport.DataSources.Clear();
			localReport.DataSources.Add(reportDataSource);

			// Refresh report  
			this.rvTest1.RefreshReport();
		}
	}
}
