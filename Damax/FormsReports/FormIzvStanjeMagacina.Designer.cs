﻿namespace Damax.Forms
{
    partial class FormIzvStanjeMagacina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rvReport = new Microsoft.Reporting.WinForms.ReportViewer();
            this.lblDatum = new System.Windows.Forms.Label();
            this.dtDatumDo = new System.Windows.Forms.DateTimePicker();
            this.dtDatumOd = new System.Windows.Forms.DateTimePicker();
            this.btnPrikazi = new System.Windows.Forms.Button();
            this.lkpMagacin = new Damax.Template.LookupComboBox();
            this.SuspendLayout();
            // 
            // rvReport
            // 
            this.rvReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rvReport.Location = new System.Drawing.Point(12, 88);
            this.rvReport.Name = "rvReport";
            this.rvReport.ServerReport.BearerToken = null;
            this.rvReport.Size = new System.Drawing.Size(858, 586);
            this.rvReport.TabIndex = 0;
            this.rvReport.WaitControlDisplayAfter = 10;
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDatum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.lblDatum.Location = new System.Drawing.Point(9, 41);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(68, 13);
            this.lblDatum.TabIndex = 7;
            this.lblDatum.Text = "Datum od-do";
            // 
            // dtDatumDo
            // 
            this.dtDatumDo.Location = new System.Drawing.Point(163, 57);
            this.dtDatumDo.Name = "dtDatumDo";
            this.dtDatumDo.Size = new System.Drawing.Size(144, 20);
            this.dtDatumDo.TabIndex = 6;
            // 
            // dtDatumOd
            // 
            this.dtDatumOd.Location = new System.Drawing.Point(12, 57);
            this.dtDatumOd.Name = "dtDatumOd";
            this.dtDatumOd.Size = new System.Drawing.Size(145, 20);
            this.dtDatumOd.TabIndex = 5;
            // 
            // btnPrikazi
            // 
            this.btnPrikazi.BackColor = System.Drawing.Color.White;
            this.btnPrikazi.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrikazi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrikazi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnPrikazi.Location = new System.Drawing.Point(327, 47);
            this.btnPrikazi.Name = "btnPrikazi";
            this.btnPrikazi.Size = new System.Drawing.Size(75, 30);
            this.btnPrikazi.TabIndex = 8;
            this.btnPrikazi.Text = "Prikaži";
            this.btnPrikazi.UseVisualStyleBackColor = false;
            this.btnPrikazi.Click += new System.EventHandler(this.btnPrikazi_Click);
            // 
            // lkpMagacin
            // 
            this.lkpMagacin.BackColor = System.Drawing.Color.White;
            this.lkpMagacin.ComboSelectedIndex = -1;
            this.lkpMagacin.DataSource = null;
            this.lkpMagacin.DeleteButtonEnable = true;
            this.lkpMagacin.DisplayMember = "";
            this.lkpMagacin.Location = new System.Drawing.Point(12, 17);
            this.lkpMagacin.LookupMember = null;
            this.lkpMagacin.Name = "lkpMagacin";
            this.lkpMagacin.RefreashButtonEnable = true;
            this.lkpMagacin.SelectedLookupValue = null;
            this.lkpMagacin.Size = new System.Drawing.Size(295, 21);
            this.lkpMagacin.TabIndex = 1;
            this.lkpMagacin.ValueMember = "";
            // 
            // FormIzvStanjeMagacina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 686);
            this.Controls.Add(this.btnPrikazi);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.dtDatumDo);
            this.Controls.Add(this.dtDatumOd);
            this.Controls.Add(this.rvReport);
            this.Controls.Add(this.lkpMagacin);
            this.Name = "FormIzvStanjeMagacina";
            this.Text = "Pregled stannja magacina";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormStanjeMagacina_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rvReport;
        private Template.LookupComboBox lkpMagacin;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.DateTimePicker dtDatumDo;
        private System.Windows.Forms.DateTimePicker dtDatumOd;
        private System.Windows.Forms.Button btnPrikazi;
    }
}