﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;

namespace Damax.Forms
{
    public partial class FormIzvPregledPrenosnica : Form
    {
        public int? prenosnicaId { get; set; }

        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        private DataTable tempTable;
        public FormIzvPregledPrenosnica()
        {
            InitializeComponent();
        }
        public FormIzvPregledPrenosnica(int? prenosnicaId)
        {
            InitializeComponent();
            this.prenosnicaId = prenosnicaId;
            lkpPrenosnica.Enabled = false;
        }

        public FormIzvPregledPrenosnica(DataTable table)
        {
            InitializeComponent();
            lkpPrenosnica.Enabled = false;
            
            tempTable = table.Clone();
            foreach (DataRow row in table.Rows)
            {
                tempTable.Rows.Add(row.ItemArray);
            }
            btnSave.Visible = true;
            btnIzmijeni.Visible = true;
            ShowReport();
        }


        private void FormPregledPrenosnica_Load(object sender, EventArgs e)
        {

            lkpPrenosnica.RefreshLookup += lookup_RefreashLookup;
            lkpPrenosnica.LookupValueChanged += lookup_ValueChanged;
            DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            List<TemplateFilter> filters;
            if (prenosnicaId != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Prenosnica.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Prenosnica.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(Prenosnica.F_PrenosnicaId, prenosnicaId.ToString(),"=")
                };
            else 
                filters = new List<TemplateFilter>() { new TemplateFilter(Prenosnica.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Prenosnica.F_DogadjajId,DogadjajId.ToString(),"=")
                                                      ,new TemplateFilter(Prenosnica.F_TipPrenosa,"'P'","=")};


            var source = (new PrenosnicaDao()).Select(filters);

            lkpPrenosnica.DisplayMember = Prenosnica.F_PrenosnicaLookup;
            lkpPrenosnica.ValueMember = Prenosnica.F_PrenosnicaId;
            lkpPrenosnica.DataSource = source;

        }
        

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                    var table = GeneralDao.PregledPrenosnicaReport((Prenosnica)lkpPrenosnica.SelectedLookupValue);
                    dataSet.Tables.Add(table);
                
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            //rvReport.ZoomMode = ZoomMode.PageWidth;
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.PregledPrenosnica.rdlc";
            var dataset = new DataSet("DataSet1");
            
            var prenosnicaId = lkpPrenosnica.LookupMember;

            if (prenosnicaId != null)
            {
                GetData(ref dataset);
            }
            else
            {
           
                dataset.Tables.Add(tempTable);
            }

            var reportDataSource = new ReportDataSource {Name = "DataSet1", Value = dataset.Tables[0]};

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        protected void lookup_RefreashLookup(object sender, EventArgs e)
        {
            List<TemplateFilter> filters;
            if (prenosnicaId != null)
                filters = new List<TemplateFilter>()
                {
                     new TemplateFilter(Prenosnica.F_Godina, Godina.ToString(), "=")
                    ,new TemplateFilter(Prenosnica.F_DogadjajId, DogadjajId.ToString(),"=")
                    ,new TemplateFilter(Prenosnica.F_PrenosnicaId, prenosnicaId.ToString(),"=")
                };
            else
                filters = new List<TemplateFilter>() { new TemplateFilter(Prenosnica.F_Godina,Godina.ToString(),"=")
                                                      ,new TemplateFilter(Prenosnica.F_DogadjajId,DogadjajId.ToString(),"=")
                                                      ,new TemplateFilter(Prenosnica.F_TipPrenosa,"'P'","=")};

            var source = (new PrenosnicaDao()).Select(filters);
            lkpPrenosnica.DataSource = source;
        }

        private void lookup_ValueChanged(object sender, EventArgs e)
        {
            if(tempTable==null)
            ShowReport();
        }
        

        private void btnIzmijeni_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult rez = MessageBox.Show("Jeste li sigurni da želite da sačuvate ovu prenosnicu?", "Prenos artikala", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (rez == DialogResult.Yes)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
