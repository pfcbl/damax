﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Damax.DAO;
using Damax.Models;
using Damax.Template;
using Microsoft.Reporting.WinForms;
using MySql.Data.MySqlClient;
using Damax.Properties;

namespace Damax.Forms
{
    public partial class FormIzvBrojIzdatihKarticaPoSatima : Form
    {
        public Blagajna blagajna { get; set; }

        public int? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public DateTime? DatumOd { get; set; }

        public DateTime? DatumDo { get; set; }

        public FormIzvBrojIzdatihKarticaPoSatima()
        {
            InitializeComponent();
        }
        //public FormBrojIzdatihKarticaPoSatima(Blagajna blagajna)
        //{
        //    InitializeComponent();
        //    this.blagajna = blagajna;
        //    lkpBlagajna.Enabled = false;
        //}

        private void FormBrojIzdatihKarticaPoSatima_Load(object sender, EventArgs e)
        {
			//lkpBlagajna.RefreshLookup += lookup_RefreashLookup;
			//lkpBlagajna.LookupValueChanged += lookup_ValueChanged;
						DogadjajId = HomeFilters.HomeFilterEvent.DogadjajId;
            Godina = HomeFilters.HomeFilterEvent.Godina;
            DatumOd = HomeFilters.HomeFilterEvent.DatumOd;
            DatumDo = HomeFilters.HomeFilterEvent.DatumDo;
            SetDateControls();

            //List<TemplateFilter> filters;
            //if (blagajna != null)
            //    filters = new List<TemplateFilter>()
            //    {
            //         new TemplateFilter(Blagajna.F_Godina, Godina.ToString(), "=")
            //        ,new TemplateFilter(Blagajna.F_DogadjajId, BlagajnaId.ToString(),"=")
            //        ,new TemplateFilter(Blagajna.F_BlagajnaId, blagajna.BlagajnaId.ToString(),"=")
            //    };
            //else
            //    filters = new List<TemplateFilter>() { new TemplateFilter(Blagajna.F_Godina,Godina.ToString(),"=")
            //                                          ,new TemplateFilter(Blagajna.F_BlagajnaId,BlagajnaId.ToString(),"=")};

            //var source = (new ProdajnoMjestoDao()).Select(filters);

            //lkpBlagajna.DisplayMember = Blagajna.F_Naziv;
            //lkpBlagajna.ValueMember = Blagajna.F_BlagajnaId;
            //lkpBlagajna.DataSource = source;
        }

        private void SetDateControls()
        {
            dtDatumOd.Format = DateTimePickerFormat.Custom;
            dtDatumOd.CustomFormat = Resources.DateTime_Format;

            dtDatumDo.Format = DateTimePickerFormat.Custom;
            dtDatumDo.CustomFormat = Resources.DateTime_Format;

            if (DatumOd != null) dtDatumOd.Value = DatumOd.Value;
            if (DatumDo != null) dtDatumDo.Value = DatumDo.Value;
        }

        public void GetData(ref DataSet dataSet)
        {
            try
            {
                
                    var table = GeneralDao.BrojIzdatihKarticaPoSatima(Godina,DogadjajId, dtDatumOd.Value, dtDatumDo.Value);
                    dataSet.Tables.Add(table);
                
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
            }
        }

        private void ShowReport()
        {
            rvReport.ProcessingMode = ProcessingMode.Local;
            var localReport = rvReport.LocalReport;
            localReport.ReportEmbeddedResource = "Damax.Reports.BrojIzdatihKarticiPoSatima.rdlc";
            var dataset = new DataSet("DataSet1");

            
                GetData(ref dataset);
            
              
            

            if (dataset.Tables.Count <= 0) return;
            var reportDataSource = new ReportDataSource { Name = "DataSet1", Value = dataset.Tables[0] };

            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            this.rvReport.RefreshReport();
        }

        

       

        private void btnPrikazi_Click(object sender, EventArgs e)
        {
            ShowReport();
        }
    }
}
