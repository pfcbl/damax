﻿namespace Damax.Template
{
    partial class TempTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtDiv1Tb = new MaterialSkin.Controls.MaterialDivider();
            this.mtDiv2Tb = new MaterialSkin.Controls.MaterialDivider();
            this.mtDiv3Tb = new MaterialSkin.Controls.MaterialDivider();
            this.mtDiv4Tb = new MaterialSkin.Controls.MaterialDivider();
            this.SuspendLayout();
            // 
            // mtDiv1Tb
            // 
            this.mtDiv1Tb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mtDiv1Tb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.mtDiv1Tb.Depth = 0;
            this.mtDiv1Tb.Location = new System.Drawing.Point(0, 0);
            this.mtDiv1Tb.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtDiv1Tb.Name = "mtDiv1Tb";
            this.mtDiv1Tb.Size = new System.Drawing.Size(166, 1);
            this.mtDiv1Tb.TabIndex = 1;
            this.mtDiv1Tb.Visible = false;
            // 
            // mtDiv2Tb
            // 
            this.mtDiv2Tb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mtDiv2Tb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.mtDiv2Tb.Depth = 0;
            this.mtDiv2Tb.Location = new System.Drawing.Point(0, 20);
            this.mtDiv2Tb.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtDiv2Tb.Name = "mtDiv2Tb";
            this.mtDiv2Tb.Size = new System.Drawing.Size(166, 1);
            this.mtDiv2Tb.TabIndex = 2;
            this.mtDiv2Tb.Visible = false;
            // 
            // mtDiv3Tb
            // 
            this.mtDiv3Tb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.mtDiv3Tb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.mtDiv3Tb.Depth = 0;
            this.mtDiv3Tb.Location = new System.Drawing.Point(0, 0);
            this.mtDiv3Tb.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtDiv3Tb.Name = "mtDiv3Tb";
            this.mtDiv3Tb.Size = new System.Drawing.Size(1, 21);
            this.mtDiv3Tb.TabIndex = 3;
            this.mtDiv3Tb.Visible = false;
            // 
            // mtDiv4Tb
            // 
            this.mtDiv4Tb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mtDiv4Tb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.mtDiv4Tb.Depth = 0;
            this.mtDiv4Tb.Location = new System.Drawing.Point(165, 0);
            this.mtDiv4Tb.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtDiv4Tb.Name = "mtDiv4Tb";
            this.mtDiv4Tb.Size = new System.Drawing.Size(1, 21);
            this.mtDiv4Tb.TabIndex = 4;
            this.mtDiv4Tb.Visible = false;
            // 
            // TempTextBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.mtDiv4Tb);
            this.Controls.Add(this.mtDiv3Tb);
            this.Controls.Add(this.mtDiv2Tb);
            this.Controls.Add(this.mtDiv1Tb);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.Name = "TempTextBox";
            this.Size = new System.Drawing.Size(166, 21);
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialDivider mtDiv1Tb;
        private MaterialSkin.Controls.MaterialDivider mtDiv2Tb;
        private MaterialSkin.Controls.MaterialDivider mtDiv3Tb;
        private MaterialSkin.Controls.MaterialDivider mtDiv4Tb;
    }
}
