﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media;
using Damax.DAO;
using Damax.Template;
using MySql.Data;
using Color = System.Drawing.Color;

namespace Damax.Forms
{
    public partial class FormTemplate<T> : Form where T:class,new()
    {
        public int FormMode = Constants.Form_Mode.MODE_VIEW;
        public T SelectedObject;
        public List<T> Table=new List<T>();
        public ITemplateDao<T> DaoObject;
        public List<TemplateFilter> Filters=new List<TemplateFilter>(); 
        public List<Form> NextForms=new List<Form>();
        public List<Form> ReportForms = new List<Form>();
        private List<ToolStripMenuItem> NextMenuItemsList=new List<ToolStripMenuItem>();
        private List<ToolStripMenuItem> ReporttsMenuItemsList = new List<ToolStripMenuItem>();
        public bool IsValid = true;
        public bool IsInNextMode = false;

        public FormTemplate()
        {
            InitializeComponent();
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_VIEW,true);
            LockUnlockMenuButtons(true,true,true);
            SelectedObject=new T();
            tsMenu.Renderer = new MenuRender();
        }

        //kada se mijenja mod forme,ako treba custom - override ove metode,poziv base metode pa onda custom kod
        public void FormModeChanged(int previewState, int currentState,bool isFirst)
        {
            FormMode = currentState;
            if (currentState == Constants.Form_Mode.MODE_ADD)
            {
                if(!isFirst)
                    Temp_enableDisableControls(previewState,currentState,true);
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
                tsMenu.Enabled = false;
                dgvItems.Enabled = false;
            }
            else if (currentState == Constants.Form_Mode.MODE_EDIT)
            {
                if (!isFirst)
                    Temp_enableDisableControls(previewState, currentState, true);
                btnSave.Enabled = true;
                btnCancel.Enabled = true;
                tsMenu.Enabled = false;
                dgvItems.Enabled = false;
            }
            else
            {
                if (!isFirst)
                    Temp_enableDisableControls(previewState, currentState, false);
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
                tsMenu.Enabled = true;
                dgvItems.Enabled = true;
            }
        }

        public virtual void Temp_BtnSaveYes()
        {
            errorProvider.Clear();
            if (!ValidateControls())
            {
                MessageBox.Show("Polja koja su boldirana su obavezna za unos.", "Nisu unesena obavezna polja.",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                IsValid = true;
                return;
            }
            if (FormMode == Constants.Form_Mode.MODE_ADD)
                AddObject();
            else if (FormMode == Constants.Form_Mode.MODE_EDIT)
                EditObject();
            RefreshGrid();
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_VIEW,false);
        }


        public virtual void Temp_BtnSaveNo()
        {
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_VIEW,false);
        }

        public virtual void Temp_BtnCancel()
        {
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_VIEW,false);
            Temp_ItemChanged();
        }

        public virtual void Temp_BtnAdd()
        {
            ClearControls();
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_ADD,false);
        }

        public virtual void Temp_BtnEdit()
        {
            FormModeChanged(FormMode, Constants.Form_Mode.MODE_EDIT,false);
        }

        public virtual void Temp_FormActivated()
        {
        }

        public virtual void Temp_BtnDelete()
        {
            DialogResult result = MessageBox.Show("Da li zelite da obrišete stavku?", "Brisanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                DeleteObject();
                RefreshGrid();
            }
        }

        public void Temp_BtnForwardUp()
        {
            int rowIndex = dgvItems.Rows.Count - 1;
            if (rowIndex >= 0)
            {
                if(dgvItems.CurrentRow!=null && dgvItems.CurrentRow.Index>0)
                    ClearControls();
                dgvItems.CurrentCell = dgvItems.Rows[0].Cells[0];
            }
        }

        public void Temp_BtnUp()
        {
            int rowIndex = dgvItems.Rows.Count - 1;
            if (rowIndex >= 0)
            {
                if (dgvItems.CurrentRow != null && dgvItems.CurrentRow.Index > 0)
                {
                    ClearControls();
                    dgvItems.CurrentCell = dgvItems.Rows[dgvItems.CurrentRow.Index - 1].Cells[0];
                }
            }
        }

        public void Temp_BtnDown()
        {
            int rowIndex = dgvItems.Rows.Count - 1;
            if (rowIndex >= 0)
            {
                if (dgvItems.CurrentRow != null && dgvItems.CurrentRow.Index < rowIndex)
                {
                    ClearControls();
                    dgvItems.CurrentCell = dgvItems.Rows[dgvItems.CurrentRow.Index + 1].Cells[0];
                }
            }
        }

        public void Temp_BtnForwardDown()
        {
            int rowIndex = dgvItems.Rows.Count - 1;
            if (rowIndex >= 0)
            {
                if(dgvItems.CurrentRow!=null && dgvItems.CurrentRow.Index<rowIndex)
                ClearControls();
                dgvItems.CurrentCell = dgvItems.Rows[rowIndex].Cells[0];
            }
        }

        public virtual void Temp_ItemChanged()
        {
            if (dgvItems.SelectedRows.Count > 0)
            {
                var selectedRow = dgvItems.SelectedRows[0];
                SelectedObject = (T)selectedRow.DataBoundItem;
                ClearControls();
                FillControls();
                AddNextForms();
                AddNextMenuIetms();
                AddReportForms();
                AddReportsMenuIetms();

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Da li zelite da odbacite izmjene koje ste uradili?", "Pitanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                errorProvider.Clear();
                Temp_BtnCancel();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //DialogResult result = MessageBox.Show("Da li zelite da sacuvate izmjene?", "Snimanje", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //if (result == DialogResult.Yes)
                Temp_BtnSaveYes();
            //else if (result == DialogResult.No)
            //{
            //    Temp_BtnSaveNo();
            //}
        }

        private void TsBtnRefresh_Click(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void TsBtnAdd_Click(object sender, EventArgs e)
        {
            Temp_BtnAdd();
        }

        private void TsBtnEdit_Click(object sender, EventArgs e)
        {
            if(SelectedObject!=null && Table!=null && Table.Count>0)
                Temp_BtnEdit();
            else
            {
                MessageBox.Show("Nije odabrana ni jedna stavka.", "Nije odabrana stavka.", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void TsBtnDelete_Click(object sender, EventArgs e)
        {
            if (SelectedObject != null && Table != null && Table.Count > 0)
                Temp_BtnDelete();
            else
            {
                MessageBox.Show("Nije odabrana ni jedna stavka.", "Nije odabrana stavka.", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void TsBtnForwardUp_Click(object sender, EventArgs e)
        {
                Temp_BtnForwardUp();
        }

        private void TsBtnUp_Click(object sender, EventArgs e)
        {
            Temp_BtnUp();
        }

        private void TsBtnDown_Click(object sender, EventArgs e)
        {
            Temp_BtnDown();
        }

        private void TsBtnForwardDown_Click(object sender, EventArgs e)
        {
            Temp_BtnForwardDown();
        }

        private void DgvItems_SelectionChanged(object sender, EventArgs e)
        {
            Temp_ItemChanged();
        }
        

        public virtual void DgvItemsSettings()
        {
            //dgvItems.AutoSizeColumnsMode=DataGridViewAutoSizeColumnsMode.DisplayedCells;

            dgvItems.ColumnHeadersDefaultCellStyle.BackColor = Color.White;
            dgvItems.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(18, 86, 136);

            //this.dgvItems.GridColor = Color.LightGray;//.FromArgb(18, 86, 136);
            //this.dgvItems.BorderStyle = BorderStyle.Fixed3D;
            //this.dgvItems.CellBorderStyle =
            //    DataGridViewCellBorderStyle.SingleHorizontal;
            //this.dgvItems.RowHeadersBorderStyle =
            //    DataGridViewHeaderBorderStyle.Single;
            //this.dgvItems.ColumnHeadersBorderStyle =
            //    DataGridViewHeaderBorderStyle.Single;

            dgvItems.BorderStyle = BorderStyle.None;
            dgvItems.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 238, 238);
            dgvItems.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgvItems.DefaultCellStyle.SelectionBackColor = Color.FromArgb(18, 86, 136);
            dgvItems.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgvItems.BackgroundColor = Color.White;
            

        }

        #region moraju se override u formi naslednici
        public virtual void FillControls()
        {
        }

        public virtual void FillObject()
        {
        }

        public virtual void ClearControls()
        {
        }

        public virtual void AddNextForms()
        {
            NextForms.Clear();
        }

        public virtual void AddReportForms()
        {
            ReportForms.Clear();
        }

        public virtual bool ValidateControls()
        {
            return IsValid;
        }

        //mora se overajdovati. U nju se dodaju sve kontrole koje se dodaju na formu i na osnovu propertija isEnable postavlja se da li je enable ili disable
        public virtual void Temp_enableDisableControls(int previewState,int currentState,bool isEnable)
        {
        }


        #endregion

        private void AddNextMenuIetms()
        {
            if (NextMenuItemsList.Count > 0)
            {
                foreach (ToolStripMenuItem menuItem in NextMenuItemsList)
                {
                   // if(cmsDgItems.Items.Contains(menuItem))
                        cmsDgItems.Items.Remove(menuItem);
                }

                NextMenuItemsList.Clear();
            }
            for (int i=0;i<NextForms.Count;i++)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(NextForms[i].Text)
                {
                    Name = i.ToString(),
                    Image = Properties.Resources.next_button
                };
                item.Click += new EventHandler(MenuItem_Click);
                cmsDgItems.Items.Add(item);
                NextMenuItemsList.Add(item);
            }
        }

        private void MenuItem_Click(object sender,EventArgs args)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            NextForms[Convert.ToInt32(item.Name)].ShowDialog();
        }

        private void AddReportsMenuIetms()
        {
                izvjestajiToolStripMenuItem.DropDownItems.Clear();
                ReporttsMenuItemsList.Clear();
         
            for (int i = 0; i < ReportForms.Count; i++)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(ReportForms[i].Text)
                {
                    Name = i.ToString(),
                    Image = Properties.Resources.next_button,
                    ForeColor=Color.White
                };
                item.Click += new EventHandler(MenuReportItem_Click);
                izvjestajiToolStripMenuItem.DropDownItems.Add(item);
                ReporttsMenuItemsList.Add(item);
            }
        }

        private void MenuReportItem_Click(object sender, EventArgs args)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            ReportForms[Convert.ToInt32(item.Name)].ShowDialog();
        }
        public virtual bool AddObject()
        {
            FillObject();
            return DaoObject.Insert(SelectedObject);
            //var rez= DaoObject.Insert(SelectedObject);
            //if (rez == Constants.SQL_OK)
            //{
            //    return true;
            //}
            //{

            //    switch (rez )
            //    {
            //        case 1062:
            //            MessageBox.Show("Objekat već postoji.", "Nije moguće sačuvati." , MessageBoxButtons.OK,MessageBoxIcon.Error);
            //            break;
            //        default:
            //            MessageBox.Show("Desila se greška u radu sa bazom.", "Nije moguće sačuvati.", MessageBoxButtons.OK,MessageBoxIcon.Error);
            //            break;

            //    }
            //    return false;
            //}



        }

        public virtual bool EditObject()
        {
            FillObject();
            return DaoObject.Update(SelectedObject);
            //int rez = DaoObject.Update(SelectedObject);
            //if (rez == Constants.SQL_OK)
            //{
            //    return true;
            //}
            //{

            //    switch (rez)
            //    {
            //        case 1062:
            //            MessageBox.Show("Objekat već postoji.", "Nije moguće sačuvati.", MessageBoxButtons.OK,MessageBoxIcon.Error);
            //            break;
            //        default:
            //            MessageBox.Show("Desila se greška u radu sa bazom.", "Nije moguće sačuvati.",
            //                MessageBoxButtons.OK,MessageBoxIcon.Error);
            //            break;

            //    }
            //    return false;

            //}
        }

        public virtual bool DeleteObject()
        {
            return DaoObject.Delete(SelectedObject);
            //int rez = DaoObject.Delete(SelectedObject);
            //if (rez == Constants.SQL_OK)
            //{
            //    return true;
            //}


            //            MessageBox.Show("Za ovaj podatak postoje povezani drugi podaci. Da bi obrisali ovaj podatak morate prvo da obrišete sve podatke koji su vezani za njega.", "Nije moguće brisanje podatka.",
            //                MessageBoxButtons.OK, MessageBoxIcon.Error);

            //    return false;


        }

        public virtual bool RefreshGrid()
        {
            SetDgvItemsDataSource();
            return true;
        }

        public void SetDgvItemsDataSource()
        {
            if (DaoObject != null)
            {
                Table = DaoObject.Select(Filters);
                dgvItems.DataSource = Table;
                DgvItemsSettings();
            }

        }

        private void FormTemplate_Load(object sender, EventArgs e)
        {
            SetDgvItemsDataSource();
            //AddNextForms();
           // AddNextMenuIetms();
            AddReportForms();
            AddReportsMenuIetms();
            this.FormModeChanged(FormMode, Constants.Form_Mode.MODE_VIEW, false);
        }

        private void DgvItems_RowContextMenuStripNeeded(object sender, DataGridViewRowContextMenuStripNeededEventArgs e)
        {
            e.ContextMenuStrip = cmsDgItems;
        }

        public void LockUnlockMenuButtons(bool isAdd,bool isEdit,bool isDelete)
        {
            tsBtnAdd.Enabled = isAdd;
            tsBtnEdit.Enabled = isEdit;
            tsBtnDelete.Enabled = isDelete;
            tsDgvEdit.Enabled = isEdit;
            tsDgvDelete.Enabled = isDelete;
        }

        public DataGridView GetDataGridView()
        {
            return dgvItems;
        }

        private void FormTemplate_SizeChanged(object sender, EventArgs e)
        {
            
        }

        public ErrorProvider GetErrorProvider()
        {
            return errorProvider;
        }

        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MAXIMIZE = 0xF030;
        const int SC_RESTORE = 0xF120;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                if (m.WParam == new IntPtr(SC_MAXIMIZE))
                {
                    dgvItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }

                else if (m.WParam == new IntPtr(SC_RESTORE))
                {
                    dgvItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    DgvItemsSettings();
                }
            }
            base.WndProc(ref m);
        }

        private void FormTemplate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Escape)
            {
                this.Close();
            }

        }

        private void DgvItems_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var hti = dgvItems.HitTest(e.X, e.Y);
                dgvItems.ClearSelection();
                if (hti.RowIndex < 0) return;
                dgvItems.Rows[hti.RowIndex].Selected = true;
            }
        }

        //private void ImportDataGridViewDataToExcelSheet()
        //{

        //    Excel.Application xlApp;
        //    Excel.Workbook xlWorkBook;
        //    Excel.Worksheet xlWorkSheet;
        //    object misValue = System.Reflection.Missing.Value;

        //    xlApp = new Excel.Application();
        //    xlWorkBook = xlApp.Workbooks.Add(misValue);
        //    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


        //    for (int x = 1; x < dgvItems.Columns.Count + 1; x++)
        //    {
        //        xlWorkSheet.Cells[1, x] = dgvItems.Columns[x - 1].HeaderText;
        //    }

        //    for (int i = 0; i < dgvItems.Rows.Count; i++)
        //    {
        //        for (int j = 0; j < dgvItems.Columns.Count; j++)
        //        {
        //            xlWorkSheet.Cells[i + 2, j + 1] = dgvItems.Rows[i].Cells[j].Value.ToString();
        //        }
        //    }

        //    var saveFileDialoge = new SaveFileDialog();
        //    saveFileDialoge.FileName = "ReportView";
        //    saveFileDialoge.DefaultExt = ".xlsx";
        //    if (saveFileDialoge.ShowDialog() == DialogResult.OK)
        //    {
        //        xlWorkBook.SaveAs(saveFileDialoge.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        //    }


        //    xlWorkBook.Close(true, misValue, misValue);
        //    xlApp.Quit();

        //    releaseObject(xlWorkSheet);
        //    releaseObject(xlWorkBook);
        //    releaseObject(xlApp);
        //}

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }
        
    }
}
