﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.DAO
{
    public class TemplateFilter
    {
        public TemplateFilter(string name, string value, string filterOperator,string alias)
        {
            this.Name = name;
            this.Value = value;
            this.FilterOperator = filterOperator;
            this.Alias = alias;
        }

        public TemplateFilter(string name, string value, string filterOperator)
        {
            this.Name = name;
            this.Value = value;
            this.FilterOperator = filterOperator;
            this.Alias = "b";
        }

        public string Name { get; set; }

        public string Value { get; set; }

        public string FilterOperator { get; set; }

        public string Alias { get; set; }
    }
}
