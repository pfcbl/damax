﻿using System.Drawing;
using System.Windows.Forms;

namespace Damax.Forms
{
    partial class FormTemplate<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvItems = new System.Windows.Forms.DataGridView();
            this.cmsDgItems = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsDgvEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDgvDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenu = new System.Windows.Forms.MenuStrip();
            this.tsBtnRefresh = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnAdd = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnEdit = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnDelete = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnForwardUp = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnUp = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnDown = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.tsBtnForwardDown = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.izvjestajiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.md4Grid = new MaterialSkin.Controls.MaterialDivider();
            this.md1Grid = new MaterialSkin.Controls.MaterialDivider();
            this.md3Grid = new MaterialSkin.Controls.MaterialDivider();
            this.md2Grid = new MaterialSkin.Controls.MaterialDivider();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.ttMenu = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
            this.cmsDgItems.SuspendLayout();
            this.tsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvItems
            // 
            this.dgvItems.AllowUserToAddRows = false;
            this.dgvItems.AllowUserToDeleteRows = false;
            this.dgvItems.AllowUserToOrderColumns = true;
            this.dgvItems.AllowUserToResizeRows = false;
            this.dgvItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvItems.BackgroundColor = System.Drawing.Color.White;
            this.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvItems.ColumnHeadersHeight = 40;
            this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItems.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvItems.EnableHeadersVisualStyles = false;
            this.dgvItems.Location = new System.Drawing.Point(12, 314);
            this.dgvItems.MultiSelect = false;
            this.dgvItems.Name = "dgvItems";
            this.dgvItems.ReadOnly = true;
            this.dgvItems.RowHeadersVisible = false;
            this.dgvItems.RowTemplate.Height = 30;
            this.dgvItems.RowTemplate.ReadOnly = true;
            this.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItems.Size = new System.Drawing.Size(856, 304);
            this.dgvItems.TabIndex = 7;
            this.dgvItems.RowContextMenuStripNeeded += new System.Windows.Forms.DataGridViewRowContextMenuStripNeededEventHandler(this.DgvItems_RowContextMenuStripNeeded);
            this.dgvItems.SelectionChanged += new System.EventHandler(this.DgvItems_SelectionChanged);
            this.dgvItems.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DgvItems_MouseDown);
            // 
            // cmsDgItems
            // 
            this.cmsDgItems.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDgvEdit,
            this.tsDgvDelete});
            this.cmsDgItems.Name = "cmsDgItems";
            this.cmsDgItems.Size = new System.Drawing.Size(136, 48);
            // 
            // tsDgvEdit
            // 
            this.tsDgvEdit.Image = global::Damax.Properties.Resources.edit;
            this.tsDgvEdit.Name = "tsDgvEdit";
            this.tsDgvEdit.Size = new System.Drawing.Size(135, 22);
            this.tsDgvEdit.Text = "Izmijeni red";
            this.tsDgvEdit.Click += new System.EventHandler(this.TsBtnEdit_Click);
            // 
            // tsDgvDelete
            // 
            this.tsDgvDelete.Image = global::Damax.Properties.Resources.delete;
            this.tsDgvDelete.Name = "tsDgvDelete";
            this.tsDgvDelete.Size = new System.Drawing.Size(135, 22);
            this.tsDgvDelete.Text = "Obrisi red";
            this.tsDgvDelete.Click += new System.EventHandler(this.TsBtnDelete_Click);
            // 
            // tsMenu
            // 
            this.tsMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.tsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnRefresh,
            this.tsBtnAdd,
            this.tsBtnEdit,
            this.tsBtnDelete,
            this.tsBtnForwardUp,
            this.tsBtnUp,
            this.tsBtnDown,
            this.tsBtnForwardDown,
            this.izvjestajiToolStripMenuItem});
            this.tsMenu.Location = new System.Drawing.Point(0, 0);
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.Size = new System.Drawing.Size(880, 51);
            this.tsMenu.TabIndex = 13;
            this.tsMenu.Text = "menuStrip1";
            // 
            // tsBtnRefresh
            // 
            this.tsBtnRefresh.AutoSize = false;
            this.tsBtnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnRefresh.Image = global::Damax.Properties.Resources.osvjezi2;
            this.tsBtnRefresh.Name = "tsBtnRefresh";
            this.tsBtnRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.tsBtnRefresh.Size = new System.Drawing.Size(47, 47);
            this.tsBtnRefresh.Tag = "Osvježi";
            this.tsBtnRefresh.Text = "Osvježi";
            this.tsBtnRefresh.ToolTipText = "Učitaj ponovo";
            this.tsBtnRefresh.Click += new System.EventHandler(this.TsBtnRefresh_Click);
            // 
            // tsBtnAdd
            // 
            this.tsBtnAdd.AutoSize = false;
            this.tsBtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnAdd.Image = global::Damax.Properties.Resources.add_2;
            this.tsBtnAdd.Name = "tsBtnAdd";
            this.tsBtnAdd.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsBtnAdd.Size = new System.Drawing.Size(47, 47);
            this.tsBtnAdd.Tag = "Dodaj";
            this.tsBtnAdd.Text = "Dodaj";
            this.tsBtnAdd.ToolTipText = "Dodaj";
            this.tsBtnAdd.Click += new System.EventHandler(this.TsBtnAdd_Click);
            // 
            // tsBtnEdit
            // 
            this.tsBtnEdit.AutoSize = false;
            this.tsBtnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnEdit.Image = global::Damax.Properties.Resources.izmijeni;
            this.tsBtnEdit.Name = "tsBtnEdit";
            this.tsBtnEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.tsBtnEdit.Size = new System.Drawing.Size(47, 47);
            this.tsBtnEdit.Tag = "Izmijeni";
            this.tsBtnEdit.Text = "Izmijeni";
            this.tsBtnEdit.ToolTipText = "Izmijeni";
            this.tsBtnEdit.Click += new System.EventHandler(this.TsBtnEdit_Click);
            // 
            // tsBtnDelete
            // 
            this.tsBtnDelete.AutoSize = false;
            this.tsBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnDelete.Image = global::Damax.Properties.Resources.izbrisi;
            this.tsBtnDelete.Name = "tsBtnDelete";
            this.tsBtnDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.tsBtnDelete.Size = new System.Drawing.Size(47, 47);
            this.tsBtnDelete.Tag = "Izbriši";
            this.tsBtnDelete.Text = "Izbriši";
            this.tsBtnDelete.ToolTipText = "Obriši";
            this.tsBtnDelete.Click += new System.EventHandler(this.TsBtnDelete_Click);
            // 
            // tsBtnForwardUp
            // 
            this.tsBtnForwardUp.AutoSize = false;
            this.tsBtnForwardUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnForwardUp.Image = global::Damax.Properties.Resources.duplo_lijevo;
            this.tsBtnForwardUp.Name = "tsBtnForwardUp";
            this.tsBtnForwardUp.Size = new System.Drawing.Size(47, 47);
            this.tsBtnForwardUp.Text = "materialToolStripMenuItem1";
            this.tsBtnForwardUp.ToolTipText = "Idi na prvi red";
            this.tsBtnForwardUp.Click += new System.EventHandler(this.TsBtnForwardUp_Click);
            // 
            // tsBtnUp
            // 
            this.tsBtnUp.AutoSize = false;
            this.tsBtnUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnUp.Image = global::Damax.Properties.Resources.lijevo;
            this.tsBtnUp.Name = "tsBtnUp";
            this.tsBtnUp.Size = new System.Drawing.Size(47, 47);
            this.tsBtnUp.ToolTipText = "Idi na red iznad";
            this.tsBtnUp.Click += new System.EventHandler(this.TsBtnUp_Click);
            // 
            // tsBtnDown
            // 
            this.tsBtnDown.AutoSize = false;
            this.tsBtnDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnDown.Image = global::Damax.Properties.Resources.desno;
            this.tsBtnDown.Name = "tsBtnDown";
            this.tsBtnDown.Size = new System.Drawing.Size(47, 47);
            this.tsBtnDown.ToolTipText = "Idi na re ispod";
            this.tsBtnDown.Click += new System.EventHandler(this.TsBtnDown_Click);
            // 
            // tsBtnForwardDown
            // 
            this.tsBtnForwardDown.AutoSize = false;
            this.tsBtnForwardDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsBtnForwardDown.Image = global::Damax.Properties.Resources.duplo_desno;
            this.tsBtnForwardDown.Name = "tsBtnForwardDown";
            this.tsBtnForwardDown.Size = new System.Drawing.Size(47, 47);
            this.tsBtnForwardDown.ToolTipText = "Idi na poslednji red";
            this.tsBtnForwardDown.Click += new System.EventHandler(this.TsBtnForwardDown_Click);
            // 
            // izvjestajiToolStripMenuItem
            // 
            this.izvjestajiToolStripMenuItem.AutoSize = false;
            this.izvjestajiToolStripMenuItem.Image = global::Damax.Properties.Resources.izvjestaji;
            this.izvjestajiToolStripMenuItem.Name = "izvjestajiToolStripMenuItem";
            this.izvjestajiToolStripMenuItem.Size = new System.Drawing.Size(47, 47);
            this.izvjestajiToolStripMenuItem.ToolTipText = "Izvještaj";
            // 
            // md4Grid
            // 
            this.md4Grid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.md4Grid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.md4Grid.Depth = 0;
            this.md4Grid.Location = new System.Drawing.Point(12, 617);
            this.md4Grid.MouseState = MaterialSkin.MouseState.HOVER;
            this.md4Grid.Name = "md4Grid";
            this.md4Grid.Size = new System.Drawing.Size(856, 1);
            this.md4Grid.TabIndex = 14;
            this.md4Grid.Text = "materialDivider1";
            // 
            // md1Grid
            // 
            this.md1Grid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.md1Grid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.md1Grid.Depth = 0;
            this.md1Grid.Location = new System.Drawing.Point(12, 314);
            this.md1Grid.MouseState = MaterialSkin.MouseState.HOVER;
            this.md1Grid.Name = "md1Grid";
            this.md1Grid.Size = new System.Drawing.Size(856, 1);
            this.md1Grid.TabIndex = 15;
            this.md1Grid.Text = "materialDivider2";
            // 
            // md3Grid
            // 
            this.md3Grid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.md3Grid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.md3Grid.Depth = 0;
            this.md3Grid.Location = new System.Drawing.Point(12, 314);
            this.md3Grid.MouseState = MaterialSkin.MouseState.HOVER;
            this.md3Grid.Name = "md3Grid";
            this.md3Grid.Size = new System.Drawing.Size(1, 304);
            this.md3Grid.TabIndex = 16;
            this.md3Grid.Text = "materialDivider3";
            // 
            // md2Grid
            // 
            this.md2Grid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.md2Grid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.md2Grid.Depth = 0;
            this.md2Grid.Location = new System.Drawing.Point(867, 314);
            this.md2Grid.MouseState = MaterialSkin.MouseState.HOVER;
            this.md2Grid.Name = "md2Grid";
            this.md2Grid.Size = new System.Drawing.Size(1, 304);
            this.md2Grid.TabIndex = 17;
            this.md2Grid.Text = "materialDivider4";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnCancel.Image = global::Damax.Properties.Resources.cancel1;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(778, 624);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 40);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Odustani";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(86)))), ((int)(((byte)(136)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(100)))));
            this.btnSave.Image = global::Damax.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(673, 624);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 40);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Sačuvaj";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // FormTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(880, 671);
            this.Controls.Add(this.md2Grid);
            this.Controls.Add(this.md3Grid);
            this.Controls.Add(this.md1Grid);
            this.Controls.Add(this.md4Grid);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvItems);
            this.Controls.Add(this.tsMenu);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.tsMenu;
            this.Name = "FormTemplate";
            this.Text = "Template";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormTemplate_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormTemplate_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
            this.cmsDgItems.ResumeLayout(false);
            this.tsMenu.ResumeLayout(false);
            this.tsMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvItems;
        private ContextMenuStrip cmsDgItems;
        private ToolStripMenuItem tsDgvEdit;
        private ToolStripMenuItem tsDgvDelete;
        private Button btnCancel;
        private Button btnSave;
        private MenuStrip tsMenu;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnRefresh;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnAdd;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnEdit;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnDelete;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnForwardUp;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnUp;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnDown;
        private MaterialSkin.Controls.MaterialToolStripMenuItem tsBtnForwardDown;
        private MaterialSkin.Controls.MaterialDivider md4Grid;
        private MaterialSkin.Controls.MaterialDivider md1Grid;
        private MaterialSkin.Controls.MaterialDivider md3Grid;
        private MaterialSkin.Controls.MaterialDivider md2Grid;
        private ErrorProvider errorProvider;
        private ToolStripMenuItem izvjestajiToolStripMenuItem;
        private ToolTip ttMenu;
    }
}

