﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax
{
    class Constants
    {
        public static bool SQL_OK = true;
        public struct Form_Mode
        {
            public static int MODE_VIEW = 0;
            public static int MODE_ADD = 1;
            public static int MODE_EDIT = 2;

        }

        public struct Tip_Korisnika
        {
            public const int TIP_KORISNIK = 0;
            public const int TIP_MAGACIONER = 1;
            public const int TIP_ADMINISTRATOR = 2;

        }

    }
}
