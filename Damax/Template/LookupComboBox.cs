﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using UserControl = System.Windows.Forms.UserControl;

namespace Damax.Template
{
    public partial class LookupComboBox : UserControl
    {
        public EventHandler RefreshLookup;
        public EventHandler LookupValueChanged;

        public object DataSource
        {
            get { return comboBox1.DataSource; }

            set { comboBox1.DataSource = value; }
        }

        public string DisplayMember
        {
            get { return comboBox1.DisplayMember; }
            set { comboBox1.DisplayMember = value; }
        }

        public string ValueMember
        {
            get { return comboBox1.ValueMember; }
            set { comboBox1.ValueMember = value; }
        }

        public string LookupMember
            {
                get { return comboBox1.SelectedValue?.ToString(); }
                set { comboBox1.SelectedValue = value; }
            }

        public object SelectedLookupValue { get; set; }

        public int ComboSelectedIndex
        {
            get { return comboBox1.SelectedIndex; }
            set { comboBox1.SelectedIndex = value; } 
        }

        public bool RefreashButtonEnable
        {
            get { return btnRefresh.Enabled; }
            set { btnRefresh.Enabled = value; }
        }

        public bool DeleteButtonEnable
        {
            get { return btnDelete.Enabled; }
            set { btnDelete.Enabled = value; }
        }

        //public string ItemsList
        //{
        //    get { return comboBox1.Items; }
        //    set { comboBox1.Items = value; }
        //}


        public LookupComboBox()
        {
            InitializeComponent();
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearLookup();
        }

        public void ClearLookup()
        {
            comboBox1.DataSource = null;
            comboBox1.SelectedIndex = -1;
            comboBox1.SelectedValue = null;
            comboBox1.ValueMember = null;
            comboBox1.DisplayMember = null;
            SelectedLookupValue = null;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (this.RefreshLookup != null)
                this.RefreshLookup(this, e);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.LookupValueChanged != null)
            {
                if(comboBox1.SelectedIndex!=-1)
                    SelectedLookupValue = comboBox1.Items[comboBox1.SelectedIndex];
                this.LookupValueChanged(this, e);
            }
        }

        //public int GetIndexForObject(object item)
        //{
        //    return comboBox1.Items.IndexOf(item);
            
        //} 

    }
}
