﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Template
{
    public partial class TempTextBox : UserControl
    {
        //public TempTextBox()
        //{
        //    this.SetStyle(ControlStyles.UserPaint, true);
        //    InitializeComponent();
        //}

        [Browsable(true)]
        [Category("Extended Properties")]
        [Description("Set TextBox border Color")]

        public bool OnlyRead
        {
            get { return textBox.ReadOnly; }
            set { textBox.ReadOnly = value; }
        }

        public string Texts
        {
            get { return textBox.Text; }
            set { textBox.Text = value; }
        }

        TextBox textBox = new TextBox();
        public TempTextBox()
        {
            this.Resize += new EventHandler(UserControl1_Resize);
            textBox.Multiline = false;
            textBox.Font = new Font(textBox.Font.FontFamily, 10);
            textBox.ForeColor = Color.FromArgb(10, 50, 100);
            textBox.BackColor = Color.White;
            textBox.BorderStyle = BorderStyle.Fixed3D;
            textBox.Anchor = (AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right);
            this.Controls.Add(textBox);
        }

        private void UserControl1_Resize(object sender, EventArgs e)
        {
            //textBox.Size = new Size(this.Width - 3, this.Height );
            //textBox.Location = new Point(2, 2);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
           // ControlPaint.DrawBorder(e.Graphics, this.ClientRectangle, Color.FromArgb(18, 86, 136), ButtonBorderStyle.Solid);

        }

    }
}
