﻿using System.Collections.Generic;
using Damax.DAO;

namespace Damax.Template
{
    public interface ITemplateDao<T>
    {
        List<T> Select(List<TemplateFilter> filters );

        T SelectOne(object id);
        bool Insert(T item);
        bool Update(T item);
        bool Delete(T item);

    }
}