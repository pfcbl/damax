﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;

namespace Damax.Template
{
    public static class HomeFilters
    {
        public static int? HomeFilterGodina { get; set; }
        public static int? HomeFilterDogadjajId { get; set; }
        public static bool HomeFilterAktivanDogadjaj { get; set; }
        public static Radnik HomeFilterLogovaniRadnik { get; set; }
        public static Event HomeFilterEvent { get; set; }
    }
}
