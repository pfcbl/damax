﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Template
{
    class MenuRender : ToolStripProfessionalRenderer
    {
        public MenuRender() : base(new MyColors())
        {
        }
    }

    public class MyColors : ProfessionalColorTable
    {
        public override Color ToolStripDropDownBackground => Color.FromArgb(12, 57, 90);

        public override Color ImageMarginGradientBegin => Color.FromArgb(12, 57, 90);

        public override Color ImageMarginGradientMiddle => Color.FromArgb(12, 57, 90);

        public override Color ImageMarginGradientEnd => Color.FromArgb(12, 57, 90);

        public override Color MenuBorder => Color.White;

        public override Color MenuItemBorder => Color.White;

        public override Color MenuItemSelected => Color.FromArgb(12, 57, 90);

        public override Color MenuStripGradientBegin => Color.FromArgb(12, 57, 90);

        public override Color MenuStripGradientEnd => Color.FromArgb(12, 57, 90);

        public override Color MenuItemSelectedGradientBegin => Color.FromArgb(12, 57, 90);

        public override Color MenuItemSelectedGradientEnd => Color.FromArgb(12, 57, 90);

        public override Color MenuItemPressedGradientBegin => Color.FromArgb(12, 57, 90);

        public override Color MenuItemPressedGradientEnd => Color.FromArgb(12, 57, 90);

    }
}
