﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace Damax
{
    class ConnectionPool
    {
        private static ArrayList connections;
        private static int connectionNumber = 10;
        private static string connectionString = "Server=localhost;Database=damax;Uid=root;Pwd=admin;";

        static ConnectionPool()
        {
            try
            {
                connections = new ArrayList();
                for (int i = 0; i < connectionNumber; i++)
                {
                    MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["damaxConnectionString"].ConnectionString);
                    conn.Open();
                    connections.Add(conn);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.StackTrace);
            }
        }

        public static MySqlConnection getSingleConnection()
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["damaxConnectionString"].ConnectionString);
            conn.Open();
            return conn;
        }

        public static void closeSingleConnection(MySqlConnection conn)
        {
            conn.Close();
        }

        public static MySqlConnection checkOutConnection()
        {
            foreach (MySqlConnection conn in connections)
            {
                if (conn.State == ConnectionState.Open)
                {
                    connections.Remove(conn);
                    return conn;
                }
            }
            MySqlConnection fallback = getSingleConnection();
            return fallback;
        }

        public static void checkInConnection(MySqlConnection conn)
        {
            connections.Add(conn);
        }


		public static void HandleMySQLDBException(MySqlException ex)
		{
			var bMessageBoxShow = true;
			switch (ex.Number)
			{
				case 0:
					if (bMessageBoxShow)
					{
						MessageBox.Show("Cannot connect to server.  Contact administrator");
					}
					bMessageBoxShow = false;
					break;

				case 1040:
					if (bMessageBoxShow)
					{
						MessageBox.Show("Too many connections to DB");
					}
					bMessageBoxShow = false;
					break;

				case 1045:
					if (bMessageBoxShow)
					{
						MessageBox.Show("Invalid username/password, please try again");
					}
					bMessageBoxShow = false;
					break;

				default:
					if (bMessageBoxShow)
					{
						MessageBox.Show(" MySQL-Exception number:" + ex.Number + "\r\n" + ex.ToString());
					}
					bMessageBoxShow = false;
					break;
			}
		}
	}
}
