﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Magacin
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_MagacinId = "MagacinId";
        public const string F_MagacinIdNadredjeni = "MagacinIdNadredjeni";
        public const string F_Naziv = "Naziv";
        public const string F_Lokacija = "Lokacija";
        public const string F_ProdajnoMjestoId = "ProdajnoMjestoId";
        public const string F_ProdajnoMjestoNaziv = "ProdajnoMjestoNaziv";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_Centralni = "Centralni";
        public const string F_NazivNadredjenog = "NazivNadredjenog";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? MagacinId { get; set; }

        public int? MagacinIdNadredjeni { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        public int? ProdajnoMjestoId { get; set; }

        public string ProdajnoMjestoNaziv { get; set; }

        public bool Centralni { get; set; }

        public string NazivNadredjenog { get; set; }
    }
}
