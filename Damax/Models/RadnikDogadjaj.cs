﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class RadnikDogadjaj
    {
        public const string F_RadnikId = "RadnikId";
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_Ime = "Ime";
        public const string F_Prezime = "Prezime";
        public const string F_Naziv = "Naziv";
        public const string F_Zaduzenje = "Zaduzenje";


        public int? RadnikId { get; set; }

        public int? DogadjajId { get; set; }
        
        public short? Godina { get; set; }

        public string Zaduzenje { get; set; }

        public string Ime { get; set; }

        public string Prezime { get; set; }
        
        public string Naziv { get; set; }
    }
}
