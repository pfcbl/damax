﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Damax.Models
{
    public class Event
    {

        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_Naziv = "Naziv";
        public const string F_Mjesto = "Mjesto";
        public const string F_Adresa = "Adresa";
        public const string F_OdgovornaOsoba = "OdgovornaOsoba";
        public const string F_DatumOd = "DatumOd";
        public const string F_DatumDo = "DatumDo";
        public const string F_Aktivan = "Aktivan";
        public const string F_Zavrsen = "Zavrsen";
        public const string F_Valuta = "Valuta";
        public const string F_ZabranaBrisanja = "ZabranaBrisanja";


        public int? DogadjajId { get; set; }


        public short? Godina { get; set; }


        public string Naziv { get; set; }


        public string Mjesto { get; set; }


        public string Adresa { get; set; }


        public string OdgovornaOsoba { get; set; }


        public DateTime? DatumOd { get; set; }


        public DateTime? DatumDo { get; set; }

        public bool Aktivan { get; set; }

        public bool Zavrsen { get; set; }

        public bool ZabranaBrisanja { get; set; }

        public string Valuta { get; set; }

        public Event GetEventFromRow(DataGridViewRow row)
        {
            Event eventNew=new Event();
            eventNew.Godina = row.Cells[1].Value as short?;
            eventNew.Naziv = row.Cells[2].Value as string;
            eventNew.Mjesto = row.Cells[3].Value as string;
            eventNew.Adresa = row.Cells[4].Value as string;
            eventNew.OdgovornaOsoba = row.Cells[5].Value as string;
            eventNew.DatumOd = row.Cells[6].Value as DateTime?;
            eventNew.DatumDo = row.Cells[7].Value as DateTime?;
            return eventNew;
        }
    }
}
