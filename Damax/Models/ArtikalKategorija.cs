﻿namespace Damax.Models
{
    public class ArtikalKategorija
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_ArtikalKategorijaId = "ArtikalKategorijaId";
        public const string F_Naziv = "Naziv";


        public int? DogadjajId { get; set; }

        public short? Godina { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ArtikalKategorijaId { get; set; }

        public string Naziv { get; set; }
    }
}