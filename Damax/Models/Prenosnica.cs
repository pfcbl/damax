﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Prenosnica
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_PrenosnicaId = "PrenosnicaId";
        public const string F_MagacinSourceId = "MagacinSourceId";
        public const string F_MagacinOdredisteId = "MagacinOdredisteId";
        public const string F_VrijemePrenosa = "VrijemePrenosa";
        public const string F_TipPrenosa = "TipPrenosa";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_NazivOdrediste = "NazivOdrediste";
        public const string F_NazivSuorce = "NazivSuorce";
        public const string F_PrenosnicaLookup = "PrenosnicaLookup";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? PrenosnicaId { get; set; }

        public int? MagacinSourceId { get; set; }

        public int? MagacinOdredisteId { get; set; }

        public string NazivSuorce { get; set; }

        public string NazivOdrediste { get; set; }

        public string TipPrenosa { get; set; }

        public DateTime? VrijemePrenosa { get; set; }

        public string PrenosnicaLookup { get { return "Broj: " + this.PrenosnicaId + (this.NazivSuorce != null && !this.NazivSuorce.Trim().Equals("") ? (" Izlazni magacin: " + this.NazivSuorce) : "") +( this.NazivOdrediste!=null && !this.NazivOdrediste.Trim().Equals("")?(" Ulazni magacin: " + this.NazivOdrediste):""); }}
    }
}
