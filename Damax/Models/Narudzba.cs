﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Narudzba
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_ProdajnoMjestoId = "ProdajnoMjestoId";
        public const string F_NarudzbaId = "NarudzbaId";
        public const string F_Iznos = "Iznos";
        public const string F_DatumNarudzbe = "DatumNarudzbe";
        public const string F_Napomena = "Napomena";
        public const string F_KarticaUId = "KarticaUID";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_NazivProdajnogMjesta = "NazivProdajnogMjesta";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ProdajnoMjestoId { get; set; }

        public string NazivProdajnogMjesta { get; set; }

        public int? NarudzbaId { get; set; }

        public decimal? Iznos { get; set; }

        public DateTime? DatumNarudzbe { get; set; }

        public string Napomena { get; set; }

        public string KarticaUID { get; set; }
    }
}
