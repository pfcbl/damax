﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
	public class Radnik
	{
        public const string F_RadnikId = "RadnikId";
        public const string F_Ime = "Ime";
        public const string F_Prezime = "Prezime";
        public const string F_KorisnickoIme = "KorisnickoIme";
        public const string F_Lozinka = "Lozinka";
        public const string F_Telefon = "Telefon";
        public const string F_Mail = "Mail";
        public const string F_Administrator = "Administrator";
        public const string F_TipKorisnika = "TipKorisnika";
        public const string F_RadnikImePrezimeLookup = "RadnikImePrezimeLookup";

        public int RadnikId { get; set; }

	    public string Ime { get; set; }

	    public string Prezime { get; set; }

	    public string KorisnickoIme { get; set; }

	    public string Lozinka { get; set; }

	    public string Telefon { get; set; }

	    public string Mail { get; set; }

	    public bool Administrator { get; set; }

        public int TipKorisnika { get; set; }

        public string RadnikImePrezimeLookup { get { return this.Ime+" "+ this.Prezime; } }

    }
}
