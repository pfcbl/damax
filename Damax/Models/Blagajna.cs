﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Blagajna
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_BlagajnaId = "BlagajnaId";
        public const string F_Naziv = "Naziv";
        public const string F_Lokacija = "Lokacija";
        public const string F_BrojKarticeOd = "BrojKarticeOd";
        public const string F_BrojKarticeDo = "BrojKarticeDo";
        public const string F_BrojIzdatihKartica = "BrojIzdatihKartica";
        public const string F_UkupnoUplata = "UkupnoUplata";
        public const string F_UkupnoIsplata = "UkupnoIsplata";
        public const string F_Username = "Username";
        public const string F_Password = "Password";
        public const string F_NazivDogadjaja = "NazivDogadjaja";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? BlagajnaId { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        public string BrojKarticeOd { get; set; }

        public string BrojKarticeDo { get; set; }

        public decimal? UkupnoUplata { get; set; }

        public decimal? UkupnoIsplata { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int? BrojIzdatihKartica { get; set; }


    }
}
