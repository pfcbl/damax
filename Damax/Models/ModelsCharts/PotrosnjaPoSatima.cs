﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models.ModelsReports
{
    class PotrosnjaPoSatima
    {
        public static string F_Iznos = "Iznos";
        public static string F_Sat = "Sat";
        public static string F_Naziv = "Naziv";

        public PotrosnjaPoSatima()
        {
            Iznos=new List<decimal>();
            Sat=new List<string>();
            Naziv ="";
            IsVisible = true;
        }

        public List<decimal> Iznos { get; set; }

        public List<string> Sat { get; set; }

        public string Naziv { get; set; }

        public bool IsVisible { get; set; }
    }
}
