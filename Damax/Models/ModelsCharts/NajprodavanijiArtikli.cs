﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models.ModelsCharts
{
    class NajprodavanijiArtikli
    {
        public short Godina { get; set; }
        public int DogadjajId { get; set; }
        public int ArtikalId { get; set; }
        public string NazivArtikla { get; set; }
        public double Kolicina { get; set; }
    }
}
