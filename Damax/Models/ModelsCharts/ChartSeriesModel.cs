﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models.ModelsReports;
using LiveCharts;
using LiveCharts.WinForms;
using LiveCharts.Wpf;

namespace Damax.Models.ModelsCharts
{
    class ChartSeriesModel
    {
        public ChartSeriesModel(List<PotrosnjaPoSatima> list)
        {
            Series = new SeriesCollection();
            var r = new Random();

            var values = new ChartValues<PotrosnjaPoSatima>();
            foreach (PotrosnjaPoSatima item in list)
            {
                values.Add(item);
            }

            Series.Add(new LineSeries { Values = values });

            //for (var i = 0; i < 30; i++) // 30 series
            //{
            //    var trend = 0d;
            //    var values = new double[10000];

            //    for (var j = 0; j < 10000; j++) // 10k points each
            //    {
            //        trend += (r.NextDouble() < .8 ? 1 : -1)*r.Next(0, 10);
            //        values[j] = trend;
            //    }

            //    var series = new GLineSeries
            //    {
            //        Values = values.AsGearedValues().WithQuality(Quality.Low),
            //        Fill = Brushes.Transparent,
            //        StrokeThickness = .5,
            //        PointGeometry = null //use a null geometry when you have many series
            //    };
            //    Series.Add(series);
            //}
        }

        public SeriesCollection Series { get; set; }
    }
}
