﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiveCharts;

namespace Damax.Models.ModelsCharts
{
    class PotrosnjaArtikala
    {
        public PotrosnjaArtikala()
        {
            NazivArtikla=new List<string>();
            Kolicina=new ChartValues<decimal>();
            Potrosnja = new ChartValues<decimal>();
            IsVisible = true;
            Naziv = "";
        }
        public List<string> NazivArtikla { get; set; }

        public ChartValues<decimal> Kolicina { get; set; }
        public ChartValues<decimal> Potrosnja { get; set; }

        public string Naziv { get; set; }

        public bool IsVisible { get; set; }

    }
}
