﻿
namespace Damax.Models
{
    public class Artikal
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_ArtikalId = "ArtikalId";
        public const string F_ArtikalKategorijaId = "ArtikalKategorijaId";
        public const string F_Naziv = "Naziv";
        public const string F_PuniNaziv = "PuniNaziv";
        public const string F_Cijena = "Cijena";
        public const string F_Kolicina = "Kolicina";
        public const string F_Favorit = "Favorit";
        public const string F_Neaktivan = "Neaktivan";
        public const string F_JedinicaMjere = "JedinicaMjere";
        public const string F_Boja = "Boja";
        public const string F_ArtikalKategorijaNaziv = "ArtikalKategorijaNaziv";

        public int? DogadjajId { get; set; }

        public short? Godina { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ArtikalId { get; set; }

        public int? ArtikalKategorijaId { get; set; }

        public string ArtikalKategorijaNaziv { get; set; }

        public string Naziv { get; set; }

        public string PuniNaziv { get; set; }

        public decimal? Cijena { get; set; }

        public decimal? Kolicina { get; set; }

        public bool Favorit { get; set; }

        public bool Neaktivan { get; set; }

        public string JedinicaMjere { get; set; }

        public string Boja { get; set; }

    }
}
