﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class NarudzbaStavka
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_ProdajnoMjestoId = "ProdajnoMjestoId";
        public const string F_NarudzbaId = "NarudzbaId";
        public const string F_StavkaId = "StavkaId";
        public const string F_Iznos = "Iznos";
        public const string F_ArtikalId = "ArtikalId";
        public const string F_Kolicina = "Kolicina";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_NazivProdajnogMjesta = "NazivProdajnogMjesta";
        public const string F_NazivAtrikla = "NazivArtikla";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ProdajnoMjestoId { get; set; }

        public string NazivProdajnogMjesta { get; set; }

        public int? NarudzbaId { get; set; }

        public int? StavkaId { get; set; }

        public decimal? Iznos { get; set; }

        public int? ArtikalId { get; set; }

        public string NazivArtikla { get; set; }

        public int? Kolicina { get; set; }

    }
}
