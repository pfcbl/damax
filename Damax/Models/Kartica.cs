﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Kartica
    {
        public const string F_KarticaUid = "KarticaUID";
        public const string F_Status = "Status";


        public string KarticaUID { get; set; }
        public string Status { get; set; }
    }
}
