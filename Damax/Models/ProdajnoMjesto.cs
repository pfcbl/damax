﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class ProdajnoMjesto
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_ProdajnoMjestoId = "ProdajnoMjestoId";
        public const string F_Naziv = "Naziv";
        public const string F_Lokacija = "Lokacija";
        public const string F_UkupnoNaplata = "UkupnoNaplata";
        public const string F_Username = "Username";
        public const string F_Password = "Password";
        public const string F_NazivDogadjaja = "NazivDogadjaja";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ProdajnoMjestoId { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        public decimal? UkupnoNaplata { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

    }
}
