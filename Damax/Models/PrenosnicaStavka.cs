﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class PrenosnicaStavka
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_PrenosnicaId = "PrenosnicaId";
        public const string F_ArtikalId = "ArtikalId";
        public const string F_NazivArtikal = "NazivArtikla";
        public const string F_StavkaBroj = "StavkaBroj";
        public const string F_Kolicina = "Kolicina";
        public const string F_NazivDogadjaja = "NazivDogadjaja";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? ArtikalId { get; set; }

        public string NazivArtikla { get; set; }

        public int? PrenosnicaId { get; set; }

        public int? StavkaBroj { get; set; }

        public int? Kolicina { get; set; }
    }
}
