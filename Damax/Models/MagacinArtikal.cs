﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class MagacinArtikal
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_MagacinId = "MagacinId";
        public const string F_ArtikalId = "ArtikalId";
        public const string F_ArtikalKategorijaId = "ArtikalKategorijaId";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_MagacinNaziv = "MagacinNaziv";
        public const string F_ArtikalKategorijaNaziv = "ArtikalKategorijaNaziv";
        public const string F_ArtikalNaziv = "ArtikalNaziv";
        public const string F_PocetnoStanje = "PocetnoStanje";
        public const string F_TrenutnoStanje = "TrenutnoStanje";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? MagacinId { get; set; }

        public int? ArtikalKategorijaId { get; set; }

        public int? ArtikalId { get; set; }

        public string MagacinNaziv { get; set; }

        public string ArtikalKategorijaNaziv { get; set; }

        public string ArtikalNaziv { get; set; }

        public decimal TrenutnoStanje { get; set; }

        public decimal PocetnoStanje { get; set; }
    }
}
