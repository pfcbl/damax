﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.Models
{
    public class Transakcija
    {
        public const string F_DogadjajId = "DogadjajId";
        public const string F_Godina = "Godina";
        public const string F_BlagajnaId = "BlagajnaId";
        public const string F_TransakcijaId = "TransakcijaId";
        public const string F_TipTransakcije = "TipTransakcije";
        public const string F_DatumTransakcije = "DatumTransakcije";
        public const string F_BrojRacuna = "BrojRacuna";
        public const string F_Iznos = "Iznos";
        public const string F_KarticaUID = "KarticaUID";
        public const string F_NazivDogadjaja = "NazivDogadjaja";
        public const string F_NazivBlagajne = "NazivBlagajne";


        public short? Godina { get; set; }

        public int? DogadjajId { get; set; }

        public string NazivDogadjaja { get; set; }

        public int? BlagajnaId { get; set; }

        public string NazivBlagajne { get; set; }

        public string TipTransakcije { get; set; }

        public int? TransakcijaId { get; set; }

        public DateTime? DatumTransakcije { get; set; }

        public string BrojRacuna { get; set; }

        public decimal? Iznos { get; set; }

        public string KarticaUID { get; set; }
    }
}
