﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class MagacinDao : ITemplateDao<Magacin>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Magacin> Select(List<TemplateFilter> filters)
        {
            var lista = new List<Magacin>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.magacinId,b.magacinIdNadredjeni,b3.Naziv as NazivNadredjenog,b.Naziv,b.Lokacija,b.ProdajnoMjestoId,b2.Naziv as ProdajnoMjestoNaziv,b.Centralni,b3.Naziv as NazivNadredjenog " +
                " from magacin as b join dogadjaj as b1 on" +
                " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina left join prodajno_mjesto b2 on b.DogadjajId=b2.DogadjajId and b.Godina=b2.Godina and b.prodajnoMjestoId=b2.prodajnoMjestoId " +
                " left join magacin as b3 on b.DogadjajId=b3.DogadjajId and b.Godina=b3.Godina and b.magacinIdNadredjeni=b3.magacinId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                comm.ExecuteNonQuery();

                var reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Magacin item = new Magacin
                    {
                        Godina = Convert.ToInt16(reader[0].ToString()),
                        DogadjajId = Convert.ToInt32(reader[1].ToString()),
                        NazivDogadjaja = reader[2].ToString(),
                        MagacinId = Convert.ToInt32(reader[3]),
                        MagacinIdNadredjeni = int.TryParse(reader[4].ToString(), out int br) ? br : (int?)null,
                        NazivNadredjenog = reader[5].ToString(),
                        Naziv = reader[6].ToString(),
                        Lokacija = reader[7].ToString(),
                        ProdajnoMjestoId = int.TryParse(reader[8].ToString(), out br) ? br : (int?)null,
                        ProdajnoMjestoNaziv = reader[9].ToString(),
                        Centralni = reader.GetBoolean(10)
                    };
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }


        public DataTable SelectTable(List<TemplateFilter> filters)
        {
            DataTable lista = new DataTable();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.magacinId,b.magacinIdNadredjeni,b.Naziv,b.Lokacija,b.ProdajnoMjestoId,b2.Naziv as ProdajnoMjestoNaziv,b.Centralni " +
                " from magacin as b join dogadjaj as b1 on" +
                " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina left join prodajno_mjesto b2 on b.DogadjajId=b2.DogadjajId and b.Godina=b2.Godina and b.prodajnoMjestoId=b2.prodajnoMjestoId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
               // comm.ExecuteNonQuery();



                // create data adapter
                MySqlDataAdapter da = new MySqlDataAdapter(comm);
                // this will query your database and return the result to your datatable
                da.Fill(lista);
                conn.Close();
                da.Dispose();

                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public Magacin SelectOne(object id)
        {
            Magacin item = new Magacin();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(Magacin item)
        {
            conn = ConnectionPool.checkOutConnection();//JedinicaMjere)
            try { 
            string query =
                @"insert into magacin (godina,dogadjajId,Magacinid,MagacinIdNadredjeni,Naziv,Lokacija,ProdajnoMjestoId,Centralni)
                            select @Godina,@DogadjajId,ifnull(max(Magacinid),0)+1 as id,@MagacinIdNadredjeni,@Naziv,@Lokacija,@ProdajnoMjestoId,@Centralni
                            from magacin where   godina=@Godina and DogadjajId=@DogadjajId";

            //,@JedinicaMjere)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinIdNadredjeni", MySqlDbType.Int32).Value = item.MagacinIdNadredjeni;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.String).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@Centralni", MySqlDbType.Bit).Value = item.Centralni;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
            return Constants.SQL_OK;
        }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
}

        public bool Update(Magacin item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update magacin set Godina=@Godina,DogadjajId=@DogadjajId,MagacinIdNadredjeni=@MagacinIdNadredjeni,Naziv=@Naziv,Lokacija=@Lokacija,ProdajnoMjestoId=@ProdajnoMjestoId,Centralni=@Centralni                   
                                 where godina=@Godina and DogadjajId=@DogadjajId and MagacinId=@MagacinId ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                    command.Parameters.Add("@MagacinIdNadredjeni", MySqlDbType.Int32).Value = item.MagacinIdNadredjeni;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.String).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@Centralni", MySqlDbType.Bit).Value = item.Centralni;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Magacin item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from magacin where Godina=@Godina and DogadjajId=@DogadjajId and MagacinId=@MagacinId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                    command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
