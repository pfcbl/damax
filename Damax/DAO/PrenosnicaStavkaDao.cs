﻿using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.DAO
{
    class PrenosnicaStavkaDao : ITemplateDao<PrenosnicaStavka>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<PrenosnicaStavka> Select(List<TemplateFilter> filters)
        {
            var lista = new List<PrenosnicaStavka>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.Godina,b.DogadjajId,b1.Naziv as NazivDogadjaja,b.PrenosnicaId,b.ArtikalId,b2.Naziv as NazivArtikla, StavkaBroj, " +
                    " b.Kolicina " +
                    "from prenosnica_stavka as b " +
                    " join dogadjaj as b1 on b1.godina=b.godina and b1.dogadjajId=b.dogadjajId " +
                    "join artikal as b2 on" +
                    " b.ArtikalId=b2.ArtikalId and  b2.godina=b.godina and b2.dogadjajId=b.dogadjajId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    var item = new PrenosnicaStavka
                    {
                        Godina = Convert.ToInt16(reader[0].ToString()),
                        DogadjajId = Convert.ToInt32(reader[1].ToString()),
                        NazivDogadjaja = reader[2].ToString(),
                        PrenosnicaId = Convert.ToInt32(reader[3].ToString()),
                        ArtikalId = Convert.ToInt32(reader[4].ToString()),
                        NazivArtikla = reader[5].ToString(),
                        StavkaBroj = Convert.ToInt32(reader[6].ToString()),
                        Kolicina = Convert.ToInt32(reader[7].ToString())
                    };

                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return lista;
            }
        }

        public PrenosnicaStavka SelectOne(object id)
        {
            Artikal item = new Artikal();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select * from artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        decimal dt;
                        bool bl;
                        item.ArtikalId = Convert.ToInt32(reader[0].ToString());
                        item.ArtikalKategorijaId = Convert.ToInt32(reader[1].ToString());
                        item.ArtikalKategorijaNaziv = reader[2].ToString();
                        item.Naziv = reader[3].ToString();
                        item.PuniNaziv = reader[4].ToString();
                        item.Cijena = decimal.TryParse(reader[5].ToString(), out dt)
                            ? decimal.Parse(reader[5].ToString())
                            : 0;
                        item.Kolicina = decimal.TryParse(reader[6].ToString(), out dt)
                            ? decimal.Parse(reader[6].ToString())
                            : 0;
                        item.Boja = reader[7].ToString();
                        item.Favorit = bool.TryParse(reader[8].ToString(), out bl) && bool.Parse(reader[8].ToString());
                        item.Neaktivan = bool.TryParse(reader[9].ToString(), out bl) && bool.Parse(reader[9].ToString());
                        item.JedinicaMjere = reader[10].ToString();

                    }
                    reader.Close();
                }

                ConnectionPool.checkInConnection(conn);
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Insert(PrenosnicaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"insert into prenosnica_stavka (Godina,DogadjajId,ArtikalId,PrenosnicaId,StavkaBroj,Kolicina)
                            select @Godina,@DogadjajId,@ArtikalId,@PrenosnicaId, ifnull(max(stavkaBroj),0)+1 as id,@Kolicina
                            from prenosnica_stavka where godina=@Godina and DogadjajId=@DogadjajId and PrenosnicaId=@PrenosnicaId";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@PrenosnicaId", MySqlDbType.Int32).Value = item.PrenosnicaId;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Decimal).Value = item.Kolicina;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Update(PrenosnicaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"update prenosnica_stavka set Kolicina=@Kolicina  
                    where Godina=@Godina and DogadjajId=@DogadjajId and PrenosnicaId=@PrenosnicaId and StavkaBroj=@StavkaBroj";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@PrenosnicaId", MySqlDbType.Int32).Value = item.PrenosnicaId;
                    command.Parameters.Add("@StavkaBroj", MySqlDbType.Int32).Value = item.StavkaBroj;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Decimal).Value = item.Kolicina;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(PrenosnicaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"delete from prenosnica_stavka where Godina=@Godina and DogadjajId=@DogadjajId 
                                        and PrenosnicaId=@PrenosnicaId and StavkaBroj=@StavkaBroj and StavkaBroj=@StavkaBroj";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@PrenosnicaId", MySqlDbType.Int32).Value = item.PrenosnicaId;
                    command.Parameters.Add("@StavkaBroj", MySqlDbType.Int32).Value = item.StavkaBroj;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
