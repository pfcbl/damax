﻿using System;
using System.Collections.Generic;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class EventDao : ITemplateDao<Event>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;

        public List<Event> Select(List<TemplateFilter> filters)
        {
            if(HomeFilters.HomeFilterLogovaniRadnik.TipKorisnika!=Constants.Tip_Korisnika.TIP_ADMINISTRATOR)
                filters.Add(new TemplateFilter(RadnikDogadjaj.F_RadnikId,HomeFilters.HomeFilterLogovaniRadnik.RadnikId.ToString(),"=","b1"));
            var lista = new List<Event>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.DogadjajId,b.Godina,b.Naziv,b.Mjesto,b.Adresa,b.OdgovornaOsoba,b.DatumOd,b.DatumDo,b.Aktivan,b.Zavrsen,b.Valuta,b.ZabranaBrisanja from dogadjaj b ";
                if(HomeFilters.HomeFilterLogovaniRadnik.TipKorisnika != Constants.Tip_Korisnika.TIP_ADMINISTRATOR)
                    comm.CommandText += " join radnik_dogadjaj b1 on b.godina=b1.godina and b.dogadjajid=b1.dogadjajid ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and "+filter.Alias+"." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.CommandText += " order by b.aktivan desc,b.dogadjajid desc";
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                bool pom;
                while (reader.Read())
                {
                    var eventNew = new Event();
                    eventNew.DogadjajId = Convert.ToInt32(reader[0]);
                    eventNew.Godina = Convert.ToInt16(reader[1]);
                    eventNew.Naziv = reader[2].ToString();
                    eventNew.Mjesto = reader[3].ToString();
                    eventNew.Adresa = reader[4].ToString();
                    eventNew.OdgovornaOsoba = reader[5].ToString();
                    DateTime dt;
                    eventNew.DatumOd = DateTime.TryParse(reader[6].ToString(), out dt) ? dt : (DateTime?)null;
                    eventNew.DatumDo = DateTime.TryParse(reader[7].ToString(), out dt) ? dt : (DateTime?)null;
                    eventNew.Aktivan =reader.GetBoolean(8);
                    eventNew.Zavrsen = reader.GetBoolean(9);
                    eventNew.Valuta = reader[10].ToString();
                    eventNew.ZabranaBrisanja = reader.GetBoolean(11);

                    lista.Add(eventNew);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException ex)
            {
                return lista;
            }
        }

        public Event SelectOne(object idDogadjaj)
        {
            Event eventNew=new Event();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select DogadjajId,Godina,Naziv,Mjesto,Adresa,OdgovornaOsoba,DatumOd,DatumDo,Aktivan from dogadjaj  where DogadjajId=@DogadjajId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = idDogadjaj;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        eventNew.DogadjajId = Convert.ToInt32(reader[0]);
                        eventNew.Godina = Convert.ToInt16(reader[1]);
                        eventNew.Naziv = reader[2].ToString();
                        eventNew.Mjesto = reader[3].ToString();
                        eventNew.Adresa = reader[4].ToString();
                        eventNew.OdgovornaOsoba = reader[5].ToString();
                        DateTime dt;
                        eventNew.DatumOd = DateTime.TryParse(reader[6].ToString(), out dt) ? dt : (DateTime?)null;
                        eventNew.DatumDo = DateTime.TryParse(reader[7].ToString(), out dt) ? dt : (DateTime?)null;
                        eventNew.Aktivan = Convert.ToBoolean(reader[8]);

                    }
                    reader.Close();
                }


                
                ConnectionPool.checkInConnection(conn);
                return eventNew;
            }
            catch (Exception)
            {
                return eventNew;
            }
        }

        public bool Insert(Event item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"insert into dogadjaj (Godina,Naziv,Mjesto,Adresa,OdgovornaOsoba,DatumOd,DatumDo,Aktivan,Zavrsen,Valuta,ZabranaBrisanja) values (
                @Godina,@Naziv,@Mjesto,@Adresa,@OdgovornaOsoba,@DatumOd,@DatumDo,@Aktivan,@Zavrsen,@Valuta,@ZabranaBrisanja)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Mjesto", MySqlDbType.String).Value = item.Mjesto;
                    command.Parameters.Add("@Adresa", MySqlDbType.String).Value = item.Adresa;
                    command.Parameters.Add("@OdgovornaOsoba", MySqlDbType.String).Value = item.OdgovornaOsoba;
                    command.Parameters.Add("@DatumOd", MySqlDbType.DateTime).Value = item.DatumOd;
                    command.Parameters.Add("@DatumDo", MySqlDbType.DateTime).Value = item.DatumDo;
                    command.Parameters.Add("@Aktivan", MySqlDbType.Bit).Value = true;
                    command.Parameters.Add("@Zavrsen", MySqlDbType.Bit).Value = item.Zavrsen;
                    command.Parameters.Add("@Valuta", MySqlDbType.VarChar).Value = item.Valuta;
                    command.Parameters.Add("@ZabranaBrisanja", MySqlDbType.Bit).Value = item.ZabranaBrisanja;
                    command.ExecuteNonQuery();
                }
                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(Event item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                var query =
                    @"update dogadjaj set Godina=@Godina,Naziv=@Naziv,Mjesto=@Mjesto,Adresa=@Adresa,OdgovornaOsoba=@OdgovornaOsoba,DatumOd=@DatumOd,DatumDo=@DatumDo,Aktivan=@Aktivan,Zavrsen=@Zavrsen,Valuta=@Valuta,ZabranaBrisanja=@ZabranaBrisanja where dogadjajId=@DogadjajId and godina=@Godina";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Mjesto", MySqlDbType.String).Value = item.Mjesto;
                    command.Parameters.Add("@Adresa", MySqlDbType.String).Value = item.Adresa;
                    command.Parameters.Add("@OdgovornaOsoba", MySqlDbType.String).Value = item.OdgovornaOsoba;
                    command.Parameters.Add("@DatumOd", MySqlDbType.DateTime).Value = item.DatumOd;
                    command.Parameters.Add("@DatumDo", MySqlDbType.DateTime).Value = item.DatumDo;
                    command.Parameters.Add("@Aktivan", MySqlDbType.Bit).Value = item.Aktivan;
                    command.Parameters.Add("@Zavrsen", MySqlDbType.Bit).Value = item.Zavrsen;
                    command.Parameters.Add("@Valuta", MySqlDbType.VarChar).Value = item.Valuta;
                    command.Parameters.Add("@ZabranaBrisanja", MySqlDbType.Bit).Value = item.ZabranaBrisanja;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Event item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"delete from dogadjaj where dogadjajId=@DogadjajId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

    }
}
