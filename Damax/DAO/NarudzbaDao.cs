﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class NarudzbaDao : ITemplateDao<Narudzba>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Narudzba> Select(List<TemplateFilter> filters)
        {
            List<Narudzba> lista = new List<Narudzba>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.prodajnoMjestoId,b2.Naziv as ProdajnoMjestoNaziv,b.NarudzbaId,b.Iznos" +
                    ", b.DatumNarudzbe,b.Napomena,b.KarticaUID from narudzba as b join dogadjaj as b1 on" +
                    " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina " +
                    "join prodajno_mjesto b2 on b2.Godina=b.Godina and b2.DogadjajId=b.DogadjajId and b2.ProdajnoMjestoId=b.ProdajnoMjestoId";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                comm.CommandText += " order by b.godina desc,b.dogadjajId desc,b.prodajnoMjestoId,b.NarudzbaId desc ";
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Narudzba item = new Narudzba();
                    decimal de;
                    DateTime dt;
                    int br;
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.NazivDogadjaja = reader[2].ToString();
                    item.ProdajnoMjestoId = Convert.ToInt32(reader[3]);
                    item.NazivProdajnogMjesta = reader[4].ToString();
                    item.NarudzbaId = Convert.ToInt32(reader[5]);
                    item.Iznos = decimal.TryParse(reader[6].ToString(), out de)
                             ? de
                             : 0;
                    item.DatumNarudzbe = DateTime.TryParse(reader[7].ToString(), out dt) ? dt : (DateTime?)null;
                    item.Napomena = reader[8].ToString();
                    item.KarticaUID = reader[9].ToString();
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public Narudzba SelectOne(object id)
        {
            Narudzba item = new Narudzba();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(Narudzba item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {

                string query =
                    @"insert into narudzba (godina,dogadjajId,ProdajnoMjestoId,Iznos,DatumNarudzbe,Napomena,KarticaUID)
                            values (@Godina,@DogadjajId,@ProdajnoMjestoId,@Iznos,@DatumNarudzbe,@Napomena,@KarticaUID)";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@DatumNarudzbe", MySqlDbType.DateTime).Value = item.DatumNarudzbe;
                    command.Parameters.Add("@Napomena", MySqlDbType.String).Value = item.Napomena;
                    command.Parameters.Add("@KarticaUID", MySqlDbType.String).Value = item.KarticaUID;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(Narudzba item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update narudzba set Godina=@Godina,DogadjajId=@DogadjajId,ProdajnoMjestoId=@ProdajnoMjestoId,Iznos=@Iznos,
                            DatumNarudzbe=@DatumNarudzbe,Napomena=@Napomena,KarticaUID=@KarticaUID where godina=@Godina and DogadjajId=@DogadjajId 
                            and ProdajnoMjestoId=@ProdajnoMjestoId and NarudzbaId=@NarudzbaId";

                using (var command = new MySqlCommand(query, conn))
                {

                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@NarudzbaId", MySqlDbType.Int32).Value = item.NarudzbaId;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@DatumNarudzbe", MySqlDbType.DateTime).Value = item.DatumNarudzbe;
                    command.Parameters.Add("@Napomena", MySqlDbType.String).Value = item.Napomena;
                    command.Parameters.Add("@KarticaUID", MySqlDbType.String).Value = item.KarticaUID;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Narudzba item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from narudzba where Godina=@Godina and DogadjajId=@DogadjajId and ProdajnoMjestoId=@ProdajnoMjestoId  and NarudzbaId=@NarudzbaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@NarudzbaId", MySqlDbType.Int32).Value = item.NarudzbaId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
