﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class TransakcijaDao : ITemplateDao<Transakcija>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Transakcija> Select(List<TemplateFilter> filters)
        {
            List<Transakcija> lista = new List<Transakcija>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.BlagajnaId,b2.Naziv as BlagajnaNaziv,b.TipTransakcije,b.TransakcijaId,b.Iznos" +
                    ", b.DatumTransakcije,b.BrojRacuna,b.KarticaUID from transakcija as b join dogadjaj as b1 on" +
                    " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina " +
                    "join blagajna b2 on b2.Godina=b.Godina and b2.DogadjajId=b.DogadjajId and b2.BlagajnaId=b.BlagajnaId";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    var item = new Transakcija();
                    decimal de;
                    DateTime dt;
                    int br;
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.NazivDogadjaja = reader[2].ToString();
                    item.BlagajnaId = Convert.ToInt32(reader[3]);
                    item.NazivBlagajne = reader[4].ToString();
                    item.TipTransakcije = reader[5].ToString();
                    item.TransakcijaId = Convert.ToInt32(reader[6]);
                    item.Iznos = decimal.TryParse(reader[7].ToString(), out de)
                             ? de
                             : 0;
                    item.DatumTransakcije = DateTime.TryParse(reader[8].ToString(), out dt) ? dt : (DateTime?)null;
                    item.BrojRacuna = reader[9].ToString();
                    item.KarticaUID = reader[10].ToString();
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public Transakcija SelectOne(object id)
        {
            var item = new Transakcija();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(Transakcija item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"insert into Transakcija (godina,dogadjajId,BlagajnaId,TipTransakcije,Iznos,DatumTransakcije,BrojRacuna,KarticaUID)
                            values (@Godina,@DogadjajId,@BlagajnaId,@TipTransakcije,@Iznos,@DatumTransakcije,@BrojRacuna,@KarticaUID)";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@BlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
                    command.Parameters.Add("@TipTransakcije", MySqlDbType.String).Value = item.TipTransakcije;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@DatumTransakcije", MySqlDbType.DateTime).Value = item.DatumTransakcije;
                    command.Parameters.Add("@BrojRacuna", MySqlDbType.String).Value = item.BrojRacuna;
                    command.Parameters.Add("@KarticaUID", MySqlDbType.String).Value = item.KarticaUID;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(Transakcija item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update Transakcija set Godina=@Godina,DogadjajId=@DogadjajId,BlagajnaId=@BlagajnaId,TipTransakcije=@TipTransakcije,Iznos=@Iznos,
                            DatumTransakcije=@DatumTransakcije,BrojRacuna=@BrojRacuna,KarticaUID=@KarticaUID where godina=@Godina and DogadjajId=@DogadjajId 
                            and BlagajnaId=@BlagajnaId and TransakcijaId=@TransakcijaId and TipTransakcije=@TipTransakcije";

                using (var command = new MySqlCommand(query, conn))
                {

                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@BlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
                    command.Parameters.Add("@TransakcijaId", MySqlDbType.Int32).Value = item.TransakcijaId;
                    command.Parameters.Add("@TipTransakcije", MySqlDbType.String).Value = item.TipTransakcije;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@DatumTransakcije", MySqlDbType.DateTime).Value = item.DatumTransakcije;
                    command.Parameters.Add("@BrojRacuna", MySqlDbType.String).Value = item.BrojRacuna;
                    command.Parameters.Add("@KarticaUID", MySqlDbType.String).Value = item.KarticaUID;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Transakcija item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from Transakcija where Godina=@Godina and DogadjajId=@DogadjajId and BlagajnaId=@BlagajnaId  and TransakcijaId=@TransakcijaId and TipTransakcije=@TipTransakcije ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                    command.Parameters.Add("@BlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
                    command.Parameters.Add("@TransakcijaId", MySqlDbType.Int32).Value = item.TransakcijaId;
                    command.Parameters.Add("@TipTransakcije", MySqlDbType.String).Value = item.TipTransakcije;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
