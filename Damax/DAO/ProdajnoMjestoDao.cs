﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class ProdajnoMjestoDao : ITemplateDao<ProdajnoMjesto>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<ProdajnoMjesto> Select(List<TemplateFilter> filters)
        {
            List<ProdajnoMjesto> lista = new List<ProdajnoMjesto>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.prodajnoMjestoId,b.Naziv,b.Lokacija" +
                    ", b.UkupnoNaplata,b.Username,b.Password from prodajno_mjesto as b join dogadjaj as b1 on" +
                    " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ProdajnoMjesto item = new ProdajnoMjesto();
                    decimal dt;
                    int br;
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.NazivDogadjaja = reader[2].ToString();
                    item.ProdajnoMjestoId = Convert.ToInt32(reader[3]);
                    item.Naziv = reader[4].ToString();
                    item.Lokacija = reader[5].ToString();
                    item.UkupnoNaplata = decimal.TryParse(reader[6].ToString(), out dt)
                             ? dt
                             : 0;
                    item.Username = reader[7].ToString();
                    item.Password = reader[8].ToString();
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public ProdajnoMjesto SelectOne(object id)
        {
            ProdajnoMjesto item = new ProdajnoMjesto();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(ProdajnoMjesto item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"insert into prodajno_mjesto (godina,dogadjajId,ProdajnoMjestoId,Naziv,Lokacija,UkupnoNaplata,Username,Password)
                            select @Godina,@DogadjajId,ifnull(max(ProdajnoMjestoId),0)+1 as id,@Naziv,@Lokacija,@UkupnoNaplata,@Username,@Password 
                            from prodajno_mjesto where   godina=@Godina and DogadjajId=@DogadjajId";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@UkupnoNaplata", MySqlDbType.Float).Value = item.UkupnoNaplata;
                    command.Parameters.Add("@Username", MySqlDbType.String).Value = item.Username;
                    command.Parameters.Add("@Password", MySqlDbType.String).Value = item.Password;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(ProdajnoMjesto item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update prodajno_mjesto set Godina=@Godina,DogadjajId=@DogadjajId,Naziv=@Naziv,Lokacija=@Lokacija,
                            UkupnoNaplata=@UkupnoNaplata,Username=@Username,Password=@Password where godina=@Godina and DogadjajId=@DogadjajId and ProdajnoMjestoId=@ProdajnoMjestoId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@UkupnoNaplata", MySqlDbType.Float).Value = item.UkupnoNaplata;
                    command.Parameters.Add("@Username", MySqlDbType.String).Value = item.Username;
                    command.Parameters.Add("@Password", MySqlDbType.String).Value = item.Password;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(ProdajnoMjesto item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from prodajno_mjesto where Godina=@Godina and DogadjajId=@DogadjajId and ProdajnoMjestoId=@ProdajnoMjestoId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
