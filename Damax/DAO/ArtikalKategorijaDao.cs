﻿using System;
using System.Collections.Generic;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    public class ArtikalKategorijaDao : ITemplateDao<ArtikalKategorija>
    {

        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters; 

        public List<TemplateFilter> GetFilter()
        {
            
            return null;
        }

        public List<ArtikalKategorija> Select(List<TemplateFilter> filters)
        {
            List<ArtikalKategorija> lista = new List<ArtikalKategorija>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText = "select  b.Godina,b.DogadjajId,b1.Naziv as NazivDogadjaja,b.ArtikalKategorijaId,b.Naziv from artikal_kategorija as b" +
                    " join dogadjaj as b1 on b.godina=b1.godina and b.dogadjajid=b1.dogadjajId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    ArtikalKategorija item = new ArtikalKategorija();
                    item.Godina = Convert.ToInt16(reader[0]);
                    item.DogadjajId = Convert.ToInt32(reader[1]);
                    item.Naziv = reader[2].ToString();
                    item.ArtikalKategorijaId = Convert.ToInt32(reader[3]);
                    item.Naziv = reader[4].ToString();
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException ex)
            {
                return lista;
            }
        }

        public ArtikalKategorija SelectOne(object id)
        {
            ArtikalKategorija item = new ArtikalKategorija();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select ArtikalKategorijaId,Naziv from artikal_kategorija where ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        item.ArtikalKategorijaId = Convert.ToInt32(reader[0]);
                        item.Naziv = reader[1].ToString();

                    }
                    reader.Close();
                }

                ConnectionPool.checkInConnection(conn);
                return item;
            }
            catch (Exception)
            {
                return item;
            }
        }



        public int SelectMaxId(List<TemplateFilter> filters)
        {
            int maxId = 0;
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select ifnull(max(ArtikalKategorijaId),0) as ArtikalKategorijaId  from artikal_kategorija b ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    maxId = Convert.ToInt32(reader[0].ToString());
                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return maxId;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return maxId;
            }
        }

        public bool Insert(ArtikalKategorija item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"insert into artikal_kategorija (Godina,DogadjajId,ArtikalKategorijaId,Naziv) select @Godina,@DogadjajId,ifnull(max(ArtikalKategorijaId),0)+1 as id,@Naziv from artikal_kategorija where  godina=@Godina and DogadjajId=@DogadjajId";
                 using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

		public bool Update(ArtikalKategorija item)
		{
			conn = ConnectionPool.checkOutConnection();
			try
			{
				string query =
                        @"update artikal_kategorija set Naziv=@Naziv,VrijemeIzmjene=@VrijemeIzmjene where Godina=@Godina and DogadjajId=@DogadjajId and  ArtikalKategorijaId=@ArtikalKategorijaId";

				using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
					command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
					command.Parameters.Add("@VrijemeIzmjene", MySqlDbType.DateTime).Value = DateTime.Now;
					command.ExecuteNonQuery();
				}

				ConnectionPool.checkInConnection(conn);
				return Constants.SQL_OK;

			}
			catch (MySqlException e)
			{

				ConnectionPool.checkInConnection(conn);

				return false;
			}
		}

        public bool Delete(ArtikalKategorija item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"delete from artikal_kategorija where Godina=@Godina and DogadjajId=@DogadjajId and  ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

    }
}
