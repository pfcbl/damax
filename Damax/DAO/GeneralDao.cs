﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Models.ModelsCharts;
using Damax.Models.ModelsReports;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class GeneralDao
    {

        public static string BrojIzdatihKartica(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                var conn = ConnectionPool.checkOutConnection();
                string query =
                    " select count(t.karticauid) as brojKartica from (select karticauid from transakcija where godina=@Godina and dogadjajID=@dogadjajId group by godina,dogadjajid,karticauid) as t ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }

        public static string BrojAktivnihKartica(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                string query =
                    " select count(t.karticauid) as brojKartica from ( select karticauid from narudzba where godina=@Godina and dogadjajID=@dogadjajId group by godina,dogadjajid,karticauid) as t ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }


        //select sum(iznos)/count(t.karticauid) as prosjecno from(select godina, dogadjajid, sum(iznos) as iznos,karticauid from narudzba where godina=2019 and dogadjajid = 16 group by godina,dogadjajid,karticauid) as t

        public static string UkupnoIsplaceno(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                string query =
                    " select sum(Iznos) as Iznos from transakcija where godina=@Godina and dogadjajID=@dogadjajId and Tiptransakcije='I' group by godina,dogadjajid ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }

        public static string UkupnoUplaceno(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                string query =
                    " select sum(Iznos)+135  as Iznos from transakcija where godina=@Godina and dogadjajID=@dogadjajId and Tiptransakcije='U' group by godina,dogadjajid ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }

        public static string ProsjecnoPoKartici(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                var conn = ConnectionPool.checkOutConnection();
                string query =
                    " select sum(t.iznos)/count(t.karticauid) as prosjecno from(select ns.godina, ns.dogadjajid, sum(ns.iznos) as iznos,n.karticauid from narudzba_stavka ns join narudzba n on ns.godina=n.godina and ns.dogadjajid=n.dogadjajid and ns.prodajnomjestoid=n.prodajnomjestoid and ns.narudzbaid=n.narudzbaid where ns.godina=@Godina and ns.dogadjajid = @DogadjajId group by ns.godina,ns.dogadjajid,n.karticauid) as t ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }


        public static string UkupnaPotrosnja(int? godina, int? dogadjajid)
        {
            string broj = "0";
            try
            {
                var conn = ConnectionPool.checkOutConnection();
                string query =
                   " select sum(Iznos) as Iznos from narudzba_stavka where godina=@Godina and dogadjajID=@dogadjajId group by godina,dogadjajid ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = reader[0].ToString();

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return broj;
            }
            catch (MySqlException e)
            {
                return "0";
            }
        }


        public static PotrosnjaPoSatima UplateSati(int godina, string dogadjajid, DateTime datumOd, DateTime datumDo)
        {
            PotrosnjaPoSatima list = new PotrosnjaPoSatima();
            decimal decPom;
            int intPom;
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                string query =
                    " select sum(iznos) as Iznos,concat(hour(datumTransakcije),'.00h',' ',day(datumTransakcije),'.',month(datumTransakcije))  as Sat " +
                    "from transakcija " +
                    "where dogadjajid = @dogadjajId and godina = @godina and datumTransakcije between @datumOd and @datumDo" +
                    " group by godina, dogadjajid,year(datumTransakcije),month(datumTransakcije),day(datumTransakcije),hour(datumTransakcije) order by godina,dogadjajid,datumTransakcije,year(datumTransakcije),month(datumTransakcije),day(datumTransakcije),hour(datumTransakcije) ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;
                    command.Parameters.Add("@datumOd", MySqlDbType.DateTime).Value = datumOd;
                    command.Parameters.Add("@datumDo", MySqlDbType.DateTime).Value = datumDo;

                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Iznos.Add(decimal.TryParse(reader[0].ToString(), out decPom) ? decPom : 0);
                        list.Sat.Add(reader[1].ToString());
                        list.Naziv = "Ukupno";
                        list.IsVisible = true;

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return list;
            }
            catch (MySqlException e)
            {
                return list;
            }
        }

		public static DataTable PregledPrenosaArtikalaReport(Magacin item,int tip,DateTime datumOd,DateTime datumDo)
		{

			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_PregledPrenosaArtikala", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int16).Value = item.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@pMagacinSourceId", MySqlDbType.Int32).Value = item.MagacinId;
                        command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = tip;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        var da = new MySqlDataAdapter(command);
                        da.Fill(table);
                        conn.Close();
                        da.Dispose();
                        //var reader = command.ExecuteReader();
                        //table.Load(reader);
                        //reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException ex)
			{
				return table;
			}
		}

		public static DataTable PregledPrenosnicaReport(Prenosnica item)
		{

			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_StampanjePrenosnice", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int16).Value = item.Godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
						command.Parameters.Add("@pPrenosnicaId", MySqlDbType.Int32).Value = item.PrenosnicaId;
						var da = new MySqlDataAdapter(command);
						da.Fill(table);
						conn.Close();
						da.Dispose();
						//var reader = command.ExecuteReader();
						//table.Load(reader);
						//reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException ex)
			{
				return table;
			}
		}

		public static PotrosnjaPoSatima UplateSatiPojedinacno(int? godina,int? dogadjajId,int? blagajnaId, DateTime datumOd, DateTime datumDo,string naziv)
        {
            PotrosnjaPoSatima list = new PotrosnjaPoSatima();
            decimal decPom;
            using (var conn = ConnectionPool.checkOutConnection())
            {
                MySqlCommand command = new MySqlCommand("proc_UplatePoSatima", conn);
                command.CommandType = CommandType.StoredProcedure;
                {
                    command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajId;
                    command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                    command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                    command.Parameters.Add("@pBlagajnaId", MySqlDbType.Int32).Value = blagajnaId;
                    command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 1;
                    var reader = command.ExecuteReader();
                    list.Naziv = naziv;
                    while (reader.Read())
                    {
                        list.Iznos.Add(decimal.TryParse(reader[0].ToString(), out decPom) ? decPom : 0);
                        list.Sat.Add(reader[1].ToString());
                        list.IsVisible = true;
                    }
                    reader.Close();
                }
                
                ConnectionPool.checkInConnection(conn);
                return list;
            }
        }

        public static PotrosnjaPoSatima PotrosnjaSati(int godina, string dogadjajid,DateTime datumOd,DateTime datumDo)
        {
            PotrosnjaPoSatima list = new PotrosnjaPoSatima();
            decimal decPom;
            int intPom;
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                string query =
                    " select sum(ns.iznos) as Iznos,concat(hour(datumNarudzbe),'.00h',' ',day(datumnarudzbe),'.',month(datumnarudzbe))  as Sat " +
                    "from narudzba_stavka " +
                    "  ns join narudzba n on ns.godina=n.godina and ns.dogadjajid=n.dogadjajid and ns.prodajnomjestoid=n.prodajnomjestoid and ns.narudzbaid=n.narudzbaid   where ns.dogadjajid = @dogadjajId and ns.godina = @godina and n.datumnarudzbe between @datumOd and @datumDo" +
                    " group by ns.godina, ns.dogadjajid, day(datumNarudzbe), hour(datumnarudzbe) order by ns.godina,ns.dogadjajid,datumnarudzbe ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
                    command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;
                    command.Parameters.Add("@datumOd", MySqlDbType.DateTime).Value = datumOd;
                    command.Parameters.Add("@datumDo", MySqlDbType.DateTime).Value = datumDo;

                    MySqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        list.Iznos.Add(decimal.TryParse(reader[0].ToString(), out decPom) ? decPom : 0);
                        list.Sat.Add(reader[1].ToString());
                        list.Naziv="Ukupno";
                        list.IsVisible = true;

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return list;
            }
            catch (MySqlException e)
            {
                return list;
            }
        }


				public static DataTable BrojUplataPoSatimaIBlagajni(Blagajna item, DateTime datumOd, DateTime datumDo,int Tip)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_brojUplataPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						command.Parameters.Add("@pBlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
						command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = Tip;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static DataTable BrojIzdatihKarticaPoSatima(int? godina, int? dogadjajid, DateTime datumOd, DateTime datumDo)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_BrojIzdatihKarticaPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajid;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static DataTable ProsjekUplataPoSatimai(Blagajna item, DateTime datumOd, DateTime datumDo)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_ProsjecnaUplataPoKarticiPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						command.Parameters.Add("@pBlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
						command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 0;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static DataTable PotrosnjaSatiReport(ProdajnoMjesto item, DateTime datumOd, DateTime datumDo,int Tip)
        {
            var table = new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_potrosnjaPoSatima", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                        command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = Tip;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }

		public static DataTable BrojAktivnihKarticaPoSatimaReport(int? godina, int? dogadjajid, DateTime datumOd, DateTime datumDo)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_BrojAktivnihKarticaPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajid;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						//command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
						//command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 0;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static DataTable ProsjecnaPotrosnjaSatiReport(ProdajnoMjesto item, DateTime datumOd, DateTime datumDo)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_prosjecnaPotrosnjaPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
						command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 0;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static DataTable BrojNaruzbiSatiReport(ProdajnoMjesto item, DateTime datumOd, DateTime datumDo,int Tip)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_brojNarudzbiPoSatima", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
						command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
						command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
						command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
						command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = Tip;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

		public static PotrosnjaPoSatima PotrosnjaSatiPojedinacno(int? godina ,int? dogadjajid, int? prodajnoMjestoid, DateTime datumOd, DateTime datumDo,string naziv)
        {
            var list = new PotrosnjaPoSatima();
            decimal decPom;
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_potrosnjaPoSatima", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajid;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = prodajnoMjestoid;
                        command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 1;
                        MySqlDataReader reader = command.ExecuteReader();
                        list.Naziv = naziv;
                        while (reader.Read())
                        {
                            list.Iznos.Add(decimal.TryParse(reader[0].ToString(), out decPom) ? decPom : 0);
                            list.Sat.Add(reader[1].ToString());
                            list.IsVisible = true;
                        }
                        reader.Close();
                    }
                

            ConnectionPool.checkInConnection(conn);
                return list;
            }

        }

        public static DataTable PotrosnjaArtikalaPoProdajnomMjestuReport(short? godina,int? dogadjajId,string prodajnoMjestoId,string artikalId, DateTime datumOd, DateTime datumDo)
        {
            var table = new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("procProdajaArtikalaPoProdajnomMjestu", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajId;
                        command.Parameters.Add("@pProdajnoMjestoId", MySqlDbType.Int32).Value = prodajnoMjestoId;
                        command.Parameters.Add("@pArtikalId", MySqlDbType.Int32).Value = artikalId;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        command.Parameters.Add("@pTip", MySqlDbType.Int32).Value = 0;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }

        public static PotrosnjaArtikala StanjeMagacina(Magacin item)
        {
            var potrosnja = new PotrosnjaArtikala();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_StanjeMagacina", conn) { CommandType = CommandType.StoredProcedure };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@pMagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            potrosnja.Kolicina.Add(decimal.TryParse(reader[4].ToString(), out decimal dPom) ? dPom : 0);
                            potrosnja.Potrosnja.Add(decimal.TryParse(reader[5].ToString(), out dPom) ? dPom : 0);
                            potrosnja.NazivArtikla.Add(reader[3].ToString());
                            //potrosnja.Naziv = reader[3].ToString();
                            potrosnja.IsVisible = true;
                        }
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return potrosnja;
                }
            }
            catch(MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return potrosnja;
            }
        }


        public static DataTable StanjeMagacinaGrid(Magacin item, DateTime datumOd, DateTime datumDo)
        {

            var table =new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("damax.proc_StanjeMagacina", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@pMagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }

        public static DataTable PrenosArtikalaGrid(string source,string destination)
        {

            var table = new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_PrenosArtikalaMagacini", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = HomeFilters.HomeFilterEvent.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = HomeFilters.HomeFilterEvent.DogadjajId;
                        command.Parameters.Add("@pMagacinIdSource", MySqlDbType.Int32).Value = source;
                        command.Parameters.Add("@pMagacinIdDest", MySqlDbType.Int32).Value = destination;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }
        public static DataTable DopunaCentralnogGrid(string magacin)
        {

            var table = new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_PrenosArtikalaMagacini", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = HomeFilters.HomeFilterEvent.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = HomeFilters.HomeFilterEvent.DogadjajId;
                        command.Parameters.Add("@pMagacinIdSource", MySqlDbType.Int32).Value = magacin;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }

        //public static PotrosnjaPoSatima StanjeMagacinaForma(Magacin item)
        //{
        //    var table = new DataTable();
        //    try
        //    {
        //        MySqlConnection conn = ConnectionPool.checkOutConnection();
        //        string query =
        //            " select * from ";

        //        using (var command = new MySqlCommand(query, conn))
        //        {
        //            command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = godina;
        //            command.Parameters.Add("@dogadjajId", MySqlDbType.Int32).Value = dogadjajid;
        //            command.Parameters.Add("@datumOd", MySqlDbType.DateTime).Value = datumOd;
        //            command.Parameters.Add("@datumDo", MySqlDbType.DateTime).Value = datumDo;

        //            MySqlDataReader reader = command.ExecuteReader();

        //            while (reader.Read())
        //            {
        //                list.Iznos.Add(decimal.TryParse(reader[0].ToString(), out decPom) ? decPom : 0);
        //                list.Sat.Add(reader[1].ToString());
        //                list.Naziv = "Ukupno";
        //                list.IsVisible = true;

        //            }
        //            reader.Close();
        //        }


        //        ConnectionPool.checkInConnection(conn);
        //        return list;
        //    }
        //    catch (MySqlException e)
        //    {
        //        return list;
        //    }
        //}
        public static int ImaLiAktivanDogadjaj()
        {
            var ima=0;
            try
            {
                MySqlConnection conn = ConnectionPool.checkOutConnection();
                const string query = " SELECT ifnull(count(*),0) as ima FROM dogadjaj where aktivan=1 ";

                using (var command = new MySqlCommand(query, conn))
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        ima = reader.GetInt32("ima");

                    }
                    reader.Close();
                }


                ConnectionPool.checkInConnection(conn);
                return ima;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return ima;
            }
        }

        public static int ZakljucavanjeDogadjaja(Event item)
        {
            var rezultat =0;
            var poruka = "";
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_ZakljucivanjeDogadjaja", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = item.Godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@pRezultat", MySqlDbType.Int32).Value = rezultat;
                        command.Parameters.Add("@pMessage", MySqlDbType.VarChar).Value = poruka;
                        command.Parameters["@pRezultat"].Direction = ParameterDirection.Output;
                        command.Parameters["@pMessage"].Direction = ParameterDirection.Output;
                        var reader = command.ExecuteReader();
                        rezultat = int.TryParse(command.Parameters["@pRezultat"].Value.ToString(), out int pom) ? pom : -1;
                        poruka = command.Parameters["@pMessage"].Value.ToString();
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return rezultat;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return rezultat;
            }
        }


        public static List<NajprodavanijiArtikli> NajprodavanijiArtikli(int godina,string dogadjajId,int magacinId)
        {
            var list = new List<NajprodavanijiArtikli>();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_NajprodavanijiArtikli", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajId;
                        command.Parameters.Add("@pMagacinId", MySqlDbType.Int32).Value = magacinId;
                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new NajprodavanijiArtikli();
                            item.Godina = reader.GetInt16(0);
                            item.DogadjajId = reader.GetInt32(1);
                            item.ArtikalId = reader.GetInt32(2);
                            item.NazivArtikla = reader.GetString(3);
                            item.Kolicina = reader.GetDouble(4);
                            list.Add(item);

                        }
                        reader.Close();
                    }

                    ConnectionPool.checkInConnection(conn);
                    return list;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return list;
            }
        }
        public static bool PrenosArtikalaMagaciniUpdate(DataTable table)
        {
            MySqlConnection conn = ConnectionPool.checkOutConnection();
            MySqlTransaction transaction=null;
            try
            {
                string query =
                    @"update  magacin_artikal set Godina=@Godina,DogadjajId=@DogadjajId,MagacinId=@MagacinId," +
                    "ArtikalId=@ArtikalId,TrenutnoStanje=@TrenutnoStanje where godina=@Godina and DogadjajId=@DogadjajId and MagacinId=@MagacinId and ArtikalId=@ArtikalId ";

                transaction = conn.BeginTransaction();
                using (var command = new MySqlCommand(query, conn))
                {
                    foreach (DataRow row in table.Rows)
                    {
                        command.Parameters.Clear();
                        command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = row["godina"];
                        command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = row["dogadjajId"];
                        command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = row["magacinDest"];
                        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = row["artikalId"];
                        command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = (Convert.ToInt32(row["StanjeDest"]) + Convert.ToInt32(row["Prenos"]));
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                        command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = row["godina"];
                        command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = row["dogadjajId"];
                        command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = row["magacinSource"];
                        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = row["artikalId"];
                        command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = (Convert.ToInt32(row["StanejSource"]) - Convert.ToInt32(row["Prenos"]));
                        command.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
                ConnectionPool.checkInConnection(conn);
                return true;
            }

            catch (MySqlException e)
            {

                transaction.Rollback();
                ConnectionPool.checkInConnection(conn);
                ConnectionPool.HandleMySQLDBException(e);

                return false;
            }
        }



        public static int? PrenosArtikalaKreiranjePrenosnice(DataTable table)
        {
            int? prenosnicaId = null;

            if (table.Rows.Count <= 0)
                return prenosnicaId;
            try
            {
                DataRow red = table.Rows[0];
                var item = new Prenosnica()
                {
                    Godina = (short?)HomeFilters.HomeFilterGodina,
                    DogadjajId = HomeFilters.HomeFilterDogadjajId,
                    MagacinOdredisteId = int.TryParse(red["UlazniMagacin"].ToString(), out int i) ? i : (int?)null,
                    MagacinSourceId = int.TryParse(red["IzlazniMagacin"].ToString(), out i) ? i : (int?)null,
                    TipPrenosa = "P"
                };
                if ((new PrenosnicaDao().Insert(item)) != Constants.SQL_OK)
                    return prenosnicaId;

                prenosnicaId = new PrenosnicaDao().SelectMaxId(new List<TemplateFilter>() {
                new TemplateFilter(Prenosnica.F_Godina, HomeFilters.HomeFilterGodina.ToString(),"="),
                new TemplateFilter(Prenosnica.F_DogadjajId,HomeFilters.HomeFilterDogadjajId.ToString(),"="),
            });
                foreach (DataRow row in table.Rows)
                {
                    var stavka = new PrenosnicaStavka()
                    {
                        Godina = (short?)HomeFilters.HomeFilterGodina,
                        DogadjajId = HomeFilters.HomeFilterDogadjajId,
                        PrenosnicaId = prenosnicaId,
                        ArtikalId = int.TryParse(row["artikalId"].ToString(), out int id) ? id : (int?)null,
                        Kolicina = int.TryParse(row["Prenos"].ToString(), out id) ? id : 0

                    };

                    new PrenosnicaStavkaDao().Insert(stavka);
                }

                return prenosnicaId;

            }
            catch
            {
                return prenosnicaId;
            }
        }


        public static bool PrenosArtikalaMagaciniInsert(DataTable table)
        {
            MySqlConnection conn = ConnectionPool.checkOutConnection();
            MySqlTransaction transaction = null;
            try
            {
                string query =
                    @"insert into magacin_artikal (godina,dogadjajId,MagacinId,ArtikalKategorijaId,ArtikalId,PocetnoStanje,TrenutnoStanje)
                            values (@Godina,@DogadjajId,@MagacinId,@ArtikalKategorijaId,@ArtikalId,@PocetnoStanje,@TrenutnoStanje)";


                using (var command = new MySqlCommand(query, conn))
                {
                    foreach (DataRow row in table.Rows)
                    {
                        command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = row["godina"];
                        command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = row["dogadjajId"];
                        command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = row["magacinDest"];
                        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = DBNull.Value;
                        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = row["artikalId"];
                        command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = Convert.ToInt32(row["Prenos"]);
                        command.Parameters.Add("@PocetnoStanje", MySqlDbType.Decimal).Value = Convert.ToInt32(row["Prenos"]);
                        command.ExecuteNonQuery();
                    }
                }

                transaction.Commit();
                ConnectionPool.checkInConnection(conn);
                return true;
            }
            catch (MySqlException e)
            {

                transaction.Rollback();
                ConnectionPool.checkInConnection(conn);
                ConnectionPool.HandleMySQLDBException(e);

                return false;
            }
        }
		public static DataTable SaldoSmjeneReport(short? godina, int? dogadjajId, string blagajnaId, string radnikId)
		{
			var table = new DataTable();
			try
			{
				using (var conn = ConnectionPool.checkOutConnection())
				{
					var command = new MySqlCommand("proc_saldoSmjene", conn)
					{
						CommandType = CommandType.StoredProcedure
					};
					{
						command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
						command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajId;
						command.Parameters.Add("@pBlagajnaId", MySqlDbType.Int32).Value = blagajnaId;
						command.Parameters.Add("@pRadnikId", MySqlDbType.Int32).Value = radnikId;
						var reader = command.ExecuteReader();
						table.Load(reader);
						reader.Close();
					}


					ConnectionPool.checkInConnection(conn);
					return table;
				}
			}
			catch (MySqlException e)
			{
				ConnectionPool.HandleMySQLDBException(e);
				return table;
			}
		}

        public static DataTable AnalitikaKarticeReport(int? godina,int? dogadjajid, string karticaUid, DateTime datumOd, DateTime datumDo)
        {
            var table = new DataTable();
            try
            {
                using (var conn = ConnectionPool.checkOutConnection())
                {
                    var command = new MySqlCommand("proc_AnalitikaKartice", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    {
                        command.Parameters.Add("@pGodina", MySqlDbType.Int32).Value = godina;
                        command.Parameters.Add("@pDogadjajId", MySqlDbType.Int32).Value = dogadjajid;
                        command.Parameters.Add("@pKarticaUid", MySqlDbType.String).Value = karticaUid;
                        command.Parameters.Add("@pDatumOd", MySqlDbType.DateTime).Value = datumOd;
                        command.Parameters.Add("@pDatumDo", MySqlDbType.DateTime).Value = datumDo;
                        var reader = command.ExecuteReader();
                        table.Load(reader);
                        reader.Close();
                    }


                    ConnectionPool.checkInConnection(conn);
                    return table;
                }
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return table;
            }
        }

    }
}


