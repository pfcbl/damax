﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class NarudzbaStavkaDao : ITemplateDao<NarudzbaStavka>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<NarudzbaStavka> Select(List<TemplateFilter> filters)
        {
            List<NarudzbaStavka> lista = new List<NarudzbaStavka>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.prodajnoMjestoId,b2.Naziv as ProdajnoMjestoNaziv,b.NarudzbaId,b.StavkaId,b.Iznos" +
                    ", b.ArtikalId,b4.Naziv as NazivArtikla,b.Kolicina from narudzba_stavka as b join dogadjaj as b1 on" +
                    " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina " +
                    "join prodajno_mjesto b2 on b2.Godina=b.Godina and b2.DogadjajId=b.DogadjajId and b2.ProdajnoMjestoId=b.ProdajnoMjestoId" +
                    " join narudzba as b3 on b.godina=b3.godina and b3.dogadjajId=b.dogadjajId and b3.prodajnoMjestoId=b.prodajnoMjestoId and b3.NarudzbaId=b.NarudzbaId" +
                    " left join artikal as b4 on b.godina=b4.godina and b4.dogadjajId=b.dogadjajId and b.artikalId=b4.artikalId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    NarudzbaStavka item = new NarudzbaStavka();
                    decimal de;
                    DateTime dt;
                    int br;
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.NazivDogadjaja = reader[2].ToString();
                    item.ProdajnoMjestoId = Convert.ToInt32(reader[3]);
                    item.NazivProdajnogMjesta = reader[4].ToString();
                    item.NarudzbaId = Convert.ToInt32(reader[5]);
                    item.StavkaId = Convert.ToInt32(reader[6]);
                    item.Iznos = decimal.TryParse(reader[7].ToString(), out de)
                             ? de
                             : 0;
                    item.ArtikalId = Convert.ToInt32(reader[8]);
                    item.NazivArtikla = reader[9].ToString();
                    item.Kolicina = Convert.ToInt32(reader[10]);
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public NarudzbaStavka SelectOne(object id)
        {
            NarudzbaStavka item = new NarudzbaStavka();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(NarudzbaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"insert into narudzba_stavka (godina,dogadjajId,ProdajnoMjestoId,NarudzbaId,Iznos,ArtikalId,Kolicina)
                            values (@Godina,@DogadjajId,@ProdajnoMjestoId,@NarudzbaId,@Iznos,@ArtikalId,@Kolicina)";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@NarudzbaId", MySqlDbType.Int32).Value = item.NarudzbaId;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Int32).Value = item.Kolicina;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(NarudzbaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update narudzba_stavka set Godina=@Godina,DogadjajId=@DogadjajId,ProdajnoMjestoId=@ProdajnoMjestoId,NarudzbaId=@NarudzbaId,Iznos=@Iznos,
                            Kolicina=@Kolicina where godina=@Godina and DogadjajId=@DogadjajId 
                            and ProdajnoMjestoId=@ProdajnoMjestoId and NarudzbaId=@NarudzbaId and StavkaId=@StavkaId ";

                using (var command = new MySqlCommand(query, conn))
                {

                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@NarudzbaId", MySqlDbType.Int32).Value = item.NarudzbaId;
                    command.Parameters.Add("@StavkaId", MySqlDbType.Int32).Value = item.StavkaId;
                    command.Parameters.Add("@Iznos", MySqlDbType.Float).Value = item.Iznos;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Int32).Value = item.Kolicina;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(NarudzbaStavka item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from narudzba_stavka where Godina=@Godina and DogadjajId=@DogadjajId and ProdajnoMjestoId=@ProdajnoMjestoId  and NarudzbaId=@NarudzbaId and StavkaId=@StavkaId ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                    command.Parameters.Add("@ProdajnoMjestoId", MySqlDbType.Int32).Value = item.ProdajnoMjestoId;
                    command.Parameters.Add("@NarudzbaId", MySqlDbType.Int32).Value = item.NarudzbaId;
                    command.Parameters.Add("@StavkaId", MySqlDbType.Int32).Value = item.StavkaId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
