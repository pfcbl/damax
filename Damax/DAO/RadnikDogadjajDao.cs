﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class RadnikDogadjajDao : ITemplateDao<RadnikDogadjaj>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<RadnikDogadjaj> Select(List<TemplateFilter> filters)
        {
            var lista = new List<RadnikDogadjaj>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();

                comm.CommandText = "select b.RadnikId,b.godina,b.dogadjajid,b2.Ime,b2.Prezime,b1.naziv,b.zaduzenje from radnik_dogadjaj as b join dogadjaj as b1 on b1.godina=b.godina and b1.dogadjajid=b.dogadjajid " +
                                   " join radnik as b2 on b.radnikid=b2.radnikId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " b." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    RadnikDogadjaj radnik_dogadjaj = new RadnikDogadjaj();
                    radnik_dogadjaj.RadnikId = Convert.ToInt32(reader[0]);
                    radnik_dogadjaj.Godina = Convert.ToInt16(reader[1]);
                    radnik_dogadjaj.DogadjajId = Convert.ToInt32(reader[2]);
                    radnik_dogadjaj.Ime = reader[3].ToString();
                    radnik_dogadjaj.Prezime = reader[4].ToString();
                    radnik_dogadjaj.Naziv = reader[5].ToString();
                    radnik_dogadjaj.Zaduzenje = reader[6].ToString();
                    lista.Add(radnik_dogadjaj);
                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException e)
            {
                MessageBox.Show(false.ToString());
                return lista;
            }
        }
        public RadnikDogadjaj SelectOne(object id)
        {
            RadnikDogadjaj radnik = new RadnikDogadjaj();
            //MySqlConnection conn = ConnectionPool.checkOutConnection();
            //MySqlCommand comm = conn.CreateCommand();
            //comm.CommandText = "select * from radnik where RadnikId='" + radnikId + "'";
            //MySqlDataReader reader = comm.ExecuteReader();
            //reader.Read();

            //radnik.Ime = reader[1].ToString();
            //radnik.Prezime = reader[2].ToString();
            //radnik.Telefon = reader[5].ToString();
            //radnik.Mail = reader[6].ToString();
            //radnik.KorisnickoIme = reader[3].ToString();
            //radnik.Lozinka = reader[4].ToString();
            //reader.Close();
            //ConnectionPool.checkInConnection(conn);

            return radnik;

        }

        public bool Insert(RadnikDogadjaj item)
        {
            conn = ConnectionPool.checkOutConnection();

            try
            {
                string query =
                @"insert into radnik_dogadjaj (radnikid,godina,dogadjajid,zaduzenje)
                            values (@radnikid,@godina,@dogadjajid,@zaduzenje)";


            using (var command = new MySqlCommand(query, conn))
            {
                command.Parameters.Add("@radnikid", MySqlDbType.Int32).Value = item.RadnikId;
                command.Parameters.Add("@godina", MySqlDbType.Int16).Value = item.Godina;
                command.Parameters.Add("@dogadjajid", MySqlDbType.Int32).Value = item.DogadjajId;
                command.Parameters.Add("@zaduzenje", MySqlDbType.String).Value = item.Zaduzenje;
                command.ExecuteNonQuery();
            }

            ConnectionPool.checkInConnection(conn);
            return Constants.SQL_OK;
        }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);
                
                return false;
            }
}

        public bool Update(RadnikDogadjaj item)
        {
            conn = ConnectionPool.checkOutConnection();
            try { 
            string query = @"update radnik_dogadjaj set radnikId=@radnikid,godina=@godina,dogadjajid=@dogadjajid,zaduzenje=@zaduzenje ";

            using (var command = new MySqlCommand(query, conn))
            {
                command.Parameters.Add("@radnikid", MySqlDbType.Int32).Value = item.RadnikId;
                command.Parameters.Add("@godina", MySqlDbType.Int16).Value = item.Godina;
                command.Parameters.Add("@dogadjajid", MySqlDbType.Int32).Value = item.DogadjajId;
                command.Parameters.Add("@zaduzenje", MySqlDbType.String).Value = item.Zaduzenje;
                command.ExecuteNonQuery();
            }

            ConnectionPool.checkInConnection(conn);
            return Constants.SQL_OK;
        }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);
                return false;
            }
}

        public bool Delete(RadnikDogadjaj item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"delete from radnik_dogadjaj where RadnikId=@RadnikId and godina=@godina and dogadjajid=@dogadjajid";
                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@RadnikId", MySqlDbType.String).Value = item.RadnikId;
                    command.Parameters.Add("@godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@dogadjajid", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.ExecuteNonQuery();
                }
                
                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
}
    }
}
