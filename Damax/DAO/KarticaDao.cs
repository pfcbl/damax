﻿using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.DAO
{
    class KarticaDao : ITemplateDao<Kartica>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Kartica> Select(List<TemplateFilter> filters)
        {
            var lista = new List<Kartica>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText = "select  b.KarticaUID,b.Status from  kartica as b";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    var item = new Kartica
                    {
                        KarticaUID = reader[0].ToString(),
                        Status = reader[1].ToString()
                    };
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException ex)
            {
                ConnectionPool.HandleMySQLDBException(ex);
                return lista;
            }
        }

        public Kartica SelectOne(object id)
        {
            var item = new Kartica();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select KarticaUID,Status from artikal_kategorija where ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        item.KarticaUID = reader[0].ToString();
                        item.Status = reader[1].ToString();

                    }
                    reader.Close();
                }

                ConnectionPool.checkInConnection(conn);
                return item;
            }
            catch (Exception)
            {
                return item;
            }
        }

        public bool Insert(Kartica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"insert into artikal_kategorija (Godina,DogadjajId,Naziv) values (@Godina,@DogadjajId,@Naziv)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@KarticaUID", MySqlDbType.Int16).Value = item.KarticaUID;
                    command.Parameters.Add("@Status", MySqlDbType.Int32).Value = item.Status;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Update(Kartica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                        @"update artikal_kategorija set Naziv=@Naziv,VrijemeIzmjene=@VrijemeIzmjene where Godina=@Godina and DogadjajId=@DogadjajId and  ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@KarticaUID", MySqlDbType.Int16).Value = item.KarticaUID;
                    command.Parameters.Add("@Status", MySqlDbType.Int32).Value = item.Status;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Kartica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query = @"delete from artikal_kategorija where Godina=@Godina and DogadjajId=@DogadjajId and  ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@KarticaUID", MySqlDbType.Int16).Value = item.KarticaUID;
                    command.Parameters.Add("@Status", MySqlDbType.Int32).Value = item.Status;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
