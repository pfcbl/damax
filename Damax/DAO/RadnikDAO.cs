﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax;
using Damax.Models;
using Damax.Template;

namespace Damax.DAO
{
	class RadnikDAO : ITemplateDao<Radnik>
	{
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

		public List<Radnik> Select(List<TemplateFilter> filters)
		{
			List<Radnik> lista = new List<Radnik>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText = "select b.RadnikId,b.Ime,b.Prezime,b.KorisnickoIme,b.Lozinka,b.Telefon,b.Mail,b.Administrator,b.TipKorisnika from radnik as b";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

            MySqlDataReader reader = comm.ExecuteReader();
			while (reader.Read())
			{
				Radnik radnik = new Radnik();
				radnik.RadnikId = Convert.ToInt32(reader[0]);
				radnik.Ime = reader[1].ToString();
				radnik.Prezime = reader[2].ToString();
                    radnik.KorisnickoIme = reader[3].ToString();
                    radnik.Lozinka = reader[4].ToString();
                    radnik.Telefon = reader[5].ToString();
                    radnik.Mail = reader[6].ToString();
                    radnik.Administrator =Convert.ToBoolean(reader[7]);
                    radnik.TipKorisnika = Convert.ToInt32(reader[8]);
                    lista.Add(radnik);
                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException e)
            {
                return lista;
            }
        }
		public  Radnik SelectOne(object id)
		{
			Radnik radnik = new Radnik();
			//MySqlConnection conn = ConnectionPool.checkOutConnection();
			//MySqlCommand comm = conn.CreateCommand();
			//comm.CommandText = "select * from radnik where RadnikId='" + radnikId + "'";
			//MySqlDataReader reader = comm.ExecuteReader();
			//reader.Read();

			//radnik.Ime = reader[1].ToString();
			//radnik.Prezime = reader[2].ToString();
			//radnik.Telefon = reader[5].ToString();
			//radnik.Mail = reader[6].ToString();
			//radnik.KorisnickoIme = reader[3].ToString();
			//radnik.Lozinka = reader[4].ToString();
			//reader.Close();
			//ConnectionPool.checkInConnection(conn);

			return radnik;

		}

        public bool Insert(Radnik item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"insert into radnik (ime,prezime,korisnickoIme,lozinka,telefon,mail,administrator,TipKorisnika)
                            values (@ime,@prezime,@korisnickoIme,@lozinka,@telefon,@mail,@administrator,@TipKorisnika)";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Ime", MySqlDbType.String).Value = item.Ime;
                    command.Parameters.Add("@Prezime", MySqlDbType.String).Value = item.Prezime;
                    command.Parameters.Add("@KorisnickoIme", MySqlDbType.String).Value = item.KorisnickoIme;
                    command.Parameters.Add("@Lozinka", MySqlDbType.String).Value = item.Lozinka;
                    command.Parameters.Add("@Telefon", MySqlDbType.String).Value = item.Telefon;
                    command.Parameters.Add("@Mail", MySqlDbType.String).Value = item.Mail;
                    command.Parameters.Add("@Administrator", MySqlDbType.Bit).Value = item.Administrator;
                    command.Parameters.Add("@TipKorisnika", MySqlDbType.Int32).Value = item.TipKorisnika;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
        
        public bool Update(Radnik item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update radnik set ime=@ime,Prezime=@Prezime,KorisnickoIme=@KorisnickoIme,Lozinka=@Lozinka,
                            Telefon=@Telefon,Mail=@Mail,Administrator=@Administrator,TipKorisnika=@TipKorisnika  where  RadnikId=@RadnikId ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@RadnikId", MySqlDbType.String).Value = item.RadnikId;
                    command.Parameters.Add("@Ime", MySqlDbType.String).Value = item.Ime;
                    command.Parameters.Add("@Prezime", MySqlDbType.String).Value = item.Prezime;
                    command.Parameters.Add("@KorisnickoIme", MySqlDbType.String).Value = item.KorisnickoIme;
                    command.Parameters.Add("@Lozinka", MySqlDbType.String).Value = item.Lozinka;
                    command.Parameters.Add("@Telefon", MySqlDbType.String).Value = item.Telefon;
                    command.Parameters.Add("@Mail", MySqlDbType.String).Value = item.Mail;
                    command.Parameters.Add("@Administrator", MySqlDbType.Bit).Value = item.Administrator;
                    command.Parameters.Add("@TipKorisnika", MySqlDbType.Int32).Value = item.TipKorisnika;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Radnik item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                comm = conn.CreateCommand();
                string query= @"delete from radnik where RadnikId=@RadnikId";
                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@RadnikId", MySqlDbType.String).Value = item.RadnikId;
                    command.ExecuteNonQuery();
                }

                comm.ExecuteNonQuery();
                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
