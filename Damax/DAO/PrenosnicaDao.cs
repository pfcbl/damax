﻿using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damax.DAO
{
    class PrenosnicaDao : ITemplateDao<Prenosnica>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Prenosnica> Select(List<TemplateFilter> filters)
        {
            List<Prenosnica> lista = new List<Prenosnica>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.Godina,b.DogadjajId,b.PrenosnicaId,b.MagacinSourceId,b.MagacinOdredisteId,b1.Naziv as NazivDogadjaja,b2.Naziv as MagacinIzlaz," +
                    " b3.Naziv as MagacinUlaz, b.VrijemePrenosa, (case when b.TipPrenosa='P' then 'Prenos' else 'Narudzba' end) as TipPrenosa " +
                    " from prenosnica as b " +
                    " join dogadjaj as b1 on b1.godina=b.godina and b1.dogadjajId=b.dogadjajId " +
                    " left join magacin as b2 on b2.godina=b.godina and b2.dogadjajId=b.dogadjajId and b2.magacinId=b.magacinSourceId " +
                    " left join magacin as b3 on b3.godina=b.godina and b3.dogadjajId=b.dogadjajId and b3.magacinId=b.magacinOdredisteId  ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.CommandText += " order by b.PrenosnicaId desc";
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Prenosnica item = new Prenosnica
                    {
                        Godina = Convert.ToInt16(reader[0].ToString()),
                        DogadjajId = Convert.ToInt32(reader[1].ToString()),
                        PrenosnicaId= Convert.ToInt32(reader[2].ToString()),
                        MagacinOdredisteId = int.TryParse(reader[3].ToString(), out int m) ? m : (int?)null,
                        MagacinSourceId = int.TryParse(reader[4].ToString(), out m) ? m : (int?)null,
                        NazivDogadjaja = reader[5].ToString(),
                        NazivSuorce = reader[6].ToString(),
                        NazivOdrediste = reader[7].ToString(),
                        VrijemePrenosa = DateTime.TryParse(reader[8].ToString(), out DateTime d) ? d : DateTime.Now,
                        TipPrenosa = reader[9].ToString()
                    };

                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return lista;
            }
        }

        public Prenosnica SelectOne(object id)
        {
            Artikal item = new Artikal();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select * from artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        decimal dt;
                        bool bl;
                        item.ArtikalId = Convert.ToInt32(reader[0].ToString());
                        item.ArtikalKategorijaId = Convert.ToInt32(reader[1].ToString());
                        item.ArtikalKategorijaNaziv = reader[2].ToString();
                        item.Naziv = reader[3].ToString();
                        item.PuniNaziv = reader[4].ToString();
                        item.Cijena = decimal.TryParse(reader[5].ToString(), out dt)
                            ? decimal.Parse(reader[5].ToString())
                            : 0;
                        item.Kolicina = decimal.TryParse(reader[6].ToString(), out dt)
                            ? decimal.Parse(reader[6].ToString())
                            : 0;
                        item.Boja = reader[7].ToString();
                        item.Favorit = bool.TryParse(reader[8].ToString(), out bl) && bool.Parse(reader[8].ToString());
                        item.Neaktivan = bool.TryParse(reader[9].ToString(), out bl) && bool.Parse(reader[9].ToString());
                        item.JedinicaMjere = reader[10].ToString();

                    }
                    reader.Close();
                }

                ConnectionPool.checkInConnection(conn);
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int SelectMaxId(List<TemplateFilter> filters)
        {
            int maxId = 0;
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select ifnull(max(prenosnicaId),0) as PrenosnicaId from prenosnica b ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                     maxId=Convert.ToInt32(reader[0].ToString());
                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return maxId;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return maxId;
            }
        }


        public bool Insert(Prenosnica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"insert into prenosnica (Godina,DogadjajId,PrenosnicaId,MagacinOdredisteId,MagacinSourceId,TipPrenosa)
                                        select @Godina,@DogadjajId,ifnull(max(prenosnicaId),0)+1 as id,@MagacinOdredisteId,@MagacinSourceId,@TipPrenosa 
                                        from prenosnica where godina=@Godina and DogadjajId=@DogadjajId";


                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinOdredisteId", MySqlDbType.Int32).Value = item.MagacinOdredisteId;
                    command.Parameters.Add("@MagacinSourceId", MySqlDbType.Int32).Value = item.MagacinSourceId;
                    command.Parameters.Add("@TipPrenosa", MySqlDbType.String).Value = item.TipPrenosa;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Update(Prenosnica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"update magacin_prenos set MagacinOdredisteId=@MagacinOdredisteId,MagacinSourceId=@MagacinSourceId,TipPrenosa=@TipPrenosa  
                    where Godina=@Godina and DogadjajId=@DogadjajId and MagacinPrenosId=@MagacinPrenosId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinPrenosId", MySqlDbType.Int32).Value = item.PrenosnicaId;
                    command.Parameters.Add("@MagacinOdredisteId", MySqlDbType.Int32).Value = item.MagacinOdredisteId;
                    command.Parameters.Add("@MagacinSourceId", MySqlDbType.Int32).Value = item.MagacinSourceId;
                    command.Parameters.Add("@TipPrenosa", MySqlDbType.String).Value = item.TipPrenosa;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Prenosnica item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"delete from magacin_prenos where Godina=@Godina and DogadjajId=@DogadjajId and MagacinPrenosId=@MagacinPrenosId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinPrenosId", MySqlDbType.Int32).Value = item.PrenosnicaId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
