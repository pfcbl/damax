﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class BlagajnaDao : ITemplateDao<Blagajna>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Blagajna> Select(List<TemplateFilter> filters)
        {
            var lista = new List<Blagajna>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.blagajnaId,b.Naziv,b.Lokacija,b.BrojKarticeOd,b.BrojKarticeDo" +
                    ", b.UkupnoUplata,b.UkupnoIsplata,b.Username,b.Password,b.BrojizdatihKartica from blagajna as b join dogadjaj as b1 on" +
                    " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Blagajna item = new Blagajna();
                    decimal dt;
                    int br;
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.NazivDogadjaja = reader[2].ToString();
                    item.BlagajnaId = Convert.ToInt32(reader[3]);
                    item.Naziv = reader[4].ToString();
                    item.Lokacija = reader[5].ToString();
                    item.BrojKarticeOd = reader[6].ToString();
                    item.BrojKarticeDo = reader[7].ToString();
                    item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
                             ? decimal.Parse(reader[8].ToString())
                             : 0;
                    item.UkupnoIsplata = decimal.TryParse(reader[9].ToString(), out dt)
                        ? decimal.Parse(reader[9].ToString())
                        : 0;
                    item.Username = reader[10].ToString();
                    item.Password = reader[11].ToString();
                    item.BrojIzdatihKartica = int.TryParse(reader[12].ToString(),out br)?br:0;
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public Blagajna SelectOne(object id)
        {
            Blagajna item = new Blagajna();
            //try
            //{
            //    conn = ConnectionPool.checkOutConnection();
            //    string query = @"select * from damax.artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

            //    using (var command = new MySqlCommand(query, conn))
            //    {
            //        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
            //        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
            //        MySqlDataReader reader = comm.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            decimal dt;
            //            item.DogadjajId = Convert.ToInt32(reader[1].ToString());
            //            item.Godina = Convert.ToInt16(reader[0].ToString());
            //            item.NazivDogadjaja = reader[2].ToString();
            //            item.BlagajnaId = Convert.ToInt32(reader[3]);
            //            item.Naziv = reader[4].ToString();
            //            item.Lokacija = reader[5].ToString();
            //            item.BrojKarticeOd = reader[6].ToString();
            //            item.BrojKarticeDo = reader[7].ToString();
            //            item.UkupnoUplata = decimal.TryParse(reader[8].ToString(), out dt)
            //                     ? decimal.Parse(reader[8].ToString())
            //                     : 0;
            //            item.UkupnoUplata = decimal.TryParse(reader[9].ToString(), out dt)
            //                ? decimal.Parse(reader[9].ToString())
            //                : 0;
            //            item.Username = reader[10].ToString();
            //            item.Password = reader[11].ToString();

            //        }
            //        reader.Close();
            //    }

            //    ConnectionPool.checkInConnection(conn);
            //    return item;
            //}
            //catch (Exception)
            //{
            return item;
            //}
        }

        public bool Insert(Blagajna item)
        {
            try
            {
                conn = ConnectionPool.checkOutConnection(); //JedinicaMjere)

                string query =
                    @"insert into blagajna (godina,dogadjajId,BlagajnaId,Naziv,Lokacija,BrojKarticeOd,BrojKarticeDo,UkupnoUplata,UkupnoIsplata,Username,Password,BrojizdatihKartica)
                            select @Godina,@DogadjajId,ifnull(max(BlagajnaId),0)+1 as id,@Naziv,@Lokacija,@BrojKarticeOd,@BrojKarticeDo,@UkupnoUplata,@UkupnoIsplata,@Username,@Password,@BrojizdatihKartica from blagajna where   godina=@Godina and DogadjajId=@DogadjajId";

                //,@JedinicaMjere)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@BrojKarticeOd", MySqlDbType.String).Value = item.BrojKarticeOd;
                    command.Parameters.Add("@BrojKarticeDo", MySqlDbType.String).Value = item.BrojKarticeDo;
                    command.Parameters.Add("@UkupnoUplata", MySqlDbType.Float).Value = item.UkupnoUplata;
                    command.Parameters.Add("@UkupnoIsplata", MySqlDbType.Float).Value = item.UkupnoIsplata;
                    command.Parameters.Add("@Username", MySqlDbType.String).Value = item.Username;
                    command.Parameters.Add("@Password", MySqlDbType.String).Value = item.Password;
                    command.Parameters.Add("@BrojizdatihKartica", MySqlDbType.Int32).Value = item.BrojIzdatihKartica;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {
                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Update(Blagajna item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update blagajna set Godina=@Godina,DogadjajId=@DogadjajId,Naziv=@Naziv,Lokacija=@Lokacija,BrojKarticeOd=@BrojKarticeOd,BrojKarticeDo=@BrojKarticeDo,
                            UkupnoUplata=@UkupnoUplata,UkupnoIsplata=@UkupnoIsplata,Username=@Username,Password=@Password,BrojizdatihKartica=@BrojizdatihKartica
                                 where godina=@Godina and DogadjajId=@DogadjajId and BlagajnaId=@BlagajnaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@BlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@Lokacija", MySqlDbType.String).Value = item.Lokacija;
                    command.Parameters.Add("@BrojKarticeOd", MySqlDbType.String).Value = item.BrojKarticeOd;
                    command.Parameters.Add("@BrojKarticeDo", MySqlDbType.String).Value = item.BrojKarticeDo;
                    command.Parameters.Add("@UkupnoUplata", MySqlDbType.Float).Value = item.UkupnoUplata;
                    command.Parameters.Add("@UkupnoIsplata", MySqlDbType.Float).Value = item.UkupnoIsplata;
                    command.Parameters.Add("@Username", MySqlDbType.String).Value = item.Username;
                    command.Parameters.Add("@Password", MySqlDbType.String).Value = item.Password;
                    command.Parameters.Add("@BrojizdatihKartica", MySqlDbType.Int32).Value = item.BrojIzdatihKartica;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {
                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Delete(Blagajna item)
        {
            conn = ConnectionPool.checkOutConnection();
            string query = @"delete from blagajna where Godina=@Godina and DogadjajId=@DogadjajId and BlagajnaId=@BlagajnaId";

            using (var command = new MySqlCommand(query, conn))
            {
                command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                command.Parameters.Add("@Godina", MySqlDbType.Int32).Value = item.Godina;
                command.Parameters.Add("@BlagajnaId", MySqlDbType.Int32).Value = item.BlagajnaId;
                command.ExecuteNonQuery();
            }

            ConnectionPool.checkInConnection(conn);
            return Constants.SQL_OK;
        }
    }
}
