﻿using System;
using System.Collections.Generic;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class ArtikalDao : ITemplateDao<Artikal>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<Artikal> Select(List<TemplateFilter> filters)
        {
            List<Artikal> lista = new List<Artikal>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select b.Godina,b.DogadjajId,b1.Naziv as NazivDogadjaja,b.ArtikalId,b.ArtikalKategorijaId,b1.Naziv as ArtikalKategorijaNaziv," +
                    "b.Naziv,b.PunNaziv,b.Cijena,b.Kolicina,b.Boja,b.Favorit,b.Neaktivan,b.JedinicaMjere " +
                    "from  artikal as b " +
                    " join dogadjaj as b1 on b1.godina=b.godina and b1.dogadjajId=b.dogadjajId " +
                    " join  artikal_kategorija as b2 on" +
                    " b.ArtikalKategorijaId=b2.ArtikalKategorijaId and  b2.godina=b.godina and b2.dogadjajId=b.dogadjajId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Artikal item = new Artikal();
                    item.Godina = Convert.ToInt16(reader[0].ToString());
                    item.DogadjajId = Convert.ToInt32(reader[1].ToString());
                    item.NazivDogadjaja =reader[2].ToString();
                    item.ArtikalId = Convert.ToInt32(reader[3].ToString());
                    item.ArtikalKategorijaId = Convert.ToInt32(reader[4].ToString());
                    item.ArtikalKategorijaNaziv = reader[5].ToString();
                    item.Naziv = reader[6].ToString();
                    item.PuniNaziv = reader[7].ToString();
                    item.Cijena = decimal.TryParse(reader[8].ToString(), out decimal dt)
                        ? dt
                        : 0;
                    item.Kolicina = decimal.TryParse(reader[9].ToString(), out dt)
                        ? dt
                        : 0;
                    item.Boja = reader[10].ToString();
                    item.Favorit = reader.GetBoolean(11);
                    item.Neaktivan = reader.GetBoolean(12);
                    item.JedinicaMjere = reader[13].ToString();
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return lista;
            }
    }

        public Artikal SelectOne(object id)
        {
            Artikal item = new Artikal();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                string query = @"select * from artikal where ArtikalId=@ArtikalId and ArtikalKategorijaId=@ArtikalKategorijaId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = id;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = id;
                    MySqlDataReader reader = comm.ExecuteReader();
                    if (reader.Read())
                    {
                        item.ArtikalId = Convert.ToInt32(reader[0].ToString());
                        item.ArtikalKategorijaId = Convert.ToInt32(reader[1].ToString());
                        item.ArtikalKategorijaNaziv = reader[2].ToString();
                        item.Naziv = reader[3].ToString();
                        item.PuniNaziv = reader[4].ToString();
                        item.Cijena = decimal.TryParse(reader[5].ToString(), out decimal dt)
                            ? decimal.Parse(reader[5].ToString())
                            : 0;
                        item.Kolicina = decimal.TryParse(reader[6].ToString(), out dt)
                            ? decimal.Parse(reader[6].ToString())
                            : 0;
                        item.Boja = reader[7].ToString();
                        item.Favorit = bool.TryParse(reader[8].ToString(), out bool bl) && bool.Parse(reader[8].ToString());
                        item.Neaktivan = bool.TryParse(reader[9].ToString(), out bl) && bool.Parse(reader[9].ToString());
                        item.JedinicaMjere = reader[10].ToString();

                    }
                    reader.Close();
                }

                ConnectionPool.checkInConnection(conn);
                return item;
            }
            catch (Exception)
            {
                return item;
            }
        }


        public int SelectMaxId(List<TemplateFilter> filters)
        {
            int maxId = 0;
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                    "select ifnull(max(artikalid),0) as ArtikalId  from artikal b ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }

                }
                comm.ExecuteNonQuery();

                MySqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    maxId = Convert.ToInt32(reader[0].ToString());
                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return maxId;
            }
            catch (MySqlException e)
            {
                ConnectionPool.HandleMySQLDBException(e);
                return maxId;
            }
        }

        public bool Insert(Artikal item)
        {
            conn = ConnectionPool.checkOutConnection();//JedinicaMjere)
            try
            {
                const string query = @"insert into artikal (Godina,DogadjajId,ArtikalKategorijaId,ArtikalId,Naziv,PunNaziv,Cijena,Kolicina,Boja,Favorit,Neaktivan,JedinicaMjere)
                           select @Godina,@DogadjajId,@ArtikalKategorijaId,ifnull(max(artikalId),0)+1 as id,@Naziv,@PuniNaziv,@Cijena,@Kolicina,@Boja,@Favorit,@Neaktivan,@JedinicaMjere
                                from artikal where godina=@Godina and DogadjajId=@DogadjajId";
                

                //,@JedinicaMjere)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@PuniNaziv", MySqlDbType.String).Value = item.PuniNaziv;
                    command.Parameters.Add("@Cijena", MySqlDbType.Float).Value = item.Cijena;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Float).Value = item.Kolicina;
                    command.Parameters.Add("@Boja", MySqlDbType.String).Value = item.Boja;
                    command.Parameters.Add("@Favorit", MySqlDbType.Bit).Value = item.Favorit;
                    command.Parameters.Add("@Neaktivan", MySqlDbType.Bit).Value = item.Neaktivan;
                    command.Parameters.Add("@JedinicaMjere", MySqlDbType.String).Value = item.JedinicaMjere;
					command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool InsertList(List<Artikal> items)
        {
            conn = ConnectionPool.checkOutConnection();//JedinicaMjere)
            const string query = @"insert into artikal (Godina,DogadjajId,ArtikalKategorijaId,ArtikalId,Naziv,PunNaziv,Cijena,Kolicina,Boja,Favorit,Neaktivan,JedinicaMjere)
                            values (@Godina,@DogadjajId,@ArtikalKategorijaId,@ArtikalId,@Naziv,@PuniNaziv,@Cijena,@Kolicina,@Boja,@Favorit,@Neaktivan,@JedinicaMjere)";
            MySqlTransaction transaction=null;
            try
            {
                transaction=conn.BeginTransaction();
                foreach (Artikal item in items)
                {
                    using (var command = new MySqlCommand(query, conn))
                    {
                        command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                        command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                        command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                        command.Parameters.Add("@PuniNaziv", MySqlDbType.String).Value = item.PuniNaziv;
                        command.Parameters.Add("@Cijena", MySqlDbType.Float).Value = item.Cijena;
                        command.Parameters.Add("@Kolicina", MySqlDbType.Float).Value = item.Kolicina;
                        command.Parameters.Add("@Boja", MySqlDbType.String).Value = item.Boja;
                        command.Parameters.Add("@Favorit", MySqlDbType.Bit).Value = item.Favorit;
                        command.Parameters.Add("@Neaktivan", MySqlDbType.Bit).Value = item.Neaktivan;
                        command.Parameters.Add("@JedinicaMjere", MySqlDbType.String).Value = item.JedinicaMjere;
                        command.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {
                transaction.Rollback();
                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Update(Artikal item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"update artikal set Naziv=@Naziv,PunNaziv=@PuniNaziv,Cijena=@Cijena,Kolicina=@Kolicina,Boja=@Boja
                            ,Favorit=@Favorit,Neaktivan=@Neaktivan,JedinicaMjere=@JedinicaMjere,VrijemeIzmjene=@VrijemeIzmjene  where Godina=@Godina and DogadjajId=@DogadjajId and ArtikalKategorijaId=@ArtikalKategorijaId and ArtikalId=@ArtikalId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.Parameters.Add("@Naziv", MySqlDbType.String).Value = item.Naziv;
                    command.Parameters.Add("@PuniNaziv", MySqlDbType.String).Value = item.PuniNaziv;
                    command.Parameters.Add("@Cijena", MySqlDbType.Float).Value = item.Cijena;
                    command.Parameters.Add("@Kolicina", MySqlDbType.Float).Value = item.Kolicina;
                    command.Parameters.Add("@Boja", MySqlDbType.String).Value = item.Boja;
                    command.Parameters.Add("@Favorit", MySqlDbType.Bit).Value = item.Favorit;
                    command.Parameters.Add("@Neaktivan", MySqlDbType.Bit).Value = item.Neaktivan;
                    command.Parameters.Add("@JedinicaMjere", MySqlDbType.String).Value = item.JedinicaMjere;
					command.Parameters.Add("@VrijemeIzmjene", MySqlDbType.DateTime).Value = DateTime.Now;
					command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool Delete(Artikal item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"delete from artikal where Godina=@Godina and DogadjajId=@DogadjajId and ArtikalKategorijaId=@ArtikalKategorijaId and ArtikalId=@ArtikalId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }


        public bool DeleteAll()
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                const string query = @"delete from artikal where Godina=@Godina and DogadjajId=@DogadjajId ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = HomeFilters.HomeFilterGodina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = HomeFilters.HomeFilterDogadjajId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;

            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }
    }
}
