﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Damax.Models;
using Damax.Template;
using MySql.Data.MySqlClient;

namespace Damax.DAO
{
    class MagacinArtikalDao : ITemplateDao<MagacinArtikal>
    {
        private MySqlConnection conn;
        private MySqlCommand comm;
        public List<TemplateFilter> Filters;

        public List<TemplateFilter> GetFilter()
        {

            return null;
        }

        public List<MagacinArtikal> Select(List<TemplateFilter> filters)
        {
            List<MagacinArtikal> lista = new List<MagacinArtikal>();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                 "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.magacinId,b2.Naziv as MagacinNaziv,b.ArtikalKategorijaId,'' as ArtikalKategorijaNaziv,b.ArtikalId,b3.Naziv as ArtikalNaziv,b.PocetnoStanje,b.TrenutnoStanje " +
                " from magacin_artikal as b join dogadjaj as b1 on" +
                " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina join  magacin b2 on b.DogadjajId=b2.DogadjajId and b.Godina=b2.Godina and b.MagacinId=b2.MagacinId" +
                " left join  artikal as b3 on b3.godina=b.godina and b3.dogadjajId=b.dogadjajId and b3.ArtikalId=b.ArtikalId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                comm.ExecuteNonQuery();

                var reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    MagacinArtikal item = new MagacinArtikal
                    {
                        Godina = Convert.ToInt16(reader[0].ToString()),
                        DogadjajId = Convert.ToInt32(reader[1].ToString()),
                        NazivDogadjaja = reader[2].ToString(),
                        MagacinId = Convert.ToInt32(reader[3]),
                        MagacinNaziv = reader[4].ToString(),
                        ArtikalKategorijaId = int.TryParse(reader[5].ToString(), out int br) ? br : (int?)null,
                        ArtikalKategorijaNaziv = reader[6].ToString(),
                        ArtikalId = int.TryParse(reader[7].ToString(), out br) ? br : (int?)null,
                        ArtikalNaziv = reader[8].ToString(),
                        PocetnoStanje = decimal.TryParse(reader[9].ToString(), out decimal bl) ? bl : 0,
                        TrenutnoStanje = decimal.TryParse(reader[10].ToString(), out bl) ? bl : 0
                    };
                    lista.Add(item);

                }
                reader.Close();
                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }

        public DataTable SelectTable(List<TemplateFilter> filters)
        {
            DataTable lista = new DataTable();
            try
            {
                conn = ConnectionPool.checkOutConnection();
                comm = conn.CreateCommand();
                comm.CommandText =
                "select b.godina,b.dogadjajId,b1.naziv as NazivDogadjaja,b.magacinId,b2.Naziv as MagacinNaziv,b.ArtikalKategorijaId,'' as ArtikalKategorijaNaziv,b.ArtikalId,b3.Naziv as ArtikalNaziv,b.PocetnoStanje,b.TrenutnoStanje " +
                " from magacin_artikal  as b join dogadjaj as b1 on" +
                " b.DogadjajId=b1.DogadjajId and b.Godina=b1.Godina join magacin b2 on b.DogadjajId=b2.DogadjajId and b.Godina=b2.Godina and b.MagacinId=b2.MagacinId" +
                " left join artikal as b3 on b3.godina=b.godina and b3.dogadjajId=b.dogadjajId and b3.ArtikalId=b.ArtikalId ";
                if (filters.Count > 0)
                {
                    comm.CommandText += " where ";
                    foreach (var filter in filters)
                    {
                        if (!comm.CommandText.EndsWith(" where "))
                        {
                            comm.CommandText += " and "+ filter.Alias + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                        else
                        {
                            comm.CommandText += " " + filter.Alias + "." + "." + filter.Name + filter.FilterOperator + filter.Value;
                        }
                    }
                }
                var da = new MySqlDataAdapter(comm);
                da.Fill(lista);
                conn.Close();
                da.Dispose();

                ConnectionPool.checkInConnection(conn);
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }
        }
        public MagacinArtikal SelectOne(object id)
        {
            return new MagacinArtikal();
        }



        public bool Insert(MagacinArtikal item)
        {
            conn = ConnectionPool.checkOutConnection();//JedinicaMjere)
            try
            {
                string query =
                    @"insert into magacin_artikal (godina,dogadjajId,MagacinId,ArtikalKategorijaId,ArtikalId,PocetnoStanje,TrenutnoStanje)
                            values (@Godina,@DogadjajId,@MagacinId,@ArtikalKategorijaId,@ArtikalId,@PocetnoStanje,@TrenutnoStanje)";

                //,@JedinicaMjere)";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@PocetnoStanje", MySqlDbType.Decimal).Value = item.PocetnoStanje;
                    command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = item.TrenutnoStanje;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

        public bool InsertList(List<MagacinArtikal> items)
        {
            conn = ConnectionPool.checkOutConnection();
            string query =
                  @"insert into magacin_artikal (godina,dogadjajId,MagacinId,ArtikalKategorijaId,ArtikalId,PocetnoStanje,TrenutnoStanje)
                            values (@Godina,@DogadjajId,@MagacinId,@ArtikalKategorijaId,@ArtikalId,@PocetnoStanje,@TrenutnoStanje)";
            MySqlTransaction transaction = null;

            try
            {
                transaction = conn.BeginTransaction();
                foreach (MagacinArtikal item in items)
                {
                    using (var command = new MySqlCommand(query, conn))
                    {
                        command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                        command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                        command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                        command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                        command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                        command.Parameters.Add("@PocetnoStanje", MySqlDbType.Decimal).Value = item.PocetnoStanje;
                        command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = item.TrenutnoStanje;
                        command.ExecuteNonQuery();
                    }
                }
                transaction.Commit();
                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {
                transaction.Rollback();
                ConnectionPool.checkInConnection(conn);
                return false;
            }
        }

        public bool Update(MagacinArtikal item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"update  magacin_artikal  set Godina=@Godina,DogadjajId=@DogadjajId,MagacinId=@MagacinId,ArtikalKategorijaId=@ArtikalKategorijaId," +
                    "ArtikalId=@ArtikalId,PocetnoStanje=@PocetnoStanje,TrenutnoStanje=@TrenutnoStanje where godina=@Godina and DogadjajId=@DogadjajId and MagacinId=@MagacinId and ArtikalId=@ArtikalId ";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.Parameters.Add("@PocetnoStanje", MySqlDbType.Decimal).Value = item.PocetnoStanje;
                    command.Parameters.Add("@TrenutnoStanje", MySqlDbType.Decimal).Value = item.TrenutnoStanje;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }

            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

       

        public bool Delete(MagacinArtikal item)
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from magacin_artikal where Godina=@Godina and DogadjajId=@DogadjajId and MagacinId=@MagacinId and ArtikalId=@ArtikalId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = item.DogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = item.Godina;
                    command.Parameters.Add("@MagacinId", MySqlDbType.Int32).Value = item.MagacinId;
                    command.Parameters.Add("@ArtikalKategorijaId", MySqlDbType.Int32).Value = item.ArtikalKategorijaId;
                    command.Parameters.Add("@ArtikalId", MySqlDbType.Int32).Value = item.ArtikalId;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }


        public bool DeleteAll()
        {
            conn = ConnectionPool.checkOutConnection();
            try
            {
                string query =
                    @"delete from magacin_artikal where Godina=@Godina and DogadjajId=@DogadjajId";

                using (var command = new MySqlCommand(query, conn))
                {
                    command.Parameters.Add("@DogadjajId", MySqlDbType.Int32).Value = HomeFilters.HomeFilterDogadjajId;
                    command.Parameters.Add("@Godina", MySqlDbType.Int16).Value = HomeFilters.HomeFilterGodina;
                    command.ExecuteNonQuery();
                }

                ConnectionPool.checkInConnection(conn);
                return Constants.SQL_OK;
            }
            catch (MySqlException e)
            {

                ConnectionPool.checkInConnection(conn);

                return false;
            }
        }

    }
}
